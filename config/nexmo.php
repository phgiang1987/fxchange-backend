<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Credentials
    |--------------------------------------------------------------------------
    |
    | If you're using API credentials, change these settings. Get your
    | credentials from https://dashboard.nexmo.com | 'Settings'.
    |
    */

    'api_key'    => function_exists('env') ? env('NEXMO_KEY', '4e89a253') : '4f9ac69d',
    'api_secret' => function_exists('env') ? env('NEXMO_SECRET', '635a3fee627f1da8') : '3082ebbe602cf809',

    /*
    |--------------------------------------------------------------------------
    | Signature Secret
    |--------------------------------------------------------------------------
    |
    | If you're using a signature secret, use this section. This can be used
    | without an `api_secret` for some APIs, as well as with an `api_secret`
    | for all APIs.
    |
    */

    'signature_secret' => function_exists('env') ? env('NEXMO_SIGNATURE_SECRET', '') : '',



    'statusCode' => [
        1 => 'You have exceeded the submission capacity allowed on this account. Please wait and retry.',
        2 => 'Your request is incomplete and missing some mandatory parameters.',
        3 => 'The value of one or more parameters is invalid.',
        4 => 'The api_key / api_secret you supplied is either invalid or disabled.',
        5 => 'There was an error processing your request in the Platform.',
        6 => 'The Platform was unable to process your request. For example, due to an unrecognised prefix for the phone number.',
        7 => 'The number you are trying to submit to is blacklisted and may not receive messages.',
        8 => 'The api_key you supplied is for an account that has been barred from submitting messages.',
        9 => 'Your pre-paid account does not have sufficient credit to process this message.',
        11 => 'This account is not provisioned for REST submission, you should use SMPP instead.',
        12 => 'The length of udh and body was greater than 140 octets for a binary type SMS request.',
        13 => 'Message was not submitted because there was a communication failure.',
        14 => 'Message was not submitted due to a verification failure in the submitted signature.',
        15 => 'Due to local regulations, the SenderID you set in from in the request was not accepted. Please check the Global messaging section.',
        16 => 'The value of ttl in your request was invalid.',
        19 => 'Your request makes use of a facility that is not enabled on your account.',
        20 => 'The value of message-class in your request was out of range. See https://en.wikipedia.org/wiki/Data_Coding_Scheme.',
        16 => 'You did not include https in the URL you set in callback.',
        19 => 'The phone number you set in to is not in your pre-approved destination list. To send messages to this phone number, add it using Dashboard.',
        20 => 'The phone number you supplied in the to parameter of your request was either missing or invalid.',
        102 => 'You tried to send a message to a destination number that has opted out of your program.',
        
    ]

];

# Fxchange Setup guide

 
### Enviroment requirement

- PHP 5.6.30
- PHP 5.6 or higher
- PDO PHP Extension
- cURL PHP Extension
- OpenSSL PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- Mcrypt PHP Extension
- GD PHP Library
- MySQL 5.5+
- Soap PHP Extension
### Get source code & composer vendor
```sh
$ git clone url-git
$ cd webroot/cms
$ git checkout stagging
$ composer install
```
### Reset source code
```sh
$ cd webroot/fxchange
$ git checkout .
```

### Asgard install
```sh
$ php artisan asgard:install
```


### Run migrate database
```sh
$ cd webroot/fxchange
$ php artisan migrate
```
But we suggest use our sql script for include all init data
### Permission of foder
Please set write permission for lis folder below
- boostrap/cache
- storage/logs
- storage/debugbar
- storage/api-docs
- storage/framework
- storage/app

### Config .env file

Use the .evn file content below and copy to folder ../cms
```sh
APP_ENV=local
APP_DEBUG=true
APP_CACHE=false
INSTALLED=true
APP_KEY=base64:LUvgMbQqpCXWlfSz/E/vD0LFhVDMtVpSFQOum8eim6s=
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=fxchange
DB_USERNAME=fxchange_user
DB_PASSWORD=fxchange_pwd

CACHE_DRIVER=array
TRANSLATIONS_CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

/* SMTP Mail config */
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=user-email
MAIL_PASSWORD=password-email
MAIL_ENCRYPTION=encryption-config [ssl|tls]
MAIL_FROM_ADDRESS=dev.php.webs@gmail.com
MAIL_FROM_NAME="Fxchange Systems"


L5_SWAGGER_GENERATE_ALWAYS = true


NEXMO_KEY=afda6e5f
NEXMO_SECRET=f9d2a75a09b1f723




AWS_KEY=AKIAJJTVJM6FMTVBKLIA
AWS_SECRET=JPlpFb7Uc6m/qmbGM3+PgTezDJIoPL42Kqbv7r4Y
AWS_REGION=ap-southeast-1
AWS_BUCKET=proj-fxchange

API_HOST=yourdomainhere (without http://) exp: fxchange.dev

```

#### Domain & Vitualhost config
Webroot will map to folder root of source code
In the webroot create a link name admin and link to folder ../fxchange/public


### Queue config
$ php artisan queue:work



### Cron config
$ php artisan fxchange:alert-rate
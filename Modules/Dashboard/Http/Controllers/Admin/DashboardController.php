<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Dashboard\Repositories\WidgetRepository;
use Modules\User\Contracts\Authentication;
use Nwidart\Modules\Repository;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\Fxchange\Entities\Currency;
use Charts;
use DB;
use Modules\Audittrail\Entities\Log;

class DashboardController extends AdminBaseController
{
    /**
     * @var WidgetRepository
     */
    private $widget;
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param Repository $modules
     * @param WidgetRepository $widget
     * @param Authentication $auth
     */
    public function __construct(Repository $modules, WidgetRepository $widget, Authentication $auth)
    {
        parent::__construct();
        $this->bootWidgets($modules);
        $this->widget = $widget;
        $this->auth = $auth;
    }

    /**
     * Display the dashboard with its widgets
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $this->requireAssets();

        $widget = $this->widget->findForUser($this->auth->id());

        $customWidgets = json_encode(null);
        if ($widget) {
            $customWidgets = $widget->widgets;
        }
        $total_user = User::with('getRoles')
                            ->join('role_users', 'users.id', '=', 'role_users.user_id')
                            ->join('roles', 'roles.id', '=', 'role_users.role_id')
                            ->whereRaw("(roles.slug != 'merchant' and roles.slug != 'customer')")
                            ->orWhereHas('getRoles', function($query) {
                                $query->where('role_id',User::MERCHANT);
                            })
                            ->orWhereHas('getRoles', function($query) {
                                $query->where('role_id',User::CUSTOMER);
                            })
                            ->count();
        //dd($total_user);
        $total_transaction = Transaction::where('status','<>','pending')->count();
        $total_currency = Currency::count();
        $total_machines = Machines::count();

        $chart_admin = Charts::database(User::select('users.*',DB::raw('roles.slug'))->with('getRoles')
                            ->join('role_users', 'users.id', '=', 'role_users.user_id')
                            ->join('roles', 'roles.id', '=', 'role_users.role_id')
                            ->whereRaw("(roles.slug != 'merchant' and roles.slug != 'customer')")->get(), 'bar', 'highcharts')
            ->elementLabel("Total users register by month")
            ->dimensions(1000, 500)
            ->responsive(true)
            ->title("Admin")
            ->lastByMonth(12,true);

        $chart_merchant = Charts::database(User::whereHas('getRoles', function($query) {
                                $query->where('role_id',User::MERCHANT);
                            })->get(), 'bar', 'highcharts')
            ->elementLabel("Total users register by month")
            ->dimensions(1000, 500)
            ->responsive(true)
            ->title("Merchant")
            ->lastByMonth(12,true);
        $chart_customer = Charts::database(User::whereHas('getRoles', function($query) {
                                $query->where('role_id',User::CUSTOMER);
                            })->get(), 'bar', 'highcharts')
            ->elementLabel("Total users register by month")
            ->dimensions(1000, 500)
            ->responsive(true)

            ->title("Customer")
            ->lastByMonth(12,true);
        $last_active_users = Log::with('performedUser')->where('performed_user_id','<>',null)->orderBy('id','DESC')->limit(10)->get();
        //dd($last_active_users);
        return view('dashboard::admin.dashboard', compact('customWidgets','total_user','total_transaction','total_currency','total_machines','chart_admin','chart_merchant','chart_customer','last_active_users'));
    }
    public function indexajax(){
        $last_active_users = Log::orderBy('created_at','DESC')->where('performed_user_id','<>',null)->limit(10)->get();
        return view("dashboard::admin.partials",compact('last_active_users'))->render();
    }
    /**
     * Save the current state of the widgets
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        $widgets = $request->get('grid');

        if (empty($widgets)) {
            return Response::json([false]);
        }

        $this->widget->updateOrCreateForUser($widgets, $this->auth->id());

        return Response::json([true]);
    }

    /**
     * Reset the grid for the current user
     */
    public function reset()
    {
        $widget = $this->widget->findForUser($this->auth->id());

        if (!$widget) {
            return redirect()->route('dashboard.index')->with('warning', trans('dashboard::dashboard.reset not needed'));
        }

        $this->widget->destroy($widget);

        return redirect()->route('dashboard.index')->with('success', trans('dashboard::dashboard.dashboard reset'));
    }

    /**
     * Boot widgets for all enabled modules
     * @param Repository $modules
     */
    private function bootWidgets(Repository $modules)
    {
        foreach ($modules->enabled() as $module) {
            if (! $module->widgets) {
                continue;
            }
            foreach ($module->widgets as $widgetClass) {
                app($widgetClass)->boot();
            }
        }
    }

    /**
     * Require necessary assets
     */
    private function requireAssets()
    {
        $this->assetPipeline->requireJs('lodash.js');
        $this->assetPipeline->requireJs('jquery-ui-core.js');
        $this->assetPipeline->requireJs('jquery-ui-widget.js');
        $this->assetPipeline->requireJs('jquery-ui-mouse.js');
        $this->assetPipeline->requireJs('jquery-ui-draggable.js');
        $this->assetPipeline->requireJs('jquery-ui-resizable.js');
        $this->assetPipeline->requireJs('gridstack.js');
        $this->assetPipeline->requireJs('chart.js');
        $this->assetPipeline->requireCss('gridstack.css')->before('asgard.css');
    }
}

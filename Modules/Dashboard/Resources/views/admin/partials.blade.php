@foreach($last_active_users as $item)
    <li class="item">
      <div class="product-info">
        <a href="javascript:void(0)" class="product-title">{{ $item->performedUser?$item->performedUser->first_name." ".$item->performedUser->last_name:null }}
          <span class="label label-warning pull-right">{{  Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</span></a>
        <span class="product-description">
              {{$item->performedUser?$item->performedUser->email:null }}
            </span>
      </div>
    </li>		
@endforeach
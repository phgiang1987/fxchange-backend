<?php

namespace Modules\Fxchange\Repositories;

use Modules\User\Repositories\UserTokenRepository;

interface FxUserTokenRepository extends UserTokenRepository
{

}

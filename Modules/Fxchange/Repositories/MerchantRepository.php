<?php

namespace Modules\Fxchange\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Entities\UserInterface;
use Modules\User\Repositories\UserRepository;

/**
 * Interface MerchantRepository
 * @package Modules\Fxchange\Repositories
 */
interface MerchantRepository extends BaseRepository
{
	/**
	 *
	 * Send Code Sign In
	 *
	 */
	public function sendCodeSignIn($user, $firebase_token, $device_type);

	/**
	 *
	 * Send code verify sign in
	 *
	 */
	public function postChangePhoneSignIn($data);

	/**
	 *
	 * Send postReSendVerifyLogin
	 *
	 */
	public function postReSendVerifyLogin($requestId);
	/**
	 *
	 * Send verifySignIn
	 *
	 */
	public function verifySignIn($requestId , $code);
	/**
	 *
	 * Send createFeeedback
	 *
	 */
	public function createFeeedback($data);
	/**
	 *
	 * Send getContactUs
	 *
	 */
	public function getContactUs();
	/**
	 *
	 * Send getPastDelivery
	 *
	 */
	public function getPastDelivery($data);
	/**
	 *
	 * Send getDeliveryOrder
	 *
	 */
	public function getDeliveryOrder($data);
	/**
	 *
	 * Send getDeliverySearch
	 *
	 */
	public function getDeliverySearch($user , $keyword);
	/**
	 *
	 * Send sendVerifyChangePhone
	 *
	 */
	public function sendVerifyChangePhone($userId, $phone,$last_name,$first_name);
	/**
	 *
	 * Send postUpdateAccount
	 *
	 */
	public function postUpdateAccount($user , $data);
	/**
	 *
	 * Send postReSendVerifyPhone
	 *
	 */
	public function postReSendVerifyPhone($user,$phone);
	/**
	 *
	 * Send verifyChangePhone
	 *
	 */
	public function verifyChangePhone($userId , $requestId, $code);
	/**
	 *
	 * Send getMapViewState
	 *
	 */
	public function getMapViewState($userId);

	/**
	 *
	 * Send postQRScanner
	 *
	 */
	public function postQRScanner($data);

	/**
	 *
	 * Send postMachineQRScanner
	 *
	 */
	public function postMachineQRScanner($data);
	/**
	 *
	 * Send postconfirmTransaction
	 *
	 */
	public function postconfirmTransaction($data);
	/**
	 *
	 * Send psotMachineConfirmTranction
	 *
	 */
	public function postMachineConfirmTransaction($data);
}

<?php

namespace Modules\Fxchange\Repositories\Sentinel;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Events\UserHasRegistered;
use Modules\User\Events\UserIsCreating;
use Modules\User\Events\UserWasCreated;
use Modules\User\Repositories\Sentinel\SentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class FxSentinelMerchantRepository extends FxSentinelUserRepository
{

}

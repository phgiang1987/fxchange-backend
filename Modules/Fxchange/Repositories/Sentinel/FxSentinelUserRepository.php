<?php

namespace Modules\Fxchange\Repositories\Sentinel;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Repositories\Sentinel\SentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class FxSentinelUserRepository extends SentinelUserRepository
{

}

<?php

namespace Modules\Fxchange\Repositories\Sentinel;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Events\UserHasRegistered;
use Modules\User\Events\UserIsCreating;
use Modules\User\Events\UserWasCreated;
use Modules\User\Repositories\Sentinel\SentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class FxSentinelCustomerRepository extends FxSentinelUserRepository
{
    public function customerSignUp($requestId, $code){
        $registerInfo = DB::table('fxchange__user_request_sms')->where('request_id',$requestId)->get()->first();

        $data = [
            'email'=>$registerInfo->email,
            'country_code'=> substr($registerInfo->phone_number,0,3),
            'phone_number'=> substr($registerInfo->phone_number,3,12),
            'password'=> decrypt($registerInfo->password),
            'timezone'=>'Asia/Singapore',
            'firebase_token'=> $registerInfo->firebase_token,
            'device_type'=>$registerInfo->device_type,
            'first_name' =>$registerInfo->first_name,
            'last_name'=>$registerInfo->last_name
        ];

        $user = $this->createWithRoles($data,[3],true , $registerInfo->firebase_token,$registerInfo->device_type);

        //do login
        if($user){
            $user->pwd = $registerInfo->password;
            $user->firebase_token = $registerInfo->firebase_token;
            $user->device_type = $registerInfo->device_type;
        }

        return $user;

    }
    /**
     * Create a user and assign roles to it
     * @param  array $data
     * @param  array $roles
     * @param bool $activated
     * @return User
     */
    public function createWithRoles($data, $roles, $activated = false,$firebase_token = false,$device_type = false)
    {
        $user = $this->create((array) $data, $activated,$firebase_token,$device_type);

        if (!empty($roles)) {
            $user->roles()->attach($roles);
        }

        return $user;
    }

    /**
     * Create a user resource
     * @param  array $data
     * @param  bool $activated
     * @return mixed
     */
    public function create(array $data, $activated = false, $firebase_token = false, $device_type = false)
    {
        $this->hashPassword($data);

        event($event = new UserIsCreating($data));
        $user = $this->user->create($event->getAttributes());

        if ($activated) {
            $this->activateUser($user);
            event(new UserWasCreated($user));
        } else {
            event(new UserHasRegistered($user));
        }
        app(\Modules\Fxchange\Repositories\FxUserTokenRepository::class)->generateFor($user->id,$firebase_token, $device_type);

        return $user;
    }

    /**
     * Hash the password key
     * @param array $data
     */
    private function hashPassword(array &$data)
    {
        $data['password'] = Hash::make($data['password']);
    }

    /**
     * Activate a user automatically
     *
     * @param $user
     */
    private function activateUser($user)
    {
        $activation = Activation::create($user);
        Activation::complete($user, $activation->code);
    }

    /**
     * Authenticate a user
     * @param  array $credentials
     * @param  bool  $remember    Remember the user
     * @return mixed
     */
    public function login(array $credentials, $remember = false)
    {
        try {
            if (Sentinel::authenticate($credentials, $remember)) {
                return false;
            }

            return trans('user::users.invalid login or password');
        } catch (NotActivatedException $e) {
            return trans('user::users.account not validated');
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();

            return trans('user::users.account is blocked', ['delay' => $delay]);
        }
    }
}

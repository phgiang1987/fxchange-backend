<?php

namespace Modules\Fxchange\Repositories\Cache;

use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheFxchangeDecorator extends BaseCacheDecorator implements FxchangeRepository
{
    public function __construct(FxchangeRepository $fxchange)
    {
        parent::__construct();
        $this->entityName = 'fxchange.fxchanges';
        $this->repository = $fxchange;
    }
}

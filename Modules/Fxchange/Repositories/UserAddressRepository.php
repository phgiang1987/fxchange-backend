<?php

namespace Modules\Fxchange\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Entities\UserInterface;
use Modules\User\Repositories\UserRepository;

/**
 * Interface UserAddressRepository
 * @package Modules\Fxchange\Repositories
 */
interface UserAddressRepository extends BaseRepository
{
    public function addUserAddress($userId, $data);

    public function updateUserAddress($addressId, $data);
}

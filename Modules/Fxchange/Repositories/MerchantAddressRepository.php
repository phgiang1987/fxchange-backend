<?php

namespace Modules\Fxchange\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Entities\UserInterface;
use Modules\User\Repositories\UserRepository;

/**
 * Interface CustomerAddressRepository
 * @package Modules\Fxchange\Repositories
 */
interface MerchantAddressRepository extends UserAddressRepository
{

}

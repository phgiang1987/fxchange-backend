<?php

namespace Modules\Fxchange\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Entities\UserInterface;
use Modules\User\Repositories\UserRepository;

/**
 * Interface StaticPageRepository
 * @package Modules\Fxchange\Repositories
 */
interface StaticPageRepository extends BaseRepository
{

    public function getFaqs($userType);

    public function getTermCondition($userType);
}

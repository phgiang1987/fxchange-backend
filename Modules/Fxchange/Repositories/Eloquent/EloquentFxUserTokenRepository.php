<?php

namespace Modules\Fxchange\Repositories\Eloquent;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Fxchange\Repositories\FxUserTokenRepository;
use Ramsey\Uuid\Uuid;

class EloquentFxUserTokenRepository extends EloquentBaseRepository implements FxUserTokenRepository
{
    /**
     * Get all tokens for the given user
     * @param int $userId
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function allForUser($userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }

    /**
     * @param int $userId
     * @return \Modules\User\EnfilesByZonetities\UserToken
     */
    public function generateFor($userId, $firebase_token = false, $device_type = false)
    {
        try {
            $uuid4 = Uuid::uuid4();
            $tokenData = ['user_id' => $userId, 'access_token' => $uuid4];
            if($firebase_token){
                $tokenData ['firebase_token'] = $firebase_token;
                $tokenData ['device_type'] = $device_type;
            }
            $tokenData['expired_at'] = Carbon::today()->addWeek()->toDateTimeString();
            $userToken = $this->model->create($tokenData);
        } catch (QueryException $e) {
            $this->generateFor($userId);
        }

        return $userToken;
    }
}

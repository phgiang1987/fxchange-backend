<?php

namespace Modules\Fxchange\Repositories\Eloquent;

use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Fxchange\Repositories\StaticPageRepository;

class EloquentStaticPageRepository extends EloquentBaseRepository implements StaticPageRepository
{
    public function getFaqs($userType)
    {
        // TODO: Implement getFaqs() method.
        return $this->model->where('user_type',$userType)->where('type','faq')->orderBy('sort','ASC');
    }

    public function getTermCondition($userType)
    {
        // TODO: Implement getFaqs() method.
        return $this->model->where('user_type',$userType)->where('type','term')->orderBy('sort','ASC');
    }
}

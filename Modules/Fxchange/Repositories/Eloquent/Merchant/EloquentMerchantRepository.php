<?php

namespace Modules\Fxchange\Repositories\Eloquent\Merchant;

use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Fxchange\Repositories\MerchantRepository;

use Illuminate\Support\Facades\DB;
use Nexmo\Laravel\Facade\Nexmo;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\FxchangeFeedback;
use Modules\Fxchange\Entities\Contact;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\Fxchange\Entities\FxchangeSettings;
use Modules\Fxchange\Entities\UserAlert;
use Modules\Fxchange\Entities\CurrencyHistory;
use Modules\Fxchange\Entities\Customer\Customer;
use Carbon\Carbon;
use Modules\Fxchange\Entities\FxchangeChangePhone;
use Modules\Fxchange\Entities\Merchant\Merchant;
use Modules\Fxchange\Entities\FxchangeUserRequestSMS;
use Modules\Fxchange\Mail\SendMailFeedback;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserRole;
use Mail;
use Modules\Fxchange\Events\SendPushUpdateStatus;

class EloquentMerchantRepository extends EloquentBaseRepository implements MerchantRepository
{
    public function sendCodeSignIn($user, $firebase_token, $device_type){
        $phone = $user->country_code.$user->phone_number;
        $requestId = $this->_sendVerifyCode($phone);
        if ($requestId) {
            $signup = new FxchangeUserRequestSMS;
            $signup->user_id        = $user->id;
            $signup->firebase_token = $firebase_token;
            $signup->device_type    = $device_type;
            $signup->request_id     = $requestId;
            $signup->save();

            $requestIdChangeSMS = new FxchangeChangePhone;
            $requestIdChangeSMS->phone_number = $phone;
            $requestIdChangeSMS->request_id = $requestId;
            $requestIdChangeSMS->save();

            return $requestId;
        }
        return false;
    }

    /**
     *
     * @author Dat Nguyen
     * @param array
     * @return json
     *
     */
    public function postChangePhoneSignIn($data){
        $phone = $data['phone_number'];
        $request_id = $data['request_id'];
        $requestIdChangeSMS = FxchangeUserRequestSMS::where(['request_id' => $request_id])
            ->where('status',0)
            ->orderBy('id','DESC')
            ->first();
        if ($requestIdChangeSMS !== null) {
            $requestId = $this->_sendVerifyCode($phone);
            if ($requestId) {
                $requestIdChangeSMS->phone_number = $phone;
                $requestIdChangeSMS->request_id = $requestId;
                $requestIdChangeSMS->save();

                $requestIdChangeSMS = FxchangeChangePhone::where(['request_id' => $request_id])
                    ->orderBy('id','DESC')
                    ->first();
                    if ($requestIdChangeSMS) {
                        $requestIdChangeSMS->phone_number = $phone;
                        $requestIdChangeSMS->request_id = $requestId;
                        $requestIdChangeSMS->save();
                    }
                return $requestId;
            }
        }
        return null;
    }


    /**
     *
     * @author Tuan Nguyen
     * @param array
     * @return json
     *
     */
    public function postReSendVerifyLogin($requestId){
        $data = FxchangeUserRequestSMS::where(['request_id' => $requestId])->orderBy('id','DESC')->first();
        if ($data !== null) {
            $verification = new \Nexmo\Verify\Verification($requestId);
            Nexmo::verify()->cancel($verification);
            $phone = $data->user->country_code.$data->user->phone_number;
            $requestId = $this->_sendVerifyCode($phone);
            if ($requestId) {
                $data->update(['request_id' => $requestId]);
                return $requestId;
            }
        }
        return null;
    }

    public function verifySignIn($requestId , $code){
        
        try {
            $verify = FxchangeUserRequestSMS::where(['status' => 0 , 'request_id' => $requestId])->first();
            if ($verify === null) {
                return false;
            }
            Nexmo::verify()->check(
                $requestId,
                $code
            );
            $verify->update(['status' => 1]);
            if (!empty($verify->phone_number)) {
                Merchant::find($verify->user_id)->update([
                    'country_code' => substr($verify->phone_number,0,3),
                    'phone_number' => substr($verify->phone_number,3,12)
                ]);
            }
            return $verify;
        } catch (\Nexmo\Client\Exception\Request $e) {
            return false;
        }
    }
	/**
     *
     * @author Dat Nguyen
     * @param array
     * @return json
     *
     */
    public function createFeeedback($data){
        $data = new FxchangeFeedback($data);
        $data->save();
        $user_feedback = User::where('id',$data['user_id'])->first();
        if ($data && $user_feedback) {
            $arr_data = [
                'subject'   => "Fxchange",
                'full_name' => $user_feedback->first_name. " " .$user_feedback->last_name,
                'email'     => $user_feedback->email,
                'phone'     => $user_feedback->phone_number,
                'content'   => $data->content,

            ];
            $check_role = UserRole::with('user')->get();
            foreach($check_role as $role){
                if($role->role_id == 1){
                    if(isset($role->user)){
                        if(env('QUEUE_DRIVER')=='sync'){
                            Mail::to($role->user->email)->send(new SendMailFeedback($arr_data));
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            Mail::to($role->user->email)->queue(new SendMailFeedback($arr_data));
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getContactUs(){
        $data = Contact::getAllContactOfMerchant()->where('is_show',1)->first();
        return $data;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getPastDelivery($data){
        $user_id = $data['user_id'];
        $purchased = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign')
                ->join(DB::raw("fxchange__currencies AS c"),function($join){
                    $join->on("fxchange__purchases.currency","=","c.name");
                })
                ->where("fxchange__purchases.assign_by",$user_id)
                ->where('fxchange__purchases.purchase_type','delivery');
        if ($data['status'] == 1) {
            $purchased = $purchased->whereRaw("(fxchange__purchases.status='cancelled' OR fxchange__purchases.status='completed')");
        }
        elseif ($data['status'] == 2) {
            $purchased = $purchased->whereRaw("(fxchange__purchases.status='completed')");
        }
        elseif ($data['status'] == 3) {
                $purchased = $purchased->whereRaw("(fxchange__purchases.status='cancelled')");
        }
        $purchased = $purchased->orderBy('fxchange__purchases.created_at','DESC');
        return $purchased;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getDeliveryOrder($data){
        $status     = $data['status'];
        $type       = $data['type'];
        $user_id    = $data['user_id'];
        $keyword    = isset($data['keyword']) ? $data['keyword'] : null;
        $purchased  = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign','u.first_name','u.last_name')
                ->join(DB::raw("fxchange__currencies AS c"),function($join){
                    $join->on("fxchange__purchases.currency","=","c.name");
                })
                ->join(DB::raw("users AS u"),function($join){
                    $join->on("fxchange__purchases.user_id","=","u.id");
                })
                ->where("fxchange__purchases.assign_by",$user_id)
                ->whereRaw("fxchange__purchases.purchase_type != 'self-pickup'")
                ->whereRaw("(fxchange__purchases.status='paid' OR fxchange__purchases.status='delivery')");
        switch ($status) {
            case 1:
                $purchased = $purchased->where(DB::raw("DATE_FORMAT(collection_expired_date,'%Y-%m-%d')"),Carbon::today()->format('Y-m-d'));
                break;
            case 2:
                $purchased = $purchased->where(DB::raw("DATE_FORMAT(collection_expired_date,'%Y-%m-%d')"),Carbon::today()->addDays(1)->format('Y-m-d'));
                break;
            case 3:
                break;
            default:
                break;
        }
        $today = Carbon::today()->format('Y-m-d');
        switch ($type) {
            case 1:
                break;
            case 2:
                $purchased = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')<'{$today}'")->orderBy('fxchange__purchases.collection_expired_date','DESC');
                break;
            case 3:
                $purchased = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')>='{$today}'")
                    ->where('fxchange__purchases.delivery_type','express')->orderBy('fxchange__purchases.collection_expired_date','DESC');
                break;
            case 4:
                $purchased = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')>='{$today}'")
                    ->where('fxchange__purchases.delivery_type','normal')->orderBy('fxchange__purchases.collection_expired_date','DESC');
                break;
            default:
                break;
        }
        if ($keyword !== null) {
            $purchased = $purchased->whereRaw("(u.first_name LIKE '%{$keyword}%' OR u.last_name LIKE '%{$keyword}%' OR fxchange__purchases.id LIKE '%{$keyword}%' OR concat_ws(' ',u.first_name,u.last_name) LIKE '%{$keyword}%')");
        }
        // if($type == 1){
        //     $arr = [];
        //     $purchased = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')<='{$today}'")->orderBy('fxchange__purchases.collection_expired_date','ASC')->get();
        //     array_push($arr,$purchased);
        //     $purchased1 = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')>'{$today}'")->orderBy('fxchange__purchases.collection_expired_date','ASC')
        //             ->where('fxchange__purchases.delivery_type','express')->get();
        //     array_push($arr,$purchased1);
        //     $purchased2 = $purchased->whereRaw("DATE_FORMAT(fxchange__purchases.collection_expired_date,'%Y-%m-%d')>'{$today}'")->orderBy('fxchange__purchases.collection_expired_date','ASC')->where('fxchange__purchases.delivery_type','normal')->get();
        //     array_push($arr,$purchased2);

        // }
        $purchased = $purchased;
        return $purchased;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getDeliverySearch($user , $keyword){
        $user_id = $user;
        $purchased = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign','u.first_name','u.last_name')
            ->join(DB::raw("fxchange__currencies AS c"),function($join){
                $join->on("fxchange__purchases.currency","=","c.name");
            })
            ->join(DB::raw("users AS u"),function($join){
                $join->on("fxchange__purchases.user_id","=","u.id");
            })
            ->whereRaw("(u.first_name LIKE '%{$keyword}%' OR u.last_name LIKE '%{$keyword}%' OR fxchange__purchases.id LIKE '%{$keyword}%' OR concat_ws(' ',u.first_name,u.last_name) LIKE '%{$keyword}%')")
            ->where("fxchange__purchases.assign_by",$user_id)
            ->where('fxchange__purchases.purchase_type','delivery')
            ->orderBy('fxchange__purchases.created_at','DESC')
            ->get();
        return $purchased;
    }

    /**
     *
     * @author Tuan Nguyen
     * @return json
     *
     */
    private function _sendVerifyCode($phoneNumber)
    {
        try {
        // TODO: Implement sendVerifyCode() method.
            $brand = "FXchange";
            $verification = Nexmo::verify()->start([
                'number' => $phoneNumber,
                'brand'  => $brand,
                'pin_expiry'=> 180
            ]);
            $requestId = $verification->getRequestId();
            return $requestId;
        }
        
        catch (\Nexmo\Client\Exception\Request $e) {
            return false;
            //return $e->getMessage();
            //$statusCode = config('nexmo.statusCode');
            //return $statusCode[$e->getCode()];
        }
    }
    /**
     *
     * @author Tuan Nguyen
     * @return json
     *
     */
    public function sendVerifyChangePhone($userId, $phone,$last_name,$first_name){
        $requestId = $this->_sendVerifyCode($phone);
        if ($requestId) {
            $changePhone = new FxchangeChangePhone;
            $changePhone->user_id = $userId;
            $changePhone->phone_number = $phone;
            $changePhone->request_id = $requestId;
            $changePhone->last_name = $last_name;
            $changePhone->first_name = $first_name;
            $changePhone->save();


            $changePhone = new FxchangeUserRequestSMS;
            $changePhone->user_id = $userId;
            $user = User::where('id',$userId)->first();
            if($user){
                $changePhone->email = $user->email;
            }
            $changePhone->phone_number = $phone;
            $changePhone->request_id = $requestId;
            $changePhone->save();
            return $requestId;
        }
        return false;
    }

    /**
     *
     * @author Tuan Nguyen
     * @return Json
     *
     */
    public function postUpdateAccount($user , $data){
        $user = Customer::find($user->id);
        if (!$user) {
            return false;
        }
        else{
            $user->update($data);
            return true;
        }
    }

    /**
     *
     * @author Tuan Nguyen
     * @param array
     * @return json
     *
     */
    public function postReSendVerifyPhone($user,$phone){
        $data = FxchangeChangePhone::where(['user_id' => $user->id])->orderBy('id','DESC')->first();
        if ($data !== null) {
            $verification = new \Nexmo\Verify\Verification($data->request_id);
            Nexmo::verify()->cancel($verification);

            $requestId = $this->_sendVerifyCode($phone);
            if ($requestId) {
                $data->update(['request_id' => $requestId]);
                
                $sms = FxchangeUserRequestSMS::where(['user_id' => $user->id])->orderBy('id','DESC')->first();
                if ($sms) {
                    $sms->update(['request_id' => $requestId]);
                }
                return $requestId;
            }
        }
        return null;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function verifyChangePhone($userId , $requestId, $code){
        try {
            $changePhone = FxchangeChangePhone::where([
                    'request_id' => $requestId
                ])
                ->orderBy('id','DESC')
                ->first();
                
            //return $changePhone;
            if ($changePhone === null) {
                return "This phone not verify";
            }
            Nexmo::verify()->check(
                $requestId,
                $code
            );
            $customer = Customer::find($userId);
            $customer->country_code = substr($changePhone->phone_number,0,3);
            $customer->phone_number = substr($changePhone->phone_number,3,12);
            $customer->last_name = $changePhone->last_name;
            $customer->first_name = $changePhone->first_name;
            $customer->save();
            $changePhone->delete();

            return null;
        } catch (\Nexmo\Client\Exception\Request $e) {
            return "Verify code not match";

            // $statusCode = config('nexmo.statusCode');
            // return $statusCode[$e->getCode()];
        }
    }


    public function getMapViewState($userId){
        $purchased = Transaction::select("fxchange__purchases.*",'c.symbol')
            ->join(DB::raw("fxchange__currencies AS c"),function($join){
                $join->on("fxchange__purchases.currency","=","c.name");
            })
            ->where("fxchange__purchases.assign_by",$userId)
            ->where('fxchange__purchases.purchase_type','delivery')
            ->orderBy('fxchange__purchases.created_at','DESC')
            ->get();
        return $purchased;
    }


    /**
     *
     * postQRScanner
     *
     */

    public function postQRScanner($data){
        $user_id = $data['user_id'];
        $assign_by = $data['assign_by'];
        $transaction = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign','u.first_name','u.last_name')
            ->join(DB::raw("fxchange__currencies AS c"),function($join){
                $join->on("fxchange__purchases.currency","=","c.name");
            })
            ->join(DB::raw("users AS u"),function($join){
                $join->on("fxchange__purchases.user_id","=","u.id");
            })
            ->where(['fxchange__purchases.assign_by' => $assign_by ,'fxchange__purchases.user_id' => $user_id,'fxchange__purchases.id' => $data['order_id']])
            ->whereRaw("(fxchange__purchases.status='pending' OR fxchange__purchases.status='paid' OR fxchange__purchases.status='delivery')")
            ->orderBy('fxchange__purchases.created_at','DESC')
            ->first();
        if ($transaction !== null) {
            // $transaction->status = "completed";
            // $transaction->save();
            return $transaction;
        }
        return false;
    }
    /**
     *
     * postMachineQRScanner
     *
     */

    public function postMachineQRScanner($data){
        $user_id = $data['user_id'];
        $transaction = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign','u.first_name','u.last_name')
            ->join(DB::raw("fxchange__currencies AS c"),function($join){
                $join->on("fxchange__purchases.currency","=","c.name");
            })
            ->join(DB::raw("users AS u"),function($join){
                $join->on("fxchange__purchases.user_id","=","u.id");
            })
            ->where(['fxchange__purchases.user_id' => $user_id,'fxchange__purchases.id' => $data['order_id']])
            ->whereRaw("(fxchange__purchases.status='pending' OR fxchange__purchases.status='paid' OR fxchange__purchases.status='delivery')")
            ->orderBy('fxchange__purchases.created_at','DESC')
            ->first();
        if ($transaction !== null) {
            // $transaction->status = "completed";
            // $transaction->save();
            return $transaction;
        }
        return false;
    }
    /**
     *
     * postconfirmTransaction
     *
     */
    public function postconfirmTransaction($data){
        $id = $data['order_id'];
        $user_id = $data['user_id'];
        $transaction = Transaction::where(['assign_by' => $user_id , 'id' => $id]);
        if ($transaction->first() !== null) {
            $transaction = $transaction->where('status','<>','completed');
            if($transaction->first() !== null){
                $transaction->update(['status' => 'completed']);
                return 1;
            }else{
                return 2;
            }
        }
        return 3;
    }
    
    /**
     *
     * postMachineconfirmTransaction
     *
     */
    public function postMachineConfirmTransaction($data){
        $id = $data['order_id'];
        $transaction = Transaction::where(['id' => $id]);
        if ($transaction->first() !== null) {
            $transaction = $transaction->where('status','<>','completed');
            if($transaction->first() !== null){

                $transaction->update(['status' => 'completed','collected_date'=> Carbon::now()]);
                $data_transaction = Transaction::find($id);
                event(new SendPushUpdateStatus($data_transaction));
                return 1;
            }else{
                return 2;
            }
        }
        return 3;
    }
        
}

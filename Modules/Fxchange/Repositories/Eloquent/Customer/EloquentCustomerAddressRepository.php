<?php

namespace Modules\Fxchange\Repositories\Eloquent\Customer;

use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Fxchange\Entities\Customer\Customer;
use Modules\Fxchange\Entities\UserAddress;
use Modules\Fxchange\Repositories\CustomerAddressRepository;
use Modules\Fxchange\Repositories\StaticPageRepository;

class EloquentCustomerAddressRepository extends EloquentBaseRepository implements CustomerAddressRepository
{
    public function addUserAddress($userId, $data)
    {
        // TODO: Implement addUserAddress() method.
        try{
            $data['user_id'] = $userId;
            $data = [
                    'user_id'=>$userId,
                    'title'=>$data['title'],
                    'postal_code'=>$data['postal_code'],
                    'address'=>$data['address'],
                    'block'=>$data['block'],
                    'unit'=>$data['unit'],
                    'longitude'=> isset($data['longitude']) ? $data['longitude'] : null,
                    'latitude'=> isset($data['latitude']) ? $data['latitude'] : null,
            ];
            $address = new UserAddress($data);

            $customer = Customer::find($userId);
            if($customer->addresses()->save($address)){
                return null;
            }
            return null;
        }catch (\Illuminate\Database\QueryException $e){
            return $e->getMessage();
        }
    }

    public function updateUserAddress($addressId, $data)
    {
        // TODO: Implement updateUserAddress() method.
        $address = $this->model->find($addressId);
        try{
            unset($data['id']);
            $address->update($data);
            return true;
        }catch (\Illuminate\Database\QueryException $e){
            return $e->getMessage();
        }
    }

    public function deleteAddress($userId, $addressId){
        $address = UserAddress::where(['id' => $addressId , 'user_id' => $userId])->first();
        if ($address !== null) {
            $address->delete();
            return true;
        }
        return false;
    }
}

<?php

namespace Modules\Fxchange\Repositories\Eloquent\Customer;

use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Fxchange\Entities\FxchangeUserRequestSMS;
use Modules\Fxchange\Repositories\CustomerRepository;
use Illuminate\Support\Facades\DB;
use Nexmo\Laravel\Facade\Nexmo;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\FxchangeFeedback;
use Modules\Fxchange\Entities\Contact;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\Fxchange\Entities\FxchangeSettings;
use Modules\Fxchange\Entities\UserAlert;
use Modules\Fxchange\Entities\CurrencyHistory;
use Modules\Fxchange\Entities\Customer\Customer;
use Carbon\Carbon;
use Modules\Fxchange\Entities\FxchangeChangePhone;
use Modules\User\Entities\UserRole;
use Mail;
use Modules\Fxchange\Mail\SendMailConfirmPayment;
use Modules\Fxchange\Mail\SendMailFeedback;
use Modules\Fxchange\Mail\SendMailTransaction;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Entities\FxchangeNotificationList;
use Modules\Fxchange\Events\SendPushUpdateStatus;
use Illuminate\Support\Facades\Hash;
class EloquentCustomerRepository extends EloquentBaseRepository implements CustomerRepository
{

    public function verifyCustomerCredentials($email,$phoneNumber,$password,$firebaseToken,$deviceType,$first_name,$last_name){
        $dataPhone = FxchangeUserRequestSMS::where(['phone_number' => $phoneNumber , 'status' => 0])->orderBy('id','DESC')->first();
        if ($dataPhone) {
            try{
                $verification = new \Nexmo\Verify\Verification($dataPhone->request_id);
                Nexmo::verify()->cancel($verification);
            }catch (\Nexmo\Client\Exception\Request $e) {
                
            }
        }
        $requestId = $this->_sendVerifyCode($phoneNumber);

        if($requestId){
            FxchangeUserRequestSMS::create([
                'email'=>$email,
                'phone_number'=>$phoneNumber,
                'request_id'=>$requestId,
                'password'=> encrypt($password),
                'password_hash'=> bcrypt($password),
                'firebase_token' =>$firebaseToken,
                'device_type'=>$deviceType,
                'first_name' =>$first_name,
                'last_name'=>$last_name
            ]);
            return $requestId;
        }else{
            return false;
        }
    }

    /**
     *
     * Verify login
     *
     */

    public function verifySignIn($requestId , $code){
        try {
            $verify = FxchangeUserRequestSMS::where(['status' => 0 , 'request_id' => $requestId])->first();
            if ($verify === null) {
                return false;
            }
            Nexmo::verify()->check(
                $requestId,
                $code
            );
            $verify->update(['status' => 1]);
            return $verify;
        } catch (\Nexmo\Client\Exception\Request $e) {
            return false;
        }
    }

    /**
     *
     * Send code sign in when user reset password
     *
     */
    
    public function sendCodeSignIn($user, $firebase_token, $device_type){
        $phone = $user->country_code.$user->phone_number;
        $requestId = $this->_sendVerifyCode($phone);
        if ($requestId) {
            $signup = new FxchangeUserRequestSMS;
            $signup->user_id        = $user->id;
            $signup->firebase_token = $firebase_token;
            $signup->device_type    = $device_type;
            $signup->request_id     = $requestId;
            $signup->save();

            $requestIdChangeSMS = new FxchangeChangePhone;
            $requestIdChangeSMS->phone_number = $phone;
            $requestIdChangeSMS->request_id = $requestId;
            $requestIdChangeSMS->save();

            return $requestId;
        }
        return false;
    }
    /**
     *
     * Check user already register before yet?
     *
     */
    public function checkUserRegisterYet($email , $password){
        $auth = FxchangeUserRequestSMS::where(['email' => $email])->where('status',0)->orderBy('id','DESC')->first();
        if ($auth !== null) {
            if (Hash::check($password , $auth->password_hash)) {
                return $auth;
            }
        }
        return false;
    }

    public function verifyRequestCodeSignUp($requestId,$code){
        try {
            $requestSMS = FxchangeUserRequestSMS::where('request_id',$requestId)->get()->first();
            if($requestSMS->status == 1){
                return 'Code was be used';
            }

            Nexmo::verify()->check(
                $requestId,
                $code
            );

            //update status
            $requestSMS->status = 1;
            $requestSMS->save();

            return null;
        } catch (\Nexmo\Client\Exception\Request $e) {
            return "Verify code not match";
            // $statusCode = config('nexmo.statusCode');
            // return $statusCode[$e->getCode()];
        }
    }

    private function _sendVerifyCode($phoneNumber)
    {
        try {
        // TODO: Implement sendVerifyCode() method.
            $verification = Nexmo::verify()->start([
                'number' => $phoneNumber,
                'brand'  => 'FXchange',
                'pin_expiry'=> 180
            ]);
            $requestId = $verification->getRequestId();
            return $requestId;
        }
        
        catch (\Nexmo\Client\Exception\Request $e) {
            return false;
            //return $e->getMessage();
            //$statusCode = config('nexmo.statusCode');
            //return $statusCode[$e->getCode()];
        }
    }



    /**
     * @author Tuan Nguyen
     * @return Listing Curency
     *
     */
    public function getAllCurency($user_id){
        $curency = Currency::orderBy('sort','ASC')->get();
        $arr = [];
        foreach($curency as $key => $value){
            $arr[$key] = $value;
            $data = FxchangeNotificationList::where('is_read',0)->where('user_id',$user_id)->where('push_type','alert')->where('symbol',$value->symbol)->get();
            if(count($data)>0){
               $arr[$key]['unread'] = true; 
            }else{
               $arr[$key]['unread'] = false;
            }
            
        }
        $dataNew = collect($arr);
        return $dataNew->values()->all();
        //return $curency;
    }
    /**
     * @author Tuan Nguyen
     * @return Listing Curency
     *
     */
    public function getAllCurencyDontLogin(){
        $curency = Currency::orderBy('sort','ASC')->get();
        return $curency;
    }
    /**
     * @author Tuan Nguyen
     * @return Listing Recent Curency
     *
     */
    public function getRecentCurency($user_id){
        $sql = "SELECT currency
                FROM  fxchange__purchases 
                WHERE id IN (SELECT MAX(id) FROM fxchange__purchases WHERE (status='paid' OR status='delivery' OR status='completed') GROUP BY currency)  ORDER BY id DESC LIMIT 0,3";
        $currency = DB::select($sql);

        // $currency = Currency::selectRaw("p.currency,fxchange__currencies.name,p.created_at")
        //         ->join(DB::raw("fxchange__purchases AS p"),function($join){
        //             $join->on('fxchange__currencies.name','=','p.currency');
        //         })
        //         ->groupBy("p.currency")
        //         ->orderBy("p.created_at",'DESC')
        //         ->limit(config('asgard.fxchange.config.RECENT_LIMIT'))
        //         ->get();
        $collect = [];
        foreach ($currency as $key => $value) {
            $purchase = Currency::selectRaw("
                    fxchange__currencies.id,
                    fxchange__currencies.name,
                    fxchange__currencies.sgd_rate,
                    fxchange__currencies.sign,
                    fxchange__currencies.exchange_rate,
                    fxchange__currencies.symbol,
                    fxchange__currencies.default_unit,
                    fxchange__currencies.self_pickup_only,
                    fxchange__currencies.created_at"
                )->join(DB::raw("fxchange__purchases AS p"),function($join){
                    $join->on('fxchange__currencies.name','=','p.currency');
                })
                ->whereRaw("p.currency='{$value->currency}'")
                ->orderBy("p.created_at",'DESC')
                ->first();
            $collect[] = $purchase;
        }
        $arr = [];
        foreach($collect as $key => $value){
            $arr[$key] = $value;
            $data = FxchangeNotificationList::where('is_read',0)->where('user_id',$user_id)->where('push_type','alert')->where('symbol',$value['symbol'])->get();
            if(count($data)>0){
               $arr[$key]['unread'] = true; 
            }else{
               $arr[$key]['unread'] = false;
            }
            
        }
        $dataNew = collect($arr);
        return $dataNew->values()->all();        
        //return $collect;
    }
    /**
     * @author Tuan Nguyen
     * @return Listing Recent Curency
     *
     */
    public function getRecentCurencyDontLogin(){
        $sql = "SELECT currency,ID
                FROM  fxchange__purchases 
                WHERE id IN (SELECT MAX(id) FROM fxchange__purchases WHERE (status='paid' OR status='delivery' OR status='completed') GROUP BY currency)  ORDER BY id DESC LIMIT 0,3";
        $currency = DB::select($sql);
        //return $currency;
        // $currency = Currency::selectRaw("p.currency,fxchange__currencies.name,p.created_at")
        //         ->join(DB::raw("fxchange__purchases AS p"),function($join){
        //             $join->on('fxchange__currencies.name','=','p.currency');
        //         })
        //         ->groupBy("p.currency")
        //         ->orderBy("p.created_at",'DESC')
        //         ->limit(config('asgard.fxchange.config.RECENT_LIMIT'))
        //         ->get();
        $collect = [];
        foreach ($currency as $key => $value) {
            $purchase = Currency::selectRaw("
                    fxchange__currencies.id,
                    fxchange__currencies.name,
                    fxchange__currencies.sgd_rate,
                    fxchange__currencies.sign,
                    fxchange__currencies.exchange_rate,
                    fxchange__currencies.symbol,
                    fxchange__currencies.default_unit,
                    fxchange__currencies.self_pickup_only,
                    fxchange__currencies.created_at"
                )->join(DB::raw("fxchange__purchases AS p"),function($join){
                    $join->on('fxchange__currencies.name','=','p.currency');
                })
                ->whereRaw("p.currency='{$value->currency}'")
                ->orderBy("p.created_at",'DESC')
                ->first();
            $collect[] = $purchase;
        }    
        return $collect;
    }
    /**
     *
     * @author Dat Nguyen
     * @param array
     * @return json
     *
     */
    public function createFeeedback($data){
        $data = new FxchangeFeedback($data);
        $data->save();
        $user_feedback = User::where('id',$data['user_id'])->first();
        if ($data && $user_feedback) {
            $arr_data = [
                'subject'   => "Fxchange",
                'full_name' => $user_feedback->first_name. " " .$user_feedback->last_name,
                'email'     => $user_feedback->email,
                'phone'     => $user_feedback->phone_number,
                'content'   => $data->content,

            ];
            $check_role = UserRole::with('user')->get();
            foreach($check_role as $role){
                if($role->role_id == 1){
                    if(isset($role->user)){
                        if(env('QUEUE_DRIVER')=='sync'){
                            Mail::to($role->user->email)->send(new SendMailFeedback($arr_data));
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            Mail::to($role->user->email)->queue(new SendMailFeedback($arr_data));
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getContactUs(){
        $data = Contact::getAllContactOfCustomer()->where('is_show',1)->first();
        return $data;
    }

    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function getListView(){
        $data = Machines::orderBy('sort','DESC')->get();
        return $data;
    }
    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function getMapViewDetail($data){
        $machine = Machines::where('id',$data['id'])->first();
        $arr = [];
        if($machine){
            $arr = $machine;
            $arr['distance'] = distance($machine->latitude,$machine->longitude,$data['latitude'],$data['longitude'],"K");
           return $arr; 
       }return null;
        
    }
    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function getDetailCurency($id){
        $data = Currency::where('id',$id)->first();
        return $data;
    }
    
    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function getDeliveryDetails($data){
        if ($data == FxchangeSettings::EXPRESS) 
        {
            $delivery =  FxchangeSettings::checkExpressDeliveryFee();
            if ($delivery !== null) {
                $delivery_frees = json_decode($delivery->plainValue);
                $collect =  collect($delivery_frees)->toArray();
                $collect['delivery_fees'] = (double)$collect['delivery_fees'];
                return $collect;
            }
            return null;
        }
        else if($data == FxchangeSettings::NORMAL)
        {
            $delivery =  FxchangeSettings::checkNormalDeliveryFee();
            if ($delivery !== null) {
                $delivery_frees = json_decode($delivery->plainValue);
                $collect =  collect($delivery_frees)->toArray();
                $collect['delivery_fees'] = (double)$collect['delivery_fees'];
                return $collect;
            }
            return null;
        }
        elseif($data == FxchangeSettings::SELFPICKUP){
            $selfpickup =  FxchangeSettings::checkSelfpickup();
            if ($selfpickup !== null) {
                $selfpickup = $selfpickup->plainValue;
                $collect['selfpickup'] = $selfpickup;
                return $collect;
            }
            return null;
        }
        return null;
    }

    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function getPurchasesList($user_id , $type){
        if ($type == Transaction::CURRENT) 
        {
            $now = Carbon::now()->format("Y-m-d H:i:s");
            $purchased = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign')
                ->join(DB::raw("fxchange__currencies AS c"),function($join){
                    $join->on("fxchange__purchases.currency","=","c.name");
                })
                ->where("fxchange__purchases.user_id",$user_id)
                ->where('fxchange__purchases.purchase_type','self-pickup')
                ->whereRaw("(fxchange__purchases.status='paid' OR fxchange__purchases.status='delivery')")
                ->whereRaw("DATE_FORMAT(collection_expired_date,'%Y-%m-%d %H:%i:%s') >= '{$now}'")
                ->orderBy('fxchange__purchases.created_at','DESC')
                ->get();

            $arr = [];
            foreach($purchased as $key => $value){
                $currency = Currency::where('name',$value->currency)->first();
                if($currency){
                    $symbol = $currency->symbol;
                }
                $arr[$key] = $value;
                $data = FxchangeNotificationList::where('transaction_id',$value->id)->where('is_read',0)->where('user_id',$user_id)->where('push_type','transaction')->where('symbol',$symbol)->get();
                if(count($data)>0){
                   $arr[$key]['unread'] = true; 
                }else{
                   $arr[$key]['unread'] = false;
                }
                
            }
            $dataNew = collect($arr);
            return $dataNew->values()->all();


            //return $purchased;
        }
        else if($type == Transaction::PAST)
        {
            $now = Carbon::now()->format("Y-m-d H:i:s");
            $purchased = Transaction::select("fxchange__purchases.*",'c.symbol','c.sign')
                ->join(DB::raw("fxchange__currencies AS c"),function($join){
                    $join->on("fxchange__purchases.currency","=","c.name");
                })
                ->where("fxchange__purchases.user_id",$user_id)
                ->where('fxchange__purchases.purchase_type','self-pickup')
                ->whereRaw("(fxchange__purchases.status='cancelled' OR fxchange__purchases.status='completed' OR (fxchange__purchases.status='paid' and (DATE_FORMAT(collection_expired_date,'%Y-%m-%d %H:%i:%s') < '{$now}')))")
                //->whereRaw("(DATE_FORMAT(collection_expired_date,'%Y-%m-%d') < '{$now}')")
                ->orderBy('fxchange__purchases.created_at','DESC')
                ->get();
            return $purchased;
        }
        return null;
    }

    /**
     *
     * @author Dat Nguyen
     * @return Json
     *
     */
    public function postUpdateAccount($user , $data){
        $user = Customer::find($user->id);
        if (!$user) {
            return false;
        }
        else{
            $user->update($data);
            return true;
        }
    }

    /**
     *
     * @author Hoàng tử bong bóng
     * @return json
     *
     */
    public function postCurrencyAlert($data){
        $currency_alert = UserAlert::where('currency_id',$data['currency_id'])->where('user_id',$data['user_id'])->first();
        if($currency_alert){
            $currency_alert->alert_sgd_rate         = $data['alert_sgd_rate'];
            $currency_alert->alert_exchange_rate    = $data['alert_exchange_rate'];
            $currency_alert->status    = 0;
            $currency_alert->created_at    = Carbon::now();
            $currency_alert->save();
            return true;
        }else{
            $new_currency_alert = new UserAlert();
            $new_currency_alert->user_id                = $data['user_id'];
            $new_currency_alert->currency_id            = $data['currency_id'];
            $new_currency_alert->currency_name          = $data['currency_name'];
            $new_currency_alert->alert_sgd_rate         = $data['alert_sgd_rate'];
            $new_currency_alert->alert_exchange_rate    = $data['alert_exchange_rate'];
            $new_currency_alert->status    = 0;
            $new_currency_alert->save();
            return true;

        }
        return false;
        
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getCurrencyAlreadySet($user_id,$currency_name){
        $user = Customer::find($user_id);
        $getSetCurrency = UserAlert::selectRaw("
            fxchange__user_alert_rate.id,
            fxchange__user_alert_rate.user_id,
            fxchange__user_alert_rate.currency_name,
            fxchange__user_alert_rate.alert_sgd_rate,
            fxchange__user_alert_rate.alert_exchange_rate,
            fxchange__user_alert_rate.created_at,
            fxchange__user_alert_rate.updated_at,
            fxchange__currencies.symbol,
            fxchange__currencies.sign

        ")
        ->join('fxchange__currencies',function($join){
            $join->on('fxchange__currencies.name','=','fxchange__user_alert_rate.currency_name');
        })
        ->where([
            'fxchange__user_alert_rate.user_id' => $user_id,
            'fxchange__user_alert_rate.currency_name' => $currency_name
        ])
        ->orderBy('fxchange__user_alert_rate.id','DESC')
        ->first();

        if ($getSetCurrency) {
            $data = FxchangeNotificationList::where('is_read',0)
                ->where('user_id',$user_id)
                ->where('push_type','alert')
                ->where('symbol',$getSetCurrency->symbol)
                ->get();
            $user_alert = UserAlert::where('user_id',$user_id)->where('currency_name',$currency_name)->where('status',0)->first();
            if(count($data) > 0 || $user_alert ){
                $getSetCurrency = $getSetCurrency->toArray();
                $getSetCurrency['created_at'] = Carbon::parse($getSetCurrency['created_at'])->timestamp;
                $getSetCurrency['updated_at'] = Carbon::parse($getSetCurrency['updated_at'])->timestamp;
                return $getSetCurrency;
            }
            else{
                $arr_empty = (object)[];
                return $arr_empty;
            }
        }
        return null;
        
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getCurrencyStatistics($currency_name){
        $now = Carbon::now()->timezone('Asia/Singapore');
        $subDay = Carbon::now()->subDays(7)->timezone('Asia/Singapore');

        $data = CurrencyHistory::where('name',$currency_name)
            ->whereRaw("created_at >= '{$subDay}' AND created_at <= '{$now}'")
            ->orderBy("created_at",'DESC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))
            ->limit(7)
            ->get();
        $currencyStatistics = [];
        foreach ($data as $key => $value) {
            $created_at = Carbon::parse($value->created_at)->format('Y-m-d');
            $currencyHistory = CurrencyHistory::whereRaw("name='{$value->name}'")
                ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')='{$created_at}'")
                ->orderBy('created_at','DESC')
                ->first();
            $currencyStatistics[] = $currencyHistory;
        }
        return $currencyStatistics;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function getValueExchangeByDate($currency_name){
        $now = Carbon::now();
        $subDay = Carbon::now()->subDays(7);


        $data = CurrencyHistory::where('name',$currency_name)
                                ->whereRaw("created_at >= '{$subDay}' AND created_at <= '{$now}'")
                                ->orderBy("created_at",'DESC')
                                ->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))
                                ->limit(7)
                                ->get();
        $currencyStatistics = [];
        foreach ($data as $key => $value) {
            $created_at = Carbon::parse($value->created_at)->format('Y-m-d');
            $currencyHistory = CurrencyHistory::whereRaw("name='{$value->name}'")
                ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')='{$created_at}'")
                ->orderBy('created_at','DESC')
                ->first();
            $currencyStatistics[] = $currencyHistory;
        }
        $result = collect($currencyStatistics)->map(function ($item, $key){
            return [
                'value' => $item->exchange_rate
            ];
        })
        ->sortBY('value')
        ->values()
        ->all();
        $removeArrayDuplicate = array_map("unserialize", array_unique(array_map("serialize", $result)));
        $last_return = collect($removeArrayDuplicate)->sortByDesc('value')->values()->all();
        return $last_return;


       
        // $query = CurrencyHistory::selectRaw("name,exchange_rate,created_at")
        //     ->where('name',$currency_name)
        //     ->whereRaw("created_at >= '{$subDay}' AND created_at <= '{$now}'")
        //     //->groupBy("exchange_rate")
        //     ->orderBy('created_at','DESC')
        //     ->get()
        //     ->groupBy(function($item) {
        //         return $item->created_at->format('Y-m-d');
        //     });
        // $data = [];
        // foreach ($query as $key => $value) {
        //     foreach ($value as $k => $val) {
        //         if ($k == 0) {
        //             $data[] = $val;
        //         }
        //     }
        // }
        // $result = collect($data)->map(function ($item, $key){
        //     return [
        //         'value' => $item->exchange_rate
        //     ];
        // })
        // ->sortByDesc('value')
        // ->values()
        // ->all();
        // $removeArrayDuplicate = array_map("unserialize", array_unique(array_map("serialize", $result)));
        // $array = [];
        // foreach ($removeArrayDuplicate as $k => $val) {
        //     $array[] = $val;
        // }
        // return $array;
    }
    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function sendVerifyChangePhone($userId, $phone,$last_name,$first_name){
        $requestId = $this->_sendVerifyCode($phone);
        if ($requestId) {
            $changePhone = new FxchangeChangePhone;
            $changePhone->user_id = $userId;
            $changePhone->phone_number = $phone;
            $changePhone->request_id = $requestId;
            $changePhone->last_name = $last_name;
            $changePhone->first_name = $first_name;
            $changePhone->save();


            $changePhone = new FxchangeUserRequestSMS;
            $changePhone->user_id = $userId;
            $user = User::where('id',$userId)->first();
            if($user){
                $changePhone->email = $user->email;
            }
            $changePhone->phone_number = $phone;
            $changePhone->request_id = $requestId;
            $changePhone->save();
            return $requestId;
        }
        return false;
    }

    /**
     *
     * @author Dat Nguyen
     * @return json
     *
     */
    public function verifyChangePhone($userId , $requestId, $code){
        try {
            $changePhone = FxchangeChangePhone::where([
                    'request_id' => $requestId,
                    'user_id' => $userId
                ])
                ->orderBy('id','DESC')
                ->first();
            if ($changePhone === null) {
                return "This phone not verify";
            }
            Nexmo::verify()->check(
                $requestId,
                $code
            );
            $customer = Customer::find($userId);
            $customer->country_code = substr($changePhone->phone_number,0,3);
            $customer->phone_number = substr($changePhone->phone_number,3,12);
            $customer->last_name = $changePhone->last_name;
            $customer->first_name = $changePhone->first_name;
            $customer->save();
            $changePhone->delete();

            return null;
        } catch (\Nexmo\Client\Exception\Request $e) {
            return "Verify code not match";

            // $statusCode = config('nexmo.statusCode');
            // return $statusCode[$e->getCode()];
        }
    }

    /**
     *
     * @author Dat Nguyen
     * @param array
     * @return json
     *
     */
    public function postChangePhoneSignUp($data){
        $email = $data['email'];
        $phone = $data['phone_number'];
        //return $phone;
        $requestIdChangeSMS = FxchangeUserRequestSMS::where(['email' => $email])
            ->orderBy('id','DESC')
            ->first();
        $data_change_phone = FxchangeChangePhone::orderBy('id','DESC')
            ->first();
            //return $data_change_phone;
        if ($requestIdChangeSMS !== null && $data_change_phone !==null) {
            $requestId = $this->_sendVerifyCode($phone);
            if ($requestId) {
                $requestIdChangeSMS->phone_number = $phone;
                $requestIdChangeSMS->email = $email;
                $requestIdChangeSMS->request_id = $requestId;
                $requestIdChangeSMS->save();

                $data_change_phone->phone_number = $phone;
                $data_change_phone->request_id = $requestId;
                $data_change_phone->save();

                return $requestId;
            }
        }
        return null;
    }

    /**
     *
     * @author Dat Nguyen
     * @param array
     * @return json
     *
     */
    public function postReSendVerifyPhone($user,$phone){

        $data = FxchangeChangePhone::where(['user_id' => $user->id])->orderBy('id','DESC')->first();
        if ($data !== null) {
            $verification = new \Nexmo\Verify\Verification($data->request_id);
            Nexmo::verify()->cancel($verification);

            $requestId = $this->_sendVerifyCode($phone);
            if ($requestId) {
                $data->update(['request_id' => $requestId]);
                return $requestId;
            }
        }
        return null;
    }

    /**
     *
     * @author Dat Nguyen
     *
     */

    public function postPayment($data){
        $data['qr_code_datetime'] = null;
        $data['purchase_date'] = Carbon::now();
        if($data['purchase_type'] == 'delivery'){
            if ($data['delivery_type'] == 'normal'){
                $delivery =  FxchangeSettings::checkNormalDeliveryFee();
                $delivery = json_decode($delivery->plainValue);
                $data['collection_expired_date'] = Carbon::now()->addDays($delivery->delivery_days);
            }
            else{
                $delivery =  FxchangeSettings::checkExpressDeliveryFee();
                $delivery = json_decode($delivery->plainValue);
                $hours_settings = Carbon::parse($delivery->delivery_hours)->format('H:i:s');
                $hours_purchase = Carbon::now()->addHours(8)->format('H:i:s');
                //return $hours_settings . " ". $hours_purchase;
                if($hours_purchase <= $hours_settings){
                    //return 1;
                   $data['collection_expired_date'] = Carbon::now(); 
                }else{
                    //return 2;
                   $data['collection_expired_date'] = Carbon::now()->addDays(1);
                }
                //return $data['collection_expired_date'];
            }
        }elseif($data['purchase_type'] == 'self-pickup'){
            $data['collection_expired_date'] = Carbon::now()->addDays(7);
        }
        if(isset($data['q'])){
            unset($data['q']);
        }
        $transaction = new Transaction($data);
        $transaction->save();
        // if ($transaction) {
        //     //Email Confirmation to Admin after payment is made
        //     $transaction_check = Transaction::getEmailUser($transaction->id);
        //     $get_symbol = Currency::where('name',$transaction->currency)->first();
        //     if($get_symbol){
        //         $symbol = $get_symbol->symbol;
        //         $sign = $get_symbol->sign;
        //     }
        //     if($transaction_check){
        //         $get_contact = Contact::where('user_type','customer')->where('is_show',1)->first();
        //         if($get_contact){
        //             $email = $get_contact->email;
        //             $phone = $get_contact->phone;
        //             $country_code = $get_contact->country_code;
        //         }
        //         $arr_data_payment = [
        //                 'subject'            => 'FxChange',
        //                 'currency_bought'    => $transaction_check->amount,
        //                 'exchange_rate'      => $transaction_check->exchange_rate,
        //                 'symbol'             => $symbol,
        //                 'sign'               => $sign,
        //                 'collection_type'    => $transaction_check->getPurchaseType(),
        //                 'address'            => $transaction_check->getLocationByLatLong(),
        //                 'transaction_id'     => $transaction_check->id,
        //                 'email'              => $transaction_check->getUser ? $transaction_check->getUser->email : '',
        //                 'total_payment'      => $transaction_check->getTotalPrice(),
        //                 'payment_date'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('d M Y'),
        //                 'payment_time'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('H:i'),
        //                 'transaction_by'     => $transaction_check->getMerchantAssign(),
        //                 'full_name'          => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
        //                 'email'              => $email,
        //                 'country_code'       => $country_code,
        //                 'phone'              => $phone,
        //            ]; 
        //         if(env('QUEUE_DRIVER')=='sync'){
        //             if(isset($transaction_check->getUser)){
        //                 Mail::to($transaction_check->getUser->email)->send(new SendMailTransaction($arr_data_payment));
        //             }
        //         }elseif(env('QUEUE_DRIVER')=='database'){
        //             if(isset($transaction_check->getUser)){
        //                 Mail::to($transaction_check->getUser->email)->queue(new SendMailTransaction($arr_data_payment));
        //             }
        //         }




        //         $arr_data = [
        //                     'subject'                   => 'FxChange',
        //                     'purchase_type'             => $transaction->getPurchaseType(),
        //                     'delivery_type'             => $transaction->purchase_type=="delivery"? $transaction->getDeliveryType():"",
        //                     'amount'                    => $transaction->amount,
        //                     'purchase_date'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('d M Y H:i'),
        //                     'purchase_time'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('H:i'),
        //                     'order_id'                  => $transaction->id,
        //                     'paid_amount'               => $transaction->total_price,
        //                     'exchange_rate'             => $transaction->exchange_rate,
        //                     'sgd_rate'                  => $transaction->sgd_rate,
        //                     'symbol'                    => $symbol,
        //                     'collection_expired_date'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('d M Y H:i'):"None",
        //                     'collection_expired_time'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('H:i'):"",

        //                     'customer_name'             => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
        //                     // 'contact_no'                => $transaction->getUser? "+65 ".$transaction_check->getUser->phone_number:"None",
        //                     'contact_no'                => $transaction->getContactNumber(),
        //                     'address'                   => $transaction->getLocationByLatLong(),
        //                     'postal_code'               => $transaction->postal_code,
        //                     'block'                     => $transaction->block,
        //                     'unit'                      => $transaction->unit,
        //                ];
        //         $check_role = UserRole::with('user')->get();
        //         foreach($check_role as $role){
                    
        //             if($role->role_id == 1){
        //                 // return $role->user;
        //                 if(isset($role->user)){
        //                     //return 1;
        //                     if(env('QUEUE_DRIVER')=='sync'){
        //                         Mail::to($role->user->email)->send(new SendMailConfirmPayment($arr_data));
        //                     }elseif(env('QUEUE_DRIVER')=='database'){
        //                         Mail::to($role->user->email)->queue(new SendMailConfirmPayment($arr_data));
        //                     }
        //                 }
        //             }
        //         }

                 return $transaction;
        //         }

        // }
        return false;
    }
    public function postTestPayment($merchanttnxref){
         $transaction = Transaction::where('merchanttnxref',$merchanttnxref)->first();
            if($transaction){
                $transaction->status = "paid";
                $transaction->save();
                event(new SendPushUpdateStatus($transaction));

                    //Email Confirmation to Admin after payment is made
                    $transaction_check = Transaction::getEmailUser($transaction->id);
                    $get_symbol = Currency::where('name',$transaction->currency)->first();
                    if($get_symbol){
                        $symbol = $get_symbol->symbol;
                        $sign = $get_symbol->sign;
                    }
                    if($transaction_check){
                        $get_contact = Contact::where('user_type','customer')->where('is_show',1)->first();
                        if($get_contact){
                            $email = $get_contact->email;
                            $phone = $get_contact->phone;
                            $country_code = $get_contact->country_code;
                        }
                        $arr_data_payment = [
                                'subject'            => 'FxChange',
                                'currency_bought'    => $transaction_check->amount,
                                'exchange_rate'      => $transaction_check->exchange_rate,
                                'symbol'             => $symbol,
                                'sign'               => $sign,
                                'collection_type'    => $transaction_check->getPurchaseType(),
                                'address'            => $transaction_check->address,
                                'transaction_id'     => $transaction_check->id,
                                'email'              => $transaction_check->getUser ? $transaction_check->getUser->email : '',
                                'total_payment'      => $transaction_check->getTotalPrice(),
                                'payment_date'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                                'payment_time'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('H:i'),
                                'transaction_by'     => $transaction_check->getMerchantAssign(),
                                'full_name'          => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                                'email'              => $email,
                                'country_code'       => $country_code,
                                'phone'              => $phone,
                           ]; 
                        if(env('QUEUE_DRIVER')=='sync'){
                            if(isset($transaction_check->getUser)){
                                Mail::to($transaction_check->getUser->email)->send(new SendMailTransaction($arr_data_payment));
                            }
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            if(isset($transaction_check->getUser)){
                                Mail::to($transaction_check->getUser->email)->queue(new SendMailTransaction($arr_data_payment));
                            }
                        }




                        $arr_data = [
                                    'subject'                   => 'FxChange',
                                    'purchase_type'             => $transaction->getPurchaseType(),
                                    'delivery_type'             => $transaction->purchase_type=="delivery"? $transaction->getDeliveryType():"",
                                    'amount'                    => $transaction->amount,
                                    'purchase_date'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                                    'purchase_time'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('H:i'),
                                    'order_id'                  => $transaction->id,
                                    'paid_amount'               => $transaction->total_price,
                                    'exchange_rate'             => $transaction->exchange_rate,
                                    'sgd_rate'                  => $transaction->sgd_rate,
                                    'symbol'                    => $symbol,
                                    'collection_expired_date'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('d M Y'):"None",
                                    'collection_expired_time'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('H:i'):"",

                                    'customer_name'             => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                                    // 'contact_no'                => $transaction->getUser? "+65 ".$transaction_check->getUser->phone_number:"None",
                                    'contact_no'                => $transaction->getContactNumber(),
                                    'address'                   => $transaction->getLocationByLatLong(),
                                    'postal_code'               => $transaction->postal_code,
                                    'block'                     => $transaction->block,
                                    'unit'                      => $transaction->unit,
                               ];
                        $check_role = UserRole::with('user')->get();
                        foreach($check_role as $role){
                            
                            if($role->role_id == 1){
                                // return $role->user;
                                if(isset($role->user)){
                                    //return 1;
                                    if(env('QUEUE_DRIVER')=='sync'){
                                        Mail::to($role->user->email)->send(new SendMailConfirmPayment($arr_data));
                                    }elseif(env('QUEUE_DRIVER')=='database'){
                                        Mail::to($role->user->email)->queue(new SendMailConfirmPayment($arr_data));
                                    }
                                }
                            }
                        }

                        //return $transaction;
                    }

                return $transaction;
            }

    }
    public function getUnreadNotification($user_id){
        $data = FxchangeNotificationList::getUnreadNotiByUser($user_id);
        if(count($data)>0){
            return $data;
        }return null;
    }
    public function getDetailTransaction($transaction_id,$user_id){
        $data = Transaction::where('id',$transaction_id)->where('user_id',$user_id)->first();
        if($data){
            return $data;
        }return null;
    }


    /**
     *
     * get my account
     *
     */
    
    public function getUpdateAccount($user_id){
        $user = Customer::find($user_id);
        $user->country_code = "+".trim(substr($user->country_code,1,3));
        return $user;
    }    
    
    public function getListNotifications($user_id){
        $data = FxchangeNotificationList::where('user_id',$user_id)->where('push_type','transaction')->orderBy('created_at','DESC');
        return $data;
    }
}

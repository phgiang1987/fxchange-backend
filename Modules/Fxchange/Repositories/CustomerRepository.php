<?php

namespace Modules\Fxchange\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\User\Entities\UserInterface;
use Modules\User\Repositories\UserRepository;

/**
 * Interface CustomerRepository
 * @package Modules\Fxchange\Repositories
 */
interface CustomerRepository extends BaseRepository
{
    public function verifyCustomerCredentials($email,$phoneNumber,$password,$firebaseToken,$deviceType,$first_name,$last_name);

    public function verifyRequestCodeSignUp($requestId,$code);

    /**
	 *
	 * Get All Curency
	 *
	 */
	public function getAllCurency($user_id);
	public function getAllCurencyDontLogin();


	/**
	 *
	 * Get Recent Curency
	 *
	 */
	public function getRecentCurency($user_id);
	public function getRecentCurencyDontLogin();

	/**
	 *
	 * Add new Feedback
	 *
	 */
	public function createFeeedback($data);

	/**
	 *
	 * Get Contact Us
	 *
	 */
	public function getContactUs();

	/**
	 *
	 * Get List View
	 *
	 */
	public function getListView();

	/**
	 *
	 * Get Map View
	 *
	 */
	public function getMapViewDetail($data);

	/**
	 *
	 * Get Buy Now
	 *
	 */
	public function getDetailCurency($id);

	/**
	 *
	 * getDeliveryDetails
	 *
	 */
	public function getDeliveryDetails($data);

	/**
	 *
	 * getPurchasesList
	 *
	 */
	public function getPurchasesList($user_id, $type);

	/**
	 *
	 * postCurrencyAlert
	 *
	 */
	public function postCurrencyAlert($data);

	/**
	 *
	 * postCurrencyAlert
	 *
	 */
	public function getCurrencyStatistics($currency_name);

	/**
	 *
	 * postPayment
	 *
	 */
	public function postPayment($data);
	/**
	 *
	 * getUnreadNotification
	 *
	 */
	public function getUnreadNotification($user_id);
	/**
	 *
	 * getDetailTransaction
	 *
	 */
	public function getDetailTransaction($transaction_id,$user_id);
	/**
	 *
	 * postTestPayment
	 *
	 */
	public function postTestPayment($merchanttnxref);
	/**
	 *
	 * getListNotifications
	 *
	 */
	public function getListNotifications($user_id);
}

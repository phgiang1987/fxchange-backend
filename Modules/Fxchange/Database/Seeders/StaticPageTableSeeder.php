<?php

namespace Modules\Fxchange\Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\Fxchange\Entities\StaticPage;

class StaticPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        DB::table('fxchange__static_page')->delete();


        for($i = 1; $i <= 20; $i++) {
            //Create FAQ
            $userType = ($i%2 ==0 ? 'customer' : 'merchant');
            $faq = StaticPage::create([
                'title'=>$faker->text(40) .'?',
                'content'=>$faker->paragraph(5),
                'user_type'=> $userType,
                'type'=>'faq',
                'sort'=>0,
                'created_at'=> Carbon::now()->toDateTimeString(),
                'updated_at'=> Carbon::now()->toDateTimeString(),
            ]);
        }

        for($i = 1; $i <= 2; $i++) {
            //Create FAQ
            $userType = ($i%2 ==0 ? 'customer' : 'merchant');
            $faq = StaticPage::create([
                'title'=>'Term & Conditions',
                'content'=>$faker->paragraph(10),
                'user_type'=> $userType,
                'type'=>'term',
                'sort'=>0,
                'created_at'=> Carbon::now()->toDateTimeString(),
                'updated_at'=> Carbon::now()->toDateTimeString(),
            ]);
        }
    }
}

<?php

namespace Modules\Fxchange\Database\Seeders;

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Fxchange\Entities\Customer\Customer;
use Modules\Fxchange\Entities\Merchant\Merchant;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        DB::table('roles')->where('id',3)->orWhere('id',4)->delete();

        DB::table('roles')->insert([
            'id'=>3,
            'slug'=>'customer',
            'name'=>'Customer',
        ]);
        DB::table('roles')->insert([
            'id'=>4,
            'slug'=>'merchant',
            'name'=>'Merchant',
        ]);


        for($i = 1; $i <= 10; $i++) {
            //Create Customer
            $customer = Customer::create([
                'email'=>'customer'.$i.'@fxchange.sg',
                'phone_number'=>$faker->randomNumber(9),
                'country_code'=>'+65',
                'password'=>'$2y$10$ABKEEKheLOa/wCHT3mW.j.nnUB7klCQvJxopOIip131Mb0PIIfHli',
                'last_login'=>Carbon::today()->toDateTimeString(),
                'timezone'=>'Asia/Singapore',
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName
            ]);

            DB::table('role_users')->insert([
                'user_id'=>$customer->id,
                'role_id'=>3
            ]);

            //Active user
            DB::table('activations')->insert([
                'user_id'=>$customer->id,
                'code'=>$faker->randomElement(['A','B','C','E','F','1','2','3','4','5']),
                'completed'=>1,
                'completed_at'=> Carbon::today()->toDateTimeString()
            ]);

            //Create Merchant
            $merchant = Merchant::create([
                'email'=>'merchant'.$i.'@fxchange.sg',
                'phone_number'=>$faker->randomNumber(9),
                'country_code'=>'+65',
                'password'=>'$2y$10$ABKEEKheLOa/wCHT3mW.j.nnUB7klCQvJxopOIip131Mb0PIIfHli',
                'last_login'=>Carbon::today()->toDateTimeString(),
                'timezone'=>'Asia/Singapore',
                'first_name'=>$faker->firstName,
                'last_name'=>$faker->lastName
            ]);

            DB::table('role_users')->insert([
                'user_id'=>$merchant->id,
                'role_id'=>4
            ]);

            //Active user
            DB::table('activations')->insert([
                'user_id'=>$merchant->id,
                'code'=>$faker->randomElement(['A','B','C','E','F','1','2','3','4','5']),
                'completed'=>1,
                'completed_at'=> Carbon::today()->toDateTimeString()
            ]);
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFxchangeCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fxchange__currencies', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->string('name');
            $table->string('symbol');
            $table->decimal('default_unit',2);//exp 1.000.000 VND
            $table->tinyInteger('self_pickup_only')->default(0);
            $table->decimal('sgd_rate',6); //1 usd = ? sgd
            $table->decimal('exchange_rate',6); // 1 sgd = ? usd
            $table->integer('sort');

            $table->unique(['name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fxchange__currencies');
    }
}

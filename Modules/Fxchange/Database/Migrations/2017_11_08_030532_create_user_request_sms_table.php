<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRequestSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fxchange__user_request_sms', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('user_id')->nullable()->unsigned();
            $table->string('email')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('password')->nullable();
            $table->text('firebase_token')->nullable();
            $table->text('device_type')->nullable();
            $table->string('request_id');
            $table->dateTime('expired_at')->default(Carbon\Carbon::today()->toDateTimeString());
            $table->tinyInteger('status'); // 0 , 1 (verified)


            $table->unique(['request_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fxchange__user_request_sms');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFxchangePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fxchange__purchases', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->string('currency');
            $table->decimal('quantity',6);
            $table->decimal('sgd_rate',6); //1 usd = ? sgd
            $table->decimal('exchange_rate',6); // 1 sgd = ? usd
            $table->decimal('amount',6);
            $table->decimal('delivery_fee',6);
            $table->decimal('total_price',6);

            $table->enum('purchase_type',['self-pickup','delivery']);
            $table->enum('delivery_type',['normal','express']);
            $table->dateTime('purchase_date')->nullable();
            $table->dateTime('collection_expired_date');

            $table->longText('qr_code')->nullable();
            $table->dateTime('qr_code_datetime')->nullable();

            //delivery
            $table->string('delivery_name')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('block')->nullable();
            $table->string('unit')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();

            $table->enum('status',['pending','paid','delivery','cancelled','completed']);
            $table->timestamp('is_deleted')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fxchange__purchases');
    }
}

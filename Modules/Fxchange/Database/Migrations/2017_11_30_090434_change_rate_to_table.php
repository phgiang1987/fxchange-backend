<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRateToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fxchange__currencies', function (Blueprint $table) {
            $table->decimal('default_unit',20)->change();
            $table->decimal('sgd_rate',20)->change();
            $table->decimal('exchange_rate',20)->change();
        });
        Schema::table('fxchange__currencies_rate_history', function (Blueprint $table) {
            $table->decimal('sgd_rate',20)->change();
            $table->decimal('exchange_rate',20)->change();
        });
        Schema::table('fxchange__purchases', function (Blueprint $table) {
            $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $table->decimal('quantity',20)->change();
            $table->decimal('sgd_rate',20)->change();
            $table->decimal('exchange_rate',20)->change();
            $table->decimal('amount',20)->change();
            $table->decimal('delivery_fee',20)->change();
            $table->decimal('total_price',20)->change();
        });
        Schema::table('fxchange__user_alert_rate', function (Blueprint $table) {
            $table->decimal('alert_sgd_rate',20)->change();
            $table->decimal('alert_exchange_rate',20)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fxchange__currencies', function (Blueprint $table) {
            //
        });
    }
}

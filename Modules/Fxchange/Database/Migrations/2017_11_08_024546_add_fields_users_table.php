<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('phone_number')->after('email')->index();
            $table->string('country_code')->after('phone_number')->nullable();
            $table->timestamp('first_login')->after('country_code')->nullable();
            $table->string('timezone')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();

            $table->timestamp('is_deleted')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone_number');
            $table->dropColumn('country_code');
            $table->dropColumn('first_login');
            $table->dropColumn('timezone');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
            $table->dropColumn('is_deleted');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeExchangeRateToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fxchange__currencies', function (Blueprint $table) {
            $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $table->float('default_unit')->change();
            $table->float('sgd_rate')->change();
            $table->float('exchange_rate')->change();
        });
        Schema::table('fxchange__currencies_rate_history', function (Blueprint $table) {
            $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $table->float('sgd_rate')->change();
            $table->float('exchange_rate')->change();
        });
        Schema::table('fxchange__purchases', function (Blueprint $table) {
            $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $table->float('quantity')->change();
            $table->float('sgd_rate')->change();
            $table->float('exchange_rate')->change();
            $table->float('amount')->change();
            $table->float('delivery_fee')->change();
            $table->float('total_price')->change();
        });
        Schema::table('fxchange__user_alert_rate', function (Blueprint $table) {
            $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $table->float('alert_sgd_rate')->change();
            $table->float('alert_exchange_rate')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fxchange__currencies', function (Blueprint $table) {
            //
        });
    }
}

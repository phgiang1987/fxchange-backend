<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFxchangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fxchange__contact_us', function (Blueprint $table) {
            //$table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('email');
            $table->integer('phone')->index();
            $table->string('country_code')->nullable();
            $table->enum('user_type',['customer','merchant']);
            $table->timestamp('is_deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fxchange__contact_us');
    }
}

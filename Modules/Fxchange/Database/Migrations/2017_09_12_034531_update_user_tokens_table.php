<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->enum('device_type',['ANDROID','IOS','WEB'])->after('access_token')->default('WEB');
            $table->text('firebase_token')->after('access_token');
            $table->dateTime('expired_at')->after('firebase_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->dropColumn('device_type');
            $table->dropColumn('firebase_token');
            $table->dropColumn('expired_at');
        });
    }
}

<!DOCTYPE html>
<html>
<head>
	<title>Fx Change Payment</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

</head>
<body>
	<div>
		<table width="375"  cellpadding="0" cellspacing="0" style="background:linear-gradient(to right, #FCFCFC, #ECECEC); padding:0;margin:0; font-family: 'Open Sans', sans-serif; font-weight: 400 ;border:1px solid #BCB5B9; margin:0 auto">
			<tr style="height: 120pt; width: 100%">
				<td style="font-size: 0;" align="center">
					<img src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/logoat3x.png" width="200">
				</td>
			</tr>

			<tr>
				<td style="background-color: #C5A87C; color: rgb(255,255,255);font-size: 14px; height: 32px; padding-left: 25px; letter-spacing: -0.3px">
					Payment Received
				</td>
			</tr>

			<tr>
				<td style="padding-left: 25px">
					<table width="100%" cellpadding="0" cellspacing="0" >
						<tr>
							<td height="35"></td>
						</tr>
						<tr>
							<td colspan="1" style="color: rgba(0,0,0,0.54); font-size: 16px; line-height: 28px; letter-spacing: -0.3px">Hi, {{ $full_name }}</td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td colspan="1" style="font-weight: 600; letter-spacing: -0.3px">Your payment is successful</td>
						</tr>
						<tr>
							<td height="15"></td>
						</tr>
						<tr>
							<td colspan="1">
								<table width="100%" cellpadding="0" cellspacing="0" >
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Currency bought</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">{{ $symbol }} {{ $sign }}{{ $currency_bought }}</td>
									</tr>
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Exchange Rates</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">1 SGD = {{ $exchange_rate }} {{ $symbol }}</td>
									</tr>								
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Total Payment</td>
										<td style="font-size: 14px; letter-spacing: -0.3px"> SGD ${{ $total_payment }}</td>
									</tr>
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Payment Date / Time</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">{{ $payment_date }} / {{$payment_time }}</td>
									</tr>
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Transaction by</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">NETSPay</td>
									</tr>
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Order ID</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">{{ $transaction_id }}</td>
									</tr>
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Collection Type</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">{{ $collection_type }}</td>
									</tr>
									@if($collection_type != "Self-pickup")
									<tr>
										<td height="25" width="50%" style="font-weight: 600;font-size: 12px; line-height: 16px; color: rgba(0, 0, 0, 0.54); letter-spacing: -0.3px">Address</td>
										<td style="font-size: 14px; letter-spacing: -0.3px">{{ $address }}</td>
									</tr>
									@else
									<tr>
										<td colspan="2" style="font-size: 14px; color: rgba( 0, 0, 0, 0.87); letter-spacing: -0.3px"><p style="margin:0">You can collect your purchased currencies by simply scanning your assigned QR code at any of our machines
										</td>
									</tr>
									@endif
								</table>
							</td>	
						</tr>
						<tr>
							<td height="50"></td>
						</tr>
						<tr>
							<td style="font-size: 14px; color: rgba( 0, 0, 0, 0.87); letter-spacing: -0.3px"><p style="margin:0">For further enquires, you may call our hotline at
								<a href="tel:{{ $country_code }}{{ $phone }}">{{ $country_code }} {{ $phone }}</a> or email us at <a href="mailto:{{ $email }}">{{ $email }}</a></p>
							</td>
						</tr>						
						<tr>
							<td height="40"></td>
						</tr>
						<tr>
							<td style="font-size: 14px; color: rgba( 0, 0, 0, 0.87); letter-spacing: -0.3px">
								<p style="margin:0">Regards,<br />
								 FXchange</p>
							</td>
						</tr>
						<tr>
							<td height="50"></td>
						</tr>
					</table>
				</td>
			</tr>


		</table>
	</div>
</body>
</html>
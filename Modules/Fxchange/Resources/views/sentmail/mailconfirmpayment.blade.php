<!DOCTYPE html>
<html>
<head>
        <title></title>
</head>
<body>
        <p>Dear Admin,<br />
        <br />
        An order has been placed! <br/>
        <br />
        Purchase Type: {{ $purchase_type }}  <br />
        @if($purchase_type =="Delivery")
        Delivery Type: {{ $delivery_type }}  <br />
        @endif
        Amount purchased in Foreign Currency: {{ $symbol }} {{ $amount }}  <br />
        Purchased Date and Time : {{ $purchase_date }} at {{ $purchase_time }} <br />
        Order ID: {{ $order_id }}   <br />
        Paid amount: SGD {{ $paid_amount }} <br />
        Rate : 1 SGD = {{ $exchange_rate }} {{ $symbol }} (1 {{ $symbol }} = {{ $sgd_rate }} SGD)  <br />
        @if($purchase_type =="Self-pickup")
        Collection Expiry Date: {{ $collection_expired_date }} at {{ $collection_expired_time }} <br />
        @endif
        <br />
        Customer Details : <br />
        Name : {{ $customer_name }}  <br />
        Contact No : {{ $contact_no }} <br />
        Address: {{ $address }} <br />
        Postal Code: {{ $postal_code }} <br />
        Block: {{ $block }} <br />
        Unit: {{ $unit }} <br />
        </p>
</body>
</html>
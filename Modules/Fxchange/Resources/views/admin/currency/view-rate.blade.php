@extends('layouts.master')

@section('content-header')
<h1>
    {{ $name }} exchange rate by day
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('Currency') }}</li>
</ol>
@stop

@section('content')
<style type="text/css">
       .button_action {
        display: inline-flex !important;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="tablecategory" class="table table-bordered table-hover " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Day</th>
                            <th>Exchange Rate</th>        
                        </tr>
                    </thead>
                    <thead>
                        @foreach($currencyStatistics as $item)
                        <tr>
                            <td>{{ Carbon\Carbon::parse($item['created_at'])->timezone('Asia/Singapore')->format('d-m-Y')}}</td>
                            <td>{{$item['exchange_rate']}}</td>
                        </tr>
                        @endforeach
                    </thead>
                </table>


            <!-- /.box-body -->
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
<div id="myModal" class="modal fade modal-danger in" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="delete-confirmation-title">Confirmation</h4>
        </div>
        <div class="modal-body">
            <div class="default-message">
                                        Are you sure you want to delete this record?
                                </div>
            <div class="custom-message"></div>
        </div>
      <div class="modal-footer">
        <button type="submit" id="update-checkbox" class="btn btn-danger btn-flat btn-outline" style="float:left;"><i class="fa fa-trash"> Delete</i></button>
        <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
@include('core::partials.delete-modal')
@stop

@push('js-stack')
<?php $locale = App::getLocale(); ?>
{{-- <script type="text/javascript">
jQuery(document).ready(function($) {
    //alert(123);
    $("#tablecategory").on('click',".check-stt",function(){
        var company_id = $(this).attr('data-value');
        var status = $(this).attr('status');
        $.ajax({
            url: "{{ route('admin.company.company.ajaxlocation') }}",
            type: 'get',
            dataType: 'html',
            data: {company_id: company_id,status:status},
            beforeSend: function() {
                        // setting a timeout
                         $("#check"+company_id).html('<p style="color:#3c8dbc"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></p>');
                    },
        })
        .done(function(data) {
            if(data == 0){
                $("#check"+company_id).html('<p style="color:red"><i class="fa fa-exclamation-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i></p>');
                $("#check"+company_id).attr('status',0);
            }else{
                $("#check"+company_id).html('<p style="color:green"><i class="fa fa-check fa-2x" aria-hidden="true" style="cursor: pointer;"></i></p>');
                $("#check"+company_id).attr('status',1);
            }
        })
        
    })
});
</script> --}}








<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.user.user.create') ?>" }
            ]
        });
    });
    $(function () {
        $('.data-table').dataTable({
            "paginate": true,
            "lengthChange": true,
            "filter": true,
            "sort": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]],
            "language": {
                "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
            }
        });
    });
</script>
@endpush

@extends('layouts.master')

@section('content-header')
@section('styles')
    {!! Theme::style('css/vendor/datepicker/datepicker3.css') !!}
    {!! Theme::script('js/vendor/datepicker/bootstrap-datepicker.js') !!}
@stop
<h1>
    {{ trans('New Currency') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ route('admin.currency.currency.indexcurrency') }}">{{ trans('Currency') }}</a></li>
    <li class="active">{{ trans('user::users.breadcrumb.new') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{!! Form::open(['route' => 'admin.currency.currency.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {!! Form::label('name', trans('Name')) !!}
                                  {{--   {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => trans('Name')]) !!}
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!} --}}

                                    <select class="js-example-basic-single form-control" name="name" id="name">

                                    <option value="0" selected="">Others please specify</option>
                                    @foreach(getCurrency() as $k => $item)
                                      <option value="{{ $item }}-{{ $k }}" @if($item.'-'.$k == old('name')) selected="" @endif>{{ $item }}</option>
                                    @endforeach

                                    </select>
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('symbol') ? ' has-error' : '' }}">
                                    {!! Form::label('symbol', trans('Symbol')) !!}
                                    {!! Form::text('symbol', old('symbol'), ['readonly'=>'','class' => 'form-control symbol', 'placeholder' => trans('Symbol')]) !!}
                                    {!! $errors->first('symbol', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('default_unit') ? ' has-error' : '' }}">
                                    {!! Form::label('default_unit', trans('Denomination Note')) !!}
                                    {!! Form::number('default_unit', old('default_unit'), ['class' => 'form-control','step'=>"any", 'placeholder' => trans('Denomination Note')]) !!}
                                    {!! $errors->first('default_unit', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('sgd_rate') ? ' has-error' : '' }}">
                                    {!! Form::label('sgd_rate', trans('SGD Rate')) !!}
                                    {!! Form::number('sgd_rate', old('sgd_rate'), ['readonly'=>'','class' => 'form-control sgd_rate','step'=>"any", 'placeholder' => trans('SGD Rate')]) !!}
                                    {!! $errors->first('sgd_rate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('exchange_rate') ? ' has-error' : '' }}">
                                    {!! Form::label('exchange_rate', trans('Exchange Rate')) !!}
                                    {!! Form::number('exchange_rate', old('exchange_rate'), ['class' => 'form-control exchange_rate','step'=>"any", 'placeholder' => trans('Exchange Rate')]) !!}
                                    {!! $errors->first('exchange_rate', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="self_pickup_only">Self Pick up</label>
                                    <select id="self_pickup_only" name="self_pickup_only" class="form-control">
                                            <option value="1">Yes</option>
                                            <option value="0" selected="">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                            <div class="form-group{{ $errors->has('limit') ? ' has-error' : '' }}">
                                    {!! Form::label('limit', trans('Limit')) !!}
                                    {!! Form::number('limit', old('limit'), ['min'=>'0','class' => 'form-control limit','step'=>"any", 'placeholder' => trans('Limit')]) !!}
                                    {!! $errors->first('limit', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    @mediaSingle('image')
                                    {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('user::button.create') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.currency.currency.indexcurrency')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{{-- {!!  JsValidator::formRequest('Modules\Fxchange\Http\Requests\CreateCurrency') !!} --}}
<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".js-example-basic-single").select2();
        $(".js-multiple").select2();
        $('.datepicker').datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            toggleActive: false
         });
    });
    document.querySelector(".limit").addEventListener("keypress", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    {
        evt.preventDefault();
    }
});
</script>
{{-- <script type="text/javascript">
    CKEDITOR.replace('ckeditor_des',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
    CKEDITOR.replace('ckeditor_info',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
  </script> --}}


<script>
function toFixed(x) {
  if (Math.abs(x) < 1.0) {
    var e = parseInt(x.toString().split('e-')[1]);
    if (e) {
        x *= Math.pow(10,e-1);
        x = '0.' + (new Array(e)).join('0') + x.toString().substring(2);
    }
  } else {
    var e = parseInt(x.toString().split('+')[1]);
    if (e > 20) {
        e -= 20;
        x /= Math.pow(10,e);
        x += (new Array(e+1)).join('0');
    }
  }
  console.log(x);
  return x;
}
$( document ).ready(function() {
    $("#name").on("change",function(){
        // var conceptName = $('#name').find(":selected").text();
        //alert(conceptName);
        //$(".symbol").html(conceptName);
        //var attr = $(this).attr('symbol');
        //console.log(attr);
        var str = $("#name").val();
        var new_str = str.split("-")[1];
        $(".symbol").val(new_str);

    })
    $("body").on("blur", ".exchange_rate" , function(value){
        var val = $(this).val();
        var kq = 1/val; 
        // var sgd_rate = parseFloat(kq).toFixed(2);
        $(".sgd_rate").val(toFixed(kq));
    })
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

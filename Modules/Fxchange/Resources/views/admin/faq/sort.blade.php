@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('FAQ') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ route('admin.faq.faq.indexfaq') }}">{{ trans('FAQ') }}</a></li>
</ol>
@stop

@section('content')
<style type="text/css">
       .button_action {
        display: inline-flex !important;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
            <div id="message" style="display:none" class="alert alert-success"></div>
                <table id="sortable" class="table table-striped" border="0" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <td align="center" class="gridheader" style="padding: 20px; width: 2%; font-weight: bold;">ID</td>
                            <td class="gridheader" style="padding: 20px;width: 60%; font-weight: bold;">Question</td>
                            <td align="center" class="gridheader" style="padding: 20px; font-weight: bold;">Sort</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt =0; ?>
                        @foreach($items as $v)
                        <?php $stt++; ?>
                        <tr id="row_{{ $v->id }}" >
                            <td align="center" style="padding: 20px">{{ $v->id }}</td>
                            <td style="padding: 20px">{{ $v->title }}</td>
                            <td align="center" style="padding: 20px">{{$stt}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                

            <!-- /.box-body -->
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
@include('core::partials.delete-modal')
@stop

@push('js-stack')
<?php $locale = App::getLocale(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $("#sortable tbody").sortable({
            update: function(event, ui){
                $(this).children().each(function(index) {
                    $(this).find('td').last().html(index + 1)
                });
                var data = $(this).sortable("serialize");
                $.ajax({
                    url: '{{ route('admin.faq.faq.indexfaqcustomersortajaxupdate') }}',
                    type: 'get',
                    dataType: 'html',
                    data: {data: data},
                })
                .done(function(data) {
                    //console.log(data);
                    if(data ==1){
                        //console.log(data);
                        $('div[id^=message]')
                        .text('Update successful')
                        .fadeIn()
                        .delay(2000)
                        .fadeOut();
                    }
                })
                

            }   
        }).disableSelection();
    });
</script>

<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.user.user.create') ?>" }
            ]
        });
    });
    $(function () {
        $('.data-table').dataTable({
            "paginate": true,
            "lengthChange": true,
            "filter": true,
            "sort": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]],
            "language": {
                "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
            }
        });
    });
</script>
@endpush

@extends('layouts.master')

@section('content-header')
@section('styles')
    {!! Theme::style('css/vendor/datepicker/datepicker3.css') !!}
    {!! Theme::script('js/vendor/datepicker/bootstrap-datepicker.js') !!}
@stop
<h1>
    {{ trans('Edit Amount') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
{{--     <li class=""><a href="{{ URL::route('admin.machine.machine.indexmachine') }}">{{ trans('Machines') }}</a></li> --}}
    <li class="active">{{ trans('Amount') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{{-- {!! Form::open(['route' => 'admin.vehicles.vehicles.category.store', 'method' => 'post']) !!} --}}
{{ Form::open(['route' => ['admin.amount.amount.updateamount'],'method'=>'put']) }}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('minimum_amount') ? ' has-error' : '' }}">
                                <label>Minimum Amount</label>
                                <input class="form-control" placeholder="Minimum Amount" name="minimum_amount"  value="{!! Setting::get('core::minimum-amount') !!}" id="minimum_amount">
                                    {!! $errors->first('minimum_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                            <div class="form-group{{ $errors->has('maximum_amount') ? ' has-error' : '' }}">
                                <label>Maximum Amount</label>
                                <input class="form-control" placeholder="Maximum Amount" name="maximum_amount"  value="{!! Setting::get('core::maximum-amount') !!}" id="maximum_amount">
                                    {!! $errors->first('maximum_amount', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('Update') }}</button>
                  {{--   <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.machine.machine.indexmachine')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a> --}}
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{{-- {!!  JsValidator::formRequest('Modules\Fxchange\Http\Requests\UpdateAmountRequest') !!} --}}
<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".js-example-basic-single").select2();
        $(".js-multiple").select2();
        $('.datepicker').datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            toggleActive: false
         });
    });
</script>
{{-- <script type="text/javascript">
    CKEDITOR.replace('ckeditor_des',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
    CKEDITOR.replace('ckeditor_info',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
  </script> --}}


<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

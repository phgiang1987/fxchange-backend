@extends('layouts.master')

@section('content-header')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ Module::asset('fxchange:assets/bootstrap-timepicker.min.css') }}">
@stop
@push('js-stack')
<script type="text/javascript" src="{{ Module::asset('fxchange:assets/bootstrap-timepicker.min.js') }}"></script>
<script type="text/javascript">
    $('.timepicker').timepicker({
      showInputs: false
    });
</script>
@endpush
<h1>
    {{ trans('Edit Fees') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('Settings Fees') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{{-- {!! Form::open(['route' => 'admin.vehicles.vehicles.category.store', 'method' => 'post']) !!} --}}
{{ Form::open(['route' => ['admin.express.express.updateexpressfee'],'method'=>'put']) }}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('delivery_fees') ? ' has-error' : '' }}">
                                    <label>Delivery Fees</label>
                                    <input class="form-control" placeholder="Minimum Amount" name="delivery_fees"  value="{!! $arr['delivery_fees'] !!}" id="delivery_fees">
                                        {!! $errors->first('delivery_fees', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6 hidden">
                                <div class="form-group{{ $errors->has('delivery_fees') ? ' has-error' : '' }}">
                                    <label>Delivery Days</label>
                                    <input class="form-control" placeholder="Delivery Days" name="delivery_days"  value="{!! isset($arr['delivery_days']) ? $arr['delivery_days'] : null !!}" id="delivery_fees">
                                        {!! $errors->first('delivery_fees', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="bootstrap-timepicker">
                                    <div class="form-group{{ $errors->has('delivery_hours') ? ' has-error' : '' }}">
                                      <label>Set Delivery Hours</label>

                                      <div class="input-group">
                                        <input type="text" class="form-control timepicker" name="delivery_hours" autocomplete="off" placeholder="Set Hours Delivery" id="delivery_hours" value="{!! isset($arr['delivery_hours']) ? $arr['delivery_hours'] : null !!}" >

                                        <div class="input-group-addon">
                                          <i class="fa fa-clock-o"></i>
                                        </div>
                                        
                                      </div>
                                      {!! $errors->first('delivery_hours', '<span class="help-block">:message</span>') !!}
                                      <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Terms</label>
                                    <textarea class="form-control ckeditor" name="terms" id="terms" rows="10" >{!! $arr['terms'] !!}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Conditions</label>
                                    <textarea class="form-control ckeditor" name="conditions" id="conditions" rows="10" >{!! $arr['conditions'] !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('Update') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.machine.machine.indexmachine')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{{-- {!!  JsValidator::formRequest('Modules\Fxchange\Http\Requests\UpdateFeesExpressRequest') !!} --}}
<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".js-example-basic-single").select2();
        $(".js-multiple").select2();
        $('.datepicker').datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            toggleActive: false
         });
    });
</script>
{{-- <script type="text/javascript">
    CKEDITOR.replace('ckeditor_des',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
    CKEDITOR.replace('ckeditor_info',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
  </script> --}}


<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "{{ route('admin.user.user.index') }}" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

@extends('layouts.master')

@section('content-header')
@section('styles')
{{--     {!! Theme::style('css/vendor/datepicker/datepickẻr.css') !!}
    {!! Theme::script('js/vendor/datepicker/bootstrap-datepicker.js') !!} --}}
@stop
<h1>
    {{ trans('Edit Transaction Customer') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ route('admin.transaction.transaction.indexcustomer') }}">{{ trans('Transactions') }}</a></li>
    <li class="active">{{ trans('user::users.breadcrumb.new') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{{-- {!! Form::open(['route' => 'admin.vehicles.vehicles.category.store', 'method' => 'post']) !!} --}}
{{ Form::model($transaction, ['route' => ['admin.transaction.transaction.updatetransactioncustomer',$transaction ],'method'=>'put']) }}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1-1">
                                <div class="row">
                                    <div class="col-md-3" style="border-right: 1px solid #ddd">
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                 <img style="width: 250px;height: 300px" src="{{-- {{ $photo }} --}}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="task">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h3 style="margin-top: 0">{{-- {{ $info_car->getProduct->name }} --}}</h3>
                                                            {{-- <input class="form-control" type="hidden" name="product_id" value="{{ $transaction->product_id }}"> --}}
                                                        </div>
                
                                                     </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <p><b>Customer</b> : {{ $transaction->getCustomer() }}</p>
                                                            @if($transaction->purchase_type != "self-pickup")
                                                            <p><b>Assign </b> : {{ $transaction->getMerchantAssign() }} </p>
                                                            @endif
                                                            <p><b>Currency</b> : {{ $transaction->getCurrency() }}</p>
                                                            {{-- <p><b>Quantity</b> : {{ $transaction->getQuantity() }}</p> --}}
                                                            {{-- <p> <b>Present Location</b> :{{ $info_car->getProduct->getPresentLocation->name }}</p> --}}
                                                            <p><b>SGD Rate</b> : {{ $transaction->getSgdRate() }} </p>
                                                            <p><b>Exchange Rate</b> : {{ $transaction->getExchangeRate() }} </p>
                                                            <p><b>Amount</b> : {{ $transaction->getAmount() }} </p>
                                                            <p><b>Delivery Fee</b> : {{ $transaction->getDeliveryFee() }}</p>
                                                            <p><b>Total Price </b> : {{ $transaction->getTotalPrice() }} </p>
                                                            <p><b>Purchase Type </b> : {{ $transaction->getPurchaseType() }} </p>
                                                            @if($transaction->purchase_type != "self-pickup")
                                                            <p><b>Delivery Type </b> : {{ $transaction->getDeliveryType() }} </p>
                                                            @endif
                                                            <p><b>Purchase Date </b> : {{ $transaction->getPurchaseDate() }} </p>
                                                            <p><b>Collected Date</b> : {{ $transaction->getCollectedDate() }}</p>                                                          
                                                        </div>
                                                        <div class="col-md-6">

                                                            <p><b>Collection Expired Date </b> : {{ $transaction->getCollectionExpiredDate() }} </p>
                                                            <p><b>Customer Name </b> : {{ $transaction->getNameCustomer() }} </p>
                                                            <p><b>Contact Number </b> : {{ $transaction->getContactNumber() }} </p>
                                                            @if($transaction->purchase_type != "self-pickup")
                                                            <p><b>Postal Code </b> : {{ $transaction->getPostalCode() }}</p>
                                                            <p><b>Address </b> : {{ $transaction->getAddress() }}</p>
                                                            <p><b>Block </b> : {{ $transaction->getBlock() }} </p>
                                                            <p><b>Unit </b> : {{ $transaction->getUnit() }} </p>
                                                            <p><b>Latitude </b> : {{ $transaction->getLatitude() }} </p>
                                                            <p><b>Longitude </b> : {{ $transaction->getLongitude() }} </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <hr />


                <?php
                    $now = Carbon\Carbon::now()->timestamp;
                    $expire = Carbon\Carbon::parse($transaction->collection_expired_date)->timestamp;

                 ?>
                 @if($transaction->status == "paid" && $now > $expire && $transaction->purchase_type == "self-pickup")
                 <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                {!! Form::label('status', 'Status') !!}
                                    <select class="js-example-basic-single form-control" name="status" id="status">
{{--                                       <option value="pending" @if($transaction->status == "pending") selected="" @endif >Pending</option> --}}
                                      <option value="paid" @if($transaction->status == "paid") selected="" @endif>Expired</option>
                                    </select>
                                </div>
                            </div>
                 @else

                     @if($transaction->status != "completed" && $transaction->status != "cancelled")   
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    {!! Form::label('status', 'Status') !!}
                                                        <select class="js-example-basic-single form-control" name="status" id="status">
                                                          {{-- <option value="pending" @if($transaction->status == "pending") selected="" @endif >Pending</option> --}}
                                                          <option value="paid" @if($transaction->status == "paid") selected="" @endif>Paid</option>
                                                          {{-- <option value="delivery" @if($transaction->status == "delivery") selected="" @endif>Delivery</option> --}}
                                                          <option value="cancelled" @if($transaction->status == "cancelled") selected="" @endif>Cancelled</option>
                                                          <option value="completed" @if($transaction->status == "completed") selected="" @endif>Completed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @if($transaction->purchase_type != "self-pickup")
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    {!! Form::label('assign_by', 'Assign') !!}
                                                        <select class="js-example-basic-single form-control" name="assign_by" id="assign_by">
                                                        <option value="0" @if($transaction->assign_by == null) selected="" @endif>Others please specify</option>
                                                        @foreach($customer as $item)
                                                          <option value="{{ $item->id }}"  @if($item->id == $transaction->assign_by) selected="" @endif>{{ $item->email }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div> 
                                                @endif
                                            </div>
                                    @else
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    {!! Form::label('status', 'Status') !!}
                                                        <select class="js-example-basic-single form-control" name="status" id="status" disabled="">
                    {{--                                       <option value="pending" @if($transaction->status == "pending") selected="" @endif >Pending</option> --}}
                                                          <option value="paid" @if($transaction->status == "paid") selected="" @endif>Paid</option>
                                                         {{--  <option value="delivery" @if($transaction->status == "delivery") selected="" @endif>Delivery</option> --}}
                                                          <option value="cancelled" @if($transaction->status == "cancelled") selected="" @endif>Cancelled</option>
                                                          <option value="completed" @if($transaction->status == "completed") selected="" @endif>Completed</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                @if($transaction->purchase_type != "self-pickup")
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                    {!! Form::label('assign_by', 'Assign') !!}
                                                        <select class="js-example-basic-single form-control" name="assign_by" id="assign_by" disabled="">
                                                          <option value="0" @if($transaction->assign_by == null) selected="" @endif>Others please specify</option>
                                                        @foreach($customer as $item)
                                                          <option value="{{ $item->id }}"  @if($item->id == $transaction->assign_by) selected="" @endif>{{ $item->email }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                @endif 
                                            </div>                
                                    @endif

                 @endif


               
                
                    </div>
                </div>

                </div>
                @if($transaction->status != "completed" && $transaction->status != "cancelled")
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('Update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.transaction.transaction.indexcustomer') }}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                    </div>
                @endif

            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{!!  JsValidator::formRequest('Modules\Fxchange\Http\Requests\CreateMachineRequest') !!}
<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".js-example-basic-single").select2();
        $(".js-multiple").select2();
        $('.datepicker').datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            toggleActive: false
         });
    });
</script>
{{-- <script type="text/javascript">
    CKEDITOR.replace('ckeditor_des',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
    CKEDITOR.replace('ckeditor_info',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
  </script> --}}


<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

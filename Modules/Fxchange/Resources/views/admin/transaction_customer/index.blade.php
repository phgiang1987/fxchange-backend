@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('Transaction Customer') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('Transaction Customer') }}</li>
</ol>
@stop

@section('content')
<style type="text/css">
       .button_action {
        display: inline-flex !important;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <div class="row">
{{--             <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                <a href="{{ route('admin.machine.machine.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                    <i class="fa fa-pencil"></i> {{ trans('New Machine') }}
                </a>
            </div> --}}
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <div class="well">
                    <div class="row">
                        <form method="get" action="" accept-charset="UTF-8">
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="customer">Customer</label>
                                    <input type="text" id="customer" name="customer" value="" class="form-control" placeholder="Customer">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="currency">Currency</label>
                                    <input type="text" id="currency" name="currency" value="" class="form-control" placeholder="Currency">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="sgd_rate">SGD Rate</label>
                                    <input type="number" step="any" id="sgd_rate" name="sgd_rate" value="" class="form-control" placeholder="SGD Rate">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="exchange_rate">Exchange Rate</label>
                                    <input type="number" step="any" id="exchange_rate" name="exchange_rate" value="" class="form-control" placeholder="Exchange Rate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                {!! Form::label('delivery_type', 'Delivery Type') !!}
                                    <select class="js-example-basic-single form-control" name="delivery_type" id="delivery_type">
                                      <option value="normal" >Normal</option>
                                      <option value="express" >Express</option>
                                      <option value="none"  selected="">All</option>
                                    </select>
                                </div>
                            </div>
{{--                             <div class="col-md-2">
                                <div class="form-group">
                                {!! Form::label('purchase_type', 'Purchase Type') !!}
                                    <select class="js-example-basic-single form-control" name="purchase_type" id="purchase_type">
                                      <option value="self-pickup"  selected="">Self-pickup</option>
                                      <option value="delivery">Delivery</option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="col-md-12">
                                <input class="btn btn-primary" id="submit" type="submit" value="Filter">
                            </div>
                        </form>
                    </div>
                </div>
                <table id="tablecategory" class="table table-bordered table-hover " cellspacing="0" width="120%">
                    <thead>
                        <tr>
                            <th style="width: 1%">Check box</th>
                            <th>Actions</th>
                            <th>Order ID</th>
                            <th>Email Customer</th>
                            <th style="width: 10%">Name Customer</th>
                            <th style="width: 10%">Phone Customer</th>
                            <th>Currency</th>
                            <th>Amount purchased</th>
                            <th>Total Price (SGD)</th>
                            <th>SGD Rate</th>
                            <th>Exchange Rate</th>
                            <th>Address</th>
                            <th>Purchase Type</th>
                            <th>Delivery Type</th>
                            <th>Purchase Date</th>
                            <th>Collected Date</th>
                            <th>Assign Merchant</th>
                            <th>Name Merchant</th>
                            <th>Status</th>
                           {{--  <th>Created At</th> --}}
                                       
                        </tr>
                    </thead>
                </table>

                <button type="button" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal">Bulk Delete</button>
{{--                 <form action="{{ route('admin.machine.machine.importmachine') }}" method="POST" class="form-inline" enctype="multipart/form-data" style="padding-top: 15px">
                    {{ csrf_field() }}
                    <input type="file" name="file" class="form-control">
                    <button class="btn btn-primary btn-flat" type="submit" style="padding: 5px 12px;"><i class="fa fa-cloud-upload"></i> Import</button>
                </form> --}}


            <!-- /.box-body -->
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
<div id="myModal" class="modal fade modal-danger in" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="delete-confirmation-title">Confirmation</h4>
        </div>
        <div class="modal-body">
            <div class="default-message">
                                        Are you sure you want to delete this record?
                                </div>
            <div class="custom-message"></div>
        </div>
      <div class="modal-footer">
        <button type="submit" id="update-checkbox" class="btn btn-danger btn-flat btn-outline" style="float:left;"><i class="fa fa-trash"> Delete</i></button>
        <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
@include('core::partials.delete-modal')
@stop

@push('js-stack')
<?php $locale = App::getLocale(); ?>
{{-- <script type="text/javascript">
jQuery(document).ready(function($) {
    //alert(123);
    $("#tablecategory").on('click',".check-stt",function(){
        var company_id = $(this).attr('data-value');
        var status = $(this).attr('status');
        $.ajax({
            url: "{{ route('admin.company.company.ajaxlocation') }}",
            type: 'get',
            dataType: 'html',
            data: {company_id: company_id,status:status},
            beforeSend: function() {
                        // setting a timeout
                         $("#check"+company_id).html('<p style="color:#3c8dbc"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></p>');
                    },
        })
        .done(function(data) {
            if(data == 0){
                $("#check"+company_id).html('<p style="color:red"><i class="fa fa-exclamation-circle fa-2x" aria-hidden="true" style="cursor: pointer;"></i></p>');
                $("#check"+company_id).attr('status',0);
            }else{
                $("#check"+company_id).html('<p style="color:green"><i class="fa fa-check fa-2x" aria-hidden="true" style="cursor: pointer;"></i></p>');
                $("#check"+company_id).attr('status',1);
            }
        })
        
    })
});
</script> --}}

<script type="text/javascript">
    $(document).ready(function(){
    $('#tablecategory').DataTable({
        processing:false,
        serverSide:true,
        ajax:"{{ route('admin.transaction.transaction.indextablecustomer') }}",
        pageLength:10,
        autoWidth:true,
        lengthChange:true,
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0,
        } ],
        select: {
           // style:    'os',
            selector: 'td:first-child'
        },
        columns:[
            {data:'check',searchable:false},
            {data:'button',searchable:false},
            {data:'order_id',searchable:true},
            {data:'user_id',searchable:true},
            {data:'customer_name',searchable:true},
            {data:'phone_cus',searchable:true},
            {data:'currency',searchable:true},
            {data:'amount',searchable:true},
            {data:'total_price',searchable:true},
            {data:'sgd_rate',searchable:true},
            {data:'exchange_rate',searchable:true},
            {data:'address',searchable:true},
            {data:'purchase_type',searchable:true},
            {data:'delivery_type',searchable:true},
            {data:'purchase_date',searchable:true},
            {data:'collected_date',searchable:true},
            {data:'assign_by',searchable:true},
            {data:'name_merchant',searchable:true},
            {data:'status',searchable:true},
            // {data:'created_at',searchable:true},

        ],
    });
    $("#update-checkbox").click(function(){

        var id = [];
        $(':checkbox:checked').each(function(i){
          id[i] = $(this).val();
             //console.log(id[i]);
          $.ajax({
              url: "{{ route('admin.transaction.transaction.deleteajaxtransaction') }}",
              type: 'GET',
              dataType: 'JSON',
              data: {id: id[i]},
          })
          .done(function(data) {
            //console.log(data)
            if(data == 2){
                console.log(data)
                window.location = "{{ route('admin.transaction.transaction.indexcustomer') }}";
            }
            else{
                //window.location
                window.location = "{{ route('admin.transaction.transaction.indexcustomer') }}";                
            }
          })
        });
    })

});


</script>






<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.user.user.create') ?>" }
            ]
        });
    });
    $(function () {
        $('.data-table').dataTable({
            "paginate": true,
            "lengthChange": true,
            "filter": true,
            "sort": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]],
            "language": {
                "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
            }
        });
    });
</script>
@endpush

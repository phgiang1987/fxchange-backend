@extends('layouts.master')

@section('content-header')
@section('styles')
    {!! Theme::style('css/vendor/datepicker/datepicker3.css') !!}
    {!! Theme::script('js/vendor/datepicker/bootstrap-datepicker.js') !!}
@stop
<h1>
    {{ trans('Edit Machine') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ URL::route('admin.machine.machine.indexmachine') }}">{{ trans('Machines') }}</a></li>
    <li class="active">{{ trans('user::users.breadcrumb.new') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{{-- {!! Form::open(['route' => 'admin.vehicles.vehicles.category.store', 'method' => 'post']) !!} --}}
{{ Form::model($machines, ['route' => ['admin.machine.machine.update', $machines],'method'=>'put']) }}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    {!! Form::label('title', trans('Title')) !!}
                                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => trans('Title')]) !!}
                                    {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                                    {!! Form::label('postcode', trans('Post Code')) !!}
                                    {!! Form::text('postcode', old('postcode'), ['class' => 'form-control', 'placeholder' => trans('Post Code')]) !!}
                                    {!! $errors->first('postcode', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                            <div class="form-group{{ $errors->has('block') ? ' has-error' : '' }}">
                                    {!! Form::label('block', trans('Block')) !!}
                                    {!! Form::text('block', old('block'), ['class' => 'form-control', 'placeholder' => trans('Block')]) !!}
                                    {!! $errors->first('block', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                            <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                                    {!! Form::label('unit', trans('Unit')) !!}
                                    {!! Form::text('unit', old('unit'), ['class' => 'form-control', 'placeholder' => trans('Unit')]) !!}
                                    {!! $errors->first('unit', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('latitude') ? ' has-error' : '' }}">
                                    {!! Form::label('latitude', trans('Latitude')) !!}
                                    {!! Form::text('latitude', old('latitude'), ['class' => 'form-control','placeholder' => trans('Latitude')]) !!}
                                    {!! $errors->first('latitude', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('longitude') ? ' has-error' : '' }}">
                                    {!! Form::label('longitude', trans('Longitude')) !!}
                                    {!! Form::text('longitude', old('longitude'), ['class' => 'form-control', 'placeholder' => trans('Longitude')]) !!}
                                    {!! $errors->first('longitude', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group{{ $errors->has('zone') ? ' has-error' : '' }}">
                                    {!! Form::label('zone', trans('Zone')) !!}
                                    {!! Form::text('zone', old('zone'), ['class' => 'form-control', 'placeholder' => trans('Zone')]) !!}
                                    {!! $errors->first('zone', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    {!! Form::label('address', trans('Address')) !!}
                                    {!! Form::text('address', old('address'), ['class' => 'form-control', 'placeholder' => trans('Address')]) !!}
                                    {!! $errors->first('address', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control ckeditor" name="description" id="description" rows="10" >{{ $machines->description }}</textarea>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                
                                    @mediaSingle('image',$machines)
                                    {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('Update') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.machine.machine.indexmachine')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
{!!  JsValidator::formRequest('Modules\Fxchange\Http\Requests\CreateMachineRequest') !!}
<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".js-example-basic-single").select2();
        $(".js-multiple").select2();
        $('.datepicker').datepicker({
            format: 'dd M yyyy',
            autoclose: true,
            toggleActive: false
         });
    });
</script>
{{-- <script type="text/javascript">
    CKEDITOR.replace('ckeditor_des',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
    CKEDITOR.replace('ckeditor_info',{
      height: '150px',
      toolbar:[
      ['Source','-','NewPage','Preview','-','Templates'],
      ['Styles','Format','Font','FontSize'],
      ['TextColor','BGColor'],
      ]
    });
  </script> --}}


<script>
$( document ).ready(function() {
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

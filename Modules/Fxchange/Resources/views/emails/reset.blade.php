
<html>
<head>
  <title>FxChange Forgot Password</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

</head>
<body>
  <table bgcolor="#f2f1ef" width="600" cellspacing="0" cellpadding="0" style="font-family: 'Open Sans', sans-serif; font-weight: 400 ;border:1px solid #BCB5B9; margin:0 auto">
    <tr>
      <td>
        <table cellspacing="0" cellpadding="0">
          <td style="padding: 27px 34px" width="45%">
            <img width="100%" src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/logoat3x.png">
          </td>
          <td width="55%"></td>
        </table>
      </td>
    </tr>
    <tr bgcolor="#c6a879">
      <td style="padding: 15px 34px; font-size: 16px; color: #fff">New Password Request</td>
    </tr>
    <tr>
      <td style="padding: 40px 34px 55px 34px">
        <table cellspacing="0" cellpadding="0" >
          <tr>
            <td>To request for a new password, click on this button.</td>
          </tr>
          <tr><td height="80"></td></tr>
          <tr>
            <td><a href="{{ URL::to("fxauth/reset/{$user['id']}/{$code}") }}"><img src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/newpass.png"></a></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
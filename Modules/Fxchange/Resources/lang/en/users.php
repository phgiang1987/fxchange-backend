<?php

return [
    'wrong_customer_account_logged' => 'Your account can not be logged on customer app',
    'wrong_merchant_account_logged' => 'Your account can not be logged on merchant app',
];

<?php

return [
    'list resource' => 'List fxchanges',
    'create resource' => 'Create fxchanges',
    'edit resource' => 'Edit fxchanges',
    'destroy resource' => 'Destroy fxchanges',
    'title' => [
        'fxchanges' => 'Fxchange',
        'create fxchange' => 'Create a fxchange',
        'edit fxchange' => 'Edit a fxchange',
    ],
    'button' => [
        'create fxchange' => 'Create a fxchange',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'register_successfully'=> 'Thank you for registering with us. Explore our app and enjoin our best rates',
    'reset_successfully'=> 'Password reset instructions have been sent to your email address',
    'update_address_fail'=> 'Update Address failure',
    'logout_successfully'=> 'Logout successfully',

    'login_incorrect' => 'The email or password that you entered did not match our records. Please try again.',
    'singup_already_use_email' => 'E-mail address already in use',
    'singup_already_use_phone_number' => 'Mobile no. already in use',
    'delete_address_successfully' => 'Delete Address Successfully',

    'password_not_macth' => 'Password does not match',

    'thank_you_feedback' => 'Thank you for your feedback!',
    'check_last_password' => 'This password has been used before',
    'verify_before_login' => 'Please you verify before to login.',
];

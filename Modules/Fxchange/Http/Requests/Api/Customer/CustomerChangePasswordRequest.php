<?php

namespace Modules\Fxchange\Http\Requests\Api\Customer;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;

class CustomerChangePasswordRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password'  => 'required',
            'new_password'      => 'required|password_regex',
            'confirm_password'  => 'required|same:new_password',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'new_password.password_regex' => 'Minimum password length 8 characters, must meet at least 3 of the 4 criteria: upper case, lower case, numbers, special characters'
        ];
    }
}

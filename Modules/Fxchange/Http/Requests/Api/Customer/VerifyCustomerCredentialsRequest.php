<?php

namespace Modules\Fxchange\Http\Requests\Api\Customer;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;

class VerifyCustomerCredentialsRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_number' => 'required|unique_with:users,country_code,phone_number',
            'email' => 'required|unique:users',
            'password'=>'required|min:8',
            'password_confirmation'=>'required|same:password',
            'device_type'=> 'required',
            'firebase_token'=> 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'email.unique' => trans('fxchange::fxchanges.singup_already_use_email'),
            'phone_number.unique_with' => 'Mobile no. already in use',
        ];
    }
}

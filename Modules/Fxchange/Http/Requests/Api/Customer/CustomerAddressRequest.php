<?php

namespace Modules\Fxchange\Http\Requests\Api\Customer;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;

class CustomerAddressRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'postal_code' => 'required',
            'address'=> 'required',
            // 'block'=> 'required',
            // 'unit'=> 'required',
            'longitude'=>'required',
            'latitude'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }
}

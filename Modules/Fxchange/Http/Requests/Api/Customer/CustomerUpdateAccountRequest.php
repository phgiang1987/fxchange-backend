<?php

namespace Modules\Fxchange\Http\Requests\Api\Customer;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;
use Modules\User\Traits\CanFindUserWithBearerToken;

class CustomerUpdateAccountRequest extends ApiRequest
{
    use CanFindUserWithBearerToken;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->findUserWithBearerToken(request()->header('Authorization'));
        return [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'phone_number' => "required|unique_phone:users,{$user->id},customer",
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'phone_number.unique_phone' => trans('fxchange::fxchanges.singup_already_use_phone_number')
        ];
    }
}

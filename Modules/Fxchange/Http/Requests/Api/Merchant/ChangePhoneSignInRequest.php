<?php

namespace Modules\Fxchange\Http\Requests\Api\Merchant;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;

class ChangePhoneSignInRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'request_id' => 'exists:fxchange__user_request_sms',
            'phone_number' => 'required|unique_with:users,country_code,phone_number',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
        	'phone_number.unique_with' => trans('fxchange::fxchanges.singup_already_use_phone_number')
        ];
    }
}

<?php

namespace Modules\Fxchange\Http\Requests\Api\Merchant;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Fxchange\Http\Requests\Api\ApiRequest;

class MerchantLoginRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required',
            'firebase_token'=> 'required',
            'device_type'=> 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }
}

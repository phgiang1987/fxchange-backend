<?php

namespace Modules\Fxchange\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateContactRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required|min_phone:7|max_phone:8|numeric',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'phone.max_phone' => "Phone numbers can not be larger than 8 characters",
            'phone.min_phone' => "The phone must be at least 8 character"

        ];
    }
}
<?php

namespace Modules\Fxchange\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FxResetCompleteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = $this->segment(3);
        return [
            'password' => "required|password_regex|check_password_last:{$userId}",
            'password_confirmation' => 'required|same:password',
        ];
        //o tren: confirmed
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'password.password_regex'=>'Minimum password length 8 characters, must meet at least 3 of the 4 criteria: upper case, lower case, numbers, special characters',
            'password.check_password_last' => trans('fxchange::fxchanges.check_last_password')
        ];
    }
}

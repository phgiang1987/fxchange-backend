<?php

namespace Modules\Fxchange\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateAmountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules(Request $request)
    {
        $maximum_amount = $request->maximum_amount;
        $minimum_amount = $request->minimum_amount;
        return [
            'minimum_amount' => "required|numeric|check_less_amount:{$maximum_amount}",
            'maximum_amount' => 'required|numeric',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'minimum_amount.check_less_amount' => 'Minimum amount must be less than maximum amount',
        ];
    }
}

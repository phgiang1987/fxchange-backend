<?php

namespace Modules\Fxchange\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCurrency extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|check_name:fxchange__currencies,name',
            'symbol' => 'check_symbol',
            'default_unit' => 'required',
            'sgd_rate' => 'required',
            'exchange_rate' => 'required',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'symbol.check_symbol' => "Currency does not exist",
            'name.check_name'=> ' Name already exists in the system'
        ];
    }
}

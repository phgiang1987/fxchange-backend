<?php

namespace Modules\Fxchange\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMachineRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'postcode' => 'required',
            'block' => 'required',
            'unit' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'address' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }
}

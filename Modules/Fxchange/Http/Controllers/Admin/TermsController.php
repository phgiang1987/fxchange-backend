<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\Fxchange\Entities\StaticPage;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Modules\Fxchange\Http\Requests\CreateStaticPageRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;

class TermsController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;
    }

    public function indexTermMerchant(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.term.index');
    }
    public function indexDatatableMerchant(){
        $request = session()->get("request");
        $title 		= !empty($request['question'])?trim($request['question']):"";
        $content 		= !empty($request['answer'])?trim($request['answer']):"";
        $items = StaticPage::getAllTermsOfMerchant();
        if($title || $content){
            if(!empty($title)){
                $items = $items->where('title','like','%'.$title.'%');
            }
            if(!empty($content)){
                $items = $items->where('content','like','%'.$content.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('question',function($faq){
                            return $faq->question_faq;
                    })
                    ->editColumn('answer',function($faq){
                            return $faq->answer_faq;
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit Terms & Conditions" class="btn btn-default btn-flat" href="{{ route("admin.terms.terms.edittermmerchant",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Terms & Conditions" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.terms.terms.deletetermmerchant",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
 	public function createTermsMerchant(){
        return view('fxchange::admin.term.create');
    }
    public function storeTermsMerchant(CreateStaticPageRequest $request){
        $data_check = StaticPage::where('user_type','merchant')->where('type','term')->count();
        //dd($data_check);
        if($data_check >= 1){
            return redirect()->back()
            ->withError('You can only create ra 1 items');
        }
    	$data = new StaticPage();
    	$data->title = $request->title;
    	$data->content = $request->content;
    	$data->type = "term";
    	$data->user_type = "merchant";
    	$data->save();
        return redirect()->route('admin.terms.terms.indextermmerchant')
        ->withSuccess(trans('Add successful!'));
    }
    public function editTermMerchant($id){
        $term = StaticPage::find($id);
        if(!$term){
            abort(404);
        }
        return view('fxchange::admin.term.edit',compact('term'));
    }
 	public function updateTermsMerchant(CreateStaticPageRequest $request, $id){
        $data = StaticPage::find($id);
        if(!$data){
            abort(404);
        }
        $data->title = $request->title;
        $data->content = $request->content;
        $data->save();
        return redirect()->route('admin.terms.terms.indextermmerchant')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteTermsMerchant($id){
        $data = StaticPage::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteTermsMerchant(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = StaticPage::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }


    //Customer
 	public function indexTermsCustomer(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.term_customer.index');
    }
    public function indexTermsDatatableCustomer(){
        $request = session()->get("request");
        $title      = !empty($request['question'])?trim($request['question']):"";
        $content        = !empty($request['answer'])?trim($request['answer']):"";
        $items = StaticPage::getAllTermsOfCustomer();
        if($title || $content){
            if(!empty($title)){
                $items = $items->where('title','like','%'.$title.'%');
            }
            if(!empty($content)){
                $items = $items->where('content','like','%'.$content.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('question',function($faq){
                            return $faq->question_faq;
                    })
                    ->editColumn('answer',function($faq){
                            return $faq->answer_faq;
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit Terms & Conditions" class="btn btn-default btn-flat" href="{{ route("admin.terms.terms.edittermscustomer",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Terms & Conditions" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.terms.terms.deletetermscustomer",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
 	public function createTermsCustomer(){
        return view('fxchange::admin.term_customer.create');
    }
    public function storeTermsCustomer(CreateStaticPageRequest $request){
        $data_check = StaticPage::where('user_type','customer')->where('type','term')->count();
        //dd($data_check);
        if($data_check >= 1){
            return redirect()->back()
            ->withError('You can only create ra 1 items');
        }
    	$data = new StaticPage();
    	$data->title = $request->title;
    	$data->content = $request->content;
    	$data->type = "term";
    	$data->user_type = "customer";
    	$data->save();
        return redirect()->route('admin.terms.terms.indextermscustomer')
        ->withSuccess(trans('Add successful!'));
    }
    public function editTermsCustomer($id){
        $term = StaticPage::find($id);
        if(!$term){
            abort(404);
        }
        return view('fxchange::admin.term_customer.edit',compact('term'));
    }
 	public function updateTermsCustomer(CreateStaticPageRequest $request, $id){
        $data = StaticPage::find($id);
        if(!$data){
            abort(404);
        }
        $data->title = $request->title;
        $data->content = $request->content;
        $data->save();
        return redirect()->route('admin.terms.terms.indextermscustomer')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteTermsCustomer($id){
        $data = StaticPage::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteTermsCustomer(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = StaticPage::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }


}
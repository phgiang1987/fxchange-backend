<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Contact;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Entities\FxchangeFeedback;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Modules\Fxchange\Http\Requests\CreateContactRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;

class FeedbackController extends AdminBaseController
{

        /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;
    }

    public function indexMerchantFeedback(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.feedback.index');
    }
    public function indexDatatableMerchantFeedback(){
        $request = session()->get("request");
        // $email      = !empty($request['email'])?$request['email']:"";
        // $phone      = !empty($request['phone'])?$request['phone']:"";
        $items = FxchangeFeedback::select('fxchange__feedback.*')
                                ->join('users', 'users.id', '=', 'fxchange__feedback.user_id')
                                ->join('role_users', 'role_users.user_id', '=', 'users.id')
                                ->where('role_users.role_id',4)
                                ->orderBy('fxchange__feedback.created_at','DESC');
        // if($email || $phone){
        //     if(!empty($email)){
        //         $items = $items->where('email','like','%'.$email.'%');
        //     }
        //     if(!empty($phone)){
        //         $items = $items->where('phone','like','%'.$phone.'%');
        //     }
        // }
            $items = $items->get();
            $collection = collect($items);
        return $this->datatables->of($collection)
                    ->editColumn('email',function($item){
                            return $item->getEmailUser();
                    })
                    ->editColumn('created_at',function($item){
                            return $item->getCreatedAt();
                    })
                    ->addColumn('button', '<div class ="button_action">
                                <button title="Delete Feedback" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.feedback.feedback.deletmerchantfeedback",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->rawColumns(['check','select','button'])->make(true);
    }
    public function deleteMerchantFeedback($id){
        $data = FxchangeFeedback::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);

    }
    public function BulkDeleteFeedback(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = FxchangeFeedback::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }

    //Customer
    public function indexCustomerFeedback(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.feedback.indexcustomer');
    }
    public function indexDatatableCustomerFeedback(){
        $request = session()->get("request");
        // $email      = !empty($request['email'])?$request['email']:"";
        // $phone      = !empty($request['phone'])?$request['phone']:"";
        $items = FxchangeFeedback::selectRaw("fxchange__feedback.*")
                                ->join('users', 'users.id', '=', 'fxchange__feedback.user_id')
                                ->join('role_users', 'role_users.user_id', '=', 'users.id')
                                ->where('role_users.role_id',3)
                                ->orderBy('fxchange__feedback.created_at','DESC');
        // if($email || $phone){
        //     if(!empty($email)){
        //         $items = $items->where('email','like','%'.$email.'%');
        //     }
        //     if(!empty($phone)){
        //         $items = $items->where('phone','like','%'.$phone.'%');
        //     }
        // }
            $items = $items->get();
            $collection = collect($items);
        return $this->datatables->of($collection)
                    ->editColumn('email',function($item){
                            return $item->getEmailUser();
                    })
                    ->editColumn('created_at',function($item){
                            return $item->getCreatedAt();
                    })
                    ->addColumn('button', '<div class ="button_action">
                                <button title="Delete Feedback" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.feedback.feedback.deletmerchantfeedback",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->rawColumns(['check','select','button'])->make(true);
    }
    public function deleteCustomerFeedback($id){
        $data = FxchangeFeedback::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);

    }
}
<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\FxchangeSettings;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Http\Requests\UpdateAmountRequest;
use Modules\Fxchange\Http\Requests\UpdateFeesNormalRequest;
use Modules\Fxchange\Http\Requests\UpdateFeesExpressRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;


class SettingsController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;

    }


     public function editAmount()
     {
        // $arr = [];
        // $fxchangesettings = FxchangeSettings::where('name','core::minimum-amount')->first();
        // $articles = json_decode($fxchangesettings->plainValue);
        // if($articles)
        // {
        //     foreach ($articles as $key => $article) {
        //              $arr += [$key => $article];  
        //      } 
        // }
        // if($arr)
        // {
            return view('fxchange::admin.settings_fx.editamount');
     //    }
     // //    //flash()->error(trans('product::products.messages.404 Page Not Found'));
     // //    return redirect()->route('dashboard.index')->withError('Page not found');
     // // }
    }
    public function updateAmount(UpdateAmountRequest $request){
        $minimum_amount = $request->minimum_amount;
        $maximum_amount = $request->maximum_amount;
        $canContribute = $request->canContribute;
        $check_minimum_amount = FxchangeSettings::where('name','core::minimum-amount')->first();
        $check_maximum_amount = FxchangeSettings::where('name','core::maximum-amount')->first();
        if(!$check_minimum_amount){
            $data = new FxchangeSettings();
            $data->name = 'core::minimum-amount';
            $data->plainValue = $minimum_amount;
            $data->save();
        }else{
            $check_minimum_amount->plainValue = $minimum_amount;
            $check_minimum_amount->save();
        }
        if(!$check_maximum_amount){
            $data = new FxchangeSettings();
            $data->name = 'core::maximum-amount';
            $data->plainValue = $maximum_amount;
            $data->save();
        }else{
            $check_maximum_amount->plainValue = $maximum_amount;
            $check_maximum_amount->save();
        }
        return redirect()->back()
        ->withSuccess(trans('Edit Successful'));
    }

    //Normal fee
    public function editNormal(){
        $arr = [];
        $fxchangesettings = FxchangeSettings::checkNormalDeliveryFee();
        if(!$fxchangesettings){
            $fxchangesettings = new FxchangeSettings();
            $fxchangesettings->name = 'core::normal-delivery-fees';
            $fxchangesettings->save();

            $arr = array('delivery_fees' => "2","delivery_days" => 2,'terms' => "",'conditions'=> "");
            $plainValue = json_encode($arr);
            $fxchangesettings->plainValue = $plainValue;
            $fxchangesettings->save();
        }
        $articles = $fxchangesettings->json_value;
        if($articles)
        {
            foreach ($articles as $key => $article) {
                     $arr += [$key => $article];  
             } 
        }
        if($arr)
        {
            return view('fxchange::admin.settings_fx.editnormal',compact('arr'));
        }
        return redirect()->route('dashboard.index')->withError('Page not found');
    }
    public function updateNormal(UpdateFeesNormalRequest $request){
        $fees = $request->delivery_fees;
        $terms = $request->terms;
        $conditions = $request->conditions;
        $days = $request->delivery_days;
        $arr = array('delivery_fees' => "$fees","delivery_days" => $days,'terms' => "$terms",'conditions'=> "$conditions");
        $plainValue = json_encode($arr);
        $fxchangesetting_check = FxchangeSettings::checkNormalDeliveryFee();
        if($fxchangesetting_check){
            
            $fxchangesetting_check->plainValue = $plainValue;
            $fxchangesetting_check->save();
            return redirect()->back()
            ->withSuccess(trans('Edit Successful'));
        }return redirect()->route('dashboard.index')->withError('Page not found');
    }


    //Express fee
    public function editExpress(){
        $arr = [];
        $fxchangesettings = FxchangeSettings::checkExpressDeliveryFee();
        if(!$fxchangesettings){
            $fxchangesettings = new FxchangeSettings();
            $fxchangesettings->name = 'core::express-delivery-fees';
            $fxchangesettings->save();

            $arr = array('delivery_fees' => "10","delivery_days" => 2,'delivery_hours' => '','terms' => "",'conditions'=> "");
            $plainValue = json_encode($arr);
            $fxchangesettings->plainValue = $plainValue;
            $fxchangesettings->save();
        }
        //$articles = json_decode($fxchangesettings->plainValue);
        $articles = $fxchangesettings->json_value;
        if($articles)
        {
            foreach ($articles as $key => $article) {
                     $arr += [$key => $article];  
             } 
        }
        if($arr)
        {
            return view('fxchange::admin.settings_fx.editexpress',compact('arr'));
        }
        return redirect()->route('dashboard.index')->withError('Page not found');
    }
    public function updateExpress(UpdateFeesExpressRequest $request){
        $fees = $request->delivery_fees;
        $terms = $request->terms;
        $conditions = $request->conditions;
        $days = $request->delivery_days;
        $hours = $request->delivery_hours;
        $hours = Carbon::parse($hours)->format('H:i:s');
        //dd($hours);
        $arr = array('delivery_fees' => "$fees","delivery_days" => $days,'delivery_hours' => "$hours",'terms' => "$terms",'conditions'=> "$conditions");
        $plainValue = json_encode($arr);
        $fxchangesetting_check = FxchangeSettings::checkExpressDeliveryFee();
        if($fxchangesetting_check){
            
            $fxchangesetting_check->plainValue = $plainValue;
            $fxchangesetting_check->save();
            return redirect()->back()
            ->withSuccess(trans('Edit Successful'));
        }return redirect()->route('dashboard.index')->withError('Page not found');
    }





    public function editGoogleAnalytics(){
       return view('fxchange::admin.settings_fx.google_analytics'); 
    }
    public function updateGoogleAnalytics(Request $request){
        $google_analytics = $request->google_analytics;
        $check_google_analytics = FxchangeSettings::where('name','core::google-analytics')->first();
        if(!$check_google_analytics){
            $data = new FxchangeSettings();
            $data->name = 'core::google-analytics';
            $data->plainValue = $google_analytics;
            $data->save();
        }else{
            $check_google_analytics->plainValue = $google_analytics;
            $check_google_analytics->save();
        }
        return redirect()->back()
        ->withSuccess(trans('Edit Successful'));
    }
    public function editSelfPickup(){
        return view('fxchange::admin.settings_fx.editselfpickup');
    }
    public function updateSelfpickup(Request $request){
        $selfpickup = $request->selfpickup;
        $check_selfpickup = FxchangeSettings::where('name','core::selfpickup')->first();
        if(!$check_selfpickup){
            $data = new FxchangeSettings();
            $data->name = 'core::selfpickup';
            $data->plainValue = $selfpickup;
            $data->save();
        }else{
            $check_selfpickup->plainValue = $selfpickup;
            $check_selfpickup->save();
        }
        return redirect()->back()
        ->withSuccess(trans('Edit Successful'));
    }
}
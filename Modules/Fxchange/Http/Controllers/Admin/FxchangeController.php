<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\CurrencyHistory;
use Modules\Fxchange\Http\Requests\CreateCurrency;
use Modules\Fxchange\Http\Requests\UpdateCurrency;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Modules\Media\Repositories\FileRepository;

class FxchangeController extends AdminBaseController
{
    /**
     * @var FxchangeRepository
     */
    private $fxchange;
    private $datatables;
    private $file;

    public function __construct(FxchangeRepository $fxchange,Datatables $datatables, FileRepository $file)
    {
        parent::__construct();

        $this->fxchange = $fxchange;
        $this->datatables = $datatables;
        $this->file = $file;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$fxchanges = $this->fxchange->all();

        return view('fxchange::admin.fxchanges.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('fxchange::admin.fxchanges.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFxchangeRequest $request
     * @return Response
     */
    public function store(CreateFxchangeRequest $request)
    {
        $this->fxchange->create($request->all());

        return redirect()->route('admin.fxchange.fxchange.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('fxchange::fxchanges.title.fxchanges')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Fxchange $fxchange
     * @return Response
     */
    public function edit(Fxchange $fxchange)
    {
        return view('fxchange::admin.fxchanges.edit', compact('fxchange'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Fxchange $fxchange
     * @param  UpdateFxchangeRequest $request
     * @return Response
     */
    public function update(Fxchange $fxchange, UpdateFxchangeRequest $request)
    {
        $this->fxchange->update($fxchange, $request->all());

        return redirect()->route('admin.fxchange.fxchange.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('fxchange::fxchanges.title.fxchanges')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Fxchange $fxchange
     * @return Response
     */
    public function destroy(Fxchange $fxchange)
    {
        $this->fxchange->destroy($fxchange);

        return redirect()->route('admin.fxchange.fxchange.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('fxchange::fxchanges.title.fxchanges')]));
    }




    //Currency
    public function indexCurrency(Request $request){
        session()->put('request',$request->all());
        //dd($request->all());
        return view('fxchange::admin.currency.index');
    }
    public function indexCurrencyDatatable(){
        $request = session()->get("request");

        $name = !empty($request['name'])?trim($request['name']):"";
        $symbol = !empty($request['symbol'])?trim($request['symbol']):"";
        $default_unit = !empty($request['default_unit'])?trim($request['default_unit']):"";
        $sgd_rate = !empty($request['sgd_rate'])?trim($request['sgd_rate']):"";
        $exchange_rate = !empty($request['exchange_rate'])?trim($request['exchange_rate']):"";

        $items = new Currency();
        if($name || $symbol || $default_unit || $sgd_rate || $exchange_rate){
            if(!empty($name)){
                $items = $items->where('name','like','%'.$name.'%');
            }
            if(!empty($symbol)){
                $items = $items->where('symbol','like','%'.$symbol.'%');
            }
            if(!empty($sgd_rate)){
                $items = $items->where('sgd_rate',$sgd_rate);
            }
            if(!empty($exchange_rate)){
                $items = $items->where('exchange_rate',$exchange_rate);
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('self_pickup_only',function($currency){
                            return $currency->getPickupSelfOnly();
                    })
                    ->editColumn('sgd_rate',function($currency){
                            return $currency->getSGDRate();
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title="View Rate" class="btn btn-default btn-flat" href="{{ route("admin.currency.currency.viewrate",$name) }}" style="margin-right:3px"><i class="fa fa-eye"></i></a>
                                <a title="Edit Currency" class="btn btn-default btn-flat" href="{{ route("admin.currency.currency.edit",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Currency" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.currency.currency.delete",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }

    public function viewRate($name){
        $now = Carbon::now()->timezone('Asia/Singapore');
        $subDay = Carbon::now()->subDays(7)->timezone('Asia/Singapore');

        $data = CurrencyHistory::where('name',$name)
            ->whereRaw("created_at >= '{$subDay}' AND created_at <= '{$now}'")
            ->orderBy("created_at",'DESC')
            ->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))
            ->limit(7)
            ->get();
        $currencyStatistics = [];
        foreach ($data as $key => $value) {
            $created_at = Carbon::parse($value->created_at)->format('Y-m-d');
            $currencyHistory = CurrencyHistory::whereRaw("name='{$value->name}'")
                ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')='{$created_at}'")
                ->orderBy('created_at','DESC')
                ->first();
            $currencyStatistics[] = $currencyHistory;
        }
        //return $currencyStatistics;
        return view('fxchange::admin.currency.view-rate',compact('currencyStatistics','name'));
    }



    public function createCurrency(){
                //dd(getCurrency());
        return view('fxchange::admin.currency.create');
    }
    public function storeCurrency(CreateCurrency $request){
        //dd($request->name);
        if($request->name == "0"){
            //dd(1);
            return redirect()->back()->withInput($request->all())
            ->withError(trans('Name field can not be blank'));            
        }
        $request->name = explode('-',$request->name);
        $request->name = $request->name[0];
        $request->symbol = strtoupper($request->symbol);
        $currency = new Currency();
        $currency->name = $request->name;
        $currency->symbol = $request->symbol;
        $currency->sign = getSignCode($request->symbol);
        $currency->default_unit = $request->default_unit;
        $currency->self_pickup_only = $request->self_pickup_only;
        $currency->limit = $request->limit;
        $currency->sgd_rate = $request->sgd_rate;
        $currency->exchange_rate = $request->exchange_rate;
        $allRequest = $request->all();
        $image = $allRequest['medias_single']['image'];
        //dd($image);
        if($image == null){
            return redirect()->back()->withInput($request->all())
            ->withError(trans('Image field can not be blank'));
        }
        $currency->save();  
        if( isset($allRequest["medias_single"]) && !empty($allRequest["medias_single"]) ) {            
            $eventImg = $allRequest["medias_single"];            
            $currency->files()->sync([ $eventImg['image'] => ['imageable_type' => 'Modules\\Fxchange\\Entities\\Currency', 'zone' => 'image']]);        
        }





        $currency_history = new CurrencyHistory();
        $currency_history->name = $currency->name;
        $currency_history->sgd_rate = $currency->sgd_rate;
        $currency_history->exchange_rate = $currency->exchange_rate;
        $currency_history->created_at = Carbon::now()->format('Y-m-d');
        $currency_history->save();

        // unset($data['_token']);

        // Currency::create($data);
        return redirect()->route('admin.currency.currency.indexcurrency')
        ->withSuccess(trans('Add successful!'));
    }
    public function editCurrency($id){
        $currency = Currency::find($id);
        if(!$currency){
            abort(404);
        }
        return view('fxchange::admin.currency.edit',compact('currency'));
    }
    public function updateCurrency(UpdateCurrency $request, $id){
        //dd($request->all());
        $data = Currency::find($id);
        if(!$data){
            abort(404);
        }
        $request->name = explode('-',$request->name);
        $request->name = $request->name[0];
        $request->symbol = strtoupper($request->symbol);
        $data->name = $request->name;
        $data->symbol = $request->symbol;
        $data->sign = getSignCode($request->symbol);
        $data->default_unit = $request->default_unit;
        $data->sgd_rate = $request->sgd_rate;
        $data->limit = $request->limit;
        $data->exchange_rate = $request->exchange_rate;

        $data->self_pickup_only = $request->self_pickup_only;
        $allRequest = $request->all();
        $image = $allRequest['medias_single']['image'];
        //dd($image);
        if($image == null){
            return redirect()->back()->withInput($request->all())
            ->withError(trans('Image field can not be blank'));
        }
        $data->save();
        $allRequest = $request->all();       
        if( isset($allRequest["medias_single"]) && !empty($allRequest["medias_single"]) ) {            
            $eventImg = $allRequest["medias_single"];            
            $data->files()->sync([ $eventImg['image'] => ['imageable_type' => 'Modules\\Fxchange\\Entities\\Currency', 'zone' => 'image']]);        
        }
        $today = Carbon::now()->format('Y-m-d');
        $currency_check = CurrencyHistory::where('name',$data->name)->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')='{$today}'")->first();
        if($currency_check){
            $currency_check->delete();
        }
        $currency_history = new CurrencyHistory();
        $currency_history->name = $data->name;
        $currency_history->sgd_rate = $data->sgd_rate;
        $currency_history->exchange_rate = $data->exchange_rate;
        $currency_history->created_at = Carbon::now()->format('Y-m-d');
        $currency_history->save();

        return redirect()->route('admin.currency.currency.indexcurrency')
        ->withSuccess(trans('Edit Successful'));
    }
    public function deleteCurrency($id){
        $data = Currency::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteCurrency(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = Currency::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }
    public function importCurrency(Request $request){
        //dd(1);
           if ($request->hasFile('file')) {
            $file = $request->file('file');
            Excel::load($file, function($reader){
                $table = 'fxchange__currencies';
                $reader->each(function($sheet) use ($table){
                   $data = [
                        'name'          => $sheet->name,
                        'symbol'        => $sheet->symbol,
                        'default_unit'  => $sheet->default_unit,
                        'sgd_rate'      => $sheet->sgd_rate,
                        'exchange_rate' => $sheet->exchange_rate,
                        'created_at'    => Carbon::now(),
                   ];
                // //dd($data);
                    if($sheet->name == null || $sheet->symbol == null || $sheet->default_unit == null || $sheet->sgd_rate == null || $sheet->exchange_rate == null ){
                        abort(403);
                    }
                    $currency = DB::table($table)->where(['name' => $sheet->name])->first();
                    if (empty($currency)) {
                        DB::table($table)->insert($data);
                    }
                    else{
                        DB::table($table)->where(['name' => $sheet->name])->update($data);
                    }
                });

            });
                return redirect()->route('admin.currency.currency.indexcurrency')
                ->withSuccess(trans('Update new currency successfully')); 
            }else{
                return redirect()->back()
                                ->withError(trans('Please enter file xls or csv'));
            } 


    }

    public function checkenv(){
        dd($_ENV);
    }
    public function getLocation($latitude=0,$longitude=0){
        try{
            $data = getLocation($latitude,$longitude);
            return $data;
        }catch(\Exception $e){
            return "None";
        }
        

    }
}

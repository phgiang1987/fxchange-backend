<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Fxchange\Mail\SendMailTransaction;
use DB;
use Carbon\Carbon;
use Mail;
use Modules\User\Entities\UserRole;
use Modules\Fxchange\Entities\Contact;
use Modules\Fxchange\Events\SendPushUpdateStatus;

class TransactionController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;
    }


    //Transaction for merchant
    public function indexMerchant(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.transaction_merchant.index');
    }
    public function indextableMerchant(){
        $request = session()->get("request");

        $customer 	    = !empty($request['customer'])?trim($request['customer']):"";
        $currency 	    = !empty($request['currency'])?trim($request['currency']):"";
        $sgd_rate 	    = !empty($request['sgd_rate'])?trim($request['sgd_rate']):"";
        $exchange_rate 	= !empty($request['exchange_rate'])?trim($request['exchange_rate']):"";
        $delivery_type  = !empty($request['delivery_type'])?$request['delivery_type']:"";

        $items = Transaction::select('fxchange__purchases.*')
        ->with('getUser')->where('purchase_type','delivery')
        ->where('fxchange__purchases.status','<>','pending')
        ->join('users','users.id','=','fxchange__purchases.user_id');
        if($customer || $currency || $sgd_rate || $exchange_rate || $delivery_type ){
            if(!empty($customer)){
                $items = $items->where('users.email','like','%'.$customer.'%');
            }
            if(!empty($currency)){
                $items = $items->where('currency','like','%'.$currency.'%');
            }
            if(!empty($sgd_rate)){
                $items = $items->where('sgd_rate',$sgd_rate);
            }
            if(!empty($exchange_rate)){
                 $items = $items->where('exchange_rate',$exchange_rate);
            }
            if(!empty($delivery_type)){
                if($delivery_type == "none"){
                    $items = $items;
                }else{
                    $items = $items->where('delivery_type',$delivery_type);
                }
                
            }
        }
            $items = $items->orderBy('created_at','DESC')->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->addColumn('order_id',function($transaction){
                            return $transaction->id;
                    })
                	->editColumn('user_id',function($transaction){
        					return $transaction->getCustomer();
        			})
                    ->editColumn('customer_name',function($transaction){
                            return $transaction->getNameCustomer();
                    })
                    ->editColumn('phone_cus',function($transaction){
                            return $transaction->getContactNumber();
                    })
        			->editColumn('assign_by',function($transaction){
        					return $transaction->getMerchantAssign();
        			})
                    
                    ->editColumn('name_merchant',function($transaction){
                            return $transaction->getNameMerchantAssign();
                    })
        			->editColumn('currency',function($transaction){
        					return $transaction->getCurrency();
        			})
        			->editColumn('sgd_rate',function($transaction){
        					return $transaction->getSgdRate();
        			})
        			->editColumn('exchange_rate',function($transaction){
        					return $transaction->getExchangeRate();
        			})
        			->editColumn('amount',function($transaction){
        					return $transaction->getAmount();
        			})
                    ->editColumn('collected_date',function($transaction){
                            return $transaction->getCollectedDate();
                    })


        			->editColumn('total_price',function($transaction){
        					return $transaction->getTotalPrice();
        			})
        			->editColumn('purchase_type',function($transaction){
        					return $transaction->getPurchaseType();
        			})
        			->editColumn('delivery_type',function($transaction){
        					return $transaction->getDeliveryType();
        			})
        			->editColumn('purchase_date',function($transaction){
        					return $transaction->getPurchaseDate();
        			})
                    ->editColumn('status',function($transaction){
                            return $transaction->getStatus();
                    })
                    ->editColumn('address',function($transaction){
                            return $transaction->address;
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title="Edit Transaction" class="btn btn-default btn-flat" href="{{ route("admin.transaction.transaction.edittransactionmerchant",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Transaction" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.transaction.transaction.deletetransactionmerchant",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
    public function editTransactionMerchant($id){
    	$transaction = Transaction::find($id);
    	$merchant = User::with('getRoles')
    	->whereHas('getRoles', function($query) {
			$query->where('id',4);
		})
    	->get();
    	//dd($merchant);
    	if($transaction){
    		return view('fxchange::admin.transaction_merchant.edit',compact('transaction','merchant'));
    	}abort(404);
    	
    }
    public function updateTransactionMerchant(Request $request,$id){
        $transaction = Transaction::find($id);
        if($request->assign_by == 0){
            return redirect()->back()->withError('Please choose specify');
        }
        if($transaction->status !== $request->status){
            $transaction->status = $request->status;
            event(new SendPushUpdateStatus($transaction));
        }
        $transaction->assign_by = $request->assign_by;
        $transaction->save();
        
        $transaction_check = Transaction::getEmailUser($id);
        $get_symbol = Currency::where('name',$transaction->currency)->first();
        if($get_symbol){
            $symbol = $get_symbol->symbol;
            $sign = $get_symbol->sign;
        }
        if($transaction_check){
            $get_contact = Contact::where('user_type','customer')->where('is_show',1)->first();
            if($get_contact){
                $email = $get_contact->email;
                $phone = $get_contact->phone;
                $country_code = $get_contact->country_code;
            }
            $arr_data = [
                        'subject'            => 'FxChange',
                        'currency_bought'    => $transaction_check->amount,
                        'exchange_rate'      => $transaction_check->exchange_rate,
                        'symbol'             => $symbol,
                        'sign'               => $sign,
                        'collection_type'    => $transaction_check->getPurchaseType(),
                        'address'            => $transaction_check->address,
                        'transaction_id'     => $transaction_check->id,
                        'email'              => $transaction_check->getUser ? $transaction_check->getUser->email : '',
                        'total_payment'      => $transaction_check->getTotalPrice(),
                        'payment_date'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                        'payment_time'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('H:i'),
                        'transaction_by'     => $transaction_check->getMerchantAssign(),
                        'full_name'          => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                        'email'              => $email,
                        'country_code'       => $country_code,
                        'phone'              => $phone,
                   ];            
        }
        //event(new SendPushUpdateStatus($transaction));

        if($transaction->status == "completed"){
            if(env('QUEUE_DRIVER')=='sync'){
                if(isset($transaction_check->getUser)){
                    Mail::to($transaction_check->getUser->email)->send(new SendMailTransaction($arr_data));
                }
            }elseif(env('QUEUE_DRIVER')=='database'){
                if(isset($transaction_check->getUser)){
                    Mail::to($transaction_check->getUser->email)->queue(new SendMailTransaction($arr_data));
                }
            }
            $check_role = UserRole::with('user')->get();
            foreach($check_role as $role){
                if($role->role_id == 1){
                    if(isset($role->user)){
                        if(env('QUEUE_DRIVER')=='sync'){
                            Mail::to($role->user->email)->send(new SendMailTransaction($arr_data));
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            Mail::to($role->user->email)->queue(new SendMailTransaction($arr_data));
                        }
                    }
                }
            }
        }
        return redirect()->route('admin.transaction.transaction.index')
        ->withSuccess(trans('Edit Successful'));
    }
    public function deleteTransactionMerchant($id){
        $data = Transaction::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }



    //Transaction for customer
    public function indexCustomer(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.transaction_customer.index');
    }
    public function indexTableCustomer(){
        $request = session()->get("request");

        $customer       = !empty($request['customer'])?trim($request['customer']):"";
        $currency       = !empty($request['currency'])?trim($request['currency']):"";
        $sgd_rate       = !empty($request['sgd_rate'])?trim($request['sgd_rate']):"";
        $exchange_rate  = !empty($request['exchange_rate'])?trim($request['exchange_rate']):"";
        $delivery_type  = !empty($request['delivery_type'])?$request['delivery_type']:"";

        $items = Transaction::select('fxchange__purchases.*')
        ->with('getUser')
        // ->where('purchase_type','delivery')
        ->where('fxchange__purchases.status','<>','pending')
        ->join('users','users.id','=','fxchange__purchases.user_id');
        if($customer || $currency || $sgd_rate || $exchange_rate || $delivery_type ){
            if(!empty($customer)){
                $items = $items->where('users.email','like','%'.$customer.'%');
            }
            if(!empty($currency)){
                $items = $items->where('currency','like','%'.$currency.'%');
            }
            if(!empty($sgd_rate)){
                $items = $items->where('sgd_rate',$sgd_rate);
            }
            if(!empty($exchange_rate)){
                 $items = $items->where('exchange_rate',$exchange_rate);
            }
            if(!empty($delivery_type)){
                if($delivery_type == "none"){
                    $items = $items;
                }else{
                    $items = $items->where('delivery_type',$delivery_type);
                }
                
            }
        }
            $items = $items->orderBy('created_at','DESC')->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->addColumn('order_id',function($transaction){
                            return $transaction->id;
                    })
                    ->editColumn('user_id',function($transaction){
                            return $transaction->getCustomer();
                    })
                    ->editColumn('customer_name',function($transaction){
                            return $transaction->getNameCustomer();
                    })
                    ->editColumn('phone_cus',function($transaction){
                            return $transaction->getContactNumber();
                    })
                    ->editColumn('assign_by',function($transaction){
                            return $transaction->getMerchantAssign();
                    })
                    ->editColumn('name_merchant',function($transaction){
                            return $transaction->getNameMerchantAssign();
                    })
                    ->editColumn('currency',function($transaction){
                            return $transaction->getCurrency();
                    })
                    ->editColumn('sgd_rate',function($transaction){
                            return $transaction->getSgdRate();
                    })
                    ->editColumn('collected_date',function($transaction){
                            return $transaction->getCollectedDate();
                    })
                    ->editColumn('exchange_rate',function($transaction){
                            return $transaction->getExchangeRate();
                    })
                    ->editColumn('amount',function($transaction){
                            return $transaction->getAmount();
                    })
                    ->editColumn('address',function($transaction){
                            return $transaction->address;
                    })
                    ->editColumn('total_price',function($transaction){
                            return $transaction->getTotalPrice();
                    })
                    ->editColumn('purchase_type',function($transaction){
                            return $transaction->getPurchaseType();
                    })
                    ->editColumn('delivery_type',function($transaction){
                            return $transaction->getDeliveryType();
                    })
                    ->editColumn('purchase_date',function($transaction){
                            return $transaction->getPurchaseDate();
                    })
                    ->editColumn('status',function($transaction){
                            return $transaction->getStatus();
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title="Edit Transaction" class="btn btn-default btn-flat" href="{{ route("admin.transaction.transaction.edittransactioncustomer",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Transaction" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.transaction.transaction.deletetransactioncustomer",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
    public function editTransactionCustomer($id){
        $transaction = Transaction::find($id);
        $customer = User::with('getRoles')
        ->whereHas('getRoles', function($query) {
            $query->where('id',4);
        })
        ->get();
        if($transaction){
            return view('fxchange::admin.transaction_customer.edit',compact('transaction','customer'));
        }abort(404);
        
    }
    public function updateTransactionCustomer(Request $request,$id){
        $transaction = Transaction::find($id);
        if($request->assign_by == 0 && $transaction->purchase_type != "self-pickup"){
            return redirect()->back()->withError('Please choose specify');
        }
        if($transaction->status !== $request->status){
            $transaction->status = $request->status;
            event(new SendPushUpdateStatus($transaction));
        }
        $transaction->status = $request->status;
        $transaction->assign_by = $request->assign_by;
        $transaction->save();


        $transaction_check = Transaction::getEmailUser($id);
        $get_symbol = Currency::where('name',$transaction->currency)->first();
        if($get_symbol){
            $symbol = $get_symbol->symbol;
            $sign = $get_symbol->sign;
        }
        if($transaction_check){
            $get_contact = Contact::where('user_type','customer')->where('is_show',1)->first();
            if($get_contact){
                $email = $get_contact->email;
                $phone = $get_contact->phone;
                $country_code = $get_contact->country_code;
            }
            $arr_data = [
                        'subject'            => 'FxChange',
                        'currency_bought'    => $transaction_check->amount,
                        'exchange_rate'      => $transaction_check->exchange_rate,
                        'symbol'             => $symbol,
                        'sign'               => $sign,
                        'collection_type'    => $transaction_check->getPurchaseType(),
                        'address'            => $transaction_check->address,
                        'transaction_id'     => $transaction_check->id,
                        'email'              => $transaction_check->getUser ? $transaction_check->getUser->email : '',
                        'total_payment'      => $transaction_check->getTotalPrice(),
                        'payment_date'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                        'payment_time'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('H:i'),
                        'transaction_by'     => $transaction_check->getMerchantAssign(),
                        'full_name'          => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                        'email'              => $email,
                        'country_code'       => $country_code,
                        'phone'              => $phone,
                   ];            
        }
        //dd(1);
        //event(new SendPushUpdateStatus($transaction));
        //dd(1);

        if($transaction->status == "completed"){
            if(env('QUEUE_DRIVER')=='sync'){
                if(isset($transaction_check->getUser)){
                    Mail::to($transaction_check->getUser->email)->send(new SendMailTransaction($arr_data));
                }
            }elseif(env('QUEUE_DRIVER')=='database'){
                if(isset($transaction_check->getUser)){
                    Mail::to($transaction_check->getUser->email)->queue(new SendMailTransaction($arr_data));
                }
            }
            $check_role = UserRole::with('user')->get();
            foreach($check_role as $role){
                if($role->role_id == 1){
                    if(isset($role->user)){
                        if(env('QUEUE_DRIVER')=='sync'){
                            Mail::to($role->user->email)->send(new SendMailTransaction($arr_data));
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            Mail::to($role->user->email)->queue(new SendMailTransaction($arr_data));
                        }
                    }
                }
            }
        }
        return redirect()->route('admin.transaction.transaction.indexcustomer')
        ->withSuccess(trans('Edit Successful'));
    }
    public function deleteTransactionCustomer($id){
        $data = Transaction::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteTransaction(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = Transaction::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }
}
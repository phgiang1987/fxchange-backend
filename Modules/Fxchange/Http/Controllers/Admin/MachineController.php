<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;
use Modules\Media\Repositories\FileRepository;
use Nexmo\Laravel\Facade\Nexmo;

class MachineController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;
    private $file;

    public function __construct(Datatables $datatables, FileRepository $file)
    {
        parent::__construct();
        $this->datatables = $datatables;
        $this->file = $file;
    }


// Machine
    public function indexMachine(Request $request){
        // Nexmo::message()->send([
        //     'to'   => '+841207800020',
        //     'from' => 'SO_TTTT',
        //     'text' => 'hi'
        // ]);
        session()->put('request',$request->all());
        //dd($request->all());
        return view('fxchange::admin.machines.index');
    }
    public function indexMachineDatatable(){
        $request = session()->get("request");

        $title 		= !empty($request['title'])?trim($request['title']):"";
        $postcode 	= !empty($request['postcode'])?trim($request['postcode']):"";
        $block 		= !empty($request['block'])?trim($request['block']):"";
        $unit 		= !empty($request['unit'])?trim($request['unit']):"";

        $items = new Machines();
        if($title || $postcode || $block || $unit){
            if(!empty($name)){
                $items = $items->where('title','like','%'.$title.'%');
            }
            if(!empty($postcode)){
                $items = $items->where('postcode','like','%'.$postcode.'%');
            }
            if(!empty($block)){
                $items = $items->where('block','like','%'.$block.'%');
            }
            if(!empty($unit)){
                $items = $items->where('unit','like','%'.$unit.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit Machine" class="btn btn-default btn-flat" href="{{ route("admin.machine.machine.edit",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Machine" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.machine.machine.delete",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
 	public function createMachine(){
        return view('fxchange::admin.machines.create');
    }
    public function storeMachine(CreateMachineRequest $request){
        // $data = $request->all();
        // unset($data['_token']);
        // Machines::create($data);
        $data = new Machines();
        $data->title = $request->title;
        $data->postcode = $request->postcode;
        $data->block = $request->block;
        $data->unit = $request->unit;
        $data->latitude = $request->latitude;
        $data->longitude = $request->longitude;
        $data->address = $request->address;
        $data->zone = $request->zone;
        $data->description = $request->description;
        $allRequest = $request->all();
        //dd($data);
        $image = $allRequest['medias_single']['image'];
        //dd($image);
        if($image == null){
            return redirect()->back()->withInput($request->all())
            ->withError(trans('Image field can not be blank'));
        }
        $data->save();
        if( isset($allRequest["medias_single"]) && !empty($allRequest["medias_single"]) ) {            
            $eventImg = $allRequest["medias_single"];            
            $data->files()->sync([ $eventImg['image'] => ['imageable_type' => 'Modules\\Fxchange\\Entities\\Machines', 'zone' => 'image']]);        
        }
        return redirect()->route('admin.machine.machine.indexmachine')
        ->withSuccess(trans('Add successful!'));
    }
    public function editMachine($id){
        $machines = Machines::find($id);
        if(!$machines){
            abort(404);
        }
        return view('fxchange::admin.machines.edit',compact('machines'));
    }
 	public function updateMachine(CreateMachineRequest $request, $id){
        $data = Machines::find($id);
        if(!$data){
            abort(404);
        }
        $data->title = $request->title;
        $data->postcode = $request->postcode;
        $data->block = $request->block;
        $data->unit = $request->unit;
        $data->latitude = $request->latitude;
        $data->longitude = $request->longitude;
        $data->address = $request->address;
        $data->zone = $request->zone;
        $data->description = $request->description;
        $allRequest = $request->all();
        $image = $allRequest['medias_single']['image'];
        //dd($image);
        if($image == null){
            return redirect()->back()->withInput($request->all())
            ->withError(trans('Image field can not be blank'));
        }
        $data->save();
        if( isset($allRequest["medias_single"]) && !empty($allRequest["medias_single"]) ) {            
            $eventImg = $allRequest["medias_single"];            
            $data->files()->sync([ $eventImg['image'] => ['imageable_type' => 'Modules\\Fxchange\\Entities\\Machines', 'zone' => 'image']]);        
        }
        return redirect()->route('admin.machine.machine.indexmachine')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteMachine($id){
        $data = Machines::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteMachine(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = Machines::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }
 	public function importMachine(Request $request){
        //dd(1);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            Excel::load($file, function($reader){
                $table = 'fxchange__machines';
                $reader->each(function($sheet) use ($table){
                    $record = $sheet->toArray();
                    $data = [
                        'title'           => isset($record['title'])?$record['title']:"",
                        'postcode'        => isset($record['postcode'])?$record['postcode']:"",
                        'block'           => isset($record['block'])?$record['block']:"",
                        'unit'            => isset($record['unit'])?$record['unit']:"",
                        'latitude'        => isset($record['latitude'])?$record['latitude']:"",
                        'longitude'       => isset($record['longitude'])?$record['longitude']:"",
                        'created_at'      => Carbon::now(),
                        ];
                    $flag = 0;
                    if (isset($record['title']) && !empty($record['title'])) {
                        $flag++;
                    }
                    if (isset($record['postcode']) && !empty($record['postcode'])) {
                        $flag++;
                    }
                    if (isset($record['block']) && !empty($record['block'])) {
                        $flag++;
                    }
                    if (isset($record['unit']) && !empty($record['unit'])) {
                       $flag++;
                    }
                    if (isset($record['latitude']) && !empty($record['latitude'])) {
                        $flag++;
                    }
                    if (isset($record['longitude']) && !empty($record['longitude'])) {
                        $flag++;
                    }
                    //dd($flag);
                    session()->put('key', $flag);
                    if($flag == 6){

                        $machine = DB::table($table)->where(['postcode' => $sheet->postcode])->first();
                        if (empty($machine)) {
                            DB::table($table)->insert($data);
                        }
                        else{
                            DB::table($table)->where(['postcode' => $sheet->postcode])->update($data);
                        }
                    }
                    
                });

            });
            $flag = session()->get('key');
            if($flag == 6){
                session()->forget('key');
                return redirect()->route('admin.machine.machine.indexmachine')->withSuccess(trans('Update new machine successfully'));
            }else{
                session()->forget('key');
                return redirect()->route('admin.machine.machine.indexmachine')->withWarning(trans('Data is incorrect format will not be added to the system'));
            }
             
        }
        else
        {
            return redirect()->back()->withError(trans('Please enter file xls or csv'));
        }
            
    }
    public function exportDataFake(Request $request){
                $data = array(
                    array('title', 'postcode', 'block', 'unit', 'latitude', 'longitude'),
                    array('Machine #2', 'MC2', '2', '4-4', '210.66546', '2312.564')
                );
        Excel::create('machines', function($excel) use ($data){


            //dd($equipments);
            $excel->sheet('Sheet 1', function($sheet)  use ($data){

                $sheet->fromArray($data, null, 'A1', false, false);
                //$sheet->loadView('precise::admin.partials.exportexcelequipment',['data' => $equipments]);
            });
        })->export('xls');

    }
}
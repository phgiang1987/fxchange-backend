<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\Machines;
use Modules\Fxchange\Entities\Transaction;
use Modules\Fxchange\Entities\StaticPage;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Modules\Fxchange\Http\Requests\CreateStaticPageRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;

class StaticPageController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;
    }

    public function indexFaq(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.faq.index');
    }
    public function indexFaqDatatable(){
        $request = session()->get("request");
        $title 		= !empty($request['question'])?trim($request['question']):"";
        $content 		= !empty($request['answer'])?trim($request['answer']):"";
        $items = StaticPage::getAllFAQOfMerchant();
        if($title || $content){
            if(!empty($title)){
                $items = $items->where('title','like','%'.$title.'%');
            }
            if(!empty($content)){
                $items = $items->where('content','like','%'.$content.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('question',function($faq){
                            return $faq->question_faq;
                    })
                    ->editColumn('answer',function($faq){
                            return $faq->answer_faq;
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit FAQ" class="btn btn-default btn-flat" href="{{ route("admin.faq.faq.edit",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete FAQ" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.faq.faq.delete",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
 	public function createFaq(){
        $count_faq = StaticPage::where('type','faq')->where('user_type','merchant')->count();
        return view('fxchange::admin.faq.create',compact('count_faq'));
    }
    public function storeFaq(CreateStaticPageRequest $request){
        $count_faq = StaticPage::where('type','faq')->where('user_type','merchant')->count();


    	$data = new StaticPage();
    	$data->title = $request->title;
    	$data->content = $request->content;
    	$data->type = "faq";
    	$data->user_type = "merchant";
        $data->sort = $count_faq + 1;
    	$data->save();
        return redirect()->route('admin.faq.faq.indexfaq')
        ->withSuccess(trans('Add successful!'));
    }
    public function editFaq($id){
        $faq = StaticPage::find($id);
        $count_faq = StaticPage::where('type','faq')->where('user_type','merchant')->count();
        if(!$faq){
            abort(404);
        }
        return view('fxchange::admin.faq.edit',compact('faq','count_faq'));
    }
 	public function updateFaq(CreateStaticPageRequest $request, $id){
        $data = StaticPage::find($id);
        if(!$data){
            abort(404);
        }
        $data->title = $request->title;
        $data->content = $request->content;
        $data->sort = $request->sort;
        $data->save();
        return redirect()->route('admin.faq.faq.indexfaq')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteFaq($id){
        $data = StaticPage::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteFaq(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = StaticPage::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }

    public function indexFaqMerchantSort(){
        $items = StaticPage::getAllFAQOfMerchant();
        $items = $items->get();
        //dd($items);
        return view('fxchange::admin.faq.sort',compact('items'));  
    }

    public function indexFaqMerchantSortUpdateAjax(Request $request){
        $row = explode("&row[]=", $request->data);
        $row = str_replace("row[]=", "", $row);
        menu($row);
        return 1;
    }





    //Customer
 	public function indexFaqCustomer(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.faq_customer.index');
    }
    public function indexFaqDatatableCustomer(){
        $request = session()->get("request");
        $title      = !empty($request['question'])?trim($request['question']):"";
        $content        = !empty($request['answer'])?trim($request['answer']):"";
        $items = StaticPage::getAllFAQOfCustomer();
        if($title || $content){
            if(!empty($title)){
                $items = $items->where('title','like','%'.$title.'%');
            }
            if(!empty($content)){
                $items = $items->where('content','like','%'.$content.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('question',function($faq){
                            return $faq->question_faq;
                    })
                    ->editColumn('answer',function($faq){
                            return $faq->answer_faq;
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit FAQ" class="btn btn-default btn-flat" href="{{ route("admin.faq.faq.editcustomer",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete FAQ" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.faq.faq.deletecustomer",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select'])->make(true);
    }
 	public function createFaqCustomer(){
        return view('fxchange::admin.faq_customer.create');
    }
    public function storeFaqCustomer(CreateStaticPageRequest $request){
        $count_faq = StaticPage::where('type','faq')->where('user_type','customer')->count();
    	$data = new StaticPage();
    	$data->title = $request->title;
    	$data->content = $request->content;
    	$data->type = "faq";
    	$data->user_type = "customer";
        $data->sort = $count_faq + 1;
    	$data->save();
        return redirect()->route('admin.faq.faq.indexfaqcustomer')
        ->withSuccess(trans('Add successful!'));
    }
    public function editFaqCustomer($id){
        $faq = StaticPage::find($id);
        $count_faq = StaticPage::where('type','faq')->where('user_type','customer')->count();
        if(!$faq){
            abort(404);
        }
        return view('fxchange::admin.faq_customer.edit',compact('faq','count_faq'));
    }
 	public function updateFaqCustomer(CreateStaticPageRequest $request, $id){
        $data = StaticPage::find($id);
        if(!$data){
            abort(404);
        }
        $data->title = $request->title;
        $data->content = $request->content;
        $data->sort = $request->sort;
        $data->save();
        return redirect()->route('admin.faq.faq.indexfaqcustomer')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteFaqCustomer($id){
        $data = StaticPage::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteFaqCustomer(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = StaticPage::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }



    public function indexFaqCustomerSort(){
        $items = StaticPage::getAllFAQOfCustomer();
        $items = $items->get();
        //dd($items);
        return view('fxchange::admin.faq_customer.sort',compact('items'));  
    }

    public function indexFaqCustomerSortUpdateAjax(Request $request){
        $row = explode("&row[]=", $request->data);
        $row = str_replace("row[]=", "", $row);
        menu($row);
        return 1;
    }

}
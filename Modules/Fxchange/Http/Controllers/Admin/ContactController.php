<?php

namespace Modules\Fxchange\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Fxchange\Entities\Fxchange;
use Modules\Fxchange\Http\Requests\CreateFxchangeRequest;
use Modules\Fxchange\Http\Requests\UpdateFxchangeRequest;
use Modules\Fxchange\Repositories\FxchangeRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

use Yajra\Datatables\Datatables;
use Modules\Fxchange\Entities\Contact;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Http\Requests\CreateMachineRequest;
use Modules\Fxchange\Http\Requests\CreateContactRequest;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;

class ContactController extends AdminBaseController
{

	    /**
     * @var FxchangeRepository
     */
    private $datatables;

    public function __construct(Datatables $datatables)
    {
        parent::__construct();
        $this->datatables = $datatables;
    }

    public function indexMerchantContact(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.contact_merchant.index');
    }
    public function indexDatatableMerchantContact(){
        $request = session()->get("request");
        $email 		= !empty($request['email'])?trim($request['email']):"";
        $phone 		= !empty($request['phone'])?trim($request['phone']):"";
        $items = Contact::getAllContactOfMerchant();
        if($email || $phone){
            if(!empty($email)){
                $items = $items->where('email','like','%'.$email.'%');
            }
            if(!empty($phone)){
                $items = $items->where('phone','like','%'.$phone.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('email',function($item){
                            return $item->email_contact;
                    })
                    ->editColumn('phone',function($item){
                            return $item->phone_contact;
                    })
                    ->addColumn('is_show', function($item) {
                        if($item->is_show == 1){
                            return '<p style="color:green"><i class="fa fa-check fa-2x"></i></p>';
                        }else
                        {
                            return '<p style="color:red"><i class="fa fa-exclamation-circle fa-2x"></i></p>';
                        }
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit Contact" class="btn btn-default btn-flat" href="{{ route("admin.contact.contact.editmerchantcontact",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Contact" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.contact.contact.deletemerchantcontact",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select','is_show'])->make(true);
    }
 	public function createMerchantContact(){
         $a = '{"msg":{"netsMid":"UMID_877448001","merchantTxnRef":"FX241519635386","netsMidIndicator":"U","netsTxnRef":"20180226165627132","paymentMode":"QR","submissionMode":"B","currencyCode":"SGD","merchantTxnDtm":"20180226 15:55:46.798","merchantTimeZone":"+8:00","paymentType":"SALE","clientType":"S","bankId":"38","stageRespCode":"3099-00000","txnRand":"28409173612148994","actionCode":"0","netsTxnDtm":"20180226 16:56:27.688","netsTxnStatus":"0","netsTxnMsg":"Successful","netsAmountDeducted":100},"ss":"1"}';
    dd(json_decode($a)->msg->merchantTxnRef);
        return view('fxchange::admin.contact_merchant.create');
    }
    public function storeMerchantContact(CreateContactRequest $request){
    	$data = new Contact();
    	$data->email = $request->email;
    	$data->phone = $request->phone;
        $data->country_code = $request->country_code;
    	$data->user_type = "merchant";
        if (isset($request->is_show) && $request->is_show == 1) {
            Contact::where("is_show",1)->where('user_type','merchant')->update(['is_show' => 0]);
            $data->is_show = 1;
        }
        else{
            $data->is_show = 0;
        }
    	$data->save();
        return redirect()->route('admin.contact.contact.indexmerchantcontact')
        ->withSuccess(trans('Add successful!'));
    }
    public function editMerchantContact($id){
        $data = Contact::find($id);
        if(!$data){
            abort(404);
        }
        return view('fxchange::admin.contact_merchant.edit',compact('data'));
    }
 	public function updateMerchantContact(CreateContactRequest $request, $id){
        //dd($request->all());
        $data = Contact::find($id);
        if(!$data){
            abort(404);
        }
        $req = $request->only(['email','phone','country_code']);
        if (isset($request->is_show)) {
            Contact::where("is_show",1)->where('id','<>',$id)->where('user_type','merchant')->update(['is_show' => 0]);
            $req['is_show'] = 1;
        }
        $data->update($req);
        return redirect()->route('admin.contact.contact.indexmerchantcontact')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteMerchantContact($id){
        $data = Contact::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    public function BulkDeleteContact(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $currency = Contact::where('id',$id)->delete(); 
            $request->session()->flash('success','Delete successfully');
            return 1;
        }       
    }


    //Customer
 	public function indexCustomerContact(Request $request){
        session()->put('request',$request->all());
        return view('fxchange::admin.contact_customer.index');
    }
    public function indexDatatableCustomerContact(){
        $request = session()->get("request");
        $email      = !empty($request['email'])?trim($request['email']):"";
        $phone      = !empty($request['phone'])?trim($request['phone']):"";
        $items = Contact::getAllContactOfCustomer();
        if($email || $phone){
            if(!empty($email)){
                $items = $items->where('email','like','%'.$email.'%');
            }
            if(!empty($phone)){
                $items = $items->where('phone','like','%'.$phone.'%');
            }
        }
            $items = $items->get();
            $collection = collect($items);
            session()->forget('request');
        return $this->datatables->of($collection)
                    ->editColumn('email',function($item){
                            return $item->email_contact;
                    })
                    ->editColumn('phone',function($item){
                            return $item->phone_contact;
                    })
                    ->addColumn('is_show', function($item) {
                        if($item->is_show == 1){
                            return '<p style="color:green"><i class="fa fa-check fa-2x"></i></p>';
                        }else
                        {
                            return '<p style="color:red"><i class="fa fa-exclamation-circle fa-2x"></i></p>';
                        }
                    })
                    ->addColumn('check', '<input class = "check" type="checkbox" name="selected_currency[]" value="{{ $id }}">')
                    ->addColumn('button', '<div class ="button_action">
                                <a title=Edit Contact" class="btn btn-default btn-flat" href="{{ route("admin.contact.contact.editcustomercontact",$id) }}" style="margin-right:3px"><i class="fa fa-pencil"></i></a> 
                                <button title="Delete Contact" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.contact.contact.deletcustomercontact",$id) }}"><i class="fa fa-trash"></i></button> </div>
                                ')
                    ->rawColumns(['check', 'action','button','select','is_show'])->make(true);
    }
 	public function createCustomerContact(){
        return view('fxchange::admin.contact_customer.create');
    }
    public function storeCustomerContact(CreateContactRequest $request){
    	$data = new Contact();
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->country_code = $request->country_code;
    	$data->user_type = "customer";

        if (isset($request->is_show) && $request->is_show == 1) {
            Contact::where("is_show",1)->where('user_type','customer')->update(['is_show' => 0]);
            $data->is_show = 1;
        }
        else{
            $data->is_show = 0;
        }

    	$data->save();
        return redirect()->route('admin.contact.contact.indexcustomercontact')
        ->withSuccess(trans('Add successful!'));
    }
    public function editCustomerContact($id){
        $data = Contact::find($id);
        if(!$data){
            abort(404);
        }
        return view('fxchange::admin.contact_customer.edit',compact('data'));
    }
 	public function updateCustomerContact(CreateContactRequest $request, $id){
        $data = Contact::find($id);
        if(!$data){
            abort(404);
        }
        $data->email = $request->email;
        $data->phone = $request->phone; 
        $data->country_code = $request->country_code;

        if (isset($request->is_show) && $request->is_show == 1) {
            Contact::where("is_show",1)->where('id','<>',$id)->where('user_type','customer')->update(['is_show' => 0]);
            $data->is_show = 1;
        }

        $data->save();
        return redirect()->route('admin.contact.contact.indexcustomercontact')
        ->withSuccess(trans('Edit Successful'));
    }
 	public function deleteCustomerContact($id){
        $data = Contact::find($id);
        if($data){
            $data->delete();
            return redirect()->back()
            ->withSuccess(trans('Delete Successful'));
        }abort(404);
    }
    // public function BulkDeleteTermsCustomer(Request $request){
    //     $id = [];
    //     $id = $request->id;
    //     if($id == null){
    //         $request->session()->flash('danger','Select item before bulk delete');
    //         return 2;
    //     }else{
    //         $currency = Contact::where('id',$id)->delete(); 
    //         $request->session()->flash('success','Delete successfully');
    //         return 1;
    //     }       
    // }


}
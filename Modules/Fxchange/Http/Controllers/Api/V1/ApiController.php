<?php
/**
 * Created by PhpStorm.
 * User: vudao
 * Date: 11/7/2017
 * Time: 4:33 PM
 */
namespace Modules\Fxchange\Http\Controllers\Api\V1;

use Illuminate\Routing\Controller;
use Modules\Fxchange\Http\Controllers\Api\FxChangeResponse;
use Modules\User\Traits\CanFindUserWithBearerToken;

/**
 * @SWG\Swagger(
 *   schemes={SCHEMES_HOST},
 *   host=API_HOST,
 *   basePath="/api",
 *   @SWG\Info(
 *     title="FXCHANGE API UI",
 *     description="<h1>Sprint 1</h1> <ul> <li> 2017/11/20: <ul> <li> [NEW] <strong>api/v1/merchant/logout</strong>: Merchant logout. <li> [NEW] <strong>api/v1/customer/logout</strong>: Customer logout. </ul> </ul> <br>",
 *     version="0.1"
 *   ),
 *   @SWG\SecurityScheme(
 *   securityDefinition="api_key",
 *   type="apiKey",
 *   in="header",
 *   name="FX-API-KEY"
 *  ),
 *  @SWG\SecurityScheme(
 *   securityDefinition="userToken",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 *  )
 * )
 */
class ApiController extends Controller {

    use FxChangeResponse, CanFindUserWithBearerToken;

    public $user;

    public function __construct()
    {
        $this->getUser();
        $this->middleware('api.log');
    }

    public function getUser(){
        $this->user = $this->findUserWithBearerToken(request()->header('Authorization'));
        return $this;
    }

}
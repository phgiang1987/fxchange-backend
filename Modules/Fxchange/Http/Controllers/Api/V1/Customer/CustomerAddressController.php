<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1\Customer;

use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Modules\Fxchange\Entities\Customer\CustomerAddress;
use Modules\Fxchange\Http\Controllers\Api\V1\ApiController;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerAddressRequest;
use Modules\Fxchange\Repositories\CustomerAddressRepository;
use Mail;
use Modules\Fxchange\Transformers\Api\Customer\CustomerAddressTransformer;

class CustomerAddressController extends ApiController
{
    use DispatchesJobs;

    protected $repository;
    protected $transformer;
    protected $fractal;

    public function __construct(CustomerAddressRepository $repository, CustomerAddressTransformer $transformer, Manager $fractal)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
    }

    /**
     * Customer add address
     * @param array
     * @return json mixed
     *
     * @SWG\Post(
     *   path="/v1/customer/add-address",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.customer.address.add",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerAddAddress")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function add(CustomerAddressRequest $request){
        $user = $this->user;

        $message = $this->repository->addUserAddress($user->id, $request->all());
        if($message != null ){
            return $this->respondWithError($message);
        }else{
            $latestAddress = $this->repository->findByAttributes(['user_id'=>$user->id])->orderBy('id','desc')->first();
            $data = $this->transformer->transform($latestAddress);

            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Add address successful!',
                'data'=> $data
            ]);
        }
    }

    /**
     * Customer update address
     * @param array
     * @return json mixed
     *
     * @SWG\Post(
     *   path="/v1/customer/update-address",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.customer.address.update",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerUpdateAddress")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function update(CustomerAddressRequest $request){
        $user = $this->user;

        $addressId = $request->id;

        $message = $this->repository->updateUserAddress($addressId , $request->all());
        if(!$message){
            return $this->respondWithError(trans('fxchange::fxchanges.update_address_fail'));
        }else{
            $address = $this->repository->find($addressId);
            $data = $this->transformer->transform($address);

            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Update address successful!',
                'data'=> $data
            ]);
        }
    }

    /**
     * Customer Delete Address
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/delete-address",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.address.delete",
     *   produces={"application/json"},
     *   tags={"Customer"},
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="address_id",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function delete(Request $request){
        $user = $this->user;
        $addressId = $request->address_id;
        $addressDelete = $this->repository->deleteAddress($user->id, $addressId);
        if(!$addressDelete)
        {
            return $this->respondWithError('Not Found');
        }
        else
        {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => trans('fxchange::fxchanges.delete_address_successfully'),
                'data' => []
            ]);
        }
    }
}

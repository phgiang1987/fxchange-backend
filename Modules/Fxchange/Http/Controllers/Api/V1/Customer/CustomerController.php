<?php 
namespace Modules\Fxchange\Http\Controllers\Api\V1\Customer;
use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Hash;
use Modules\Fxchange\Http\Controllers\Api\V1\ApiController;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerAddressRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerLoginRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerResetRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerVerifyCodeSignUpRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\VerifyCustomerCredentialsRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerFeedbackRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerUpdateAccountRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerChangePasswordRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerVerifyChangePhone;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerResendCodePhone;

use Modules\Fxchange\Notifications\VerifyCodeSMS;
use Modules\Fxchange\Repositories\CustomerRepository;
use Modules\Fxchange\Repositories\Sentinel\FxSentinelCustomerRepository;
use Modules\Fxchange\Services\Enets2;
use Modules\Fxchange\Transformers\Api\Customer\CustomerTransformer;
use Modules\Fxchange\Repositories\FxUserTokenRepository;
use Modules\Fxchange\Repositories\Sentinel\FxSentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Mail;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\Fxchange\Services\FxUserResetter;
use Modules\Fxchange\Entities\UserRecoverPwd;
use Modules\Fxchange\Entities\Customer\Customer;
use Modules\Fxchange\Entities\Transaction;

use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use Modules\Fxchange\Transformers\Api\Customer\CustomerContactTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerCurencyTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerCurencyLoginTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerMachinesTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerPurchaseTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerCurrencyStatisticsTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerUnreadNotificationTransformer;
use Modules\Fxchange\Transformers\Api\Customer\CustomerNotificationsTransformer;
use Carbon\Carbon;
use Modules\Fxchange\Entities\FxchangeNotificationList;
use Modules\Fxchange\Events\SendPushUpdateStatus;
use Modules\Fxchange\Mail\SendMailConfirmPayment;
use Modules\Fxchange\Mail\SendMailTransaction;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserRole;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\Contact;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class CustomerController extends ApiController
{
    use DispatchesJobs;

    protected $fractal;

    protected $transformer;

    protected $contactTransformer;

    protected $curentcyTransformer;

    protected $notificationListEntities;

    protected $transformerpurchase;

    protected $transformermachine;

    protected $transformernotification;

    public function __construct(
    	CustomerRepository $customerRepository, 
    	CustomerTransformer $transformer , 
    	Manager $fractal , 
    	CustomerContactTransformer $contactTransformer,
    	CustomerCurencyTransformer $curentcyTransformer,
        FxchangeNotificationList $notificationListEntities,
        CustomerPurchaseTransformer $transformerpurchase,
        CustomerMachinesTransformer $transformermachine,
        CustomerNotificationsTransformer $transformernotification

    ){
        parent::__construct();
        $this->repository = $customerRepository;
        $this->transformer = $transformer;
        $this->fractal = $fractal;
        $this->contactTransformer = $contactTransformer;
        $this->curentcyTransformer = $curentcyTransformer;
        $this->notificationListEntities = $notificationListEntities;
        $this->transformerpurchase = $transformerpurchase;
        $this->transformermachine = $transformermachine;
        $this->transformernotification = $transformernotification;
    }

    /**
     * Customer get list Curency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/contact-us",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.contactus",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description="")
     * )
    */
    public function getContactUs(){
    	$data = $this->repository->getContactUs();
    	if ($data !== null) {
    		$contact = $this->contactTransformer->transform($data);
	        if(count($contact) > 0){
	            return $this->respond([
	                'status' => true,
	                'status_code' => $this->getStatusCode(),
	                'message' => 'Contacts Us',
	                'data' => $contact
	            ]);
	        }
	        else{
	            return $this->respondNotFound('Not Found');
	        }
    	}
    	else{
    		return $this->respondNotFound('Not Found');
    	}
    	
    }

    /**
     * Create Feedback
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/post-feedback",
     *   summary="View",
     *   operationId="api.customer.post.feedback",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Create Feedback",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/FeedbackCreate")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function createFeeedback(CustomerFeedbackRequest $request){
        $user = $this->user;
        //$data['user_id'] = $user->id;
        $data = $request->only(['content']);
        $data['user_id'] = $user->id;
        $status = $this->repository->createFeeedback($data);
        if ($status) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => trans('fxchange::fxchanges.thank_you_feedback'),
                'data' => []
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Customer get list Curency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-curency",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.curency.all",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getAllCurency(){
        $user_id = $this->user->id;
    	$data = $this->repository->getAllCurency($user_id);
    	$curency = new Collection($data, new CustomerCurencyLoginTransformer());
    	$curency = $this->fractal->createData($curency)->toArray();
        //$curency['unread'] = FxchangeNotificationList::checkIsReadByTypeAlert($this->user->id);
        if(count($curency) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Curency',
                'data' => $curency
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer get list Curency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-curency-dont-login",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.curency.all",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
    */
    public function getAllCurencyDontLogin(){
        //$user_id = $this->user->id;
        $data = $this->repository->getAllCurencyDontLogin();
        $curency = new Collection($data, new CustomerCurencyTransformer());
        $curency = $this->fractal->createData($curency)->toArray();
        //$curency['unread'] = FxchangeNotificationList::checkIsReadByTypeAlert($this->user->id);
        if(count($curency) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Curency',
                'data' => $curency
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer Get Recent currency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-recent-currency",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.recent.currency",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getRecentCurency(){
        //return 1;
        $user_id = $this->user->id;
        $data = $this->repository->getRecentCurency($user_id);
        $curency = new Collection($data, new CustomerCurencyLoginTransformer());
        $curency = $this->fractal->createData($curency)->toArray();
        if(count($curency) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Recent Curency',
                'data' => $curency
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer Get Recent currency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-recent-currency-dont-login",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.recent.currency",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
    */
    public function getRecentCurencyDontLogin(){
        //return 1;
        //$user_id = $this->user->id;
        $data = $this->repository->getRecentCurencyDontLogin();
        //return $data;
        $curency = new Collection($data, new CustomerCurencyTransformer());
        $curency = $this->fractal->createData($curency)->toArray();
        if(count($curency) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Recent Curency',
                'data' => $curency
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    

    /**
     * Customer get list View
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-listview",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.listview",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
    */
    public function getListView(){
    	$data = $this->repository->getListView();
    	$machines = new Collection($data, new CustomerMachinesTransformer());
    	$machines = $this->fractal->createData($machines)->toArray();
        if(count($machines) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Map View',
                'data' => $machines
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer get list View
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-map-view",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.listview",
     *   produces={"application/json"},
     *   tags={"Customer"},
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="id",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="latitude",
     *     required=true,
     *     type="number",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="longitude",
     *     required=true,
     *     type="number",
     *     default="1"
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
    */
    public function getMapViewDetail(Request $request){
        $data['id'] = $request->get('id');
        $data['latitude'] = $request->get('latitude');
        $data['longitude'] = $request->get('longitude');
        //return response()->json($data);
        $machines = $this->repository->getMapViewDetail($data);
        $machines = $this->transformermachine->transform($machines);
        // $machines = new Collection($data, new CustomerMachinesTransformer());
        // $machines = $this->fractal->createData($machines)->toArray();
        if(count($machines) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'List Map View',
                'data' => $machines
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer buy now Curency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-detail-curency",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.detail.curency",
     *   produces={"application/json"},
     *   tags={"Customer"},
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="curency",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description="")
     * )
    */
    public function getDetailCurency(Request $request){
    	$curencyId = $request->has('curency')?$request->get('curency') : null;
    	$data = $this->repository->getDetailCurency($curencyId);

    	if ($data !== null) {
    		$curency = $this->curentcyTransformer->transform($data);
	    	if(count($curency) > 0){
	            return $this->respond([
	                'status' => true,
	                'status_code' => $this->getStatusCode(),
	                'message' => 'Buy now curency',
	                'data' => $curency
	            ]);
	        }
	        else{
	            return $this->respondNotFound('Not Found');
	        }
    	}
    	else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer buy now Curency
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-remaining",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.detail.curency",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getRemaining(Request $request){
            $user_id = $this->user->id;
            //return $user_id;
            $data = [];
            $data['remaining'] = getRemainingAmount($user_id);
            //return $data['remaining'];
            // if($data['remaining'] != null && $data['remaining'] == 0){
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Get remaining',
                    'data' => $data
                ]);
            // }
            // else{
            //     return $this->respondNotFound('Not Found');
            // }
        
    }
    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/delivery-details",
     *   description="Params : delivery (1 EXPRESS | 2 NORMAL | 3 SELF-PICKUP)",
     *   summary="View",
     *   operationId="api.customer.get.delivery.details",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="delivery",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getDeliveryDetails(Request $request){
        $user = Customer::find($this->user->id);
        $delivery = $request->has('delivery')?$request->get('delivery'):null;
        if ($delivery !== null) {
            $deliveryDetails = $this->repository->getDeliveryDetails($delivery);

            //fields addresses
            $deliveryDetails['addresses']['data'] = $user->addresses ? $user->addresses : []; 

            if ($deliveryDetails !== null) {
                $response = [
                    'status' => true,
                    'status_code' => 200,
                    'message' => 'Delivery',
                    'data' => $deliveryDetails
                ];
                return response()->json($response);
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            return $this->respondNotFound('Not Found');
        }
        
    }

    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/purchases-list",
     *   description="Params : type (1 CURRENT | 2 PAST)",
     *   summary="View",
     *   operationId="api.customer.get.purchases.list",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="type",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getPurchasesList(Request $request){
        $user = $this->user;
        $type = $request->has('type')?$request->get('type'):null;
        if ($type !== null) {
            $data = $this->repository->getPurchasesList($user->id, $type);
            //return $data;
            if ($data  !== null) {
                $purchased = new Collection($data, new CustomerPurchaseTransformer());
                $purchased = $this->fractal->createData($purchased)->toArray();
                if (count($purchased)) {
                    return $this->respond([
                        'status' => true,
                        'status_code' => $this->getStatusCode(),
                        'message' => 'Purchases',
                        'data' => $purchased
                    ]);
                }
                else{
                    return $this->respondNotFound('Not Found');
                }
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Customer get my account
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-my-account",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.myaccount",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getUpdateAccount(){
        $user = $this->repository->getUpdateAccount($this->user->id);
        $resource = new Item($user,$this->transformer);

        $this->fractal->parseIncludes(['addresses']);
        $data['data'] = $this->fractal->createData($resource)->toArray();
        if($data !== null){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Get My Account',
                'data' => $data['data']
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Update My Account
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/post-my-account",
     *   summary="View",
     *   operationId="api.customer.post.myaccount",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Update My Account",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/UpdateMyAccount")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postUpdateAccount(CustomerUpdateAccountRequest $request){
        $user = $this->user;
        $dataReuest = $request->only(['last_name','first_name']);
        $country_code = substr($request->phone_number,0,3);
        $phone_number = substr($request->phone_number,3,12);
        $verifyPhone = Customer::where([
            'id' => $user->id,
            'country_code' => $country_code, 
            'phone_number' => $phone_number
        ])->first();
        if ($verifyPhone !== null) {
            $dataReuest['phone_number'] = $phone_number;
            $dataReuest['country_code'] = $country_code;
            $data = $this->repository->postUpdateAccount($user, $dataReuest);
            if ($data) {
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Update My Account',
                    'data' => []
                ]);
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            $requestId = $this->repository->sendVerifyChangePhone($user->id,$request->phone_number,$request->last_name,$request->first_name);
            if ($requestId) {
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Your need verify code to update number phone',
                    'data' => [
                        'request_id' => $requestId
                    ]
                ]);
            }
            else{
                return $this->respondWithError('Can not send verify sms code to your phone number.');
            }
            
        }
        
    }

    /**
     * Re-send Verify code Phone Number
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/resend-code-phone",
     *   summary="View",
     *   operationId="api.customer.post.resendcodephone",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Re-send Verify code Phone Number",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ReSendCodePhone")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */

    public function postResendCode(CustomerResendCodePhone $request){
        $user = $this->user;
        $verify = $this->repository->postReSendVerifyPhone($user , $request->phone_number);
        if ($verify !== null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Re-send code verify successful',
                'data' => [
                    'request_id' => $verify
                ]
            ]);
        }
        else{
            return $this->respondWithError('Can not send verify sms code to your phone number.');
        }
        
    }

    /**
     * Post Verify Change Phone
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/verify-change-phone",
     *   description="<ul>
     *     <li>phone_number : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.customer.post.change.phone",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Verify Change Phone",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerVerifyCodeSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *  security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postVerifyChangePhone(CustomerVerifyChangePhone $request){
        $user = $this->user;
        $verify = $this->repository->verifyChangePhone($user->id , $request->request_id,$request->code);
        if ($verify === null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Change number phone successfully',
                'data' => []
            ]);
        }
        else{
            $response = [
                'status' => false,
                'status_code' => 201,
                'message' => $verify,
                'data' => []
            ];
            return $this->respond($response);
        }
    }
    /**
     * Update My Account
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/post-change-password",
     *   summary="View",
     *   operationId="api.customer.post.change.password",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Change Password",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ChangePassword")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postChangePassword(CustomerChangePasswordRequest $request){
        $user = $this->user;
        $countRecoverd = UserRecoverPwd::where('user_id',$user->id)->count();
        if (Hash::check($request->current_password,$user->password)) {
            if ($countRecoverd < config('asgard.fxchange.config.PWD_RECOVERD')) {
                $checkPassword = UserRecoverPwd::where(['user_id' => $user->id , 'pwd' => sha1($request->confirm_password)])->first();
                if ($checkPassword !== null) {
                    $statusCode = [
                        'status' => false,
                        'status_code' => 201,
                        'message' => trans('fxchange::fxchanges.check_last_password'),
                        'data' => []
                    ];
                    return response()->json($statusCode);
                }
            }
            else
            {
                $skip = $countRecoverd - config('asgard.fxchange.config.PWD_RECOVERD');
                $checkPassword = UserRecoverPwd::where(['user_id' => $user->id])
                    ->limit(config('asgard.fxchange.config.PWD_RECOVERD'))
                    ->orderBy('id','DESC')
                    ->pluck('pwd')->toArray();
                if (in_array(sha1($request->confirm_password), $checkPassword)) {
                    $statusCode = [
                        'status' => false,
                        'status_code' => 201,
                        'message' => trans('fxchange::fxchanges.check_last_password'),
                        'data' => []
                    ];
                    return response()->json($statusCode);
                }
            }
            $user->password = bcrypt($request->confirm_password);
            $user->save();
            if ($user) {
                save_password_recoverd($user->id,$request->confirm_password);
            }
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Update password successfully',
                'data' => []
            ]);
        }
        else{
            $statusCode = [
                'status' => false,
                'status_code' => 201,
                'message' => trans('fxchange::fxchanges.password_not_macth'),
                'data' => []
            ];
            return response()->json($statusCode);
        }
    }


 /**
     * Set alert currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/post-curency-alert",
     *   summary="View",
     *   operationId="api.customer.post.curency.alert",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Set Alert",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/SetAlert")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */

    public function postCurrencyAlert(Request $request){
        $data['user_id'] = $this->user->id;
        $data['currency_id']    = $request->currency_id;
        $data['currency_name']  = $request->currency_name;
        $data['alert_sgd_rate'] = $request->alert_sgd_rate;
        $data['alert_exchange_rate'] = $request->alert_exchange_rate;
        //return $data;
        $result = $this->repository->postCurrencyAlert($data);
        if($result == true){
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Update successfully',
                'data' => []
            ]);
        }else{
            return $this->respond([
                'status' => false,
                'status_code' => 201,
                'message' => 'Id currency do not exists',
            ]);
        }

    }

    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-curency-statistics",
     *   description="Params : currency_name",
     *   summary="View",
     *   operationId="api.customer.get.purchases.list",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="currency_name",
     *     required=true,
     *     type="string",
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getCurrencyStatistics(Request $request){
        $user = $this->user->id;
        $currency_name = $request->has('currency_name')?$request->get('currency_name'):null;
        if($currency_name !== null) {
            $data = $this->repository->getCurrencyStatistics($currency_name);
            $dataExchangeByDate = $this->repository->getValueExchangeByDate($currency_name);
            $getCurrencyAlreadySet = $this->repository->getCurrencyAlreadySet($user,$currency_name);
            if ($data  !== null) {
                $statistics = new Collection($data, new CustomerCurrencyStatisticsTransformer());
                $statistics = $this->fractal->createData($statistics)->toArray();
                $statistics['curency_alert'] = $getCurrencyAlreadySet;
                $statistics['exchange_rate'] = $dataExchangeByDate;
                if (count($statistics)) {
                    return $this->respond([
                        'status' => true,
                        'status_code' => $this->getStatusCode(),
                        'message' => 'Data Curency Statistics',
                        'data' => $statistics
                    ]);
                }
                else{
                    return $this->respondNotFound('Not Found');
                }
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }

    }

    /**
     *
     * Purchased Currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/post-payment",
     *   summary="View",
     *   operationId="api.customer.post.payment",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Confirm Tracsaction",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/PaymentConfirm")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postPayment(Request $purchase){
        $request = $purchase->all();
        //return ($request);
        $request['user_id'] = $this->user->id;
        if($purchase->purchase_type == "self-pickup"){
            $request['status'] = "pending";
        }else{
           $request['status'] = "pending"; 
        }
        $request['quantity'] = 1;
        //$request['address'] = getLocation($request['latitude'],$request['longitude']);
        $request['merchanttnxref'] = "FX".$request['user_id'].Carbon::now()->timestamp;
        $amount_check = checkAmountBeforePayment($this->user->id);
        if($amount_check == false){
            return $this->respondWithError('The amount of currency traded today has exceeded the limit'); 
        }
        $data = $this->repository->postPayment($request);
        //return $data;
        if ($data) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Transaction successful',
                'data' => $data
            ]);
        }
        else{
            return $this->respondWithError('Transaction Error');
        }
    }
    /**
     *
     * Purchased Currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/test-payment",
     *   summary="View",
     *   operationId="api.customer.post.payment",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Confirm Tracsaction",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/UpdatePayment")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postTestPayment(Request $request){
        $merchanttnxref = $request->merchanttxnref;
        $data = $this->repository->postTestPayment($merchanttnxref);
        if($data){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Transaction successful',
                'data' => $data
            ]);
        }else{
            return $this->respondWithError('Update transaction failed');
        }
    }
    /**
     *
     * Check Password Currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/check-current-password",
     *   summary="View",
     *   operationId="api.customer.post.current.password",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Check Password Current",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CheckCurrentPassword")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postCheckCurrentPassword(Request $request){
        $user = $this->user;
        if (Hash::check($request->current_password,$user->password)) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Password is correct',
                'data' => []
            ]); 
        }
        else{
            return $this->respondWithError('Password current not mach');
        }
    }

    /**
     * Customer getCallbackDataFromNetpay
     * @param array
     * @return json array
     *
     * @SWG\Post(
     *   path="/v1/customer/callback-data-from-netpay",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.callback.data.from.netpay",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
    */
    public function getCallbackDataFromNetpay(Request $request){
        $data = $request->all();
        $enets = new Enets2();
        try {
            $enets->setSecretKey("42a3c0af-cb54-4941-83c4-3c28f3f11319");
            $response = $enets->getBackendResponse();
            //$data_response = json_decode($response);
            $merchanttxnref = $response['merchantTxnRef'];
            $netsTxnMsg = $response['netsTxnMsg'];
            if($merchanttxnref != null){
                $transaction = Transaction::where('merchanttnxref',$merchanttxnref)->first();
                if($transaction && $netsTxnMsg=="Successful"){
                    $transaction->status = "paid";
                    $transaction->status = "paid";
                    $transaction->save();
                    event(new SendPushUpdateStatus($transaction));
                    //Email Confirmation to Admin after payment is made
                    $transaction_check = Transaction::getEmailUser($transaction->id);
                    $get_symbol = Currency::where('name',$transaction->currency)->first();
                    if($get_symbol){
                        $symbol = $get_symbol->symbol;
                        $sign = $get_symbol->sign;
                    }
                    if($transaction_check){
                        $get_contact = Contact::where('user_type','customer')->where('is_show',1)->first();
                        if($get_contact){
                            $email = $get_contact->email;
                            $phone = $get_contact->phone;
                            $country_code = $get_contact->country_code;
                        }
                        $arr_data_payment = [
                                'subject'            => 'FxChange',
                                'currency_bought'    => $transaction_check->amount,
                                'exchange_rate'      => $transaction_check->exchange_rate,
                                'symbol'             => $symbol,
                                'sign'               => $sign,
                                'collection_type'    => $transaction_check->getPurchaseType(),
                                'address'            => $transaction_check->address,
                                'transaction_id'     => $transaction_check->id,
                                'email'              => $transaction_check->getUser ? $transaction_check->getUser->email : '',
                                'total_payment'      => $transaction_check->getTotalPrice(),
                                'payment_date'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                                'payment_time'       => Carbon::parse($transaction_check->created_at)->timezone('Asia/Singapore')->format('H:i'),
                                'transaction_by'     => $transaction_check->getMerchantAssign(),
                                'full_name'          => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                                'email'              => $email,
                                'country_code'       => $country_code,
                                'phone'              => $phone,
                           ]; 
                        if(env('QUEUE_DRIVER')=='sync'){
                            if(isset($transaction_check->getUser)){
                                Mail::to($transaction_check->getUser->email)->send(new SendMailTransaction($arr_data_payment));
                            }
                        }elseif(env('QUEUE_DRIVER')=='database'){
                            if(isset($transaction_check->getUser)){
                                Mail::to($transaction_check->getUser->email)->queue(new SendMailTransaction($arr_data_payment));
                            }
                        }
                        $arr_data = [
                                    'subject'                   => 'FxChange',
                                    'purchase_type'             => $transaction->getPurchaseType(),
                                    'delivery_type'             => $transaction->purchase_type=="delivery"? $transaction->getDeliveryType():"",
                                    'amount'                    => $transaction->amount,
                                    'purchase_date'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('d M Y'),
                                    'purchase_time'             => Carbon::parse($transaction->created_at)->timezone('Asia/Singapore')->format('H:i'),
                                    'order_id'                  => $transaction->id,
                                    'paid_amount'               => $transaction->total_price,
                                    'exchange_rate'             => $transaction->exchange_rate,
                                    'sgd_rate'                  => $transaction->sgd_rate,
                                    'symbol'                    => $symbol,
                                    'collection_expired_date'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('d M Y'):"None",
                                    'collection_expired_time'   => $transaction->purchase_type == "self-pickup"? Carbon::parse($transaction->collection_expired_date)->timezone('Asia/Singapore')->format('H:i'):"",
                                    'customer_name'             => $transaction->getUser? $transaction_check->getUser->first_name. " " .$transaction_check->getUser->last_name:"No Name",
                                    // 'contact_no'                => $transaction->getUser? "+65 ".$transaction_check->getUser->phone_number:"None",
                                    'contact_no'                => $transaction->getContactNumber(),
                                    'address'                   => $transaction->address,
                                    'postal_code'               => $transaction->postal_code,
                                    'block'                     => $transaction->block,
                                    'unit'                      => $transaction->unit,
                               ];
                        $check_role = UserRole::with('user')->get();
                        foreach($check_role as $role){
                            
                            if($role->role_id == 1){
                                // return $role->user;
                                if(isset($role->user)){
                                    //return 1;
                                    if(env('QUEUE_DRIVER')=='sync'){
                                        Mail::to($role->user->email)->send(new SendMailConfirmPayment($arr_data));
                                    }elseif(env('QUEUE_DRIVER')=='database'){
                                        Mail::to($role->user->email)->queue(new SendMailConfirmPayment($arr_data));
                                    }
                                }
                            }
                        }

                        //return $transaction;
                    }
                }
            }
            return response()->json([
                'status' => true,
                'result' => $response,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'result' => $e->getMessage()
            ]);
        }
    }



    /**
     *
     * Purchased Currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/customer/readNotification",
     *   summary="View",
     *   operationId="api.customer.ReadNotification",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="ReadNotification",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ReadNotification")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function readNotification(Request $request){
        //$notificationId = $request->get('id');
        $push_type = $request->get('push_type');
        $symbol = $request->get('symbol');
        $transaction_id = $request->get('transaction_id');
        if($transaction_id ==0){
            $item = $this->notificationListEntities
             ->where('push_type',$push_type)
             ->where('user_id',$this->user->id)
             ->where('symbol',$symbol)
             ->get();
        }else{
            $item = $this->notificationListEntities
             ->where('push_type',$push_type)
             ->where('user_id',$this->user->id)
             ->where('symbol',$symbol)
             ->where('transaction_id',$transaction_id)
             ->get();
        }


        if(count($item)>0){
            //update is read
            foreach($item as $v){
                $v->is_read = 1;
                $v->save(); 
            }


            $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($this->user->id);

            return $this->respond([
                "status"=> "success",
                "status_code"=> 200,
                "message"=> "Updated read notification",
                "data"=> [
                    'badge_number'=>$badgeNumbers,
                ]
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-unreadnotification-by-user",
     *   description="Params : user_id",
     *   summary="View",
     *   operationId="api.customer.get.unread",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="user_id",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getUnreadNotification(Request $request){
        $user_id = $request->get('user_id');
        $item = $this->repository->getUnreadNotification($user_id);
        if($item !== null){
            $list = new Collection($item, new CustomerUnreadNotificationTransformer());
            $list = $this->fractal->createData($list)->toArray();
            return $this->respond([
                "status"=> "success",
                "status_code"=> 200,
                "message"=> "Get list unread notification by user",
                "data"=> $list,
            ]);            
        }
        // return $this->respondNotFound('Not Found');
            $items = [];
            return $this->respond([
                "status"=> "success",
                "status_code"=> 200,
                "message"=> "Get list unread notification by user",
                "data"=> $items,
            ]);

    }
    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/detail-transaction",
     *   description="Params : transaction_id",
     *   summary="View",
     *   operationId="api.customer.get.unread",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="transaction_id",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */    
    public function getDetailTransaction(Request $request){
        $transaction_id = $request->get('transaction_id');
        $user_id = $user = $this->user->id;
        $data = $this->repository->getDetailTransaction($transaction_id,$user_id);
        //return response()->json($data);
        if ($data !== null) {
            $item = $this->transformerpurchase->transform($data);
            if(count($item) > 0){
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Contacts Us',
                    'data' => $item
                ]);
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            return $this->respondNotFound('Not Found');
        }        
    }
    /**
     * Customer get Delivery Details
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/get-list-notifications",
     *   description="",
     *   summary="View",
     *   operationId="api.customer.get.unread",
     *   produces={"application/json"},
     *   tags={"Customer"},
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="limit",
     *     required=false,
     *     type="integer",
     *     default="10"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="page",
     *     required=false,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */  
    public function getListNotifications(Request $request){
        $perPage = $request->has('limit') ? $request->get('limit') : 10;
        $currentPage = $request->has('page') ? $request->get('page') : 1;

        $user_id = $user = $this->user->id;
        $item = $this->repository->getListNotifications($user_id);


        $total = count($item->get());

        $item = $item->paginate($perPage);
        $paginate = new Paginator($item, $total, $perPage, $currentPage);


        if($item !=null){
            $list = new Collection($item, new CustomerNotificationsTransformer());
            $list = $this->fractal->createData($list)->toArray();
            if (count($list)) {
                return $this->respondWithPagination($paginate, $list,'Get list notification');
                }
                else{
                    return $this->respondNotFound('Not Found');
                }
        }else{
            return $this->respondNotFound('Not Found');
        }

    }
}

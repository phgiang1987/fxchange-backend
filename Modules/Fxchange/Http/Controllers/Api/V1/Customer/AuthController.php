<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1\Customer;

use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Notification;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Modules\Fxchange\Http\Controllers\Api\V1\ApiController;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerLoginRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerResetRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerVerifyCodeSignUpRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\VerifyCustomerCredentialsRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\ChangePhoneSignUpRequest;
use Modules\Fxchange\Notifications\VerifyCodeSMS;
use Modules\Fxchange\Repositories\CustomerRepository;
use Modules\Fxchange\Repositories\Sentinel\FxSentinelCustomerRepository;
use Modules\Fxchange\Transformers\Api\Customer\CustomerTransformer;
use Modules\Fxchange\Repositories\FxUserTokenRepository;
use Modules\Fxchange\Repositories\Sentinel\FxSentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Mail;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\Fxchange\Services\FxUserResetter;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Entities\FxchangeNotificationList;
use Modules\Fxchange\Entities\Customer\Customer;
class AuthController extends ApiController
{
    use DispatchesJobs;

    protected $customerTransformer;

    private $customerRepository;
    protected $repository;
    protected $userTokenRepository;
    protected $notificationListRepository;
    protected $fxSentinelCustomerRepository;
    protected $fractal;

    public function __construct(FxSentinelUserRepository $repository, FxUserTokenRepository $userTokenRepository, CustomerTransformer  $customerTransformer , CustomerRepository $customerRepository, FxSentinelCustomerRepository $fxSentinelCustomerRepository, Manager $fractal, FxchangeNotificationList $notificationListEntities)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->userTokenRepository = $userTokenRepository;
        $this->customerTransformer = $customerTransformer;
        $this->customerRepository = $customerRepository;
        $this->fxSentinelCustomerRepository = $fxSentinelCustomerRepository;
        $this->fractal = $fractal;
        $this->notificationListEntities = $notificationListEntities;
    }
    /**
     * Customer login to customer app
     * @param array
     * @return json response user profile vs token
     *
     * @SWG\Post(
     *   path="/v1/customer/login",
     *   description="<ul>
     *     <li>phone_number : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.v1.customer.postLogin",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerLogin")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postLogin(CustomerLoginRequest $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        try {
            $remember = false;
            $customer = Sentinel::authenticate($credentials, $remember);
            if ($customer) {
                //login success
                $user = $this->customerRepository->find($customer->id);

                if($customer->hasRoleSlug('customer')){
                    if ($user->check_reset_password) {
                        $deviceExisted = $this->userTokenRepository->findByAttributes(['user_id'=>$customer->id,'firebase_token'=>$request->get('firebase_token')]);
                        if($deviceExisted){
                            $this->userTokenRepository->destroy($deviceExisted);
                        }
                        $this->userTokenRepository->generateFor($customer->id,$request->get('firebase_token'),$request->get('device_type'));

                        $resource = new Item($user,$this->customerTransformer);
                        $this->fractal->parseIncludes(['addresses']);
                        $data = $this->fractal->createData($resource)->toArray();

                        $getCurrentToken = $this->userTokenRepository->findByAttributes(['user_id'=>$customer->id,'firebase_token'=>$request->get('firebase_token')]);

                        $data['data']['access_token'] = $getCurrentToken ? $getCurrentToken->access_token : '';

                        //get badge number
                        $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($customer->id);
                        $data['data']['badge_number'] = $badgeNumbers;

                        //get total mount in day by user
                        $data['data']['total_amount_inday'] = getTransactionAmountInDateByUser($customer->id);

                        return $this->respond([
                            'status' => true,
                            'status_code' => $this->getStatusCode(),
                            'message' => 'Login successful!',
                            'data' => $data['data']
                        ]);
                    }
                    else
                    {
                        //User already reset new password
                        $firebase_token = $request->get('firebase_token');
                        $device_type = $request->get('device_type');
                        $request_id = $this->customerRepository->sendCodeSignIn($user,$firebase_token,$device_type);
                        if ($request_id) {
                            return $this->respond([
                                'status' => true,
                                'status_code' => $this->getStatusCode(),
                                'message' => 'Please enter code verify to sign in',
                                'data' => [
                                    'status' => 'reset',
                                    'request_id' => $request_id,
                                    'phone_number' => $user->phone_number,
                                    'country_code' => $user->country_code
                                ]
                            ]);
                        }
                        else{
                            return $this->respondWithError('Can not send verify sms code to your phone number.');
                        }
                        
                    }
                    
                }else{
                    return $this->respondWithError(trans('fxchange::users.wrong_customer_account_logged'));
                }
            }
            else{
                $register = $this->customerRepository->checkUserRegisterYet($request->email , $request->password);
                if ($register) {
                    return $this->respond([
                        'status' => true,
                        'status_code' => $this->getStatusCode(),
                        'message' => trans('fxchange::fxchanges.verify_before_login'),
                        'data' => [
                            'status' => 'active',
                            'request_id' => $register->request_id,
                            'phone_number' => substr($register->phone_number ,3,12),
                            'country_code' => substr($register->phone_number,0,3)
                        ]
                    ]);
                }
                return $this->respondWithError(trans('fxchange::fxchanges.login_incorrect'));
            }
        } catch (NotActivatedException $e) {
            return $this->respondWithError(trans('user::users.account not validated'));
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return $this->respondWithError(trans('user::users.account is blocked', ['delay' => $delay]));
        }
    }


    /**
     * postVerifyOPT
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/verify-otp",
     *   description="<ul>
     *     <li>request_id : string (required)</li>
     *     <li>code : interger (required)</li></ul>",
     *   summary="View",
     *   operationId="api.customer.verify.otp",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerVerifyCodeSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postVerifyOPT(Request $request){
        $verify = $this->customerRepository->verifySignIn($request->request_id, $request->code);
        if ($verify) {
            $deviceExisted = $this->userTokenRepository->findByAttributes(['user_id'=>$verify->user_id,'firebase_token'=> $verify->firebase_token]);
            if($deviceExisted){
                $this->userTokenRepository->destroy($deviceExisted);
            }
            $user = Customer::find($verify->user_id);
            $this->userTokenRepository->generateFor($verify->user_id ,$verify->firebase_token,$verify->device_type);

            $resource = new Item($user,$this->customerTransformer);
            $this->fractal->parseIncludes(['addresses']);
            $data = $this->fractal->createData($resource)->toArray();

            $getCurrentToken = $this->userTokenRepository->findByAttributes(['user_id' => $verify->user_id, 'firebase_token'=> $verify->firebase_token]);

            $data['data']['access_token'] = $getCurrentToken ? $getCurrentToken->access_token : '';
            $user->check_reset_password = 1;
            $user->save();
            $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($user->id);
            $data['data']['badge_number'] = $badgeNumbers;
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Login successful!',
                'data' => $data['data']
            ]);
        }
        else
        {
            return $this->respondWithError("This phone not verify or verify not match");
        }
        
    }


    /**
     * SignUp
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/sign-up",
     *   description="<ul>
     *     <li>phone_number : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.v1.customer.postSignUp",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postSignUp(VerifyCustomerCredentialsRequest $request){
        $phoneNumber = $request->get('phone_number');
        $email = $request->get('email');
        $password = $request->get('password');
        $firebaseToken = $request->get('firebase_token');
        $deviceType = $request->get('device_type');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');

        $requestId = $this->customerRepository->verifyCustomerCredentials($email,$phoneNumber,$password,
            $firebaseToken,$deviceType,$first_name,$last_name);
        if($requestId){
            return $this->respond([
                'status'=>true,
                "status_code" => 200,
                'message'=> 'Verify code sms was be sent successfully.',
                'data'=>[
                    'request_id' => $requestId
                ]
            ]);
        }else{
            return $this->respondWithError('Can not send verify sms code to your phone number.');
        }
    }

    /**
     * postVerifyCodeSignUp
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/verify-sign-up",
     *   description="<ul>
     *     <li>phone_number : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.v1.customer.postVerifyCodeSignUp",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerVerifyCodeSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postVerifyCodeSignUp(CustomerVerifyCodeSignUpRequest $request){

        $requestId = $request->get('request_id');
        $code = $request->get('code');

        //verify request
        $verifyError = $this->customerRepository->verifyRequestCodeSignUp($requestId,$code);
        if($verifyError !== null){
            return $this->respondWithError($verifyError);
        }
        //sign up
        $user = $this->fxSentinelCustomerRepository->customerSignUp($requestId,$code);
        //login
        $credentials = [
            'phone_number'=> $user->phone_number,
            'password'=>decrypt($user->pwd),
        ];
        $loginStatus = $this->fxSentinelCustomerRepository->login($credentials);
        if($loginStatus !== false){
            //login fail
            return $this->respondWithError($loginStatus);
        }

        $customer = $this->customerRepository->find($user->id);


        $resource = new Item($customer,$this->customerTransformer);
        $this->fractal->parseIncludes(['addresses']);
        $data = $this->fractal->createData($resource)->toArray();
        
        $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($user->id);
        $data['data']['badge_number'] = $badgeNumbers;


        $getCurrentToken = $this->userTokenRepository->findByAttributes(['user_id'=>$user->id,'firebase_token'=>$user->firebase_token]);
        $data['data']['access_token'] = $getCurrentToken ? $getCurrentToken->access_token : '';


        return $this->respond([
            'status'=>true,
            "status_code"=> 200,
            'message'=> trans('fxchange::fxchanges.register_successfully'),
            'data'=> $data['data']
        ]);
    }

    /**
     * postReset
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/change-phone-sign-up",
     *   description="<ul>
     *     <li>email : string (required) | email</li></ul>",
     *   summary="View",
     *   operationId="api.customer.postChangePhoneSingUp",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ChangePhoneSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postChangePhoneSignUp(ChangePhoneSignUpRequest $request){
        $data = $request->only(['phone_number','email']);

        $verifyChangePhone = $this->customerRepository->postChangePhoneSignUp($data);
        //return $verifyChangePhone;
        if ($verifyChangePhone !== null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Your need verify code to update number phone',
                'data' => [
                    'request_id' => $verifyChangePhone
                ]
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
        
    }


    /**
     * postReset
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/customer/post-reset",
     *   description="<ul>
     *     <li>email : string (required) | email</li></ul>",
     *   summary="View",
     *   operationId="api.v1.customer.postReset",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerReset")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */

    public function postReset(CustomerResetRequest $request){
        $data_check = User::join('role_users', 'users.id', '=', 'role_users.user_id')
                            ->join('roles', 'roles.id', '=', 'role_users.role_id')
                            ->where('roles.slug','customer')
                            ->where('users.email',$request->email)
                            ->first();
        if(!$data_check){
            return $this->respondWithError(trans('user::messages.no user found'));
        }else{
                try {
                app(FxUserResetter::class)->startReset($request->all());

                return $this->respond([
                    'status'=>true,
                    "status_code"=> 200,
                    'message'=> trans('fxchange::fxchanges.reset_successfully'),
                    'data'=>[]
                ]);
            } catch (UserNotFoundException $e) {
                return $this->respondWithError(trans('user::messages.no user found'));
            }
        }
        
    }

    /**
     * getLogout
     * @param array
     * @return json status true/false
     *
     * @SWG\Get(
     *   path="/v1/customer/logout",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.customer.getLogout",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=403, description="Unauthorized - invalid token"),
     * @SWG\Response(response=200, description=""),
     *   security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function getLogout(Request $request){
        $user = $this->user;
        $token = str_replace('Bearer ', '', $request->header('authorization'));
        $userToken = $this->userTokenRepository->findByAttributes( ['user_id' => $user->id, 'access_token' => $token] );

        if(count($userToken)){
            $this->userTokenRepository->destroy($userToken);

            return $this->respond([
                'status'=>true,
                "status_code"=> 200,
                'message'=> trans('fxchange::fxchanges.logout_successfully'),
                'data'=>[]
            ]);
        }
        return $this->respondWithError(trans('user::messages.no user found'));
    }
}

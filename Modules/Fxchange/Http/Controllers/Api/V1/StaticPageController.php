<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use Modules\Fxchange\Repositories\StaticPageRepository;
use Mail;
use Modules\Fxchange\Transformers\Api\StaticPageTransformer;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class StaticPageController extends ApiController
{
    use DispatchesJobs;

    protected $transformer;
    protected $repository;
    protected $fractal;

    public function __construct(StaticPageRepository $repository, Manager $fractal, StaticPageTransformer $transformer)
    {
        parent::__construct();

        $this->repository = $repository;
        $this->fractal = $fractal;
        $this->transformer = $transformer;

    }

    /**
     * Customer FAQ list
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/customer/faqs",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.customer.getCustomerFaq",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function getCustomerFaq(){
        $perPage = 30;
        $currentPage = 1;

        $items = $this->repository->getFaqs('customer');
        $total = count($items->get());
        $faqs = $items->paginate($perPage);
        $paginate = new Paginator($items, $total, $perPage, $currentPage);

        $faqs = new Collection($faqs,$this->transformer);
        $faqs = $this->fractal->createData($faqs)->toArray();

        if(count($faqs) > 0){
            return $this->respondWithPagination($paginate,$faqs,'Get list faq items');
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Merchant FAQ list
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/faqs",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.merchant.getMerchantFaq",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function getMerchantFaq(){
        $perPage = 30;
        $currentPage = 1;

        $items = $this->repository->getFaqs('merchant');
        $total = count($items->get());
        $faqs = $items->paginate($perPage);
        $paginate = new Paginator($items, $total, $perPage, $currentPage);

        $faqs = new Collection($faqs,$this->transformer);
        $faqs = $this->fractal->createData($faqs)->toArray();

        if(count($faqs) > 0){
            return $this->respondWithPagination($paginate,$faqs,'Get list faq items');
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Customer Term Conditions
     * @param array
     * @return json single object
     *
     * @SWG\Get(
     *   path="/v1/customer/term-condition",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.customer.getCustomerTermCondition",
     *   produces={"application/json"},
     *   tags={"Customer"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function getCustomerTermCondition(){
        $perPage = 30;
        $currentPage = 1;

        $item = $this->repository->getTermCondition('customer');
        $item = $item->first();


        // $data = new Collection($item,$this->transformer);
        // $data = $this->fractal->createData($data)->toArray();

        $data = $this->transformer->transform($item);

        if(count($data) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Result',
                'data' => $data
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }
    /**
     * Merchant Term Conditions
     * @param array
     * @return json single object
     *
     * @SWG\Get(
     *   path="/v1/merchant/term-condition",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.merchant.getMerchantTermCondition",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function getMerchantTermCondition(){
        $perPage = 30;
        $currentPage = 1;

        $item = $this->repository->getTermCondition('merchant');
        $item = $item->first();



        // $data = new Collection($item,$this->transformer);
        // $data = $this->fractal->createData($data)->toArray();

        $data = $this->transformer->transform($item);

        if(count($data) > 0){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Result',
                'data' => $data
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

}

<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Fxchange\Http\Requests\FxResetCompleteRequest;
use Modules\Fxchange\Services\FxUserResetter;
use Modules\User\Exceptions\InvalidOrExpiredResetCode;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\User\Http\Requests\LoginRequest;
use Modules\User\Http\Requests\RegisterRequest;
use Modules\User\Http\Requests\ResetCompleteRequest;
use Modules\User\Http\Requests\ResetRequest;
use Modules\User\Services\UserRegistration;
use Modules\User\Services\UserResetter;

class ResetController extends BasePublicController
{
    use DispatchesJobs;

    public function __construct()
    {
        parent::__construct();
    }

    public function getResetComplete()
    {
        return view('fxchange::public.reset.reset');
    }

    public function postResetComplete($userId, $code, FxResetCompleteRequest $request)
    {
        try {

            // $flag = 0;
            // if(preg_match("/^(?=.*[A-Z]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*?[a-z]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*[0-9]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if($flag >=3){

                app(FxUserResetter::class)->finishReset(
                    array_merge($request->all(), ['userId' => $userId, 'code' => $code])
                );
                if ($request->has('password')) {
                    save_password_recoverd($userId,$request->password);
                }
                return redirect()->route('reset.successful')
                ->withSuccess(trans('user::messages.password reset'));
            // }return redirect()->back()->withInput()
            //     ->withError(trans('Minimum password length 8 characters, must meet at least 3 of the 4 criteria: upper case, lower case, numbers, special characters')); 

        } catch (UserNotFoundException $e) {
            return redirect()->back()->withInput()
                ->withError(trans('user::messages.user no longer exists'));
        } catch (InvalidOrExpiredResetCode $e) {
            return redirect()->back()->withInput()
                ->withError(trans('user::messages.invalid reset code'));
        }

    }

    public function getResetSuccessful(){
        return view('fxchange::public.reset.success');
    }
}

<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1\Merchant;

use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Hash;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;

use Modules\Fxchange\Http\Controllers\Api\V1\ApiController;
use Modules\Fxchange\Repositories\MerchantRepository;
use Modules\Fxchange\Http\Requests\Api\Merchant\MerchantFeedbackRequest;
use Modules\Fxchange\Transformers\Api\Merchant\MerchantContactTransformer;
use Modules\Fxchange\Transformers\Api\Merchant\MerchantPurchaseTransformer;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerUpdateAccountRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerVerifyChangePhone;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerResendCodePhone;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerChangePasswordRequest;
use Modules\Fxchange\Entities\UserRecoverPwd;
use Modules\Fxchange\Entities\Customer\Customer;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Carbon\Carbon;
use Modules\Fxchange\Http\Requests\Api\Merchant\MerchantUpdateAccountRequest;
use Modules\Fxchange\Entities\Transaction;
use Illuminate\Pagination\LengthAwarePaginator;

class MerchantController extends ApiController
{
    use DispatchesJobs;

    protected $fractal;

    protected $contactTransformer;

    protected $purchaseTransformer;

    public function __construct(
    	MerchantRepository $merchantRepository,
    	Manager $fractal,
        MerchantContactTransformer $contactTransformer,
        MerchantPurchaseTransformer $purchaseTransformer
    ){
        parent::__construct();
        $this->repository = $merchantRepository;
        $this->fractal = $fractal;
        $this->contactTransformer = $contactTransformer;
        $this->purchaseTransformer = $purchaseTransformer;
    }

    /**
     * Merchant get Contact US
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/contact-us",
     *   description="",
     *   summary="View",
     *   operationId="api.merchant.get.contactus",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description="")
     * )
    */
    public function getContactUs(){
        $data = $this->repository->getContactUs();
        if ($data !== null) {
            $contact = $this->contactTransformer->transform($data);
            if(count($contact) > 0){
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Contacts Us',
                    'data' => $contact
                ]);
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            return $this->respondNotFound('Not Found');
        }
        
    }

    /**
     * Create Feedback Merchant
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/create-feedback",
     *   summary="View",
     *   operationId="api.merchant.post.feedback",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Create Feedback",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/MerchantFeedback")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function createFeeedback(MerchantFeedbackRequest $request){
        $user = $this->user;
        $data = $request->only(['content']);
        $data['user_id'] = $user->id;
        $status = $this->repository->createFeeedback($data);
        if ($status) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => trans('fxchange::fxchanges.thank_you_feedback'),
                'data' => []
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Merchant getPastDelivery
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/get-past-delivery",
     *   description="Params : (1 All ,2 Delivered | 3 Cancelled)",
     *   summary="View",
     *   operationId="api.merchant.past.delivery",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="status",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
    *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="limit",
     *     required=false,
     *     type="integer",
     *     default="10"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="page",
     *     required=false,
     *     type="integer",
     *     default="1"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getPastDelivery(Request $request){
        $user = $this->user;
        $data = $request->all();
        
        $perPage = $request->has('limit') ? $request->get('limit') : 10;
        $currentPage = $request->has('page') ? $request->get('page') : 1;

        $data['user_id'] = $user->id;
        $purchased = $this->repository->getPastDelivery($data);

        $total = count($purchased->get());

        $purchased = $purchased->paginate($perPage);
        $paginate = new Paginator($purchased, $total, $perPage, $currentPage);


        if (count($purchased) > 0) {
            $purchased = new Collection($purchased, new MerchantPurchaseTransformer());
            $purchased = $this->fractal->createData($purchased)->toArray();
            return $this->respondWithPagination($paginate, $purchased,'Get list purchased');
            
        }
        else{
            return $this->respondWithError('List Order Delivery Null');
        }


        // if ($results !== null) {
        //     $purchased = new Collection($results, new MerchantPurchaseTransformer());
        //     $purchased = $this->fractal->createData($purchased)->toArray();
        //     return $this->respond([
        //         'status' => true,
        //         'status_code' => $this->getStatusCode(),
        //         'message' => 'Get Past Delivery',
        //         'data' => $purchased
        //     ]);
        // }
        // else{
        //     return $this->respondNotFound('Not Found');
        // }
    }

    /**
     * Merchant get my account
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/get-my-account",
     *   description="",
     *   summary="View",
     *   operationId="api.merchant.get.myaccount",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */
    public function getUpdateAccount(){
        $data = $this->user;
        $data->country_code = "+".trim(substr($data->country_code,1,3));
        if($data !== null){
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Get My Account',
                'data' => $data
            ]);
        }
        else{
            return $this->respondNotFound('Not Found');
        }
    }

    /**
     * Update My Account
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/post-my-account",
     *   summary="View",
     *   operationId="api.merchant.post.myaccount",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Update My Account",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/UpdateMyAccount")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postUpdateAccount(MerchantUpdateAccountRequest $request){
        $user = $this->user;
        $dataReuest = $request->only(['last_name','first_name']);
        $country_code = substr($request->phone_number,0,3);
        $phone_number = substr($request->phone_number,3,12);
        $verifyPhone = Customer::where([
            'id' => $user->id,
            'country_code' => $country_code, 
            'phone_number' => $phone_number
        ])->first();
        if ($verifyPhone !== null) {
            $dataReuest['phone_number'] = $phone_number;
            $dataReuest['country_code'] = $country_code;
            $data = $this->repository->postUpdateAccount($user, $dataReuest);
            if ($data) {
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Update My Account',
                    'data' => []
                ]);
            }
            else{
                return $this->respondNotFound('Not Found');
            }
        }
        else{
            $requestId = $this->repository->sendVerifyChangePhone($user->id,$request->phone_number,$request->last_name,$request->first_name);
            if ($requestId) {
                return $this->respond([
                    'status' => true,
                    'status_code' => $this->getStatusCode(),
                    'message' => 'Your need verify code to update number phone',
                    'data' => [
                        'request_id' => $requestId
                    ]
                ]);
            }
            else{
                return $this->respondWithError('Can not send verify sms code to your phone number.');
            }
            
        }
        
    }

    /**
     * Re-send Verify code Phone Number
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/resend-code-phone",
     *   summary="View",
     *   operationId="api.merchant.post.resendcodephone",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Re-send Verify code Phone Number",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ReSendCodePhone")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */

    public function postResendCode(CustomerResendCodePhone $request){
        $user = $this->user;
        $verify = $this->repository->postReSendVerifyPhone($user , $request->phone_number);
        if ($verify !== null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Re-send code verify successful',
                'data' => [
                    'request_id' => $verify
                ]
            ]);
        }
        else{
            return $this->respondWithError('Can not send verify sms code to your phone number.');
        }
        
    }


    /**
     * Post Verify Change Phone
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/merchant/verify-change-phone",
     *   description="<ul>
     *     <li>phone_number : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.merchant.post.change.phone",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Verify Change Phone",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerVerifyCodeSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *  security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postVerifyChangePhone(CustomerVerifyChangePhone $request){
        $user = $this->user;
        $verify = $this->repository->verifyChangePhone($user->id , $request->request_id,$request->code);
        if ($verify === null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Changed number phone successful',
                'data' => []
            ]);
        }
        else{
            $response = [
                'status' => false,
                'status_code' => 201,
                'message' => $verify,
                'data' => []
            ];
            return $this->respond($response);
        }
    }
    /**
     * Update My Account
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/post-change-password",
     *   summary="View",
     *   operationId="api.merchant.post.change.password",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Change Password",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ChangePassword")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postChangePassword(CustomerChangePasswordRequest $request){
        $user = $this->user;
        $countRecoverd = UserRecoverPwd::where('user_id',$user->id)->count();
        if (Hash::check($request->current_password,$user->password)) {
            if ($countRecoverd < config('asgard.fxchange.config.PWD_RECOVERD')) {
                $checkPassword = UserRecoverPwd::where(['user_id' => $user->id , 'pwd' => sha1($request->confirm_password)])->first();
                if ($checkPassword !== null) {
                    $statusCode = [
                        'status' => false,
                        'status_code' => 201,
                        'message' => trans('fxchange::fxchanges.check_last_password'),
                        'data' => []
                    ];
                    return response()->json($statusCode);
                }
            }
            else
            {
                $skip = $countRecoverd - config('asgard.fxchange.config.PWD_RECOVERD');
                $checkPassword = UserRecoverPwd::where(['user_id' => $user->id])
                    ->limit(config('asgard.fxchange.config.PWD_RECOVERD'))
                    ->orderBy('id','DESC')
                    ->pluck('pwd')->toArray();
                if (in_array(sha1($request->confirm_password), $checkPassword)) {
                    $statusCode = [
                        'status' => false,
                        'status_code' => 201,
                        'message' => trans('fxchange::fxchanges.check_last_password'),
                        'data' => []
                    ];
                    return response()->json($statusCode);
                }
            }
            $user->password = bcrypt($request->confirm_password);
            $user->save();
            if ($user) {
                save_password_recoverd($user->id,$request->confirm_password);
            }
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Update password successfully',
                'data' => []
            ]);
        }
        else{
            $statusCode = [
                'status' => false,
                'status_code' => 201,
                'message' => trans('fxchange::fxchanges.password_not_macth'),
                'data' => []
            ];
            return response()->json($statusCode);
        }
    }

    /**
     * Merchant Delivery Order
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/delivery-order",
     *   description="
     Status : 1 Today ,2 Tomorrow, 3 All
     Type: 1 All, 2 Overdue, 3 Express, 4 Normal
     Keyword : Keyword Filter",
     *   summary="View",
     *   operationId="api.merchant.post.delivery.order",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *  @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="status",
     *     required=true,
     *     type="integer",
     *     default="3"
     *   ),
     *  @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="type",
     *     required=true,
     *     type="integer",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="limit",
     *     required=false,
     *     type="integer",
     *     default="10"
     *   ),
     *   @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="page",
     *     required=false,
     *     type="integer",
     *     default="1"
     *   ),
     *   @SWG\Parameter(
     *     description="Keyword search",
     *     in="query",
     *     name="keyword",
     *     required=false,
     *     type="integer",
     *     default=""
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */  

    public function getDeliveryOrder(Request $request){
        $perPage = $request->has('limit') ? $request->get('limit') : 10;
        $currentPage = $request->has('page') ? $request->get('page') : 1;


        $data = $request->all();
        $user = $this->user->id;
        $data['user_id'] = $user;
        $purchased = $this->repository->getDeliveryOrder($data);


        $total = count($purchased->get());

        if (count($purchased) > 0) {
            
            if($request->type==1){

                $purchased = new Collection($purchased->get(), new MerchantPurchaseTransformer());
                $purchased = $this->fractal->createData($purchased)->toArray();

                $rs = $purchased['data'];

                $arrOverdue = collect($rs)->where('type',1)->sortByDesc('collection_expired_date')->all();
                $express = collect($rs)->where('type',2)->sortByDesc('collection_expired_date')->all();
                $normal = collect($rs)->where('type',3)->sortByDesc('collection_expired_date')->all();

                $result = array_merge($arrOverdue,$express,$normal);


                // Get current page form url e.x. &page=1
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                //$currentPage = $request->has('page') ? $request->get('page') : 1;
         
                // Create a new Laravel collection from the array data
                $itemCollection = collect($result);
         
                // Define how many items we want to be visible in each page
                //$perPage = $perPage;
         
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
         
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
         
                // set url path for generted links
                $paginatedItems->setPath($request->url());
                $newData = $paginatedItems;
                
                
                //return $newData->items();
                //
                return $this->respond([
                    'status' => true,
                    'status_code' => 200,
                    'message' => 'Get list purchased',
                    'data' => [
                        'paginator' => [
                            'total_count'  => count($itemCollection),
                            'total_pages' => ceil(count($itemCollection) / $perPage),
                            'current_page' => $currentPage,
                            'limit' => $perPage,
                        ],
                        'data' => array_values($newData->items()),
                        ]
                    ]);

            }else{

                //return 1;
                //$total = count($purchased->get());
                $purchased = $purchased->paginate($perPage);
                $paginate = new Paginator($purchased, $total, $perPage, $currentPage);

                $purchased = new Collection($purchased, new MerchantPurchaseTransformer());
                $purchased = $this->fractal->createData($purchased)->toArray();

                //$purchased = $purchased->paginate($perPage);
                // $purchased = [];//
                // $newData = $purchased; // after pagination\
                return $this->respondWithPagination($paginate, $purchased,'Get list purchased');
            }   
        } else{
            return $this->respondWithError('List Order Delivery Null');
        }


        

        // $newData = [];
        // $today = Carbon::today()->format('Y-m-d');
        // foreach ($purchased as $key => $value) {
        //     $day_is_db = Carbon::parse($value->collection_expired_date)->format('Y-m-d');
        //     $newData[$key] = $value;
        //     $newData[$key]->delivery_type = ($today == $day_is_db) ? 'Overdue' : ucfirst($value->delivery_type);
        // }
        // $newData = collect($newData)->groupBy(['delivery_type']);

        // return response()->json($newData);
        


        if (count($purchased) > 0) {
            $purchased = new Collection($purchased, new MerchantPurchaseTransformer());
            $purchased = $this->fractal->createData($purchased)->toArray();
             // $collection = collect($purchased['data']);
            // $sorted = $collection->sortBy('id');
            // return $sorted->values()->all();
            if($request->type==1){
                $newData = collect($purchased['data'])->sortBy('type')->values()->all();
                return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Get list purchased',
                'data' => [
                    'paginator' => [
                        'total_count'  => $paginate->total(),
                        'total_pages' => ceil($paginate->total() / $paginate->perPage()),
                        'current_page' => $paginate->currentPage(),
                        'limit' => $paginate->perPage(),
                    ],
                    'data' => $newData,
                    ]
                ]);
                //return $this->respondWithPagination($paginate, $purchased,'Get list purchased');
            }else{
                return $this->respondWithPagination($paginate, $purchased,'Get list purchased');
            }
            
            
        }
        else{
            return $this->respondWithError('List Order Delivery Null');
        }
    }

    /**
     * Merchant get Search
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/delivery-search",
     *   description="",
     *   summary="View",
     *   operationId="api.merchant.post.delivery.search",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Parameter(
     *     description="",
     *     in="query",
     *     name="keyword",
     *     required=true,
     *     type="integer",
     *     default="Name"
     *   ),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */  

    public function getDeliverySearch(Request $request){
        $keyword = $request->keyword;
        $user = $this->user->id;
        $purchased = $this->repository->getDeliverySearch($user,$keyword);
        if ($purchased) {
            $purchased = new Collection($purchased, new MerchantPurchaseTransformer());
            $purchased = $this->fractal->createData($purchased)->toArray();
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'List Purchase',
                'data' => $purchased
            ]);
        }
        else{
            return $this->respondWithError('List Purchase null');
        }
    }

    /**
     * Merchant Map View Selected State
     * @param array
     * @return json array
     *
     * @SWG\Get(
     *   path="/v1/merchant/map-view-state",
     *   description="",
     *   summary="View",
     *   operationId="api.merchant.get.state",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
    */  

    public function getMapViewState(Request $request){
        $user = $this->user->id;
        $purchased = $this->repository->getMapViewState($user);
        if ($purchased) {
            $purchased = new Collection($purchased, new MerchantPurchaseTransformer());
            $purchased = $this->fractal->createData($purchased)->toArray();
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'List map view State',
                'data' => $purchased
            ]);
        }
        else{
            return $this->respondWithError('List map view state null');
        }
    }


    /**
     * QR Scanner
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/check-qr-scanner",
     *   summary="View",
     *   operationId="api.merchant.post.qrscanner",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="QR Scanner",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CheckQRCode")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */

    public function postQRScanner(Request $request){
        $user = $this->user;
        $data = $request->all();
        $data['assign_by'] = $user->id;

        $check_qr_expiry = Carbon::parse($request->qr_code_generation_date." ".$request->qr_code_generation_time)->format('Y-m-d H:i:s');
        $one_minute_now = Carbon::now();


        $data_check = Transaction::where('user_id',$request->user_id)->where('id',$request->order_id)->whereRaw("status != 'completed'")->where('assign_by',$user->id)->first();
        //return $data_check;
        if($data_check){
            $qr_datetime = $data_check->qr_code_datetime;
            if($qr_datetime == null){
                $data_check->qr_code_datetime = $check_qr_expiry;
                $data_check->save();

            }else{
                $qr_datetime_timestamp = Carbon::parse($qr_datetime)->timestamp;
                $check_qr_expiry_timestamp = Carbon::parse($request->qr_code_generation_date." ".$request->qr_code_generation_time)->timestamp;
                //return $qr_datetime_timestamp. " " .$check_qr_expiry_timestamp;
                if($qr_datetime_timestamp == $check_qr_expiry_timestamp){
                    //return 1;
                    return $this->respondWithError('QR code has been scanned ! You have to wait 1 minute later to receive a new QR code');
                }else{
                    //return $check_qr_expiry;
                    $data_check->qr_code_datetime = $check_qr_expiry;
                    $data_check->save();
                }
            }
        }


        //return $check_qr_expiry ." " .$one_minute_now;
        if($data_check){
            if($one_minute_now > $check_qr_expiry){
                return $this->respondWithError('QR Code Expired'); 
            }
        }
        //return response()->json($one_minute_now);
        $qr_code = $this->repository->postQRScanner($data);
        if ($qr_code) {
            $transform = $this->purchaseTransformer->transform($qr_code);
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Check QR Code Successful',
                'data' => $transform
            ]);
        }
        else{
            return $this->respondWithError('Scanning unsuccessful!
 Please try again');
        }
    }

    /**
     * Confirm Transactions
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/confirm-transaction",
     *   summary="View",
     *   operationId="api.merchant.post.confirm.transaction",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Confirm Transaction",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ConfirmTransaction")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */

    public function postconfirmTransaction(Request $request){
        $user = $this->user;
        $data = $request->all();
        $data['user_id'] = $user->id;
        $qr_code = $this->repository->postconfirmTransaction($data);
        if ($qr_code == 1) {
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Confirm Purchase Successful',
                'data' => []
            ]);
        }
        elseif($qr_code == 3){
            return $this->respondWithError('Confirm Purchase Error');
        }elseif($qr_code == 2){
            return $this->respondWithError('This transaction has been scanned');
        }
    }
    /**
     * QR Scanner
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/machine-check-qr-scanner",
     *   summary="View",
     *   operationId="api.merchant.post.qrscanner",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="QR Scanner",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/MachineCheckQRCode")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */

    public function postMachineQRScanner(Request $request){
        $data = $request->all();
        $check_qr_expiry = Carbon::parse($request->qr_code_generation_date." ".$request->qr_code_generation_time)->format('Y-m-d H:i:s');
        //return $check_qr_expiry;
        $one_minute_now = Carbon::now();

        $data_check = Transaction::where('user_id',$request->user_id)->where('id',$request->order_id)->whereRaw("status != 'completed'")->first();
        if($data_check){
            $qr_datetime = $data_check->qr_code_datetime;
            if($qr_datetime == null){
                $data_check->qr_code_datetime = $check_qr_expiry;
                $data_check->save();

            }else{
                $qr_datetime_timestamp = Carbon::parse($qr_datetime)->timestamp;
                $check_qr_expiry_timestamp = Carbon::parse($request->qr_code_generation_date." ".$request->qr_code_generation_time)->timestamp;
                //return $qr_datetime_timestamp. " " .$check_qr_expiry_timestamp;
                if($qr_datetime_timestamp == $check_qr_expiry_timestamp){
                    //return 1;
                     return $this->respond([
                        'status'=> false,
                        "status_code"=> 501,
                        "message"=> 'QR Code has been scanned',
                        "data"=>[]
                    ]);
                    //return $this->respondWithError('QR Code has been scanned');
                }else{
                    //return $check_qr_expiry;
                    $data_check->qr_code_datetime = $check_qr_expiry;
                    $data_check->save();
                }
            }
        }

        //return $check_qr_expiry ." " .$one_minute_now;
        if($data_check){
            if($one_minute_now > $check_qr_expiry || $one_minute_now > $data_check->collection_expired_date){
                    return $this->respond([
                        'status'=> false,
                        "status_code"=> 502,
                        "message"=> 'QR Code Expired',
                        "data"=>[]
                    ]);
                //return $this->respondWithError('QR Code Expired'); 
            }
        }
       
        //return response()->json($one_minute_now);
        $qr_code = $this->repository->postMachineQRScanner($data);
        if ($qr_code) {
            $transform = $this->purchaseTransformer->transform($qr_code);
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Check QR Code Successful',
                'data' => $transform
            ]);
        }
        else{
            return $this->respond([
                'status'=> false,
                "status_code"=> 504,
                "message"=> 'Check QR Code Error',
                "data"=>[]
            ]);
            //return $this->respondWithError('Check QR Code Error');
        }
        
    }

    /**
     * Confirm Transactions
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/machine-confirm-transaction",
     *   summary="View",
     *   operationId="api.merchant.post.confirm.transaction",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Confirm Transaction",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ConfirmTransaction")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */

    public function postMachineConfirmTransaction(Request $request){
        $data = $request->all();
        $qr_code = $this->repository->postMachineConfirmTransaction($data);
        if ($qr_code == 1) {
            return $this->respond([
                'status' => true,
                'status_code' => 200,
                'message' => 'Confirm Purchase Successful',
                'data' => []
            ]);
        }
        elseif($qr_code == 3){
            return $this->respond([
                'status'=> false,
                "status_code"=> 500,
                "message"=> 'Confirm Purchase Error',
                "data"=>[]
            ]);

            

            //return $this->respondWithError('Confirm Purchase Error');
        }elseif($qr_code == 2){

            return $this->respond([
                'status'=> false,
                "status_code"=> 503,
                "message"=> 'This transaction has been scanned',
                "data"=>[]
            ]);

            //return $this->respondWithError('This transaction has been scanned');
        }
    }
    /**
     *
     * Check Password Currency
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/check-current-password",
     *   summary="View",
     *   operationId="api.merchant.post.current.password",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Check Password Current",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CheckCurrentPassword")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     *    security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function postCheckCurrentPassword(Request $request){
        $user = $this->user;
        if (Hash::check($request->current_password,$user->password)) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Password is correct',
                'data' => []
            ]); 
        }
        else{
            return $this->respondWithError('Password current not mach');
        }
    }     
}

<?php

namespace Modules\Fxchange\Http\Controllers\Api\V1\Merchant;

use Illuminate\Http\Request;
use App\Http\Requests\ApiRequest;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Modules\Fxchange\Http\Controllers\Api\V1\ApiController;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerLoginRequest;
use Modules\Fxchange\Http\Requests\Api\Customer\CustomerResetRequest;
use Modules\Fxchange\Http\Requests\Api\Merchant\MerchantLoginRequest;
use Modules\Fxchange\Repositories\CustomerRepository;
use Modules\Fxchange\Repositories\MerchantRepository;
use Modules\Fxchange\Services\FxUserResetter;
use Modules\Fxchange\Transformers\Api\Customer\CustomerTransformer;
use Modules\Fxchange\Repositories\FxUserTokenRepository;
use Modules\Fxchange\Repositories\Sentinel\FxSentinelUserRepository;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Mail;
use Modules\Fxchange\Transformers\Api\Merchant\MerchantTransformer;
use Modules\User\Exceptions\UserNotFoundException;
use Modules\Fxchange\Entities\Merchant\Merchant;
use Modules\Fxchange\Http\Requests\Api\Merchant\ChangePhoneSignInRequest;
use Modules\User\Entities\Sentinel\User;
class AuthController extends ApiController
{
    use DispatchesJobs;

    /**
     * @var Modules\Fxchange\Transformers\Api\Merchant\MerchantTransformer
     * */
    protected $merchantTransformer;

    private $merchantRepository;
    protected $repository;
    protected $userTokenRepository;

    public function __construct(FxSentinelUserRepository $repository, FxUserTokenRepository $userTokenRepository, MerchantTransformer  $merchantTransformer , MerchantRepository $merchantRepository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->userTokenRepository = $userTokenRepository;
        $this->merchantRepository = $merchantRepository;
        $this->merchantTransformer = $merchantTransformer;
    }
    /**
     * Merchant login to merchant app
     * @param array
     * @return json response user profile vs token
     *
     * @SWG\Post(
     *   path="/v1/merchant/login",
     *   description="<ul>
     *     <li>email : string (required)</li>
     *     <li>password : string (required)</li></ul>",
     *   summary="View",
     *   operationId="api.v1.merchant.postLogin",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/MerchantLogin")
     *   ),
     * * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description="")
     * )
     */
    public function postLogin(MerchantLoginRequest $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        try {
            $remember = false;
            $merchant = Sentinel::authenticate($credentials, $remember);
            if ($merchant) {
                //login success
                $user = $this->merchantRepository->find($merchant->id);

                if($merchant->hasRoleSlug('merchant')){
                    $request_id = $this->merchantRepository->sendCodeSignIn($user,$request->get('firebase_token'),$request->get('device_type'));
                    if ($request_id) {
                        return $this->respond([
                            'status' => true,
                            'status_code' => $this->getStatusCode(),
                            'message' => 'Please enter code verify to sign in',
                            'data' => [
                                'request_id' => $request_id,
                                'phone_number' => $user->phone_number,
                                'country_code' => $user->country_code
                            ]
                        ]);
                    }
                    else{
                        return $this->respondWithError("Can not send verify sms code to your phone number");
                    }

                    // $deviceExisted = $this->userTokenRepository->findByAttributes(['user_id'=>$merchant->id,'firebase_token'=>$request->get('firebase_token')]);
                    // if($deviceExisted){
                    //     $this->userTokenRepository->destroy($deviceExisted);
                    // }
                    // $this->userTokenRepository->generateFor($merchant->id,$request->get('firebase_token'),$request->get('device_type'));
                    // $data = $this->merchantTransformer->transform($user);
                    // $getCurrentToken = $this->userTokenRepository->findByAttributes(['user_id'=>$merchant->id,'firebase_token'=>$request->get('firebase_token')]);

                    // $data['access_token'] = $getCurrentToken ? $getCurrentToken->access_token : '';

                    // return $this->respond([
                    //     'status' => true,
                    //     'status_code' => $this->getStatusCode(),
                    //     'message' => 'Login successful!',
                    //     'data' => $data
                    // ]);
                }else{
                    return $this->respondWithError(trans('fxchange::users.wrong_merchant_account_logged'));
                }
            }
            else{
                return $this->respondWithError(trans('fxchange::fxchanges.login_incorrect'));
            }
        } catch (NotActivatedException $e) {
            return $this->respondWithError(trans('user::users.account not validated'));
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return $this->respondWithError(trans('user::users.account is blocked', ['delay' => $delay]));
        }
    }

    /**
     * Re-send Verify code Phone Number
     * @param array
     * @return json array
     * @SWG\Post(
     *   path="/v1/merchant/resend-verify-otp",
     *   summary="View",
     *   operationId="api.merchant.post.resendverifyotp",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Re-send Verify code Phone Number",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ReSendCodeLogin")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     * @SWG\Response(response=101, description="Wrong email or password"),
     * @SWG\Response(response=102, description="You need to confirm your account"),
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description="")
     * )
     */

    public function postResendVerifyOtp(Request $request){
        $verify = $this->merchantRepository->postReSendVerifyLogin($request->request_id);
        if ($verify !== null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Re-send code verify successful',
                'data' => [
                    'request_id' => $verify
                ]
            ]);
        }
        else{
            return $this->respondWithError('Can not send verify sms code to your phone number.');
        }
        
    }


    /**
     * postVerifyOPT
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/merchant/verify-otp",
     *   description="<ul>
     *     <li>request_id : string (required)</li>
     *     <li>code : interger (required)</li></ul>",
     *   summary="View",
     *   operationId="api.merchant.verify.otp",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerVerifyCodeSignUp")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postVerifyOPT(Request $request){
        $verify = $this->merchantRepository->verifySignIn($request->request_id, $request->code);
        if ($verify) {
            $deviceExisted = $this->userTokenRepository->findByAttributes(['user_id'=>$verify->user_id,'firebase_token'=> $verify->firebase_token]);
            if($deviceExisted){
                $this->userTokenRepository->destroy($deviceExisted);
            }
            $user = Merchant::find($verify->user_id);
            $this->userTokenRepository->generateFor($verify->user_id ,$verify->firebase_token,$verify->device_type);
            $data = $this->merchantTransformer->transform($user);
            $getCurrentToken = $this->userTokenRepository->findByAttributes(['user_id' => $verify->user_id, 'firebase_token'=> $verify->firebase_token]);

            $data['access_token'] = $getCurrentToken ? $getCurrentToken->access_token : '';
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Login successful!',
                'data' => $data
            ]);
        }
        else
        {
            return $this->respondWithError("This phone not verify or verify not match");
        }
        
    }


    /**
     * postReset
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/merchant/post-reset",
     *   description="<ul>
     *     <li>email : string (required) | email</li></ul>",
     *   summary="View",
     *   operationId="api.v1.merchant.postReset",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/CustomerReset")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postReset(CustomerResetRequest $request){
        $data_check = User::join('role_users', 'users.id', '=', 'role_users.user_id')
                            ->join('roles', 'roles.id', '=', 'role_users.role_id')
                            ->where('roles.slug','merchant')
                            ->where('users.email',$request->email)
                            ->first();
        if(!$data_check){
            return $this->respondWithError(trans('user::messages.no user found'));
        }else{
            try {
                app(FxUserResetter::class)->startReset($request->all());

                return $this->respond([
                    'status'=>true,
                    "status_code"=> 200,
                    'message'=> 'Password reset instructions have been sent to your email address',
                    'data'=>[]
                ]);
            } catch (UserNotFoundException $e) {
                return $this->respondWithError(trans('user::messages.no user found'));
            }
        }
    }

    /**
     * getLogout
     * @param array
     * @return json status true/false
     *
     * @SWG\Get(
     *   path="/v1/merchant/logout",
     *   description="",
     *   summary="View",
     *   operationId="api.v1.merchant.getLogout",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=403, description="Unauthorized - invalid token"),
     * @SWG\Response(response=200, description=""),
     *   security={
     *       {"userToken": {}}
     *   }
     * )
     */
    public function getLogout(Request $request){
        $user = $this->user;
        $token = str_replace('Bearer ', '', $request->header('authorization'));
        $userToken = $this->userTokenRepository->findByAttributes( ['user_id' => $user->id, 'access_token' => $token] );

        if(count($userToken)){
            $this->userTokenRepository->destroy($userToken);

            return $this->respond([
                'status'=>true,
                "status_code"=> 200,
                'message'=> trans('fxchange::fxchanges.logout_successfully'),
                'data'=>[]
            ]);
        }
        return $this->respondWithError(trans('user::messages.no user found'));
    }

    /**
     * postReset
     * @param array
     * @return json status true/false
     *
     * @SWG\Post(
     *   path="/v1/merchant/change-phone-singin",
     *   description="<ul>
     *     <li>email : string (required) | email</li></ul>",
     *   summary="View",
     *   operationId="api.merchant.post.change.singin",
     *   produces={"application/json"},
     *   tags={"Merchant"},
     * * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Target customer.",
     *     required=true,
     *    @SWG\Schema(ref="#/definitions/ChangePhoneSignIn")
     *   ),
     * @SWG\Parameter(name="FX-API-KEY",in="header",required=true,type="string",description="Authorise connection",default="dev-api-key"),
     *
     * @SWG\Response(response=500, description="internal server error"),
     * @SWG\Response(response=200, description=""),
     * )
     */
    public function postChangePhoneSignIn(ChangePhoneSignInRequest $request){
        $data = $request->only(['phone_number','request_id']);

        $verifyChangePhone = $this->merchantRepository->postChangePhoneSignIn($data);
        if ($verifyChangePhone !== null) {
            return $this->respond([
                'status' => true,
                'status_code' => $this->getStatusCode(),
                'message' => 'Your need verify code to update number phone',
                'data' => [
                    'request_id' => $verifyChangePhone
                ]
            ]);
        }
        else{
            return $this->respondNotFound('Can not send verify sms code to your phone number.');
        }
        
    }
}

<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => 'fxauth'], function (Router $router) {

    # Reset password
    $router->get('reset/{id}/{code}', ['as' => 'fxreset.complete', 'uses' => 'Api\V1\ResetController@getResetComplete']);
    $router->get('reset/successful', ['as' => 'reset.successful', 'uses' => 'Api\V1\ResetController@getResetSuccessful']);
    $router->post('reset/{id}/{code}', ['as' => 'fxreset.complete.post', 'uses' => 'Api\V1\ResetController@postResetComplete']);
});

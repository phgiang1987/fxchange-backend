<?php

use Illuminate\Routing\Router;
/** @var Router $router */

/** callback url from netpays **/
$router->post('/v1/customer/callback-data-from-netpay', [
    'as' => 'api.customer.get.callback.data.from.netpay',
    'uses' => 'V1\Customer\CustomerController@getCallbackDataFromNetpay',
]);

$router->group(['prefix'=>'v1','middleware' => 'fxchange.api.key'], function (Router $router) {

    /** @var Router $router */
    $router->group(['prefix' => 'customer'], function (Router $router){
        $router->post('login', [
            'as' => 'api.customer.login',
            'uses' => 'V1\Customer\AuthController@postLogin',
        ]);

        $router->post('sign-up', [
            'as' => 'api.customer.postSignUp',
            'uses' => 'V1\Customer\AuthController@postSignUp',
        ]);
        
        $router->post('verify-otp', [
            'as' => 'api.merchant.verify.otp',
            'uses' => 'V1\Customer\AuthController@postVerifyOPT',
        ]);

        $router->post('change-phone-sign-up', [
            'as' => 'api.customer.postChangePhoneSingUp',
            'uses' => 'V1\Customer\AuthController@postChangePhoneSignUp',
        ]);

        $router->post('verify-sign-up', [
            'as' => 'api.customer.postVerifyCodeSignUp',
            'uses' => 'V1\Customer\AuthController@postVerifyCodeSignUp',
        ]);

        $router->post('post-reset', [
            'as' => 'api.customer.postReset',
            'uses' => 'V1\Customer\AuthController@postReset',
        ]);
        /* List View */
        $router->get('get-listview', [
            'as' => 'api.customer.get.listview',
            'uses' => 'V1\Customer\CustomerController@getListView',
        ]);
        /* Map View */
        $router->get('get-map-view', [
            'as' => 'api.customer.get.mapview',
            'uses' => 'V1\Customer\CustomerController@getMapViewDetail',
        ]);
            
        $router->get('faqs', [
            'as' => 'api.customer.getCustomerFaq',
            'uses' => 'V1\StaticPageController@getCustomerFaq',
        ]);

        $router->get('term-condition', [
            'as' => 'api.customer.getCustomerTermCondition',
            'uses' => 'V1\StaticPageController@getCustomerTermCondition',
        ]);

        /* Router get all curency */
        $router->get('get-curency-dont-login', [
            'as' => 'api.customer.curency.alldontlogin',
            'uses' => 'V1\Customer\CustomerController@getAllCurencyDontLogin',
        ]);

        /* Router get recent curency */
        $router->get('get-recent-currency-dont-login', [
            'as' => 'api.customer.curency.alldontlogin',
            'uses' => 'V1\Customer\CustomerController@getRecentCurencyDontLogin',
        ]);

        /* Router get all contact us */
        $router->get('contact-us', [
            'as' => 'api.customer.get.contactus',
            'uses' => 'V1\Customer\CustomerController@getContactUs',
        ]);

        /* get detail currency */
        $router->get('get-detail-curency', [
            'as' => 'api.customer.get.detail.curency',
            'uses' => 'V1\Customer\CustomerController@getDetailCurency',
        ]);

        /* get curency statistics*/
        $router->get('get-curency-statistics', [
            'as' => 'api.customer.get.curency.statistics',
            'uses' => 'V1\Customer\CustomerController@getCurrencyStatistics',
        ]);
        
        /** @var need authorization */
        $router->group(['middleware' => 'fxchange.api.token'], function (Router $router) {


            /* Router get recent curency */
            $router->get('get-recent-currency', [
                'as' => 'api.customer.curency.all',
                'uses' => 'V1\Customer\CustomerController@getRecentCurency',
            ]);

            /* Router get all curency */
            $router->get('get-curency', [
                'as' => 'api.customer.curency.all',
                'uses' => 'V1\Customer\CustomerController@getAllCurency',
            ]);
            /* Router get remaining */
            $router->get('get-remaining', [
                'as' => 'api.customer.curency.all',
                'uses' => 'V1\Customer\CustomerController@getRemaining',
            ]);

            $router->post('add-address', [
                'as' => 'api.customer.address.add',
                'uses' => 'V1\Customer\CustomerAddressController@add',
            ]);
            $router->post('update-address', [
                'as' => 'api.customer.address.update',
                'uses' => 'V1\Customer\CustomerAddressController@update',
            ]);

            $router->get('delete-address', [
                'as' => 'api.customer.address.delete',
                'uses' => 'V1\Customer\CustomerAddressController@delete',
            ]);

            $router->get('logout', [
                'as' => 'api.customer.getLogout',
                'uses' => 'V1\Customer\AuthController@getLogout',
            ]);
            
            /* Router Add Feedback Contact */
            $router->post('post-feedback', [
                'as' => 'api.customer.post.feedback',
                'uses' => 'V1\Customer\CustomerController@createFeeedback',
            ]);


            /* get detail currency */
            $router->get('delivery-details', [
                'as' => 'api.customer.get.delivery.details',
                'uses' => 'V1\Customer\CustomerController@getDeliveryDetails',
            ]);

            /* get detail currency */
            $router->get('purchases-list', [
                'as' => 'api.customer.get.purchases.list',
                'uses' => 'V1\Customer\CustomerController@getPurchasesList',
            ]);

            /* get detail currency */
            $router->get('purchases-list', [
                'as' => 'api.customer.get.purchases.list',
                'uses' => 'V1\Customer\CustomerController@getPurchasesList',
            ]);

            /* get my account*/
            $router->get('get-my-account', [
                'as' => 'api.customer.get.myaccount',
                'uses' => 'V1\Customer\CustomerController@getUpdateAccount',
            ]);


            /* get list notifications*/
            $router->get('get-list-notifications', [
                'as' => 'api.customer.get.listnotifications',
                'uses' => 'V1\Customer\CustomerController@getListNotifications',
            ]);


            /* get detail currency */
            $router->post('post-my-account', [
                'as' => 'api.customer.post.myaccount',
                'uses' => 'V1\Customer\CustomerController@postUpdateAccount',
            ]);

            $router->post('verify-change-phone', [
                'as' => 'api.customer.post.change.phone',
                'uses' => 'V1\Customer\CustomerController@postVerifyChangePhone',
            ]);

            /* post update password */
            $router->post('post-change-password', [
                'as' => 'api.customer.post.change.password',
                'uses' => 'V1\Customer\CustomerController@postChangePassword',
            ]);

            /* get currency statistics password */
            $router->post('post-curency-alert', [
                'as' => 'api.customer.post.curency.alert',
                'uses' => 'V1\Customer\CustomerController@postCurrencyAlert',
            ]);

            $router->post('resend-code-phone', [
                'as' => 'api.customer.post.resendcodephone',
                'uses' => 'V1\Customer\CustomerController@postResendCode',
            ]);

            $router->post('post-payment', [
                'as' => 'api.customer.post.payment',
                'uses' => 'V1\Customer\CustomerController@postPayment',
            ]);
            $router->post('test-payment', [
                'as' => 'api.customer.post.testpayment',
                'uses' => 'V1\Customer\CustomerController@postTestPayment',
            ]);
            $router->post('check-current-password', [
                'as' => 'api.customer.post.current.password',
                'uses' => 'V1\Customer\CustomerController@postCheckCurrentPassword',
            ]);



            /* Read Notification */
            $router->post('readNotification', [
                'as' => 'api.customer.post.readnotification',
                'uses' => 'V1\Customer\CustomerController@readNotification',
            ]);

            /* unRead Notification */
            $router->get('get-unreadnotification-by-user', [
                'as' => 'api.customer.get.unreadreadnotification',
                'uses' => 'V1\Customer\CustomerController@getUnreadNotification',
            ]);
            /* Detail Transaction */
            $router->get('detail-transaction', [
                'as' => 'api.customer.get.detailtransaction',
                'uses' => 'V1\Customer\CustomerController@getDetailTransaction',
            ]);         
        });

    });

    /** @var Router $router */
    $router->group(['prefix' => 'merchant'], function (Router $router) {
        $router->post('login', [
            'as' => 'api.merchant.login',
            'uses' => 'V1\Merchant\AuthController@postLogin',
        ]);

        $router->post('resend-verify-otp', [
            'as' => 'api.merchant.post.resendverifyotp',
            'uses' => 'V1\Merchant\AuthController@postResendVerifyOtp',
        ]);

        $router->post('change-phone-singin', [
            'as' => 'api.merchant.post.change.signup',
            'uses' => 'V1\Merchant\AuthController@postChangePhoneSignIn',
        ]);

        $router->post('verify-otp', [
            'as' => 'api.merchant.verify.otp',
            'uses' => 'V1\Merchant\AuthController@postVerifyOPT',
        ]);

        $router->post('post-reset', [
            'as' => 'api.merchant.postReset',
            'uses' => 'V1\Merchant\AuthController@postReset',
        ]);
        $router->get('faqs', [
            'as' => 'api.merchant.getMerchantFaq',
            'uses' => 'V1\StaticPageController@getMerchantFaq',
        ]);
        $router->get('term-condition', [
            'as' => 'api.merchant.getMerchantTermCondition',
            'uses' => 'V1\StaticPageController@getMerchantTermCondition',
        ]);
        /* QR scanner from machine*/
        $router->post('machine-check-qr-scanner', [
            'as' => 'api.merchant.post.machineqrscanner',
            'uses' => 'V1\Merchant\MerchantController@postMachineQRScanner',
        ]);

        /* QR scanner from machine*/
        $router->post('machine-confirm-transaction', [
            'as' => 'api.merchant.post.confirm.machinetransaction',
            'uses' => 'V1\Merchant\MerchantController@postMachineConfirmTransaction',
        ]);
        
        /* Router get all contact us */
        $router->get('contact-us', [
            'as' => 'api.merchant.get.contactus',
            'uses' => 'V1\Merchant\MerchantController@getContactUs',
        ]);
        /** @var need authorization */
        $router->group(['middleware' => 'fxchange.api.token'], function (Router $router) {
            $router->get('logout', [
                'as' => 'api.merchant.getLogout',
                'uses' => 'V1\Merchant\AuthController@getLogout',
            ]);
            /* Post feedback merchant */
            $router->post('create-feedback', [
                'as' => 'api.merchant.post.feedback',
                'uses' => 'V1\Merchant\MerchantController@createFeeedback',
            ]);

            /* Get getPastDelivery */
            $router->get('get-past-delivery', [
                'as' => 'api.merchant.past.delivery',
                'uses' => 'V1\Merchant\MerchantController@getPastDelivery'
            ]);

            /* get my account*/
            $router->get('get-my-account', [
                'as' => 'api.merchant.get.myaccount',
                'uses' => 'V1\Merchant\MerchantController@getUpdateAccount',
            ]);

            /* get detail currency */
            $router->post('post-my-account', [
                'as' => 'api.merchant.post.myaccount',
                'uses' => 'V1\Merchant\MerchantController@postUpdateAccount',
            ]);

            $router->post('resend-code-phone', [
                'as' => 'api.merchant.post.resendcodephone',
                'uses' => 'V1\Merchant\MerchantController@postResendCode',
            ]);
            $router->post('verify-change-phone', [
                'as' => 'api.merchant.post.change.phone',
                'uses' => 'V1\Merchant\MerchantController@postVerifyChangePhone',
            ]);
            /* post update password */
            $router->post('post-change-password', [
                'as' => 'api.merchant.post.change.password',
                'uses' => 'V1\Merchant\MerchantController@postChangePassword',
            ]);

            /* get map view*/
            $router->get('map-view-state', [
                'as' => 'api.merchant.get.state',
                'uses' => 'V1\Merchant\MerchantController@getMapViewState',
            ]);

            /* QR scanner*/
            $router->post('check-qr-scanner', [
                'as' => 'api.merchant.post.qrscanner',
                'uses' => 'V1\Merchant\MerchantController@postQRScanner',
            ]);

            /* QR scanner*/
            $router->post('confirm-transaction', [
                'as' => 'api.merchant.post.confirm.transaction',
                'uses' => 'V1\Merchant\MerchantController@postconfirmTransaction',
            ]);

            /* Delivery Order */
            $router->get('delivery-order', [
                'as' => 'api.merchant.post.delivery.order',
                'uses' => 'V1\Merchant\MerchantController@getDeliveryOrder',
            ]);

            /* Delivery Order */
            $router->get('delivery-search', [
                'as' => 'api.merchant.post.delivery.search',
                'uses' => 'V1\Merchant\MerchantController@getDeliverySearch',
            ]);

            $router->post('check-current-password', [
                'as' => 'api.merchant.post.current.password',
                'uses' => 'V1\Merchant\MerchantController@postCheckCurrentPassword',
            ]);
        });
    });
});
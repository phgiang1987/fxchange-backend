<?php

namespace Modules\Fxchange\Http\Middleware;

use Illuminate\Support\Facades\DB;
use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;
use Closure;
use Modules\User\Traits\CanFindUserWithBearerToken;
use Carbon\Carbon;

class ApiLog extends BaseEncrypter
{
    use CanFindUserWithBearerToken;
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    protected $startTime;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->startTime = microtime(true);

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $endTime = microtime(true);
        $duration = $endTime - $this->startTime;
        $user = $this->user = $this->findUserWithBearerToken(request()->header('Authorization'));
        $userId = $user ? $user->id : 0;
        $time = Carbon::now()->format('Y-m-d H:i:s');
        $userToken = $user ? request()->header('Authorization') : null;

        if(env('APP_DEBUG')){
            DB::insert('INSERT INTO `api_logs` (method, request_url, request_header,request_string,response_string,user_id,token,request_ip,device_type,duration,platform,agent_info,created_at) 
            values (?,?,?,?,?,?,?,?,?,?,?,?,?)',
            [$request->getMethod(), $request->path(),json_encode($request->header()),json_encode($request->all()),$response->getContent(),$userId,$userToken,$request->getClientIp(),'MOBILE','','','',$time]);
        }
    }
}

<?php

namespace Modules\Fxchange\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Repositories\UserTokenRepository;

class FxChangeAuthorisedApiToken
{
    /**
     * @var UserTokenRepository
     */
    private $userToken;

    private $apiKey;

    public function __construct(UserTokenRepository $userToken)
    {
        $this->userToken = $userToken;
    }

    public function handle(Request $request, \Closure $next)
    {
        if ($request->header('Authorization') === null) {
            // If the request wants JSON (AJAX doesn't always want JSON)
            if ($request->wantsJson()) {
                return response()->json([
                    'status'=> false,
                    "status_code"=> 403,
                    "message"=> 'Unauthorized',
                    "data"=>[]
                ], 200);
            }
            else{
                return new Response('Forbidden', 403);
            }
        }

        if ($this->isValidToken($request->header('Authorization')) === false) {

            // If the request wants JSON (AJAX doesn't always want JSON)
            if ($request->wantsJson()) {
                return response()->json([
                    'status'=> false,
                    "status_code"=> 403,
                    "message"=> 'Unauthorized',
                    "data"=>[]
                ], 200);
            }
            else{
                return new Response('Forbidden', 403);
            }
        }

        return $next($request);
    }

    private function isValidToken($token)
    {
        $found = $this->userToken->findByAttributes(['access_token' => $this->parseToken($token)]);

        if ($found === null) {
            return false;
        }

        return true;
    }

    private function parseToken($token)
    {
        return str_replace('Bearer ', '', $token);
    }
}

<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/fxchange'], function (Router $router) {
    $router->bind('fxchange', function ($id) {
        return app('Modules\Fxchange\Repositories\FxchangeRepository')->find($id);
    });


    $router->get('check-env', [
        'as' => 'admin.fxchange.fxchange.checkenv',
        'uses' => 'FxchangeController@checkenv',
        'middleware' => 'can:currency.currencys.index'
    ]);


    $router->get('test-geolocation', [
        'as' => 'admin.fxchange.fxchange.testgeolocation',
        'uses' => 'FxchangeController@getLocation',
        'middleware' => 'can:currency.currencys.index'
    ]);


    $router->get('fxchanges', [
        'as' => 'admin.fxchange.fxchange.index',
        'uses' => 'FxchangeController@index',
        'middleware' => 'can:fxchange.fxchanges.index'
    ]);
    $router->get('fxchanges/create', [
        'as' => 'admin.fxchange.fxchange.create',
        'uses' => 'FxchangeController@create',
        'middleware' => 'can:fxchange.fxchanges.create'
    ]);
    $router->post('fxchanges', [
        'as' => 'admin.fxchange.fxchange.store',
        'uses' => 'FxchangeController@store',
        'middleware' => 'can:fxchange.fxchanges.create'
    ]);
    $router->get('fxchanges/{fxchange}/edit', [
        'as' => 'admin.fxchange.fxchange.edit',
        'uses' => 'FxchangeController@edit',
        'middleware' => 'can:fxchange.fxchanges.edit'
    ]);
    $router->put('fxchanges/{fxchange}', [
        'as' => 'admin.fxchange.fxchange.update',
        'uses' => 'FxchangeController@update',
        'middleware' => 'can:fxchange.fxchanges.edit'
    ]);
    $router->delete('fxchanges/{fxchange}', [
        'as' => 'admin.fxchange.fxchange.destroy',
        'uses' => 'FxchangeController@destroy',
        'middleware' => 'can:fxchange.fxchanges.destroy'
    ]);
// append



    //Currency
    $router->get('currency', [
        'as' => 'admin.currency.currency.indexcurrency',
        'uses' => 'FxchangeController@indexCurrency',
        'middleware' => 'can:currency.currencys.index'
    ]);
    $router->post('currency-import', [
        'as' => 'admin.currency.currency.importcurrency',
        'uses' => 'FxchangeController@importCurrency',
        'middleware' => 'can:currency.currencys.index'
    ]);     
    $router->get('currency-datatable', [
        'as' => 'admin.currency.currency.indexcurrencydatatable',
        'uses' => 'FxchangeController@indexCurrencyDatatable',
        'middleware' => 'can:currency.currencys.index'
    ]);   
    $router->get('currency/create', [
        'as' => 'admin.currency.currency.create',
        'uses' => 'FxchangeController@createCurrency',
        'middleware' => 'can:currency.currencys.create'
    ]);
    $router->post('currency', [
        'as' => 'admin.currency.currency.store',
        'uses' => 'FxchangeController@storeCurrency',
        'middleware' => 'can:currency.currencys.create'
    ]); 
    $router->get('currency/{id}/edit', [
        'as' => 'admin.currency.currency.edit',
        'uses' => 'FxchangeController@editCurrency',
        'middleware' => 'can:currency.currencys.edit'
    ]);
    $router->get('currency/{name}/view-rate', [
        'as' => 'admin.currency.currency.viewrate',
        'uses' => 'FxchangeController@viewRate',
        'middleware' => 'can:currency.currencys.edit'
    ]);
    $router->put('currency/{id}', [
        'as' => 'admin.currency.currency.update',
        'uses' => 'FxchangeController@updateCurrency',
        'middleware' => 'can:currency.currencys.edit'
    ]);
    $router->delete('currency/{id}', [
        'as' => 'admin.currency.currency.delete',
        'uses' => 'FxchangeController@deleteCurrency',
        'middleware' => 'can:currency.currencys.destroy'
    ]);
    $router->get('currency-bulk-delete', [
        'as' => 'admin.currency.currency.bulkdelete',
        'uses' => 'FxchangeController@BulkDeleteCurrency',
        'middleware' => 'can:currency.currencys.destroy'
    ]);



 //Machine
    $router->get('machine', [
        'as' => 'admin.machine.machine.indexmachine',
        'uses' => 'MachineController@indexMachine',
        'middleware' => 'can:machine.machine.index'
    ]);
    $router->post('machine-import', [
        'as' => 'admin.machine.machine.importmachine',
        'uses' => 'MachineController@importMachine',
        'middleware' => 'can:machine.machine.index'
    ]);
    $router->get('machine-export-data-fake', [
        'as' => 'admin.machine.machine.exportdatafake',
        'uses' => 'MachineController@exportDataFake',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('machine-datatable', [
        'as' => 'admin.machine.machine.indexmachinedatatable',
        'uses' => 'MachineController@indexMachineDatatable',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('machine/create', [
        'as' => 'admin.machine.machine.create',
        'uses' => 'MachineController@createMachine',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('machine', [
        'as' => 'admin.machine.machine.store',
        'uses' => 'MachineController@storeMachine',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('machine/{id}/edit', [
        'as' => 'admin.machine.machine.edit',
        'uses' => 'MachineController@editMachine',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('machine/{id}', [
        'as' => 'admin.machine.machine.update',
        'uses' => 'MachineController@updateMachine',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('machine/{id}', [
        'as' => 'admin.machine.machine.delete',
        'uses' => 'MachineController@deleteMachine',
        'middleware' => 'can:machine.machine.destroy'
    ]);
    $router->get('machine-bulk-delete', [
        'as' => 'admin.machine.machine.bulkdelete',
        'uses' => 'MachineController@BulkDeleteMachine',
        'middleware' => 'can:machine.machine.destroy'
    ]);


    //Transaction

    $router->get('merchant-transaction', [
        'as' => 'admin.transaction.transaction.index',
        'uses' => 'TransactionController@indexMerchant',
        'middleware' => 'can:transaction.transaction.index',
    ]);
    $router->get('customers-transaction', [
        'as' => 'admin.transaction.transaction.indexcustomer',
        'uses' => 'TransactionController@indexCustomer',
        'middleware' => 'can:transaction.transaction.index',
    ]);

    //datatable server side

    //merchant
    $router->get('merchant-table-transaction', [
        'as' => 'admin.transaction.transaction.indextablemerchant',
        'uses' => 'TransactionController@indextableMerchant',
        'middleware' => 'can:transaction.transaction.index',
    ]);
    //customer
    $router->get('customer-table-transaction', [
        'as' => 'admin.transaction.transaction.indextablecustomer',
        'uses' => 'TransactionController@indexTableCustomer',
        'middleware' => 'can:transaction.transaction.index',
    ]);

    //Event group Merchant Transaction
    $router->get('merchant-transaction/{id}/edit', [
        'as' => 'admin.transaction.transaction.edittransactionmerchant',
        'uses' => 'TransactionController@editTransactionMerchant',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('merchant-transaction/{id}/edit', [
        'as' => 'admin.transaction.transaction.updatetransactionmerchant',
        'uses' => 'TransactionController@updateTransactionMerchant',
        'middleware' => 'can:transaction.transaction.edit'
    ]);

    $router->delete('transaction-merchant-delete/{id}', [
        'as' => 'admin.transaction.transaction.deletetransactionmerchant',
        'uses' => 'TransactionController@deleteTransactionMerchant',
        'middleware' => 'can:transaction.transaction.destroy'
    ]);

    //Event group Customer Transaction
    $router->get('customer-transaction/{id}/edit', [
        'as' => 'admin.transaction.transaction.edittransactioncustomer',
        'uses' => 'TransactionController@editTransactionCustomer',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('customer-transaction/{id}/edit', [
        'as' => 'admin.transaction.transaction.updatetransactioncustomer',
        'uses' => 'TransactionController@updateTransactionCustomer',
        'middleware' => 'can:transaction.transaction.edit'
    ]);

    $router->delete('transaction-customer-delete/{id}', [
        'as' => 'admin.transaction.transaction.deletetransactioncustomer',
        'uses' => 'TransactionController@deleteTransactionCustomer',
        'middleware' => 'can:transaction.transaction.destroy'
    ]);



    $router->get('transaction-ajax-bulk-delete', [
        'as' => 'admin.transaction.transaction.deleteajaxtransaction',
        'uses' => 'TransactionController@BulkDeleteTransaction',
        'middleware' => 'can:transaction.transaction.destroy',
    ]);




 //Settings
    //amount
    $router->get('edit-amount', [
        'as' => 'admin.amount.amount.editamount',
        'uses' => 'SettingsController@editAmount',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('edit-amount', [
        'as' => 'admin.amount.amount.updateamount',
        'uses' => 'SettingsController@updateAmount',
        'middleware' => 'can:transaction.transaction.edit'
    ]);

    //normal
    $router->get('edit-normal', [
        'as' => 'admin.normal.normal.editnormalfee',
        'uses' => 'SettingsController@editNormal',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('edit-normal', [
        'as' => 'admin.normal.normal.updatenormalfee',
        'uses' => 'SettingsController@updateNormal',
        'middleware' => 'can:transaction.transaction.edit'
    ]);    

    //express
    $router->get('edit-express', [
        'as' => 'admin.express.express.editexpressfee',
        'uses' => 'SettingsController@editExpress',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('edit-express', [
        'as' => 'admin.express.express.updateexpressfee',
        'uses' => 'SettingsController@updateExpress',
        'middleware' => 'can:transaction.transaction.edit'
    ]);

    //Google analytics
    $router->get('settings-google-analytics', [
        'as' => 'admin.setting.google.analytics',
        'uses' => 'SettingsController@editGoogleAnalytics',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('settings-google-analytics', [
        'as' => 'admin.setting.google.analyticsupdate',
        'uses' => 'SettingsController@updateGoogleAnalytics',
        'middleware' => 'can:transaction.transaction.edit'
    ]);  

    //Self-pickup
    $router->get('edit-self-pickup', [
        'as' => 'admin.setting.selfpickup',
        'uses' => 'SettingsController@editSelfPickup',
        'middleware' => 'can:transaction.transaction.edit'
    ]);
    $router->put('edit-self-pickup', [
        'as' => 'admin.setting.google.editselfpickupupdate',
        'uses' => 'SettingsController@updateSelfpickup',
        'middleware' => 'can:transaction.transaction.edit'
    ]);

//FAQ


    //Merchant
    $router->get('faq-merchant', [
        'as' => 'admin.faq.faq.indexfaq',
        'uses' => 'StaticPageController@indexFaq',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('faq-datatable-merchant', [
        'as' => 'admin.faq.faq.indexfaqdatatable',
        'uses' => 'StaticPageController@indexFaqDatatable',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('faq-merchant/create', [
        'as' => 'admin.faq.faq.create',
        'uses' => 'StaticPageController@createFaq',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('faq-merchant', [
        'as' => 'admin.faq.faq.store',
        'uses' => 'StaticPageController@storeFaq',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('faq-merchant/{id}/edit', [
        'as' => 'admin.faq.faq.edit',
        'uses' => 'StaticPageController@editFaq',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('faq-merchant/{id}', [
        'as' => 'admin.faq.faq.update',
        'uses' => 'StaticPageController@updateFaq',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('faq-merchant/{id}', [
        'as' => 'admin.faq.faq.delete',
        'uses' => 'StaticPageController@deleteFaq',
        'middleware' => 'can:machine.machine.destroy'
    ]);
    //Sort FAQ
    $router->get('faq-merchant-sort', [
        'as' => 'admin.faq.faq.indexfaqmerchantsort',
        'uses' => 'StaticPageController@indexFaqMerchantSort',
        'middleware' => 'can:machine.machine.index'
    ]); 
    $router->get('faq-merchant-sort-ajax-update', [
        'as' => 'admin.faq.faq.indexfaqmerchantsortajaxupdate',
        'uses' => 'StaticPageController@indexFaqMerchantSortUpdateAjax',
        'middleware' => 'can:machine.machine.index'
    ]); 


    $router->get('faq-merchant-bulk-delete', [
        'as' => 'admin.faq.faq.bulkdelete',
        'uses' => 'StaticPageController@BulkDeleteFaq',
        'middleware' => 'can:machine.machine.destroy'
    ]);


    //Customer
    $router->get('faq-customer', [
        'as' => 'admin.faq.faq.indexfaqcustomer',
        'uses' => 'StaticPageController@indexFaqCustomer',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('faq-customer-datatable', [
        'as' => 'admin.faq.faq.indexfaqdatatablecustomer',
        'uses' => 'StaticPageController@indexFaqDatatableCustomer',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('faq-customer/create', [
        'as' => 'admin.faq.faq.createcustomer',
        'uses' => 'StaticPageController@createFaqCustomer',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('faq-customer', [
        'as' => 'admin.faq.faq.storecustomer',
        'uses' => 'StaticPageController@storeFaqCustomer',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('faq-customer/{id}/edit', [
        'as' => 'admin.faq.faq.editcustomer',
        'uses' => 'StaticPageController@editFaqCustomer',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('faq-customer/{id}', [
        'as' => 'admin.faq.faq.updatecustomer',
        'uses' => 'StaticPageController@updateFaqCustomer',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('faq-customer/{id}', [
        'as' => 'admin.faq.faq.deletecustomer',
        'uses' => 'StaticPageController@deleteFaqCustomer',
        'middleware' => 'can:machine.machine.destroy'
    ]);

    //Sort FAQ
    $router->get('faq-customer-sort', [
        'as' => 'admin.faq.faq.indexfaqcustomersort',
        'uses' => 'StaticPageController@indexFaqCustomerSort',
        'middleware' => 'can:machine.machine.index'
    ]); 
    $router->get('faq-customer-sort-ajax-update', [
        'as' => 'admin.faq.faq.indexfaqcustomersortajaxupdate',
        'uses' => 'StaticPageController@indexFaqCustomerSortUpdateAjax',
        'middleware' => 'can:machine.machine.index'
    ]); 




//Terms


    //Merchant
    $router->get('terms-merchant', [
        'as' => 'admin.terms.terms.indextermmerchant',
        'uses' => 'TermsController@indexTermMerchant',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('terms-datatable-merchant', [
        'as' => 'admin.terms.terms.indexdatatalbemerchant',
        'uses' => 'TermsController@indexDatatableMerchant',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('terms-merchant/create', [
        'as' => 'admin.terms.terms.createtermmerchant',
        'uses' => 'TermsController@createTermsMerchant',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('terms-merchant', [
        'as' => 'admin.terms.terms.storetermmerchant',
        'uses' => 'TermsController@storeTermsMerchant',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('terms-merchant/{id}/edit', [
        'as' => 'admin.terms.terms.edittermmerchant',
        'uses' => 'TermsController@editTermMerchant',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('terms-merchant/{id}', [
        'as' => 'admin.terms.terms.updatetermmerchant',
        'uses' => 'TermsController@updateTermsMerchant',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('terms-merchant/{id}', [
        'as' => 'admin.terms.terms.deletetermmerchant',
        'uses' => 'TermsController@deleteTermsMerchant',
        'middleware' => 'can:machine.machine.destroy'
    ]);



    $router->get('terms-bulk-delete', [
        'as' => 'admin.terms.terms.bulkdeleteterm',
        'uses' => 'TermsController@BulkDeleteTermsMerchant',
        'middleware' => 'can:machine.machine.destroy'
    ]);


    //Customer
    $router->get('terms-customer', [
        'as' => 'admin.terms.terms.indextermscustomer',
        'uses' => 'TermsController@indexTermsCustomer',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('terms-customer-datatable', [
        'as' => 'admin.terms.terms.indextermsdatatablecustomer',
        'uses' => 'TermsController@indexTermsDatatableCustomer',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('terms-customer/create', [
        'as' => 'admin.terms.terms.createtermscustomer',
        'uses' => 'TermsController@createTermsCustomer',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('terms-customer', [
        'as' => 'admin.terms.terms.storetermscustomer',
        'uses' => 'TermsController@storeTermsCustomer',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('terms-customer/{id}/edit', [
        'as' => 'admin.terms.terms.edittermscustomer',
        'uses' => 'TermsController@editTermsCustomer',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('terms-customer/{id}', [
        'as' => 'admin.terms.terms.updatecustomer',
        'uses' => 'TermsController@updateTermsCustomer',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('terms-customer/{id}', [
        'as' => 'admin.terms.terms.deletetermscustomer',
        'uses' => 'TermsController@deleteTermsCustomer',
        'middleware' => 'can:machine.machine.destroy'
    ]);



//Contact


    //Merchant
    $router->get('contact-merchant', [
        'as' => 'admin.contact.contact.indexmerchantcontact',
        'uses' => 'ContactController@indexMerchantContact',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('contact-datatable-merchant', [
        'as' => 'admin.contact.contact.indexdatatalbecontactmerchant',
        'uses' => 'ContactController@indexDatatableMerchantContact',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('contact-merchant/create', [
        'as' => 'admin.contact.contact.createmerchantcontact',
        'uses' => 'ContactController@createMerchantContact',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('contact-merchant', [
        'as' => 'admin.contact.contact.storemerchantcontact',
        'uses' => 'ContactController@storeMerchantContact',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('contact-merchant/{id}/edit', [
        'as' => 'admin.contact.contact.editmerchantcontact',
        'uses' => 'ContactController@editMerchantContact',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('contact-merchant/{id}', [
        'as' => 'admin.contact.contact.updatemerchantcontact',
        'uses' => 'ContactController@updateMerchantContact',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('contact-merchant/{id}', [
        'as' => 'admin.contact.contact.deletemerchantcontact',
        'uses' => 'ContactController@deleteMerchantContact',
        'middleware' => 'can:machine.machine.destroy'
    ]);



    $router->get('contact-bulk-delete', [
        'as' => 'admin.contact.contact.bulkdeletecontact',
        'uses' => 'ContactController@BulkDeleteContact',
        'middleware' => 'can:machine.machine.destroy'
    ]);


    //Customer
    $router->get('contact-customer', [
        'as' => 'admin.contact.contact.indexcustomercontact',
        'uses' => 'ContactController@indexCustomerContact',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('contact-customer-datatable', [
        'as' => 'admin.contact.contact.indexdatatalbecontactcustomer',
        'uses' => 'ContactController@indexDatatableCustomerContact',
        'middleware' => 'can:machine.machine.index'
    ]);   
    $router->get('contact-customer/create', [
        'as' => 'admin.contact.contact.createcustomercontact',
        'uses' => 'ContactController@createCustomerContact',
        'middleware' => 'can:machine.machine.create'
    ]);
    $router->post('contact-customer', [
        'as' => 'admin.contact.contact.storecustomercontact',
        'uses' => 'ContactController@storeCustomerContact',
        'middleware' => 'can:machine.machine.create'
    ]); 
    $router->get('contact-customer/{id}/edit', [
        'as' => 'admin.contact.contact.editcustomercontact',
        'uses' => 'ContactController@editCustomerContact',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->put('contact-customer/{id}', [
        'as' => 'admin.contact.contact.updatecustomercontact',
        'uses' => 'ContactController@updateCustomerContact',
        'middleware' => 'can:machine.machine.edit'
    ]);
    $router->delete('contact-customer/{id}', [
        'as' => 'admin.contact.contact.deletcustomercontact',
        'uses' => 'ContactController@deleteCustomerContact',
        'middleware' => 'can:machine.machine.destroy'
    ]);



//Feedback
    //Merchant
    $router->get('feedback-merchant', [
        'as' => 'admin.feedback.feedback.indexmerchantfeedback',
        'uses' => 'FeedbackController@indexMerchantFeedback',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('feedback-datatable-merchant', [
        'as' => 'admin.feedback.feedback.indexdatatalbefeedbackmerchant',
        'uses' => 'FeedbackController@indexDatatableMerchantFeedback',
        'middleware' => 'can:machine.machine.index'
    ]);
    $router->delete('feedback-merchant/{id}', [
        'as' => 'admin.feedback.feedback.deletmerchantfeedback',
        'uses' => 'FeedbackController@deleteMerchantFeedback',
        'middleware' => 'can:machine.machine.destroy'
    ]);




    //Customer
    $router->get('feedback-customer', [
        'as' => 'admin.feedback.feedback.indexcustomerfeedback',
        'uses' => 'FeedbackController@indexCustomerFeedback',
        'middleware' => 'can:machine.machine.index'
    ]);    
    $router->get('feedback-datatable-customer', [
        'as' => 'admin.feedback.feedback.indexdatatalbefeedbackcustomer',
        'uses' => 'FeedbackController@indexDatatableCustomerFeedback',
        'middleware' => 'can:machine.machine.index'
    ]);
    $router->delete('feedback-customer/{id}', [
        'as' => 'admin.feedback.feedback.deletcustomerfeedback',
        'uses' => 'FeedbackController@deleteCustomerFeedback',
        'middleware' => 'can:machine.machine.destroy'
    ]);




    $router->get('feedback-bulk-delete', [
        'as' => 'admin.feedback.feedback.bulkdelete',
        'uses' => 'FeedbackController@BulkDeleteFeedback',
        'middleware' => 'can:machine.machine.destroy'
    ]);
});



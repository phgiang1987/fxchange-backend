<?php

return [
    'fxchange.fxchanges' => [
        'index' => 'fxchange::fxchanges.list resource',
        'create' => 'fxchange::fxchanges.create resource',
        'edit' => 'fxchange::fxchanges.edit resource',
        'destroy' => 'fxchange::fxchanges.destroy resource',
    ],
// append
    'currency.currencys' => [
        'index' => 'List',
        'create' => 'Create',
        'edit' => 'Edit',
        'destroy' => 'Destroy',
    ],
    'machine.machine' => [
        'index' => 'List',
        'create' => 'Create',
        'edit' => 'Edit',
        'destroy' => 'Destroy',
    ],
    'transaction.transaction' => [
        'index' => 'List',
        // 'create' => 'Create',
        'edit' => 'Edit',
        'destroy' => 'Destroy',
    ],
];

<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Merchant;

use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Merchant\Merchant;

class MerchantTransformer extends TransformerAbstract{
    public function transform(Merchant $user){
        return [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'country_code' => $user->country_code,
            'phone_number' => $user->phone_number,
            'email'=>$user->email,
            'timezone'=>$user->timezone,
            'access_token' => $user->getFirstApiKey()
        ];
    }
}
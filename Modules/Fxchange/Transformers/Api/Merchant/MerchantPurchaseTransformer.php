<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Merchant;

use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Transaction;
use Carbon\Carbon;
use Modules\Fxchange\Entities\Currency;
class MerchantPurchaseTransformer extends TransformerAbstract{
    public function transform(Transaction $item){
        $today = Carbon::today()->format('Y-m-d');
        $day_is_db = Carbon::parse($item->collection_expired_date)->format('Y-m-d');
        $currency = currency::where('name',$item->currency)->first();
        if($currency){
            $symbol = $currency->symbol;
            $sign = $currency->sign;
        }
        $type = "";
        if($item->delivery_type == "normal"){
            $type = 3;
        }elseif($item->delivery_type == "express"){
            $type = 2;
        }
        if($today > $day_is_db){
            $type = 1;
        }
        return [
            'id'=> $item->id,
            'first_name' => $item->getUser ? ucfirst($item->getUser->first_name) : null,
            'last_name' => $item->getUser ? ucfirst($item->getUser->last_name) : null,
            'currency'=> $item->currency,
            'postcode'=> $item->postcode,
            'symbol'    => $symbol?$symbol:"",
            'symbol_code'    => $sign?$sign:"",
            'quantity'=> $item->quantity,
            'sgd_rate'=> $item->sgd_rate,
            'exchange_rate'=> $item->exchange_rate,
            'amount'=> $item->amount,
            'delivery_fee'=> $item->delivery_fee,
            'total_price'=> $item->total_price,
            'purchase_type'=> ucfirst($item->purchase_type),
            'delivery_type'=> ($today > $day_is_db) ? 'Overdue' : ucfirst($item->delivery_type),
            'purchase_date'=> Carbon::parse($item->purchase_date)->timestamp,
            'collection_expired_date'=> Carbon::parse($item->collection_expired_date)->timestamp,
            'qr_code'=> $item->qr_code,
            'qr_code_datetime'=> $item->qr_code_datetime,
            'delivery_name'=> $item->delivery_name,
            'contact_number'=> $item->contact_number,
            'address'=> $item->address,
            'postal_code'=> $item->postal_code,
            'block'=> $item->block,
            'unit'=> $item->unit,
            'longitude'=> $item->longitude,
            'latitude'=> $item->latitude,
            'status'=> $item->status == 'completed' ? 'Delivered': ucfirst($item->status),
            'completed_delivery_date'=> Carbon::parse($item->updated_at)->timestamp,
            'type' => $type,
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Merchant;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Contact;

class MerchantContactTransformer extends TransformerAbstract{
	
    public function transform(Contact $item){
        return [
            'id'=> $item->id,
            'email'=> (string) $item->email,
            'phone'=> (string) $item->phone,
            'country_code'=> (string) $item->country_code
        ];
    }
}
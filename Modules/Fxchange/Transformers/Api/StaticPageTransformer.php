<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\StaticPage;

class StaticPageTransformer extends TransformerAbstract{
    public function transform(StaticPage $item){
        return [
            'id'=> $item->id,
            'title'=> $item->title,
            'content'=> $item->content,
            'sort'=> $item->sort
        ];
    }
}
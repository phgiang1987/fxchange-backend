<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Machines;

class CustomerMachinesTransformer extends TransformerAbstract{
    public function transform(Machines $item){
        $media = $item->filesByZone('image')->get()->first();        
        $photo = '';        
        if($media){            
            $photo = $media->path_string;        
        }
        return [
            'id'=> $item->id,
            'title'=> $item->title,
            'postcode'=> $item->postcode,
            'block'=> $item->block,
            'unit'=> $item->unit,
            'longitude'=> $item->longitude,
            'latitude'=> $item->latitude,
            'photo' => $photo,
            'zone' => $item->zone,
            'description' => $item->description,
            'address' => $item->address,
            'distance' => $item->distance?round($item->distance):"",
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Transaction;
use Carbon\Carbon;
use Modules\Fxchange\Entities\Currency;
class CustomerPurchaseTransformer extends TransformerAbstract{
    public function transform(Transaction $item){
        $currency = currency::where('name',$item->currency)->first();
        if($currency){
            $symbol = $currency->symbol;
            $sign = $currency->sign;
        }
        if($item->purchase_type == "self-pickup"){
            $purchase_type = "To Collect";
        }else{
            $purchase_type = ucfirst($item->purchase_type);
        }
        $now = Carbon::now()->timestamp;
        $expire = Carbon::parse($item->collection_expired_date)->timestamp;
        if($item->status == "paid" && $now > $expire){
            $status = "Expired";
        }else{
            $status = ucfirst($item->status);
        }
        if($item->collected_date == null){
            $collected_date = null;
        }else{
            $collected_date = Carbon::parse($item->collected_date)->timestamp;
        }
        return [
            'id'=> $item->id,
            'currency'=> $item->currency,
            'postcode'=> $item->postcode,
            'symbol'    => $symbol?$symbol:"",
            'symbol_code'    => $sign?$sign:"",
            'quantity'=> $item->quantity,
            'sgd_rate'=> $item->sgd_rate,
            'exchange_rate'=> $item->exchange_rate,
            'amount'=> $item->amount,
            'delivery_fee'=> $item->delivery_fee,
            'total_price'=> $item->total_price,
            'purchase_type'=> $purchase_type,
            'delivery_type'=> ucfirst($item->delivery_type),
            'purchase_date'=> Carbon::parse($item->purchase_date)->timestamp,
            'collection_expired_date'=> Carbon::parse($item->collection_expired_date)->timestamp,
            'qr_code'=> $item->qr_code,
            'qr_code_datetime'=> Carbon::parse($item->qr_code_datetime)->timestamp,
            'collected_date'=> $collected_date,
            'delivery_name'=> $item->delivery_name,
            'contact_number'=> $item->getContactNumber(),
            'address'=> $item->address,
            'postal_code'=> $item->postal_code,
            'block'=> $item->block,
            'unit'=> $item->unit,
            'assign_by' => $item->assign_by,
            'longitude'=> $item->longitude,
            'latitude'=> $item->latitude,
            'status'=> $status,
            'unread' => $item->unread,
            'created_at'=> Carbon::parse($item->created_at)->timestamp,
            'updated_at'=> Carbon::parse($item->updated_at)->timestamp,
        ];
    }
}
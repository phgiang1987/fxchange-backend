<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;

class CustomerTransformer extends TransformerAbstract{
    protected $availableIncludes = [
        'addresses'
    ];
    public function transform(Customer $user){
        return [
            'user_id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'phone_number' => $user->phone_number,
            'country_code' => $user->country_code,
            'email'=>$user->email,
            'timezone'=>$user->timezone,
            'access_token' => $user->getFirstApiKey()
        ];
    }

    public function includeAddresses(Customer $customer)
    {
        $addresses = $customer->addresses;
        return $this->collection($addresses, \App::make(CustomerAddressTransformer::class));
    }
}
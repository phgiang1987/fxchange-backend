<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\CurrencyHistory;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Machines;
use Carbon\Carbon;

class CustomerCurrencyStatisticsTransformer extends TransformerAbstract{
    public function transform(CurrencyHistory $item){
        return [
            'id'=> $item->id,
            'name'=> $item->name,
            'sgd_rate'=> $item->sgd_rate,
            'exchange_rate' => $item->exchange_rate,
            'created_at'=> Carbon::parse($item->created_at)->timestamp, //->timezone('Asia/Singapore')
        ];
    }
}
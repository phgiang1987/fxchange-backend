<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Customer\CustomerAddress;
use Modules\Fxchange\Entities\UserAddress;

class CustomerAddressTransformer extends TransformerAbstract{
    public function transform(CustomerAddress $item){
        return [
            'id'=>$item->id,
            'title'=>$item->title,
            'postal_code'=>$item->postal_code,
            'address'=>$item->address,
            'block'=>$item->block,
            'unit'=>$item->unit,
            'longitude'=>$item->longitude,
            'latitude'=>$item->latitude,
            'sort'=>$item->sort,
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\Customer\Customer;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\FxchangeSettings;

class CustomerCurencyTransformer extends TransformerAbstract{
    public function transform(Currency $item){
        $media = $item->filesByZone('image')->get()->first();        
        $photo = '';        
        if($media){            
            $photo = $media->path_string;        
        }
        return [
            'id'=> $item->id,
            'name'=> $item->name,
            'symbol'=> $item->symbol,
            'symbol_code' => $item->sign,
            'default_unit'=> $item->default_unit,
            'self_pickup_only'=> $item->self_pickup_only,
            'sgd_rate'=> $item->sgd_rate,
            'limit'=> $item->limit,
            'exchange_rate'=> $item->exchange_rate,
            'photo' => $photo,

            'minimum' => FxchangeSettings::showValue('core::minimum-amount'),
            'maximum' => FxchangeSettings::showValue('core::maximum-amount'),
        ];
    }
}
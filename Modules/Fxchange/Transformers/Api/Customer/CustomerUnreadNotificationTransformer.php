<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\FxchangeNotificationList;
use League\Fractal\TransformerAbstract;

class CustomerUnreadNotificationTransformer extends TransformerAbstract{
    public function transform(FxchangeNotificationList $list){
        return [
            'id' => $list->id,
            'transaction_id' => $list->transaction_id,
            'user_id' => $list->user_id,
            'is_read' => $list->is_read,
            'symbol'  => $list->symbol,
            'push_type' => $list->push_type,
        ];
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: eric
 * Date: 7/6/17
 * Time: 15:33
 */

namespace Modules\Fxchange\Transformers\Api\Customer;


use Modules\Fxchange\Entities\CurrencyHistory;
use League\Fractal\TransformerAbstract;
use Modules\Fxchange\Entities\FxchangeNotificationList;
use Carbon\Carbon;

class CustomerNotificationsTransformer extends TransformerAbstract{
    public function transform(FxchangeNotificationList $item){
        return [
            'id'=> $item->id,
            'title'=> $item->title,
            'transaction_id'=> $item->transaction_id,
            'purchase_type'=> $item->purchase_type,
            'is_read'=> $item->is_read,
            'created_at'=> Carbon::parse($item->created_at)->timestamp, //->timezone('Asia/Singapore')
        ];
    }
}
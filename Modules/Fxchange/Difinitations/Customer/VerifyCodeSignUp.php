<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerVerifyCodeSignUp"))
 */
class CustomerVerifyCodeSignUp
{
    /**
     * @SWG\Property(example="123456abcdef")
     * @var string
     * @required true
     */
    public $request_id;

    /**
     * @SWG\Property(example="1234")
     * @var string
     * @required true
     */
    public $code;
}
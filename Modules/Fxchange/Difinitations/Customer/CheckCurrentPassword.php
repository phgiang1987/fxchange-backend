<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CheckCurrentPassword"))
 */
class CheckCurrentPassword
{
    /**
     * @SWG\Property(example="123@abc")
     * @var string
     */
    public $current_password;
}
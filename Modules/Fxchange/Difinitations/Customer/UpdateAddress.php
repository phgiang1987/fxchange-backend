<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerUpdateAddress"))
 */
class CustomerUpdateAddress
{
    /**
     * @SWG\Property(example=13)
     * @var integer
     */
    public $id;
    /**
     * @SWG\Property(example="Office")
     * @var string
     */
    public $title;
    /**
     * @SWG\Property(example="408600")
     * @var integer
     */
    public $postal_code;

    /**
     * @SWG\Property(example="10 Eunos Road")
     * @var string
     */
    public $address;

    /**
     * @SWG\Property(example="B2")
     * @var string
     */
    public $block;

    /**
     * @SWG\Property(example="12")
     * @var string
     */
    public $unit;

    /**
     * @SWG\Property(example="1.318815")
     * @var string
     */
    public $longitude;

    /**
     * @SWG\Property(example="103.8924707")
     * @var string
     */
    public $latitude;
}
<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerLogin"))
 */
class CustomerLogin
{
    /**
     * @SWG\Property(example="customer1@fxchange.sg")
     * @var string
     */
    public $email;
    /**
     * @SWG\Property(example="123@abc")
     * @var string
     */
    public $password;

    /**
     * @SWG\Property(example="ANDROID")
     * @var string
     */
    public $device_type;

    /**
     * @SWG\Property(example="ajdbhakdqlw3e12312e8912edqwdjhfbewbfwebf123eh12iudwkqdkqwjdbkadb")
     * @var string
     */
    public $firebase_token;
}
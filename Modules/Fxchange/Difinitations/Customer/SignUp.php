<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerLogin"))
 */
class CustomerSignUp
{
    /**
     * @SWG\Property(example="+84986981718")
     * @var string
     * @required true
     */
    public $phone_number;

    /**
     * @SWG\Property(example="Tom")
     * @var string
     * @required true
     */
    public $first_name;
    /**
     * @SWG\Property(example="Tom")
     * @var string
     * @required true
     */
    public $last_name;
    
    /**
     * @SWG\Property(example="customer1@fxchange.sg")
     * @var string
     * @required true
     */
    public $email;
    /**
     * @SWG\Property(example="123@Aabc")
     * @var string
     */
    public $password;

    /**
     * @SWG\Property(example="123@Aabc")
     * @var string
     */
    public $password_confirmation;

    /**
     * @SWG\Property(example="ajdbhakdqlw3e12312e8912edqwdjhfbewbfwebf123eh12iudwkqdkqwjdbkadb")
     * @var string
     */
    public $firebase_token;

    /**
     * @SWG\Property(example="ANDROID")
     * @var string
     */
    public $device_type;
}
<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerReset"))
 */
class CustomerReset
{
    /**
     * @SWG\Property(example="customer1@fxchange.sg")
     * @var string
     */
    public $email;
}
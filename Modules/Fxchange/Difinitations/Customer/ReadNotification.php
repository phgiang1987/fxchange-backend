<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerReset"))
 */
class ReadNotification
{
    /**
     * @SWG\Property(example="alert")
     * @var string
     */
    public $push_type;
    /**
     * @SWG\Property(example="USD")
     * @var string
     */
    public $symbol;
    /**
     * @SWG\Property(example=0)
     * @var integer
     */
    public $transaction_id;
}
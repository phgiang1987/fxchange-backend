<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CustomerAddAddress"))
 */
class SetAlert
{
    /**
     * @SWG\Property(example="2")
     * @var int
     */
    public $currency_id;
    /**
     * @SWG\Property(example="Bitcoin")
     * @var string
     */
    public $currency_name;
    /**
     * @SWG\Property(example="0.5")
     * @var string
     */
    public $alert_sgd_rate;

    /**
     * @SWG\Property(example="0.3")
     * @var string
     */
    public $alert_exchange_rate;
}
<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="UpdateMyAccount"))
 */
class UpdatePayment
{
    /**
     * @SWG\Property(example="FX21519270703")
     * @var string
     */
    public $merchanttxnref;

}
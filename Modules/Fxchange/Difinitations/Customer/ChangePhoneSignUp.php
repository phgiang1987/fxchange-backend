<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ChangePhoneSignUp"))
 */
class ChangePhoneSignUp
{
    /**
     * @SWG\Property(example="+841234567890")
     * @var string
     */
    public $phone_number;
    /**
     * @SWG\Property(example="customer1@fxchange.sg")
     * @var integer
     */
    public $email;
<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="PaymentConfirm"))
 */
class PaymentConfirm
{
    /**
     * @SWG\Property(example="US Dollar")
     * @var int
     */
    public $currency;
    /**
     * @SWG\Property(example="2")
     * @var string
     */
    public $quantity;
    /**
     * @SWG\Property(example="1.8")
     * @var string
     */
    public $sgd_rate;

    /**
     * @SWG\Property(example="2.0")
     * @var string
     */
    public $exchange_rate;
    /**
     * @SWG\Property(example="8000")
     * @var int
     */
    public $amount;
    /**
     * @SWG\Property(example="6")
     * @var string
     */
    public $delivery_fee;
    /**
     * @SWG\Property(example="8000")
     * @var string
     */
    public $total_price;

    /**
     * @SWG\Property(example="delivery")
     * @var string
     */
    public $purchase_type
    /**
     * @SWG\Property(example="normal")
     * @var int
     */
    public $delivery_type
    /**
     * @SWG\Property(example="Delivery Name")
     * @var string
     */
    public $delivery_name;

    /**
     * @SWG\Property(example="01207800020")
     * @var string
     */
    public $contact_number;
    /**
     * @SWG\Property(example="123 Quang Trung, Go Vap, Ho Chi Minh")
     * @var int
     */
    public $address;
    /**
     * @SWG\Property(example="123ZFSY")
     * @var string
     */
    public $postal_code;
    /**
     * @SWG\Property(example="-")
     * @var string
     */
    public $block;

    /**
     * @SWG\Property(example="7-3")
     * @var string
     */
    public $unit;
    /**
     * @SWG\Property(example="123.344")
     * @var string
     */
    public $longitude;

    /**
     * @SWG\Property(example="123.345")
     * @var string
     */
    public $latitude;

}
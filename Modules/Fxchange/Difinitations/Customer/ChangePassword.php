<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ChangePassword"))
 */
class ChangePassword
{
    /**
     * @SWG\Property(example="123@abc")
     * @var string
     */
    public $current_password;
    
    /**
     * @SWG\Property(example="abc@123")
     * @var integer
     */
    public $new_password;

    /**
     * @SWG\Property(example="abc@123")
     * @var string
     */
    public $confirm_password;
}
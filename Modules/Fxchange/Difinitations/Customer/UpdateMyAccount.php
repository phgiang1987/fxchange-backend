<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="UpdateMyAccount"))
 */
class UpdateMyAccount
{
    /**
     * @SWG\Property(example="Timmy")
     * @var string
     */
    public $last_name;
    
    /**
     * @SWG\Property(example="Nitzsche")
     * @var integer
     */
    public $first_name;

    /**
     * @SWG\Property(example="+84986981718")
     * @var string
     */
    public $phone_number;
}
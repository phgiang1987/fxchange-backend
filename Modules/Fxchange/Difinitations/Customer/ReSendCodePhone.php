<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ReSendCodePhone"))
 */
class ReSendCodePhone
{
    /**
     * @SWG\Property(example="+84123456789")
     * @var string
     */
    public $phone_number;

}
<?php

namespace Modules\Fxchange\Definitions\Customer;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="FeedbackCreate"))
 */
class FeedbackCreate
{
    /**
     * @SWG\Property(example="Content")
     * @var string
     */
    public $content;
}
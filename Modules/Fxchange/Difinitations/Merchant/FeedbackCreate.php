<?php

namespace Modules\Fxchange\Definitions\Merchant;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="MerchantFeedback"))
 */
class MerchantFeedback
{
    /**
     * @SWG\Property(example="Content")
     * @var string
     */
    public $content;
}
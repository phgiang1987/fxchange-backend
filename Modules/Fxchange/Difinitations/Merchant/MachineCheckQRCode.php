<?php

namespace Modules\Fxchange\Definitions\Merchant;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="CheckQRCode"))
 */
class MachineCheckQRCode
{
    /**
     * @SWG\Property(example="1")
     * @var string
     */
    public $user_id;
    /**
     * @SWG\Property(example="1")
     * @var string
     */
    public $machine_id;
        /**
     * @SWG\Property(example="23")
     * @var string
     */
    public $order_id;

     /**
     * @SWG\Property(example="2018-06-06")
     * @var string
     */
    public $qr_code_generation_date;
     /**
     * @SWG\Property(example="12:00:00")
     * @var string
     */
    public $qr_code_generation_time;

}
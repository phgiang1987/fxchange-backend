<?php

namespace Modules\Fxchange\Definitions\Merchant;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ReSendCodeLogin"))
 */
class ReSendCodeLogin
{
    /**
     * @SWG\Property(example="1ec9ba7c17d2458098ada800ef465c89")
     * @var string
     */
    public $request_id;
}
<?php

namespace Modules\Fxchange\Definitions\Merchant;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ConfirmTransaction"))
 */
class ConfirmTransaction
{
    /**
     * @SWG\Property(example="001")
     * @var string
     */
    public $order_id ;

} 
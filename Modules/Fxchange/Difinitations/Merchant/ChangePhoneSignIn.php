<?php

namespace Modules\Fxchange\Definitions\Merchant;
/**
 * @SWG\Definition(type="object", @SWG\Xml(name="ChangePhoneSignIn"))
 */
class ChangePhoneSignIn
{
    /**
     * @SWG\Property(example="1ec9ba7c17d2458098ada800ef465c89")
     * @var string
     */
    public $request_id;

    /**
     * @SWG\Property(example="+84123456789")
     * @var string
     */

    public $phone_number;
}
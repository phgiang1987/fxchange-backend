<?php

namespace Modules\Fxchange\Entities\Merchant;

use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserToken;

class Merchant extends User
{
    protected $fillable = [
        'id',
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
        'phone_number'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api_keys()
    {
        return $this->hasMany(UserToken::class,'user_id');
    }
}

<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class StaticPage extends Model
{
    use SoftDeletes, AuditTrailModelTrait;

    protected $table = 'fxchange__static_page';
    //protected $fillable = ['title','content','user_type','type','sort','is_deleted','question'];
    protected $guarded = [];


    const DELETED_AT = 'is_deleted';
    const MERCHANT = "merchant";
    const CUSTOMER = "customer";
    const FAQ = "faq";
    const TERM = "term";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['is_deleted'];

    //FAQ
    public static function getAllFAQOfMerchant(){
        $data = self::where('user_type',self::MERCHANT)->where('type',self::FAQ)->orderBy('sort','ASC');
        return $data;
    }
    public static function getAllFAQOfCustomer(){
        $data = self::where('user_type',self::CUSTOMER)->where('type',self::FAQ)->orderBy('sort','ASC');
        return $data;
    }
    //Terms
    public static function getAllTermsOfMerchant(){
        $data = self::where('user_type',self::MERCHANT)->where('type',self::TERM)->orderBy('sort','ASC');
        return $data;
    }
    public static function getAllTermsOfCustomer(){
        $data = self::where('user_type',self::CUSTOMER)->where('type',self::TERM)->orderBy('sort','ASC');
        return $data;
    }



    public function getQuestionFaqAttribute(){
        return $this->title;
    }
    public function getAnswerFaqAttribute(){
        return $this->content;
    }
    public function getCreatedAt(){
        return Carbon::parse($this->created_at)->timezone('Asia/Singapore')->format('d-m-Y');
    }


}

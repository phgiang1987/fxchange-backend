<?php

namespace Modules\Fxchange\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Fxchange\Entities\Customer\Customer;
use Modules\Fxchange\Entities\Merchant\Merchant;
use Modules\User\Entities\Sentinel\User;

class FxUserToken extends Model
{
    protected $table = 'user_tokens';
    protected $fillable = ['user_id', 'access_token','firebase_token','device_type','expired_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class,'user_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function merchant()
    {
        return $this->belongsTo(Merchant::class,'user_id');
    }
}

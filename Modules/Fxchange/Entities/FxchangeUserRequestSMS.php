<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class FxchangeUserRequestSMS extends Model
{
    protected $table = 'fxchange__user_request_sms';
    protected $fillable = ['user_id','email','phone_number','password','password_hash','firebase_token','device_type','request_id','expired_at','status','first_name','last_name'];

    public function user(){
    	return $this->belongsTo(\Modules\Fxchange\Entities\Merchant\Merchant::class,'user_id');
    }

    protected $hidden = ['password','password_hash'];
}

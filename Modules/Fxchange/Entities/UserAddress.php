<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Fxchange\Entities\Customer\Customer;

class UserAddress extends Model
{
    protected $table = 'fxchange__user_address';
    protected $fillable = ['user_id','title','postal_code','address','block','unit','longitude','latitude','is_primary'];


    public function customer(){
        return $this->belongsTo(Customer::class,'user_id');
    }


}

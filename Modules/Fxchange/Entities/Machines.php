<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class Machines extends Model
{
	use MediaRelation, AuditTrailModelTrait;
    protected $table = 'fxchange__machines';
    protected $guarded = [];

}

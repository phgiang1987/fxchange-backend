<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Fxchange extends Model
{
    use Translatable;

    protected $table = 'fxchange__fxchanges';
    public $translatedAttributes = [];
    protected $fillable = [];
}

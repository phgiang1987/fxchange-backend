<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class Currency extends Model
{
    //use Translatable;
    use MediaRelation, AuditTrailModelTrait;
    const DELIVERY = 1;
    const CANCEL = 0;

    protected $table = 'fxchange__currencies';
    //public $translatedAttributes = [];
    protected $guarded = [];


    public function getPickupSelfOnly(){
    	if($this->self_pickup_only == self::DELIVERY){
    		return "Yes";
    	}return "No";
    }
    public function getSGDRate(){
        if($this->sgd_rate == 0){
            return 0;
        }if($this->sgd_rate == 10){
            return 10;
        }if($this->sgd_rate == 0.1){
            return 0.1;
        }
       //return number_format($this->sgd_rate,17,'.','.');
       return rtrim(number_format($this->sgd_rate,17,'.','.'),".0");
    }
}

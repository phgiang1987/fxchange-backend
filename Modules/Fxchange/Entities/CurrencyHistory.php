<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CurrencyHistory extends Model
{
    //use Translatable;
    const DELIVERY = 1;
    const CANCEL = 0;

    protected $table = 'fxchange__currencies_rate_history';
    //public $translatedAttributes = [];
    protected $guarded = [];
}

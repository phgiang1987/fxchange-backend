<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class FxchangeSettings extends Model
{
    const EXPRESS   = 1;
    const NORMAL    = 2;
    const SELFPICKUP = 3;
    use AuditTrailModelTrait;

    protected $fillable = ['name', 'value', 'description', 'isTranslatable', 'plainValue'];
    protected $table = 'setting__settings';

    public function getJsonValueAttribute(){
    	return json_decode($this->plainValue);
    }
    public static function checkNormalDeliveryFee(){
    	$data = self::where('name','core::normal-delivery-fees')->first();
    	if($data){
    		return $data;
    	}
        return null;
    }
    public static function checkExpressDeliveryFee(){
    	$data = self::where('name','core::express-delivery-fees')->first();
    	if($data){
    		return $data;
    	}
        return null;
    }
    public static function checkSelfpickup(){
        $data = self::where('name','core::selfpickup')->first();
        if($data){
            return $data;
        }
        return null;        
    }
    public static function showValue($key){
        $minimum = self::where('name',$key)->first();
        if ($minimum !== null) {
            return $minimum->plainValue;
        }
        return null;
    }
}

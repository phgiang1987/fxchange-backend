<?php

namespace Modules\Fxchange\Entities;

use Illuminate\Database\Eloquent\Model;

class FxchangeTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'fxchange__fxchange_translations';
}

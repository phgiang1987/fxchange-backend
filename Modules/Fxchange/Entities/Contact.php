<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class Contact extends Model
{
    use SoftDeletes, AuditTrailModelTrait;

    protected $table = 'fxchange__contact_us';
    // protected $fillable = ['title','content','user_type','type','sort','is_deleted','question'];
    protected $guarded = [];


    const DELETED_AT = 'is_deleted';
    const MERCHANT = "merchant";
    const CUSTOMER = "customer";

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['is_deleted'];

    //Merchant
    public static function getAllContactOfMerchant(){
        $data = self::where('user_type',self::MERCHANT);
        return $data;
    }

    //Customer
    public static function getAllContactOfCustomer(){
        $data = self::where('user_type',self::CUSTOMER);
        return $data;
    }



    public function getEmailContactAttribute(){
        return $this->email;
    }
    public function getPhoneContactAttribute(){
        return $this->phone;
    }



}

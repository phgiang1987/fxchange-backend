<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRecoverPwd extends Model
{
    protected $table = 'fxchange__user_recover_pwd';

    protected $guarded = [];
}

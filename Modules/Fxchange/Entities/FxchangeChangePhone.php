<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class FxchangeChangePhone extends Model
{
    protected $table = 'fxchange__user_change_phone';
    
    protected $fillable = [
    	'user_id',
    	'phone_number',
    	'request_id',
    	'expired_at',
    	'status'
    ];
}

<?php

namespace Modules\Fxchange\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\Sentinel\User;
use Carbon\Carbon;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class FxchangeFeedback extends Model
{
    use AuditTrailModelTrait;
    protected $table = 'fxchange__feedback';
    protected $fillable = ['id','user_id','content'];





    public function getUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function getEmailUser(){
    	$data = $this->getUser()->first();
    	if($data){
    		return $data->email;
    	}return null;
    }
    public function getCreatedAt(){
    	return Carbon::parse($this->created_at)->timezone('Asia/Singapore')->format('d-m-Y H:i:s');
    }
}

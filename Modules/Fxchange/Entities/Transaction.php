<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\Sentinel\User;
use Carbon\Carbon;
use Modules\Audittrail\Traits\AuditTrailModelTrait;

class Transaction extends Model
{
    use AuditTrailModelTrait;

    protected $table = 'fxchange__purchases';
    protected $guarded = [];

    const CURRENT   = 1;
    const PAST      = 2;

    const COMPLETED = "completed";

    public function getUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public static function getEmailUser($id){
        $data = self::where('id',$id)->with('getUser')->first();
        if($data){
            return $data;
        }return null;
    }



    public function getMerchant()
    {
        return $this->belongsTo(User::class,'assign_by');
    }
    public function getCustomer(){
    	$customer = $this->getUser()->first();
    	if($customer != null){
    		return $customer->email;
    	}return null;
    }
    public function getnameCustomer(){
        $customer = $this->getUser()->first();
        if($customer != null){
            return $customer->first_name. " ". $customer->last_name;
        }return null;        
    }
    public function getMerchantAssign(){
     	$merchant = $this->getMerchant()->first();
    	if($merchant != null){
    		return $merchant->email;
    	}return null;   	
    }
    public function getNameMerchantAssign(){
        $merchant = $this->getMerchant()->first();
        if($merchant != null){
            return $merchant->first_name. " ". $merchant->last_name;
        }return null;        
    }
    public function getCurrency(){
    	return $this->currency;
    }
    public function getSgdRate(){
        if($this->sgd_rate == 0){
            return 0;
        }
        return rtrim(number_format($this->sgd_rate,15,'.','.'),".0");
    }
    public function getExchangeRate(){
    	return $this->exchange_rate;
    }
    public function getAmount(){
    	return $this->amount;
    }
    public function getTotalPrice(){
    	return number_format(round($this->total_price,2),2,'.','.');
        
    }
    public function getPurchaseType(){
    	return ucfirst($this->purchase_type);
    }
    public function getDeliveryType(){
    	return ucfirst($this->delivery_type);
    }
    public function getPurchaseDate(){
    	return Carbon::parse($this->purchase_date)->timezone('Asia/Singapore')->format('d-m-Y H:i:s');
    }
    public function getCollectedDate(){
        if($this->collected_date == null){
            $collected_date = null;
        }else{
            $collected_date = Carbon::parse($this->collected_date)->timezone('Asia/Singapore')->format('d-m-Y H:i:s');
        }
        return $collected_date;
    }
    public function getLocationByLatLong(){
        if($this->latitude != null && $this->longitude != null){
           $data = getLocation($this->latitude,$this->longitude);
           return $data;
        }return "None";
    }

    public function getQuantity(){
        return $this->quantity;
    }
    public function getDeliveryFee(){
        return $this->delivery_fee;
    }
    public function getCollectionExpiredDate(){    
        return Carbon::parse($this->collection_expired_date)->timezone('Asia/Singapore')->format('d-m-Y');
    }
    public function getQRCode(){
        return $this->qr_code;
    }
    public function getQRCodeDateTime(){
        return Carbon::parse($this->qr_code_datetime)->timezone('Asia/Singapore')->format('d-m-Y');
    }
    public function getDeliveryName(){
        return $this->delivery_name;
    }
    public function getContactNumber(){
        return $this->contact_number;
    }
    public function getAddress(){
        return $this->address;
    }
    public function getPostalCode(){
        return $this->postal_code;
    }
    public function getBlock(){
        return $this->block;
    }
    public function getUnit(){
        return $this->unit;
    }
    public function getLatitude(){
        return $this->latitude;
    }
    public function getLongitude(){
        return $this->longitude;
    }
    public function getStatus(){
        $now = Carbon::now()->timestamp;
        $expire = Carbon::parse($this->collection_expired_date)->timestamp;
        if($this->status == "paid" && $now > $expire && $this->purchase_type == "self-pickup"){
            $result = "Expired";
        }else{
            $result = ucfirst($this->status);
        }

        return $result;
    }
}

<?php

namespace Modules\Fxchange\Entities;

//use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class FxchangeNotificationList extends Model
{
	//use MediaRelation;
    protected $table = 'fxchange__notification';
    protected $guarded = [];



    public static function getBadgeNumbers($userId){
        $number = self::where('user_id',$userId)->where('is_read',0)->count();
        if($number){
            return $number;
        }
        return 0;
    }
    public static function getUnreadNotiByUser($user_id){
        $data = self::where('is_read',0)->where('user_id',$user_id)->get();
        return $data;
    }
    public static function checkIsReadByTypeAlert($user_id){
        $data = self::where('is_read',0)->where('user_id',$user_id)->where('push_type','alert')->get();
        //dd($data);
        if(count($data)>0){
            return true;
        }return false;
    }

}

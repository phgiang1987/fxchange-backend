<?php

namespace Modules\Fxchange\Entities\Customer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Modules\Fxchange\Entities\UserAddress;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserToken;

class CustomerAddress extends Model
{
    protected $table = 'fxchange__user_address';
    protected $fillable = [
        'id',
        'user_id',
        'title',
        'postal_code',
        'address',
        'block',
        'unit',
        'longitude',
        'latitude',
        'sort',
        'is_primary'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class,'user_id');
    }
}

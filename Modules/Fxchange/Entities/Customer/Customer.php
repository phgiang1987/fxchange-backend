<?php

namespace Modules\Fxchange\Entities\Customer;

use Illuminate\Notifications\Notifiable;
use Modules\Fxchange\Entities\UserAddress;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserToken;

class Customer extends User
{
    use Notifiable;

    protected $fillable = [
        'id',
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
        'phone_number',
        'country_code',
        'check_reset_password'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api_keys()
    {
        return $this->hasMany(UserToken::class,'user_id');
    }

    public function addresses(){
        return $this->hasMany(CustomerAddress::class,'user_id');
    }
}

<?php

namespace Modules\Fxchange\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Fxchange\Events\Handlers\RegisterFxchangeSidebar;
use Modules\Fxchange\Http\Middleware\FxChangeAuthorisedApiKey;
use Modules\Fxchange\Http\Middleware\FxChangeAuthorisedApiToken;


class FxchangeServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
	
	/**
     * @var array
     */
    protected $middleware = [
        'fxchange.api.token' => FxChangeAuthorisedApiToken::class,
        'fxchange.api.key' => FxChangeAuthorisedApiKey::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterFxchangeSidebar::class);
    }

    public function boot()
    {
        $this->registerMiddleware();
        //$this->registerCommands();
        $this->publishConfig('fxchange', 'permissions');
		$this->publishConfig('fxchange', 'config');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Fxchange\Repositories\FxchangeRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\EloquentFxchangeRepository(new \Modules\Fxchange\Entities\Fxchange());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Fxchange\Repositories\Cache\CacheFxchangeDecorator($repository);
            }
        );
// add bindings

        $this->app->bind(
            'Modules\Fxchange\Repositories\CustomerRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\Customer\EloquentCustomerRepository(new \Modules\Fxchange\Entities\Customer\Customer());

                return $repository;
            }
        );
        $this->app->bind(
            'Modules\Fxchange\Repositories\MerchantRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\Merchant\EloquentMerchantRepository(new \Modules\Fxchange\Entities\Merchant\Merchant());

                return $repository;
            }
        );
        $this->app->bind(
            'Modules\Fxchange\Repositories\FxUserTokenRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\EloquentFxUserTokenRepository(new \Modules\Fxchange\Entities\FxUserToken());

                return $repository;
            }
        );

        $this->app->bind(
            'Modules\Fxchange\Repositories\StaticPageRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\EloquentStaticPageRepository(new \Modules\Fxchange\Entities\StaticPage());

                return $repository;
            }
        );
        $this->app->bind(
            'Modules\Fxchange\Repositories\CustomerAddressRepository',
            function () {
                $repository = new \Modules\Fxchange\Repositories\Eloquent\Customer\EloquentCustomerAddressRepository(new \Modules\Fxchange\Entities\Customer\CustomerAddress());

                return $repository;
            }
        );
    }
    private function registerMiddleware()
    {
        foreach ($this->middleware as $name => $class) {
            $this->app['router']->aliasMiddleware($name, $class);
        }
    }
    public function registerCommands()
    {
        $this->commands([
            \Modules\Fxchange\Console\AlertExchangeRateCommand::class,
        ]);
        $this->commands([
            \Modules\Fxchange\Console\UpdateExchangeRateByDateCommand::class,
            \Modules\Fxchange\Console\UpdateUserAlertEveryDaysCommand::class,
            \Modules\Fxchange\Console\DeleteApiLogCommand::class,
        ]);
    }
}

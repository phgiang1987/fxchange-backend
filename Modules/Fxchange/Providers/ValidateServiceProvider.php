<?php

namespace Modules\Fxchange\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Fxchange\Services\Validation\PasswordValidate;
use Modules\Fxchange\Services\Validation\CustomValidation;

class ValidateServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */



    public function boot(){
        $this->app->validator->resolver(function ($translator, $data, $rules, $messages){
            return new PasswordValidate($translator, $data, $rules, $messages);
        });
        // $this->app->validator->resolver(function ($translator, $data, $rules, $messages){
        //     return new CustomValidation($translator, $data, $rules, $messages);
        // });
    }

    public function provides()
    {
        return [];
    }
}

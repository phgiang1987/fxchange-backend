<?php

namespace Modules\Fxchange\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Maatwebsite\Sidebar\Domain\Events\FlushesSidebarCache;
use Modules\Fxchange\Events\FxUserResetProcess;
use Modules\Fxchange\Events\Handlers\FxSendResetCodeEmail;
use Modules\User\Events\Handlers\SendRegistrationConfirmationEmail;
use Modules\User\Events\Handlers\SendResetCodeEmail;
use Modules\User\Events\RoleWasUpdated;
use Modules\User\Events\UserHasBegunResetProcess;
use Modules\User\Events\UserHasRegistered;
use Modules\User\Events\UserWasUpdated;

use Modules\Fxchange\Events\SendPushUpdateStatus;
use Modules\Fxchange\Events\Handlers\SendPushUpdateStatusToDevices;

use Modules\Fxchange\Events\AlertRate;
use Modules\Fxchange\Events\Handlers\SendPushAlertRateToDevices;

class FxchangeEventServiceProvider extends ServiceProvider
{
    protected $listen = [
        FxUserResetProcess::class => [
            FxSendResetCodeEmail::class,
        ],
        SendPushUpdateStatus::class => [
            SendPushUpdateStatusToDevices::class,
        ],
        AlertRate::class => [
            SendPushAlertRateToDevices::class,
        ],
    ];
}

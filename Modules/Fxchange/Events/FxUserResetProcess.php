<?php

namespace Modules\Fxchange\Events;

class FxUserResetProcess
{
    public $user;
    public $code;

    public function __construct($user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }
}

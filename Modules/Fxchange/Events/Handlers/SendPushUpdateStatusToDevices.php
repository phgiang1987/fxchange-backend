<?php

namespace Modules\Fxchange\Events\Handlers;

use Modules\Fxchange\Events\SendPushUpdateStatus;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Fxchange\Entities\Currency;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Fxchange\Components\Firebase\CloudMessaging;
use Modules\Fxchange\Components\Firebase\CloudMessagingException;
use Modules\Fxchange\Entities\FxchangeNotificationList;

class SendPushUpdateStatusToDevices
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $tries = 1;
    public $timeout = 360;
    protected $notificationListEntities;
    use InteractsWithQueue;

    public function __construct(FxchangeNotificationList $notificationListEntities)
    {
        $this->notificationListEntities = $notificationListEntities;
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Fxchange\Events\SendPushUpdateStatus $event
     * @return void
     */
    public function handle(\Modules\Fxchange\Events\SendPushUpdateStatus $event)
    {
        
        //
        $userId = $event->transaction->user_id;

        //get user tokens
        $rows = DB::table('user_tokens')
                    ->select('firebase_token')
                    ->where('user_id',$userId)
                    ->where('firebase_token','!=',null)->get();

        $tokens = collect($rows)->map(function($item) {
            return $item->firebase_token;
        })->toArray();

        if(count($tokens) >0){
            $currency_check = Currency::where('name',$event->transaction->currency)->first();
            if($currency_check){
                $symbol = $currency_check->symbol;
                $currency_id = $currency_check->id;
            }



            if($event->transaction->status == "pending"){
                $title = $event->transaction->currency. ' has been pending.';
            }elseif($event->transaction->status == "paid"){
                $title = $event->transaction->currency. ' has been paid.';
            }elseif($event->transaction->status == "delivery"){
                $title = $event->transaction->currency. ' has been delivery.';
            }
            elseif($event->transaction->status == "cancelled"){
                $title = $event->transaction->currency. ' has been cancelled.';
            }
            elseif($event->transaction->status == "completed"){
                $title = $event->transaction->currency. ' has been completed.';
            }
            //insert to notification list
            // $push = $this->notificationListRepository->create([
            //     'user_id'=>$userId,
            //     'transaction_id'=>$event->transaction->id,
            //     'title'=>$title,
            //     'is_read'=> 0,
            //     'status_id'=> $event->transaction->booking_status
            // ]);
            // $badgeNumbers = $this->notificationListRepository->getBadgeNumbers($userId);
            $amount = $event->transaction->amount;
            $id = $event->transaction->id;
            $purchase_type = $event->transaction->purchase_type;
            $title = " ";
            $message_push = "Your payment for $amount $symbol is successful!";
            if($event->transaction->status == "delivery"){
                $message_push = "Your order has been assigned for delivery! ";
            }elseif($event->transaction->status == "cancelled"){
                $message_push = "Your order has been cancelled.";
            }elseif($event->transaction->status == "completed"){
                $message_push = "You have successfully collected $amount $symbol!";
            }

            //insert to notification list
            $push = $this->notificationListEntities->create([
                'user_id'=> $userId,
                'symbol' => $symbol,
                'push_type' => 'transaction',
                'purchase_type' => $purchase_type,
                'title' => $message_push,
                'transaction_id' => $event->transaction->id,
                'is_read'=> 0,
            ]);
            $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($userId);

            $mobileData = [
                'message'=> $title,
                'title'=> $message_push,
                'badge'=> $badgeNumbers,
                'status' => $event->transaction->status,
                'purchase_date' => Carbon::parse($event->transaction->purchase_date)->timestamp,
                'amount' => $event->transaction->amount,
                'total_amount_inday' => getTransactionAmountInDateByUser($userId),
                'push_type'=> 'transaction',
                'data'=>[
                    'created_at'=> Carbon::parse($event->transaction->created_at)->toDateTimeString(),
                    'transaction_id'=> $event->transaction->id,
                    'purchase_type'=> $event->transaction->purchase_type,
                    'status'=> $event->transaction->status,
                    'push_type'=> 'transaction',
                    'title'=>  $message_push,
                    'photo' => '',
                    'badge'=> $badgeNumbers,
                    'purchase_date' => Carbon::parse($event->transaction->purchase_date)->timestamp,
                    'amount' => $event->transaction->amount,
                    'symbol'=> $symbol,
                    'currency_id' => isset($currency_id)?$currency_id:"",
                    'total_amount_inday' => getTransactionAmountInDateByUser($userId),
                    'push_id' => $push->id,
                    // 'push_id'=>$push->id,
                    // 'push_type'=>1 ,  //booking,
                ]
            ];

            $cloudMessaging = new CloudMessaging();
            try {
                $cloudMessaging->toMultiple($tokens)
                    ->message($mobileData['message'])
                    ->title($mobileData['title'])
                    ->setData($mobileData['data'])
                    ->send();

                //add push history data success
                // event(new SentNotificationMessageSuccess($cloudMessaging,$event->transaction, false,$mobileData));
            } catch (CloudMessagingException $e) {
               return $e->getMessage();

            }
        }

    }   
}

<?php

namespace Modules\Fxchange\Events\Handlers;

use Modules\Fxchange\Events\AlertRate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Modules\Fxchange\Components\Firebase\CloudMessaging;
use Modules\Fxchange\Components\Firebase\CloudMessagingException;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\FxchangeNotificationList;

class SendPushAlertRateToDevices
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    public $tries = 1;
    public $timeout = 360;
    protected $notificationListEntities;
    use InteractsWithQueue;

    public function __construct(FxchangeNotificationList $notificationListEntities)
    {
        $this->notificationListEntities = $notificationListEntities;
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Fxchange\Events\AlertRate $event
     * @return void
     */
    public function handle(\Modules\Fxchange\Events\AlertRate $event)
    {
        //
        $userId = $event->userAlert->user_id;

        //get user tokens
        $rows = DB::table('user_tokens')
                    ->select('firebase_token')
                    ->where('user_id',$userId)
                    ->where('firebase_token','!=',null)->get();

        $tokens = collect($rows)->map(function($item) {
            return $item->firebase_token;
        })->toArray();

        if(count($tokens) >0){
            $currency_check = Currency::where('name',$event->userAlert->currency_name)->first();
            if($currency_check){
                $symbol = $currency_check->symbol;
                $currency_id = $currency_check->id;
                $exchange_rate = $currency_check->exchange_rate;
            }
            //insert to notification list
            $push = $this->notificationListEntities->create([
                'user_id'=> $userId,
                'symbol' => $symbol,
                'push_type' => 'alert',
                'is_read'=> 0,
            ]);
            $badgeNumbers = $this->notificationListEntities->getBadgeNumbers($userId);
            //dd($badgeNumbers);

            $msg = "1 SGD = $exchange_rate $symbol. Click here to buy now.";

            $mobileData = [
                'message'=> $msg,
                'title'=> "Exchange Rate Alert ($symbol)",
                'push_type' => 'alert',
                'badge'=> $badgeNumbers,
                'data'=>[
                    'title'=> "Exchange Rate Alert ($symbol)",
                    'message'=> $msg,
                    'photo' => '',
                    'symbol' => isset($symbol)?$symbol:"",
                    'push_type' => 'alert',
                    'currency_id' => isset($currency_id)?$currency_id:"",
                    'push_id' => $push->id,
                    'badge'=> $badgeNumbers,
                ]
            ];
            
            $cloudMessaging = new CloudMessaging();
            try {
                $cloudMessaging->toMultiple($tokens)
                    ->message($mobileData['message'])
                    ->title($mobileData['title'])
                    ->setData($mobileData['data'])
                    ->send();

                //add push history data success
                //event(new SentNotificationMessageSuccess($cloudMessaging,$event->transaction, false,$mobileData));
            } catch (CloudMessagingException $e) {
               return $e->getMessage();

            }
        }
    }
}

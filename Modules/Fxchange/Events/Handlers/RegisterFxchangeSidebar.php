<?php

namespace Modules\Fxchange\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterFxchangeSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            // $group->item(trans('fxchange::fxchanges.title.fxchanges'), function (Item $item) {
            //     $item->icon('fa fa-copy');
            //     $item->weight(10);
            //     $item->authorize(
            //          /* append */
            //     );
            //     $item->item(trans('fxchange::fxchanges.title.fxchanges'), function (Item $item) {
            //         $item->icon('fa fa-copy');
            //         $item->weight(0);
            //         $item->append('admin.fxchange.fxchange.create');
            //         $item->route('admin.fxchange.fxchange.index');
            //         $item->authorize(
            //             $this->auth->hasAccess('fxchange.fxchanges.index')
            //         );
            //     });
            // });



            $group->item(trans('Transactions'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-themeisle');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Merchant'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle-o');
                        $item->route('admin.transaction.transaction.index');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Customer'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle');
                        $item->route('admin.transaction.transaction.indexcustomer');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
            $group->item(trans('Feedback'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-commenting-o');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Merchant'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle-o');
                        $item->route('admin.feedback.feedback.indexmerchantfeedback');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Customer'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle');
                        $item->route('admin.feedback.feedback.indexcustomerfeedback');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
            $group->item(trans('FxChange Settings'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-cog');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Settings Amount'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-usd');
                        $item->route('admin.amount.amount.editamount');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Settings Normal Delivery Fees'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-usd');
                        $item->route('admin.normal.normal.editnormalfee');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Settings Express Delivery Fees'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-usd');
                        $item->route('admin.express.express.editexpressfee');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Settings Self-Pickup'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-usd');
                        $item->route('admin.setting.selfpickup');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Settings Google Analytics'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-google-plus-square');
                        $item->route('admin.setting.google.analytics');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
            $group->item(trans('Currency Management'), function (Item $item) {
                $item->weight(10);
                $item->icon('fa fa-money');
                $item->route('admin.currency.currency.indexcurrency');
                $item->authorize(
                    $this->auth->hasAccess('currency.currencys.index')
                );
            });

            $group->item(trans('Machines'), function (Item $item) {
                $item->weight(11);
                $item->icon('fa fa-trello');
                $item->route('admin.machine.machine.indexmachine');
                $item->authorize(
                    $this->auth->hasAccess('machine.machine.index')
                );
            });


            $group->item(trans('FAQ'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-question-circle');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Merchant'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle-o');
                        $item->route('admin.faq.faq.indexfaq');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Customer'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle');
                        $item->route('admin.faq.faq.indexfaqcustomer');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
            $group->item(trans('Contact Us'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-phone-square');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Merchant'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle-o');
                        $item->route('admin.contact.contact.indexmerchantcontact');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Customer'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle');
                        $item->route('admin.contact.contact.indexcustomercontact');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
            $group->item(trans('Terms & Conditions'), function (Item $item) {
                    $item->weight(12);
                    $item->icon('fa fa-question-circle-o');
                    $item->authorize(
                        $this->auth->hasAccess('transaction.transaction.index') 
                    );

                    $item->item(trans('Merchant'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle-o');
                        $item->route('admin.terms.terms.indextermmerchant');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                    $item->item(trans('Customer'), function (Item $item) {
                        $item->weight(0);
                        $item->icon('fa fa-user-circle');
                        $item->route('admin.terms.terms.indextermscustomer');
                        $item->authorize(
                            $this->auth->hasAccess('transaction.transaction.index')
                        );
                    });
                });
        });

        return $menu;
    }
}

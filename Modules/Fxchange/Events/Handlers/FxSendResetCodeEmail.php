<?php

namespace Modules\Fxchange\Events\Handlers;

use Illuminate\Contracts\Mail\Mailer;
use Modules\Fxchange\Emails\FxResetPasswordEmail;
use Modules\Fxchange\Events\FxUserResetProcess;
use Modules\User\Emails\ResetPasswordEmail;
use Modules\User\Events\UserHasBegunResetProcess;

class FxSendResetCodeEmail
{
    /**
     * @var Mailer
     */
    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(FxUserResetProcess $event)
    {
        $this->mailer->to($event->user->email)->send(new FxResetPasswordEmail($event->user, $event->code));
    }
}

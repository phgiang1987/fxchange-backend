<?php

namespace Modules\Fxchange\Events;

use Illuminate\Queue\SerializesModels;
use Modules\Fxchange\Entities\Transaction;

class SendPushUpdateStatus
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $transaction;


    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

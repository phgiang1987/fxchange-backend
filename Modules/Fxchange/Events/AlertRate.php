<?php

namespace Modules\Fxchange\Events;

use Illuminate\Queue\SerializesModels;
use Modules\User\Entities\Sentinel\User;
use Modules\Fxchange\Entities\UserAlert;

class AlertRate
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $userAlert;

    public function __construct(UserAlert $userAlert)
    {
        $this->userAlert = $userAlert;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}

<?php 
namespace Modules\Fxchange\Services\Validation;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;
use Modules\Fxchange\Entities\UserRecoverPwd;
class PasswordValidate extends Validator {

    public function validatePasswordRegex($attribute, $value, $parameters)
    {
    	$flag = 0;
        if(preg_match("/^(?=.*[A-Z]).{8,}$/", $value)){
            $flag++;
        }
        if(preg_match("/^(?=.*?[a-z]).{8,}$/", $value)){
            $flag++;
        }
        if(preg_match("/^(?=.*[0-9]).{8,}$/", $value)){
            $flag++;
        }
        if(preg_match("/^(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$/", $value)){
            $flag++;
        }
        if($flag >=3){
    		return true;
    	}
    	return false;
    }
    public function validateUniqueWith($attribute, $value, $parameters)
    {
        $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            $query->where(DB::raw("CONCAT({$parameters[1]},'',{$parameters[2]})"),$value);
        })->first();
        return $result ? false : true;
    }

    public function validatePhoneNumber($attribute, $value, $parameters){
        $flag = 0;
        if(preg_match("/^(?=.*[0-9])$/", $value)){
            $flag++;
        }
        if($flag == 0){
            return false; 
        }return true;
    }

    public function validateUniqueEmail($attribute, $value, $parameters){
        if(!isset($parameters[1])){

            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("email = '$value'")->where('is_deleted',null);
            })->first();
        }else{
            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("email = '$value'")->where('id','<>',$parameters[1])->where('is_deleted',null);
            })->first();
        }
        return $result ? false : true;          
    }


    public function validateUniquePhone($attribute, $value, $parameters){
        //dd($parameters[1]);
        if(!isset($parameters[1])){

            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("phone_number = '$value'")->where('is_deleted',null);
            })->first();
        }else{
            $result = DB::table($parameters[0])
            ->selectRaw("users.*,roles.slug")
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("CONCAT(users.country_code,'',users.phone_number)='{$value}'")
                ->where('users.id','<>',$parameters[1])
                ->where("roles.slug",$parameters[2])
                ->where('is_deleted',null);
            })->first();
        }
        return $result ? false : true;        
    }

    public function validateUniqueStringPhone($attribute, $value, $parameters){
        //dd($parameters[1]);
        if(!isset($parameters[1])){

            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("phone_number = '$value'")->where('is_deleted',null);
            })->first();
        }else{
            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
                $query->whereRaw("phone_number = '$value'")->where('id','<>',$parameters[1])->where('is_deleted',null);
            })->first();
        }

        return $result ? false : true;        
    }
    public function validateCheckSymbol($attribute, $value, $parameters){
        $arr_symbol = getArraySymbolName();
        if(in_array(strtoupper($value),$arr_symbol)){
            return true;
        }return false;
    }
    public function validateCheckName($attribute, $value, $parameters){
        //dd($parameters[2]);
        if(!isset($parameters[2])){
            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            $value = explode('-',$value);
            $value = $value[0];
                $query->where('name',$value);
            })->first();
        }else{
            $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            $value = explode('-',$value);
            $value = $value[0];
                $query->where('name',$value)->where('id','<>',$parameters[2]);
            })->first();
        }
        return $result ? false : true;        
    }
    public function validateMaxPhone($attribute, $value, $parameters){
        $value = trim($value);
        if($value[0] == 0){
            $value = substr($value,1);
        }
        $result = strlen($value);
        if($result <= $parameters[0]){
            return true;
        }
        return false;
    }
    public function validateMinPhone($attribute, $value, $parameters){
        $value = trim($value);
        if($value[0] == 0){
            $value = substr($value,1);
        }
        $result = strlen($value);
        if($result > $parameters[0]){
            return true;
        }
        return false;
    }

    public function validateCheckPasswordLast($attribute, $value, $parameters){
        $flag = false;
        $userId = $parameters[0];
        $countRecoverd = UserRecoverPwd::where('user_id',$userId)->count();
        $message = "This password has been used before";
        if ($countRecoverd < config('asgard.fxchange.config.PWD_RECOVERD')) {
            $checkPassword = UserRecoverPwd::where(['user_id' => $userId , 'pwd' => sha1($value)])->first();
            if ($checkPassword !== null) {
                $flag = true;
            }
        }
        else
        {
            $skip = $countRecoverd - config('asgard.fxchange.config.PWD_RECOVERD');
            $checkPassword = UserRecoverPwd::where(['user_id' => $userId])
                ->limit(config('asgard.fxchange.config.PWD_RECOVERD'))
                ->orderBy('id','DESC')
                ->pluck('pwd')->toArray();
            if (in_array(sha1($value), $checkPassword)) {
                if ($checkPassword !== null) {
                    $flag = true;
                }
            }
        }
        return $flag ? false : true;
    }
    public function validateCheckLessAmount($attribute, $value, $parameters){
        //dd($parameters[0]);
        if((int)$value > (int)$parameters[0]){
            return false;
        }else{
            return true;
        }
    }
}
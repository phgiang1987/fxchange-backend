<?php 
namespace Modules\Fxchange\Services\Validation;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

class CustomValidation extends Validator {

    public function validateUniqueWith($attribute, $value, $parameters)
    {
        $result = DB::table($parameters[0])->where(function($query) use ($attribute, $value, $parameters) {
            $query->where(DB::raw("CONCAT({$parameters[1]},'',{$parameters[2]})"),$value);
        })->first();
        return $result ? false : true;
    }

}
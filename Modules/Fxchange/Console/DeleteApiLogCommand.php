<?php

namespace Modules\Fxchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Fxchange\Entities\ApiLog;
use Carbon\Carbon;

class DeleteApiLogCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fxchange:delete-api-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete api log more than 30 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $api_log;

    public function __construct(ApiLog $api_log)
    {
        parent::__construct();
        $this->api_log = $api_log;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('System will...');
        $subDay = Carbon::now()->subDays(30);
        $data = $this->api_log->whereRaw("created_at <= '{$subDay}'")->get();
        if(count($data)>0){
            foreach($data as $k => $v){
                $v->delete();     
            }
        }
        $this->info('Delete successful!');
    }

    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force the installation, even if already installed'],
        ];
    }
}

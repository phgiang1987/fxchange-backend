<?php

namespace Modules\Fxchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Fxchange\Entities\Currency;
use Modules\Fxchange\Entities\CurrencyHistory;
use Carbon\Carbon;

class UpdateExchangeRateByDateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fxchange:update-exchange-rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all exchange rate of currency every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $currency;
    protected $currency_history;


    public function __construct(Currency $currency, CurrencyHistory $currency_history)
    {
        parent::__construct();
        $this->currency = $currency;
        $this->currency_history = $currency_history;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('System will...');
        $currency = $this->currency->get();
        foreach($currency as $k => $v){
            $history = new CurrencyHistory();
            $history->name = $v->name;
            $history->sgd_rate = $v->sgd_rate;
            $history->exchange_rate = $v->exchange_rate;
            $history->sort = 0;
            $history->created_at = Carbon::now();
            $history->save();         
        }
        $this->info('Update successful!');
    }

        /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force the installation, even if already installed'],
        ];
    }
}

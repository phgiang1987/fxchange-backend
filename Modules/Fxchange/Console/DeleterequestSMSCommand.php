<?php

namespace Modules\Fxchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Fxchange\Entities\FxchangeUserRequestSMS;
use Carbon\Carbon;

class DeleterequestSMSCommand extends Command
{
/**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fxchange:delete-request-sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete request sms every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $table;

    public function __construct(FxchangeUserRequestSMS $table)
    {
        parent::__construct();
        $this->table = $table;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('System will...');
        $subDay = Carbon::now();
        $data = $this->table->whereRaw("expired_at <= '{$subDay}'")->get();
        if(count($data)>0){
            foreach($data as $k => $v){
                $v->delete();     
            }
        }
        $this->info('Delete successful!');
    }

    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force the installation, even if already installed'],
        ];
    }
}

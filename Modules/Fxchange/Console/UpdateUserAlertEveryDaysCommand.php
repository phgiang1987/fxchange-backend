<?php

namespace Modules\Fxchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Fxchange\Entities\UserAlert;
use Carbon\Carbon;

class UpdateUserAlertEveryDaysCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fxchange:update-user-alert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $user_alert;

    public function __construct(UserAlert $user_alert)
    {
        parent::__construct();
        $this->user_alert = $user_alert;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('System will...');
        $fxchange__user_alert_rate = UserAlert::get();
        foreach($fxchange__user_alert_rate as $value){
            //$value->status = 0;
            $value->created_at = Carbon::now();
            $value->save();
            $this->info('Update successful');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array

        /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force the installation, even if already installed'],
        ];
    }
}

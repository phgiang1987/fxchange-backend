<?php

namespace Modules\Fxchange\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Modules\Fxchange\Events\AlertRate;
use Modules\Fxchange\Entities\UserAlert;
use Modules\Fxchange\Entities\CurrencyHistory;
use Carbon\Carbon;
use DB;

class AlertExchangeRateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fxchange:alert-rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Notifications when the exchange rate alert criteria is meet';

    /**
     * Create a new command instance.
     *
     * @return void
     */


    protected $user_alert;
    protected $currency_history;

    public function __construct(CurrencyHistory $currency_history, UserAlert $user_alert)
    {
        parent::__construct();
        $this->user_alert = $user_alert;
        $this->currency_history = $currency_history;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('System will...');
        // $currency_history = $this->currency_history->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') >= curdate()-1")->orderBy('created_at','ASC')->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"))->get();

        // $currencyStatistics = [];
        // foreach ($currency_history as $key => $value) {
        //     $created_at = Carbon::parse($value->created_at)->format('Y-m-d');
        //     $currencyHistory = CurrencyHistory::whereRaw("name='{$value->name}'")
        //         ->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')='{$created_at}'")
        //         ->orderBy('created_at','DESC')
        //         ->first();
        //     $currencyStatistics[] = $currencyHistory;
        // }


        // foreach($currencyStatistics as $item){
        //     $user_alert = $this->user_alert->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') >= curdate()-1")
        //                                    ->where('currency_name',$item['name'])
        //                                    ->where('alert_exchange_rate',$item['exchange_rate'])
        //                                    ->where('status',0)
        //                                    ->whereRaw("DATE_FORMAT('$item->created_at','%Y-%m-%d') = DATE_FORMAT(created_at,'%Y-%m-%d')")
        //                                    ->first();
        //     //dd($user_alert);

        $fxchange__user_alert_rate = UserAlert::where('status',0)->get();
        foreach ($fxchange__user_alert_rate as $key => $value) {
            //$user_alert_rate = UserAlert::where(['id' => $value->id])->first();
            $fxchange__currencies_rate_history = DB::table('fxchange__currencies_rate_history')
                ->where('name',$value->currency_name)
                ->where('exchange_rate',$value->alert_exchange_rate)
                ->where(DB::raw("DATE_FORMAT(created_at,'%Y-%m-%d')"),Carbon::parse($value->created_at)->format('Y-m-d'))
                ->orderBy('created_at','DESC')
                ->first();
            if($fxchange__currencies_rate_history){
                event(new AlertRate($value));


                //Update all user alert
                DB::table('fxchange__user_alert_rate')
                    ->where('id', $value->id)
                    ->update(['status' => 1]);

                //$user_alert_rate->update(['status' => 1]);
                $this->info('Push notifications for user');
            }
        }
        $this->info('Push notification successful');
    }

        /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Force the installation, even if already installed'],
        ];
    }
}

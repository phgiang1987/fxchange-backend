<?php
namespace Modules\Precise\Components;
use Modules\Precise\Entities\NotificationMessage;
class NotificationComponent{

    public function __construct( ) {
        // ... initialization before configuration is applied
    }

    public function addNotificationMessage($message, $resultsUsers, $pushType,$productId , $productImage) {
        $can_foreach = is_array($resultsUsers) || is_object($resultsUsers);
        if ($can_foreach) {
            if(count($resultsUsers)>0){
                foreach($resultsUsers as $user){
                    $notification = new NotificationMessage();
                    $notification->userId = $user->user_id;
                    $notification->senderId = "1";
                    $notification->pushType = $pushType;
                    $notification->messageTitle = $message;
                    $notification->messageDetail = $message;
                    $notification->read = 0;
                    $notification->productId = $productId;
                    $notification->productImage = $productImage;
                    $notification->createdAt = time();
                    $notification->save();
                }
            }
        }else{
            // Insert single message user
            $notification = new NotificationMessage();
            $notification->userId = $resultsUsers;
            $notification->senderId = "1";
            $notification->pushType = $pushType;
            $notification->messageTitle = $message;
            $notification->messageDetail = $message;
            $notification->read = 0;
            $notification->productId = $productId;
            $notification->productImage = $productImage;
            $notification->createdAt = time();
            $notification->save();
        }
        return $notification;
    }

    public function getUnreadMessage($userId){
        return NotificationMessage::where(['read'=>0,'userId'=>$userId])->get()->count();
    }

    public function addNotificationGiftMessage($message, $title, $shortDesc,$resultsUsers, $pushType, $productImage) {
        $can_foreach = is_array($resultsUsers) || is_object($resultsUsers);
        if ($can_foreach) {
            if(count($resultsUsers)>0){
                foreach($resultsUsers as $user){
                    $notification = new NotificationMessage();
                    $notification->userId = $user->user_id;
                    $notification->senderId = "1";
                    $notification->pushType = $pushType;
                    $notification->messageTitle = $title;
                    $notification->shortDesc = $shortDesc;
                    $notification->messageDetail = $message;
                    $notification->read = 0;
                    $notification->productImage = $productImage;
                    $notification->createdAt = time();
                    $notification->save();
                }
            }
        }else{
            // Insert single message user
            $notification = new NotificationMessage();
            $notification->userId = $resultsUsers;
            $notification->senderId = "1";
            $notification->pushType = $pushType;
            $notification->messageTitle = $title;
            $notification->messageDetail = $message;
            $notification->shortDesc = $shortDesc;
            $notification->read = 0;
            $notification->productImage = $productImage;
            $notification->createdAt = time();
            $notification->save();
        }
        return $notification;
    }

    public function getUnreadGiftMessage($userId){
        return NotificationMessage::where(['read'=>0,'userId'=>$userId , 'pushType'=>2])->get()->count();
    }
}
<?php 

namespace Modules\Fxchange\Components\Firebase;

use Illuminate\Notifications\Notification;

class CloudMessagingChannel
{
	public function send($notifiable, Notification $notification)
    {
        $message = $notification->toFirebase($notifiable);
        $token = $notifiable->fb_token;

        $cloudMessaging = new CloudMessaging();

        try {
            $cloudMessaging->to($token)
                           ->message($message)
                           ->send();
        } catch (CloudMessagingException $e) {
            \Log::error('Firebase cloud messaging error: ' . $e->getMessage());
        }
    }
}


<?php 

namespace Modules\Fxchange\Components\Firebase;

use Exception;

class CloudMessaging
{
	private $token;
	private $message;
	private $priority;
	private $title;
	private $data;
	public $registrationsId;
	private $badgeNumber;

	public function __construct($priority = 'high')
	{
		$this->priority = $priority;
		$this->title = $this->token = $this->message = '';
		$this->data = [];
		$this->badgeNumber = 0;
		$this->serverKey = config('asgard.fxchange.config.firebase_key');
		$this->endPoint = config('asgard.fxchange.config.firebase_url');
	}

	/**
	 * Set firebase token of reciever
	 * 
	 * @param  string $token
	 * @return $this
	 */
	public function to($token)
	{
		$this->token = $token;

		return $this;
	}

	public function message($message)
	{
		$this->message = $message;

		return $this;
	}

	public function title($title)
	{
		$this->title = $title;

		return $this;
	}
	
	public function toMultiple($arrToken) {
		
		$this->registrationsId = $arrToken;
		
		return $this;
	}
	
	public function setData($arrData) {
		
		$this->data = $arrData;
		
		return $this;
	}

	public function setBadge($value) {

		$this->badgeNumber = $value;

		return $this;
	}

	/**
	 * Check agruments before calling
	 * 
	 * @return boolean
	 */
	private function checkAgruments()
	{
		if ( ! $this->token && ! count($this->registrationsId) ) throw new CloudMessagingException('The Token missing');
		if ( ! $this->message) throw new CloudMessagingException('The Message missing');
		if ( ! $this->serverKey) throw new CloudMessagingException('The Server Key missing');
		if ( ! $this->endPoint) throw new CloudMessagingException('The Endpoint URL missing');
	}

	/**
	 * Create header info
	 * 
	 * @return array
	 */
	private function createHeader()
	{
		return ['Authorization:key='.$this->serverKey, 'Content-Type: application/json'];
	}

	/**
	 * Create request fields
	 * 
	 * @return array
	 */
	private function createContent($registrationIds = [])
	{
		$arrData = [
			'notification' => [
				'title' 	=> $this->title,
				'text' 		=> $this->message,
				'badge'		=> $this->badgeNumber
			],
			'data' => $this->data,
			'content_available' => true,
			'priority' => $this->priority
		];
		
		if(!empty($this->token)) {
			$arrData['to'] = $this->token;
		}
		
		if(count($this->registrationsId)) {
			$arrData['registration_ids'] = $this->registrationsId;
		}	
		
		return $arrData;
		
	}

	/**
	 * Send message 
	 * @return void | \KiweeApp\Components\Firebase\CloudMessagingException
	 */
	public function send()
	{
		$this->checkAgruments();

		$headers = $this->createHeader();
		$content = $this->createContent();

		$this->makeCall($this->endPoint, $headers, $content);
	}

	/**
	 * Make a call HTTP to FCM
	 * @param  string $url    
	 * @param  array  $headers
	 * @param  array  $content
	 * @return boolean | \KiweeApp\Components\Firebase\CloudMessagingException
	 */
	public function makeCall($url = '', $headers = [], $content = [])
	{
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($content));

        $result = curl_exec($ch);
        $resultDecoded = json_decode($result);

        if($result == null) {
        	throw new CloudMessagingException($result);
        }

        if ($resultDecoded->failure) {
        	throw new CloudMessagingException($result);
        }

        return true;
	}
}
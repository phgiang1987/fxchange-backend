<?php
use Modules\Fxchange\Entities\UserRecoverPwd;

if (function_exists('current_permission_value') === false) {
    function current_permission_value($model, $permissionTitle, $permissionAction)
    {
        $value = array_get($model->permissions, "$permissionTitle.$permissionAction");
        if ($value === true) {
            return 1;
        }
        if ($value === false) {
            return -1;
        }

        return 0;
    }
}

if (!function_exists('save_password_recoverd')) {
    function save_password_recoverd($user_id,$value){
        $recoverd = new UserRecoverPwd;
        $recoverd->user_id  = $user_id;
        $recoverd->pwd      = sha1($value);
        $recoverd->save();
    }
}

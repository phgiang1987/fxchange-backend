<?php

namespace Modules\User\Http\Controllers\Admin;

use Illuminate\Http\Response;
use Modules\User\Contracts\Authentication;
use Modules\User\Events\UserHasBegunResetProcess;
use Modules\User\Http\Requests\CreateUserRequest;
use Modules\User\Http\Requests\UpdateUserRequest;
use Modules\User\Permissions\PermissionManager;
use Modules\User\Repositories\RoleRepository;
use Modules\User\Repositories\UserRepository;
use Yajra\Datatables\Datatables;
use Modules\User\Entities\Sentinel\User;
use Modules\User\Entities\UserRole;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Fxchange\Entities\UserAddress;
use Carbon\Carbon;
use DB;
use Modules\Fxchange\Entities\UserRecoverPwd;
use Modules\User\Entities\UserToken;

class UserController extends BaseUserModuleController
{
    /**
     * @var UserRepository
     */
    private $user;
    /**
     * @var RoleRepository
     */
    private $role;
    /**
     * @var Authentication
     */
    private $auth;

    private $datatables;
    /**
     * @param PermissionManager $permissions
     * @param UserRepository    $user
     * @param RoleRepository    $role
     * @param Authentication    $auth
     */
    public function __construct(
        PermissionManager $permissions,
        UserRepository $user,
        RoleRepository $role,
        Authentication $auth,
        Datatables $datatables
    ) {
        parent::__construct();

        $this->permissions = $permissions;
        $this->user = $user;
        $this->role = $role;
        $this->auth = $auth;
        $this->datatables = $datatables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */




    public function indexAdmin(Request $request)
    {
        session()->put('request',$request->all());
        session()->put('name_route',\Request::route()->getName());
        $users = $this->user->all();

        $currentUser = $this->auth->user();

        return view('user::admin.users.indexadmin', compact('users', 'currentUser'));
    }

    //NOTE ACC : This is ways to get your data into DataTables,Server-side processing is enabled by setting the serverSide option to true and providing an Ajax data source through the ajax option.
    public function indexTableAdmin(Request $request){
        // $request = session()->get("request");

        // $email = !empty($request['email'])?trim($request['email']):"";
        // $full_name = !empty($request['full_name'])?trim($request['full_name']):"";
        // $gender = !empty($request['gender'])?$request['gender']:"";
        // $age = !empty($request['age'])?trim($request['age']):"";
        // $phone_number = !empty($request['phone_number'])?trim($request['phone_number']):"";
            $items = User::select('users.*',DB::raw('roles.slug'))->with('getRoles')
                            ->join('role_users', 'users.id', '=', 'role_users.user_id')
                            ->join('roles', 'roles.id', '=', 'role_users.role_id')
                            ->whereRaw("(roles.slug != 'merchant' and roles.slug != 'customer')")
                            ->orderBy('id','DESC');
                            // ->where('roles.name','<>','Merchant')
                            // ->where('roles.name','<>','Customer');
                            // ->whereHas('getRoles', function($query) {
                            //     $query->whereRaw("roles.name != 'Merchant' and roles.name != 'Customer'");
                            // });
                    //dd($items);
        // if($email || $full_name || $gender || $age || $phone_number){
        //     if(!empty($email)){
        //         $items = $items->where('email','like','%'.$email.'%');
        //     }
        //     if(!empty($full_name)){
        //         $items = $items->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
        //     }
        //     if(!empty($gender)){
        //         $items = $items->where('gender',$gender);
        //     }
        //     if(!empty($age)){
        //         $items = $items->where('age',$age);
        //     }
        //     if(!empty($phone_number)){
        //         $items = $items->where('phone_number',$phone_number);
        //     }
        // }
        //$items = $items->get();                     
        $collection = collect($items);
        //session()->forget("request");

        return $this->datatables->of($items)
                            ->filter(function ($query) use ($request) {
                                if($request->has('full_name')){
                                    $full_name = $request->get('full_name');
                                    $query->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
                                }
                                if ($request->has('email')) {
                                    $query->where('email', 'like', "%{$request->get('email')}%");
                                }
                                if ($request->has('age')) {
                                    $query->where('age',"{$request->get('age')}");
                                }
                                if ($request->has('phone_number')) {
                                    $query->where('phone_number',"{$request->get('phone_number')}");
                                }
                                if ($request->has('gender')) {
                                    if($request->gender !=0){
                                        $query->where('gender',"{$request->get('gender')}");
                                    }
                                }
                            },true)
                            // ->filterColumn('full_name', function($query, $keyword) use ($request) {
                            //     //$sql = "CONCAT(users.first_name,'-',users.last_name)  like ?";
                            //     $query->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
                            // })
                            ->editColumn('gender', function(User $user) {
                                return  $user->getGender();
                            })
                            ->editColumn('role_name', function(User $user) {
                                return  $user->getNameRole();
                            })
                            ->addColumn('check', '<input class = "check" type="checkbox" name="selected_users[]" value="{{ $id }}">')
                            ->addColumn('button', '<a title=Edit User" class="btn btn-default btn-flat" href="{{ route("admin.user.user.edit",$id) }}"><i class="fa fa-pencil"></i></a> <button title="Delete User" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.user.user.destroy",$id) }}"><i class="fa fa-trash"></i></button>')
                            ->addColumn('view', '<a title=View" class="btn btn-default btn-flat chitiet" idcus="{{$id}}" href="javascript:void(0)" style="margin-right:3px"><i class="fa fa-eye"></i></a> ')

                            ->rawColumns(['check', 'action','button','view'])->make(true);
        
    }















    public function indexMerchant(Request $request)
    {
        session()->put('request',$request->all());
        session()->put('name_route',\Request::route()->getName());
        $users = $this->user->all();

        $currentUser = $this->auth->user();

        return view('user::admin.users.index', compact('users', 'currentUser'));
    }

    //NOTE ACC : This is ways to get your data into DataTables,Server-side processing is enabled by setting the serverSide option to true and providing an Ajax data source through the ajax option.
    public function indextableMerchant(Request $request){
        // $request = session()->get("request");
        // $email = !empty($request['email'])?trim($request['email']):"";
        // $full_name = !empty($request['full_name'])?trim($request['full_name']):"";
        // $gender = !empty($request['gender'])?$request['gender']:0;
        // $age = !empty($request['age'])?trim($request['age']):"";
        // $phone_number = !empty($request['phone_number'])?trim($request['phone_number']):"";
            $items = User::with('getRoles')
                            ->whereHas('getRoles', function($query) {
                                $query->where('role_id',User::MERCHANT);
                            })
                            ->orderBy('id','DESC');
            // $items = User::select('users.*',DB::raw('roles.slug'))->with('getRoles')
            //                 ->join('role_users', 'users.id', '=', 'role_users.user_id')
            //                 ->join('roles', 'roles.id', '=', 'role_users.role_id')
            //                 ->whereRaw("(roles.slug != 'merchant' and roles.slug != 'customer')")
            //                 ->orderBy('id','DESC');
        // if(!empty($email)){
        //     $items = $items->where('email','like','%'.$email.'%');
        // }
        // if(!empty($full_name)){
        //     $items = $items->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
        // }
        // if (isset($request['gender'])) {
        //     $items = $items->where('gender',$request['gender']);
        // }
        

        // if(!empty($age)){
        //     $items = $items->where('age',$age);
        // }
        // if(!empty($phone_number)){
        //     $items = $items->where('phone_number',$phone_number);
        // }






        //$items = $items->get();                     
        //$collection = collect($items);

        //dd($request->all());

        //session()->forget("request");
        return $this->datatables->of($items)
                            ->filter(function ($query) use ($request) {
                                if($request->has('full_name')){
                                    $full_name = $request->get('full_name');
                                    $query->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
                                }
                                if ($request->has('email')) {
                                    $query->where('email', 'like', "%{$request->get('email')}%");
                                }
                                if ($request->has('age')) {
                                    $query->where('age',"{$request->get('age')}");
                                }
                                if ($request->has('phone_number')) {
                                    $query->where('phone_number',"{$request->get('phone_number')}");
                                }
                                if ($request->has('gender')) {
                                    if($request->gender !=0){
                                        $query->where('gender',"{$request->get('gender')}");
                                    }
                                }
                            },true)
                            ->editColumn('gender', function(User $user) {
                                return  $user->getGender();
                            })
                            ->editColumn('role_name', function(User $user) {
                                return  $user->getNameRole();
                            })
                            ->addColumn('check', '<input class = "check" type="checkbox" name="selected_users[]" value="{{ $id }}">')
                            ->addColumn('button', '<a title=Edit User" class="btn btn-default btn-flat" href="{{ route("admin.user.user.edit",$id) }}"><i class="fa fa-pencil"></i></a> <button title="Delete User" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.user.user.destroy",$id) }}"><i class="fa fa-trash"></i></button>')

                            ->rawColumns(['check', 'action','button'])->make(true);
        
    }
    public function indexCustomer(Request $request)
    {
        session()->put('request',$request->all());
        session()->put('name_route',\Request::route()->getName());
        $users = $this->user->all();

        $currentUser = $this->auth->user();

        return view('user::admin.users.indexcustomer', compact('users', 'currentUser'));
    }
    //NOTE ACC : This is ways to get your data into DataTables,Server-side processing is enabled by setting the serverSide option to true and providing an Ajax data source through the ajax option.
    public function indexTableCustomer(Request $request){
        // $request = session()->get("request");


        // $email = !empty($request['email'])?trim($request['email']):"";
        // $full_name = !empty($request['full_name'])?trim($request['full_name']):"";
        // $gender = !empty($request['gender'])?$request['gender']:0;
        // $age = !empty($request['age'])?trim($request['age']):"";
        // $phone_number = !empty($request['phone_number'])?trim($request['phone_number']):"";
        
        $items = User::with('getRoles')
                        ->whereHas('getRoles', function($query) {
                            $query->where('role_id',User::CUSTOMER);
                        })
                        ->orderBy('id','DESC');
        // if(!empty($email)){
        //     $items = $items->where('email','like','%'.$email.'%');
        // }
        // if(!empty($full_name)){
        //     $items = $items->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
        // }
        // if (isset($request['gender'])) {
        //     $items = $items->where('gender',$request['gender']);
        // }
        

        // if(!empty($age)){
        //     $items = $items->where('age',$age);
        // }
        // if(!empty($phone_number)){
        //     $items = $items->where('phone_number',$phone_number);
        // }
        // $items = $items->get();              
        $collection = collect($items);
        //session()->forget("request");
        return $this->datatables->of($items)
                            ->editColumn('gender', function(User $user) {
                                return  $user->getGender();
                            })
                            ->addColumn('check', '<input class = "check" type="checkbox" name="selected_users[]" value="{{ $id }}">')
                            ->addColumn('button', '<a title=Edit User" class="btn btn-default btn-flat" href="{{ route("admin.user.user.edit",$id) }}"><i class="fa fa-pencil"></i></a> <button title="Delete User" class="btn btn-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route("admin.user.user.destroy",$id) }}"><i class="fa fa-trash"></i></button>')
                            ->addColumn('view', '<a title=View class="btn btn-default btn-flat chitiet" idcus="{{$id}}" href="javascript:void(0)" style="margin-left:20px;"><i class="fa fa-eye"></i></a> ')
                            ->filter(function ($query) use ($request) {
                                if($request->has('full_name')){
                                    $full_name = $request->get('full_name');
                                    $query->whereRaw("(first_name LIKE '%$full_name%' OR last_name LIKE '%$full_name%' OR concat_ws(' ',first_name,last_name) LIKE '%$full_name%')");
                                }
                                if ($request->has('email')) {
                                    $query->where('email', 'like', "%{$request->get('email')}%");
                                }
                                if ($request->has('age')) {
                                    $query->where('age',"{$request->get('age')}");
                                }
                                if ($request->has('phone_number')) {
                                    $query->where('phone_number',"{$request->get('phone_number')}");
                                }
                                if ($request->has('gender')) {
                                    if($request->gender !=0){
                                        $query->where('gender',"{$request->get('gender')}");
                                    }
                                }
                            },true)
                            ->rawColumns(['check', 'action','button','view'])->make(true);

    }
    //bulk delete user by id using ajax 
    public function indextableajax(Request $request){
        $id = [];
        $id = $request->id;
        if($id == null){
            $request->session()->flash('danger','Select item before bulk delete');
            return 2;
        }else{
            $flag = 0;
            // $item = User::with('getRoles')
            //      ->whereHas('getRoles', function($query) {
            //                     $query->where('role_id',User::CUSTOMER);
            //                 })
            //     ->where('id',$id)
            //     ->first();
            // if($item){
            //     $flag = 3;
            // }else{
            //     $flag = 4;
            // }

            $name_route = session()->get('name_route');
            if($name_route == "admin.user.user.index"){
                $flag = 4;
            }elseif($name_route == "admin.user.user.indexadmin"){
                $flag = 0;
            }else{
                $flag = 3;  
            }
            //return $flag;
            $role = UserRole::where('user_id',$id)->delete();
            $user = User::find($id);
            $user->email = $user->email.Carbon::now()->timestamp;
            $user->phone_number = $user->phone_number.Carbon::now()->timestamp;
            $user->country_code = $user->country_code.Carbon::now()->timestamp;
            $user->save();
            $user = User::where('id',$id)->delete(); 

            $request->session()->flash('success','Delete successfully');
            return $flag;
        }  
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = $this->role->all();
        $name_route = session()->get('name_route');
        //dd($name_route);
        return view('user::admin.users.create', compact('roles','name_route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUserRequest $request
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        //dd($request->all());
            //    $flag = 0;
            // if(preg_match("/^(?=.*[A-Z]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*?[a-z]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*[0-9]).{8,}$/", $request->password)){
            //     $flag++;
            // }
            // if(preg_match("/^(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$/", $request->password)){
            //     $flag++;
            // }
            //dd($flag);
        // if($flag >= 3){

            $data = $this->mergeRequestWithPermissions($request);
            $user = $this->user->createWithRoles($data, $request->roles, true);
            if($request->has('phone_number')){
                if($request->phone_number[0] == 0){
                    $request->phone_number = substr($request->phone_number,1);
                }
                //dd($request->phone_number);
                $user->phone_number = $request->phone_number;
                $user->save();
            }if($request->has('gender')){
                $user->gender = $request->gender;
                $user->save();
            }if($request->has('country_code')){
                $user->country_code = $request->country_code;
                $user->save();
            }if($request->has('age')){
                $user->age = $request->age;
                $user->save();
            }
            //dd($user);
            if($request->name_route == "admin.user.user.indexcustomer"){
                $check_role = UserRole::where('user_id',$user->id);
                if($check_role->first()){
                    //dd(1);
                    //$check_role->user_id = $user->id;
                    $check_role->update(['role_id' => 3]);
                }else{
                    $role = new UserRole();
                    $role->user_id = $user->id;
                    $role->role_id = 3;
                    $role->save(); 
                }

            }if($request->name_route == "admin.user.user.index"){
                $check_role = UserRole::where('user_id',$user->id);
                if($check_role->first()){
                    //dd(1);
                    //$check_role->user_id = $user->id;
                    $check_role->update(['role_id' => 4]);
                }else{
                    $role = new UserRole();
                    $role->user_id = $user->id;
                    $role->role_id = 4;
                    $role->save(); 
                }
            }
            if($request->name_route != "admin.user.user.index" && $request->name_route != "admin.user.user.indexcustomer"){
                $check_role = UserRole::where('user_id',$user->id);
                //dd($check_role);
                if($check_role->first()){
                    //dd(1);
                    //$check_role->user_id = $user->id;
                    $check_role->update(['role_id' => 1]);
                }else{
                    //dd(2);
                    $role = new UserRole();
                    $role->user_id = $user->id;
                    $role->role_id = 1;
                    $role->save(); 
                }
            }
            //dd(1);
            // if($request->roles[0] == 4){
            if($request->name_route == "admin.user.user.index"){
                return redirect()->route('admin.user.user.index')
                ->withSuccess(trans('user::messages.user created'));
            }elseif($request->name_route == "admin.user.user.indexadmin"){
                return redirect()->route('admin.user.user.indexadmin')
                ->withSuccess(trans('user::messages.user created'));
            }else{
              return redirect()->route('admin.user.user.indexcustomer')
                ->withSuccess(trans('user::messages.user created'));  
            }
        // }else{
        //    return redirect()->back()
        //         ->withError(trans('The password have to greater than or equal 8 characters and meet at least 3 of the 4 criteria: Upper case, lower case, numbers, special characters (!@#$%^&*)'));  
        // }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $name_route = session()->get('name_route');
        if (!$user = $this->user->find($id)) {
            return redirect()->route('admin.user.user.index')
                ->withError(trans('user::messages.user not found'));
        }
        $roles = $this->role->all();

        $currentUser = $this->auth->user();

        return view('user::admin.users.edit', compact('user', 'roles', 'currentUser','name_route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int               $id
     * @param  UpdateUserRequest $request
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $data = $this->mergeRequestWithPermissions($request);
        $user = $this->user->find($id);
        $this->user->updateAndSyncRoles($id, $data, $request->roles);

        // if($request->has('password')){
        //     $flag = 0;
        //         if(preg_match("/^(?=.*[A-Z]).{8,}$/", $request->password)){
        //             $flag++;
        //         }
        //         if(preg_match("/^(?=.*?[a-z]).{8,}$/", $request->password)){
        //             $flag++;
        //         }
        //         if(preg_match("/^(?=.*[0-9]).{8,}$/", $request->password)){
        //             $flag++;
        //         }
        //         if(preg_match("/^(?=.*[@#$%^&+=!])(?=\\S+$).{8,}$/", $request->password)){
        //             $flag++;
        //         }
        //     if($flag < 3){
        //         return redirect()->back()
        //         ->withError(trans('The password have to greater than or equal 8 characters and meet at least 3 of the 4 criteria: Upper case, lower case, numbers, special characters (!@#$%^&*)'));
        //     }

        // }
        if ($request->has('password')) {
            save_password_recoverd($id,$request->password);
        }

        if($request->has('phone_number')){
            if($request->phone_number[0] == 0){
                $request->phone_number = substr($request->phone_number,1);
            }
            $user->phone_number = $request->phone_number;
            $user->save();
        }if($request->has('gender')){
            $user->gender = $request->gender;
            $user->save();
        }if($request->has('country_code')){
                $user->country_code = $request->country_code;
                $user->save();
        }if($request->has('age')){
            $user->age = $request->age;
            $user->save();
        }

        if ($request->get('button') === 'index') {

            if($request->name_route == "admin.user.user.index"){
                return redirect()->route('admin.user.user.index')
                ->withSuccess(trans('user::messages.user updated'));
            }elseif($request->name_route == "admin.user.user.indexadmin"){
                return redirect()->route('admin.user.user.indexadmin')
                ->withSuccess(trans('user::messages.user updated'));
            }else{
              return redirect()->route('admin.user.user.indexcustomer')
                ->withSuccess(trans('user::messages.user updated'));  
            }
        }
        //dd($request->name_route);
        if($request->name_route == "admin.user.user.index"){
            return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.user updated'));
        }elseif($request->name_route == "admin.user.user.indexadmin"){
            return redirect()->route('admin.user.user.indexadmin')
            ->withSuccess(trans('user::messages.user updated'));
        }else{
          return redirect()->route('admin.user.user.indexcustomer')
            ->withSuccess(trans('user::messages.user updated'));  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->email = $user->email.Carbon::now()->timestamp;
        $user->phone_number = $user->phone_number.Carbon::now()->timestamp;
        $user->country_code = $user->country_code.Carbon::now()->timestamp;
        $user->save();
        $user_token = UserToken::where('user_id',$id)->get();
        if(count($user_token)){
            foreach($user_token as $value){
                $value->delete();
            }
        }
        $this->user->delete($id);

        $name_route = session()->get('name_route');
        if($name_route == "admin.user.user.index"){
            return redirect()->route('admin.user.user.index')
            ->withSuccess(trans('user::messages.user deleted'));
        }elseif($name_route == "admin.user.user.indexadmin"){
            return redirect()->route('admin.user.user.indexadmin')
            ->withSuccess(trans('user::messages.user deleted'));
        }else{
          return redirect()->route('admin.user.user.indexcustomer')
            ->withSuccess(trans('user::messages.user deleted'));  
        }

    }

    public function sendResetPassword($user, Authentication $auth)
    {
        $user = $this->user->find($user);
        $code = $auth->createReminderCode($user);

        event(new UserHasBegunResetProcess($user, $code));

        return redirect()->route('admin.user.user.edit', $user->id)
            ->withSuccess(trans('user::auth.reset password email was sent'));
    }




    public function viewAddress(Request $request){
        $idcus = $request->get('idcus');

        $address = UserAddress::where('user_id',$idcus)->get();
        return view('user::admin.partials.addressdetail',compact('address'))->render(); 
    }
}
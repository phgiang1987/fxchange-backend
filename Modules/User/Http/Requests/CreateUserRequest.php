<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique_email:users|email',
            'password' => 'required|min:3|confirmed|password_regex',
            'phone_number' => 'required|min_phone:7|max_phone:8|unique_string_phone:users',
            // 'age' => 'required|numeric',

        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'password.password_regex'=>'Minimum password length 8 characters, must meet at least 3 of the 4 criteria: upper case, lower case, numbers, special characters',
            'phone_number.unique_string_phone' => 'Mobile no. already in use',
            'phone_number.max_phone' => "Phone numbers can not be larger than 8 characters",
            'phone_number.min_phone' => "The phone must be at least 8 character",
            'email.unique_email' => 'The email has already been taken',

        ];
    }
}

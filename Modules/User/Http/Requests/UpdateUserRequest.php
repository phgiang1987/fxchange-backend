<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function rules()
    {
        $userId = $this->route()->parameter('users');
// unique:users,email,{$userId}
        return [
            'email' => "required|email|unique_email:users,{$userId}",
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => "confirmed|password_regex|check_password_last:{$userId}",
            'phone_number' => "required|min_phone:7|max_phone:8|unique_string_phone:users,{$userId}",
            // 'age' => 'required|numeric',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'password.password_regex'=>'Minimum password length 8 characters, must meet at least 3 of the 4 criteria: upper case, lower case, numbers, special characters',
             'phone_number.unique_string_phone' => trans('fxchange::fxchanges.singup_already_use_phone_number'),
             'phone_number.max_phone' => "Phone numbers can not be larger than 8 characters",
             'phone_number.min_phone' => "The phone must be at least 8 character",
             'email.unique_email' => 'The email has already been taken',
             'password.check_password_last' => trans('fxchange::fxchanges.check_last_password'),
        ];
    }
}





{{-- <!DOCTYPE html>
<html>
<head>
  <title>Fx Change Fogot Password</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

</head>
<body>
  <div>
    <table width="375"  cellpadding="0" cellspacing="0" style="background:linear-gradient(to right, #FCFCFC, #ECECEC); padding:0;margin:0; font-family: 'Open Sans', sans-serif; font-weight: 400 ;border:1px solid #BCB5B9; margin:0 auto">
      <tr style="height: 120pt; width: 100%">
        <td style="font-size: 0;" align="center">
          <img src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/logoat3x.png" width="200">
        </td>
      </tr>

      <tr>
        <td style="background-color: #C5A87C; color: rgb(255,255,255); height: 32px; padding-left: 25px; font-size: 14px; letter-spacing: -0.3px">
          Reset Password Request
        </td>
      </tr>

      <tr>
        <td style="padding-left: 25px; padding-right: 25px">
          <table width="100%" cellpadding="0" cellspacing="0" >
            <tr>
              <td height="35"></td>
            </tr>
            <tr>
              <td colspan="1" style="color: rgba(0,0,0,0.54); font-size: 16px; line-height: 28px; letter-spacing: -0.3px">Hi, {{ $user['first_name'] }} {{ $user['last_name'] }}</td>
            </tr>
            <tr>
              <td height="16"></td>
            </tr>
            <tr>
              <td colspan="1" style="line-height: 24px; letter-spacing: -0.4px; font-size: 14px">We received a reset password request form your account</td>
            </tr>
            <tr>
              <td height="15"></td>
            </tr>
            <tr>
              <td colspan="1" style="line-height: 24px; letter-spacing: -0.4px; font-size: 14px; color: rgba( 0, 0, 0, 0.87);">Click on the button to set a new password:</td>
            </tr>
            <tr>
              <td height="40"></td>
            </tr>
            <tr>
              <td height="48" style="background-color: rgb(51, 46, 103); text-align: center; border-radius: 4px;" align="center">
                <a href="{{ URL::to("auth/reset/{$user['id']}/{$code}") }}" style=" color: #fff; text-decoration: none; font-size: 16px;
                line-height: 28px; letter-spacing: -0.3px">Set a New Password</a>
              </td>
            </tr>
            <tr>
              <td height="150"></td>
            </tr>
            
            <tr>
              <td style="font-size: 14px; color: rgba( 0, 0, 0, 0.87); letter-spacing: -0.3px">
                Regards,<p style="margin:0">FX Change</p>
              </td>
            </tr>
            <tr>
              <td height="50"></td>
            </tr>
          </table>
        </td>
      </tr>


    </table>
  </div>
</body>
</html> --}}


<html>
<head>
  <title>FxChange Forgot Password</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

</head>
<body>
  <table bgcolor="#f2f1ef" width="600" cellspacing="0" cellpadding="0" style="font-family: 'Open Sans', sans-serif; font-weight: 400 ;border:1px solid #BCB5B9; margin:0 auto">
    <tr>
      <td>
        <table cellspacing="0" cellpadding="0">
          <td style="padding: 27px 34px" width="45%">
            <img width="100%" src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/logoat3x.png">
          </td>
          <td width="55%"></td>
        </table>
      </td>
    </tr>
    <tr bgcolor="#c6a879">
      <td style="padding: 15px 34px; font-size: 16px; color: #fff">New Password Request</td>
    </tr>
    <tr>
      <td style="padding: 40px 34px 55px 34px">
        <table cellspacing="0" cellpadding="0" >
          <tr>
            <td>To request for a new password, click on this button.</td>
          </tr>
          <tr><td height="80"></td></tr>
          <tr>
            <td><a href="{{ URL::to("auth/reset/{$user['id']}/{$code}") }}"><img src="https://proj-fxchange.s3.ap-southeast-1.amazonaws.com/assets/media/newpass.png"></a></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
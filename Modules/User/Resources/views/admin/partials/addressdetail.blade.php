<div class="box-body table-responsive" style="overflow: auto; height: 500px;">
	<table class="table table-bordered table-hover " cellspacing="0" width="100%">
		<thead>
		    <tr>
		        {{-- <th>ID</th> --}}
		        <th>Title</th>
		        <th>Postal Code</th>
		        <th>Address</th>
		        <th>Block</th>
		        <th>Unit</th>        
		    </tr>
		    @foreach($address as $item)
		    <tr>
		    	{{-- <td>{{ $item->id }}</td> --}}
		    	<td>{{ $item->title}}</td>
		    	<td>{{ $item->postal_code }}</td>
		    	<td>{{ $item->address }}</td>
		    	<td>{{ $item->block }}</td>
		    	<td>{{ $item->unit }}</td>
		    </tr>
		    @endforeach
		</thead>
	</table>
</div>
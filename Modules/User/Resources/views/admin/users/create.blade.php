@extends('layouts.master')

@section('content-header')
<h1>
    @if($name_route == "admin.user.user.indexcustomer")
    {{ trans('New Customer') }}
    @elseif($name_route == "admin.user.user.indexadmin")
    {{ trans('New User') }}
    @else
    {{ trans('New Merchant') }}
    @endif
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class=""><a href="{{ URL::route('admin.user.user.index') }}"> @if($name_route == "admin.user.user.indexcustomer")
    {{ trans('Customers') }}
    @elseif($name_route == "admin.user.user.indexadmin")
    {{ trans('Users') }}
    @else
    {{ trans('Merchant') }}
    @endif</a></li>
    <li class="active">{{ trans('user::users.breadcrumb.new') }}</li>
</ol>
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop
@section('content')
{!! Form::open(['route' => 'admin.user.user.store', 'method' => 'post']) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
      
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1-1" data-toggle="tab">{{ trans('user::users.tabs.data') }}</a></li>
                @if($name_route === "admin.user.user.indexadmin")
                    <li class=""><a href="#tab_2-2" data-toggle="tab">{{ trans('user::users.tabs.roles') }}</a></li>
                    <li class=""><a href="#tab_3-3" data-toggle="tab">{{ trans('user::users.tabs.permissions') }}</a></li>
                @elseif($name_route =="admin.user.user.indexcustomer" || $name_route == "admin.user.user.index")
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    {!! Form::label('first_name', trans('user::users.form.first-name')) !!}
                                    {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.first-name')]) !!}
                                        <input type="hidden" name="name_route" value="{{ $name_route }}">
                                    {!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    {!! Form::label('last_name', trans('user::users.form.last-name')) !!}
                                    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.last-name')]) !!}
                                    {!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                        </div>
                            <div class="row">
                                <div class="col-sm-6">
                                <div class="row">
                                     <div class="col-sm-4">
                                        <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }}">
                                            {!! Form::label('age', trans('Age')) !!}
                                            {!! Form::text('age', old('age'), ['class' => 'form-control','min'=> '0','onekeypress'=>'validate(event)','onkeyup'=>"this.value=this.value.replace(/[^\d]/,'')", 'placeholder' => trans('Age')]) !!}
                                            {!! $errors->first('age', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            {!! Form::label('email', trans('user::users.form.email')) !!}
                                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => trans('user::users.form.email')]) !!}
                                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                            {!! Form::label('country_code', trans('Country Code')) !!}
                                                <select class="js-example-basic-single form-control" name="country_code" id="country_code">
                                                {{-- <option value="0">Others please specify</option> --}}
                                                @foreach(config('fxchange.country_code') as $k => $item)
                                                  <option value="{{ $k }}">{{ $k }}</option>
                                                @endforeach
                                                </select>
                                      
                                            </div>
                                        </div>
                                        <div class="col-sm-8"> 
                                            <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                                                {!! Form::label('phone_number', trans('Phone number')) !!}
                                                {!! Form::text('phone_number', old('phone_number'), ['class' => 'form-control phone_number', 'placeholder' => trans('Phone number')]) !!}
                                                {!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {!! Form::label('password', trans('user::users.form.password')) !!}
                                    {!! Form::password('password', ['class' => 'form-control']) !!}
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    {!! Form::label('password_confirmation', trans('user::users.form.password-confirmation')) !!}
                                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    {!! Form::label('gender', trans('Gender')) !!}
                                    <label class="radio-inline">Male
                                        <input class="flat-blue" type="radio" name="gender" value="1">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="radio-inline">Female
                                        <input class="flat-blue" type="radio" name="gender" value="2">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2-2">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{ trans('user::users.tabs.roles') }}</label>
                                    <select multiple="" class="form-control" name="roles[]">
                                        <?php foreach ($roles as $role): ?>
                                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_3-3">
                    @include('user::admin.partials.permissions-create')
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-flat">{{ trans('user::button.create') }}</button>
                    <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.user.user.index')}}"><i class="fa fa-times"></i> {{ trans('user::button.cancel') }}</a>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@stop
@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('user::users.navigation.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script>
$( document ).ready(function() {
 $(".phone_number").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    $(document).keypressAction({
        actions: [
            { key: 'b', route: "<?= route('admin.user.user.index') ?>" }
        ]
    });
    $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });
});
</script>
@endpush

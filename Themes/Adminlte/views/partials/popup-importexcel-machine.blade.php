<div id="modal-import-machine" class="modal fade modal-default in" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="delete-confirmation-title">Import Field Inspection</h4>
        </div>
        <div class="modal-body">
            <div class="default-message">
                <form action="{{ route('admin.machine.machine.importmachine') }}" method="POST" class="form-inline" enctype="multipart/form-data" style="padding-top: 15px">
                    {{ csrf_field() }}
                    <input type="file" name="file" class="form-control">
                    <button class="btn btn-primary btn-flat" type="submit" style="padding: 5px 12px;"><i class="fa fa-cloud-upload"></i> Import</button>
                </form>
            </div>
            <div class="custom-message">
              <b>Format of excel file must include:</b>
              <p>Title, Postcode, Block, Unit, Latitude, Longitude</p>
              <p><a href="{{ route('admin.machine.machine.exportdatafake') }}">Download sample Excel file</a></p>
            </div>
        </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default btn-flat btn-outline-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
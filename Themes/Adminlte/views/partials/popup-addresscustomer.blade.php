<div id="modal-detail-customer" class="modal fade modal-default in" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="delete-confirmation-title">Address Detail</h4>
        </div>
        <div class="modal-body">
            <div class="default-message">
                                </div>
            <div class="custom-message"></div>
        </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-default btn-flat btn-outline-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
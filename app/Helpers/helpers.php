<?php 
	use Modules\Fxchange\Entities\StaticPage;
	use Modules\Fxchange\Entities\Transaction;
	use Modules\User\Entities\Sentinel\User;
	use Carbon\Carbon;
	use Modules\Fxchange\Entities\FxchangeSettings;

	function getSymbolCode(){
		$url = "https://gist.githubusercontent.com/Fluidbyte/2973986/raw/b0d1722b04b0a737aade2ce6e055263625a0b435/Common-Currency.json";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,$url);
		$result=curl_exec($ch);
		curl_close($ch);

		$result = json_decode($result,false);

		return $result;
	}
	function getLocation($lastitude=0, $longitude=0){
		// $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lastitude).','.trim($longitude).'&sensor=true'); 
		// $output = json_decode($geocodeFromLatLong);
		// $status = $output->status;
		// $address = ($status=="OK") ? $output->results[0]->formatted_address : '';
		// return $address;


		try{
			$url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$lastitude,$longitude&sensor=true";
			$ch = curl_init();
			// Disable SSL verification
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Will return the response, if false it print the response
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// Set the url
			curl_setopt($ch, CURLOPT_URL,$url);
			// Execute
			$result=curl_exec($ch);
			// Closing
			curl_close($ch);

			// Will dump a beauty json :3
			$data = json_decode($result, false);
			return $data->results[0]->formatted_address;
		}catch (\Exception $e) {
			return "None";
		}

	}
	function getItemCode(){
		$data = getSymbolCode();
		return $data;
	}
	function getArraySymbolName(){
		$data = getItemCode();
		$arr = [];
		foreach($data as $k =>$v){
			$arr[] = $k;
			// return $k;
		}
		return $arr;
	}
	function getSignCode($sign='$'){
		$data = getItemCode();
		$data = $data->$sign->symbol;
		return $data;
	}
	function getCurrency(){
		$data = getItemCode();
		$arr = [];
		foreach($data as $k => $v){
			$arr[$k] = $v;
			$arr[$k]= $v->name;
		}
		return $arr;
	}


	function menu($menuItem)
	{
		foreach ($menuItem as $key => $value) {
			$staticpage = StaticPage::where("id",$value)->update(['sort' => ++$key]);
		}
	    
	}
	function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return ($miles * 1.609344);
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	      }
	}
	function getTransactionAmountInDateByUser($user_id){
		$today = Carbon::now()->format('Y-m-d');
		$data = Transaction::selectRaw("fxchange__purchases.*,SUM(total_price) AS TotalInDay")->where('user_id',$user_id)->whereRaw("DATE_FORMAT(purchase_date,'%Y-%m-%d') = '{$today}' and (status='paid' or status='completed')")->get();
		if(count($data)){
			return (int)$data[0]->TotalInDay;
		}
		return 0;
	}
	function checkAmountBeforePayment($user_id){
		$totalinday = getTransactionAmountInDateByUser($user_id);
		$amount_settings = FxchangeSettings::showValue('core::maximum-amount');
		if($totalinday > $amount_settings){
			return false;
		}
		return true;
	}
	function getRemainingAmount($user_id){
		$totalinday = getTransactionAmountInDateByUser($user_id);
		$amount_settings = FxchangeSettings::showValue('core::maximum-amount');
		$result = $amount_settings - $totalinday;
		return $result;
	}
?>
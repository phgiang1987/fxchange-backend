<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\User\Contracts\Authentication;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Contracts\Container\Container;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($request->expectsJson()) {
            $message = $e->getMessage();
            $code = $e->getCode();

            if ($e instanceof \Illuminate\Auth\AuthenticationException) {
                $code = Response::HTTP_FORBIDDEN;
            }

            if ($e instanceof \Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException) {
                $message = 'Too many requests. Slow your roll!';
                $code = $e->getCode();
            }

            if ($e instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
                $message = 'Not Found.';
                $code = Response::HTTP_NOT_FOUND;
            }

            if ($e instanceof \Illuminate\Database\Eloquent\ModelNotFoundException) {
                $message = 'Not Found.';
                $code = Response::HTTP_NOT_FOUND;
            }
            if ($e instanceof ValidationException) {
                $message = 'Not Found.';
                $code = Response::HTTP_BAD_REQUEST;
            }

            return response()->json([
                'status' => false,
                "status_code"=> $code,
                "message"=> $message,
                "data"=>[],
            ], Response::HTTP_OK);

            return parent::render($request, $exception);
        }

        if ($e instanceof ValidationException) {
            return parent::render($request, $e);
        }

        if ($e instanceof TokenMismatchException) {
            return redirect()->back()
                 ->withInput($request->except('password'))
                 ->withErrors(trans('core::core.error token mismatch'));
        }

        if (config('app.debug') === false) {
            return $this->handleExceptions($e);
        }

        return parent::render($request, $e);
    }

    private function handleExceptions($e)
    {
        if ($e instanceof ModelNotFoundException) {
            return response()->view('errors.404', [], 404);
        }

        if ($e instanceof NotFoundHttpException) {
            return response()->view('errors.404', [], 404);
        }

        return response()->view('errors.500', [], 500);
    }
}

<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        \Modules\Fxchange\Console\AlertExchangeRateCommand::class,
        \Modules\Fxchange\Console\UpdateExchangeRateByDateCommand::class,
        \Modules\Fxchange\Console\UpdateUserAlertEveryDaysCommand::class,
        \Modules\Fxchange\Console\DeleteApiLogCommand::class,
        \Modules\Fxchange\Console\DeleterequestSMSCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('fxchange:alert-rate')
                 ->everyMinute();
        $schedule->command('fxchange:update-exchange-rate')
                 ->daily();
        $schedule->command('fxchange:update-user-alert')
                 ->daily();
        $schedule->command('fxchange:delete-api-log')
                 ->daily();
        $schedule->command('fxchange:delete-request-sms')
                 ->monthly();
    }
}

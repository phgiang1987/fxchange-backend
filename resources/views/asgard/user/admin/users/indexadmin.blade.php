@extends('layouts.master')

@section('content-header')
<h1>
    {{ trans('Admin') }}
</h1>
<ol class="breadcrumb">
    <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    <li class="active">{{ trans('Admin') }}</li>
</ol>
@stop

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="row">
            <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                <a href="{{ URL::route('admin.user.user.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                    <i class="fa fa-pencil"></i> {{ trans('New User') }}
                </a>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <div class="well">
                    <div class="row">
                        <form method="post" id="search-form" accept-charset="UTF-8">
                            <div class="col-md-2">
                                <div class="form-group">
                                <label for="full_name">Full Name</label>
                                    <input type="text" id="full_name" name="full_name" value="" class="form-control" placeholder="Full name">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="email">Email</label>
                                    <input type="text" id="email" name="email" value="" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select id="gender" name="gender" class="form-control">
                                                <option value="0">All</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="age">Age</label>
                                        <input type="text" id="age" name="age" value="" class="form-control" placeholder="Age">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                <label for="phone_number">Phone Number</label>
                                    <input type="number" id="phone_number" name="phone_number" value="" class="form-control" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input class="btn btn-primary" id="submit" type="submit" value="Filter">
                            </div>
                        </form>
                    </div>
                </div>

                <table id="tableuser" class="table table-bordered table-hover " cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Check box</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Gender</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Age</th>
                            <th>Created At</th>
                            <th>Actions</th>            
                        </tr>
                    </thead>
                </table>
            <!-- /.box-body -->
            <button type="button" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#myModal">Bulk Delete</button>
            </div>
        <!-- /.box -->
    </div>
<!-- /.col (MAIN) -->
</div>
</div>
<div id="myModal" class="modal fade modal-danger in" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="delete-confirmation-title">Confirmation</h4>
        </div>
        <div class="modal-body">
            <div class="default-message">
                                        Are you sure you want to delete this record?
                                </div>
            <div class="custom-message"></div>
        </div>
      <div class="modal-footer">
        <button type="submit" id="update-checkbox" class="btn btn-danger btn-flat btn-outline" style="float:left;"><i class="fa fa-trash"> Delete</i></button>
        <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
@include('core::partials.delete-modal')
@stop

@push('js-stack')
<?php $locale = App::getLocale(); ?>
<script type="text/javascript">
    $(document).ready(function(){
    var oTable =  $('#tableuser ').DataTable({
        processing:false,
        serverSide:true,
        searching: true,
        ajax: {
            url: '{{ route('admin.user.user.indextableadmin') }}',
            data: function (d) {
                d.full_name = $('input[name=full_name]').val();
                d.email = $('input[name=email]').val();
                d.age = $('input[name=age]').val();
                d.phone_number = $('input[name=phone_number]').val();
                d.gender = $( "#gender option:selected" ).val();
            }
        },
        // pageLength:10,
        // autoWidth:true,
        // lengthChange:true,
        // columnDefs: [ {
        //     orderable: false,
        //     className: 'select-checkbox',
        //     targets:   0,
        // } ],
        // select: {
        //     style:    'os',
        //     selector: 'td:first-child'
        // },
        columns:[
            {data:'check',searchable:false},
            {data:'first_name',searchable:true},
            {data:'last_name',searchable:true},
            {data:'gender',searchable:false},
            {data:'email',searchable:true},
            {data:'phone_number',searchable:true},
            {data:'age',searchable:true},
            {data:'created_at',searchable:false},
            {data:'button',searchable:false},
        ],
    });
    
    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });
    $("#update-checkbox").click(function(){

        var id = [];
        $(':checkbox:checked').each(function(i){
          id[i] = $(this).val();
          //    alert(id[i]);
          $.ajax({
              url: "{{ route('admin.user.user.indextableajax') }}",
              type: 'GET',
              dataType: 'JSON',
              data: {id: id[i]},
          })
          .done(function(data) {
            console.log(data)
            if(data == 4){
                console.log(data)
                window.location = "{{ route('admin.user.user.index') }}";
            }
            else if(data == 3){
                //window.location
                window.location = "{{ route('admin.user.user.indexcustomer') }}";                
            }else{
                window.location = "{{ route('admin.user.user.indexadmin') }}";     
            }
          })
        });
    })

});
</script>
<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypressAction({
            actions: [
                { key: 'c', route: "<?= route('admin.user.user.create') ?>" }
            ]
        });
    });
    $(function () {
        $('.data-table').dataTable({
            "paginate": true,
            "lengthChange": true,
            "filter": true,
            "sort": true,
            "info": true,
            "autoWidth": true,
            "order": [[ 0, "desc" ]],
            "language": {
                "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
            }
        });
    });
</script>
@endpush
